/*
         .���� �    � ����.     �    � �����.  ����� .����. �    �
        .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
        �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
      .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
     .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
     �.      � �    � �      .� �   .� �    .� ����� .����. �    �
     (c) 1998 AnakreoN Freeware

   Berusky (C) Komat za Anakreon fwp. 1997-1998-1999-?

   Soubor:     berusky.c  - hlavni modul hry
   Autor:      Komat za Anakreon
   Datum:      furt se opravuje
   Verze:      0.0 -> vyvoj
   Prekladac:  djgpp -> gcc + nasm
   Nakladac:   Zetor BS-564 Mk III C

   Dalsi soubory pro projekt Berusky:

   libanak.h

   berusky.dat       -datovy soubor
   berusky.ini       -konfiguracni soubor

   .......

   Plus hromada dalsich

   Ukoly:
   ------

   Hromady ruznych veci
*/

#define  HUDBA   1
#define  SAMPLY  1

#define  VERZE  "v0.1.1"

#ifdef __AMIGA__
#define itoa(num, str, base) sprintf(str, "%d", (num))
#include <findfirst.h>
extern void BE_ST_DebugText(int x, int y, const char *fmt, ...);
#else
#include <dir.h>
#include <dpmi.h>
#endif
#include <stdio.h>
//#include <dos.h>
//#include <process.h> /* k cemu to je ? */
//#include <stdlib.h>
//#include <ctype.h>
//#include <assert.h>
//#include <conio.h>
#include <time.h>
#include "berusky.h"
#ifdef __AMIGA__
#define getch()
#endif


#define  LOW_MEM_MUSIC     8000
// pokud je volne pameti mene nez tato konstanta,
// vyradi se ze seznamu pametove narocne skladby
// (ty kolem 1 Mega)

#define  LOW_MEM_RUN       4000
// Pokud je volne pameti mene nez toto,
// meli by se hrat berusky uplne bez zvuku


int LFB = 1;

byte *spr[MAX_SPR];
byte *p_pozadi = NULL;   //Pointer na buffer s pozadim
byte *p_lista = NULL;
byte *p_mys = NULL;
byte *pp_spr;          // pomocne pointery pro rotace
byte *pp_spr_2;
byte *pp_anim;

DISK_LEVEL pl;         //Adresa struktury s levelem
DISK_LEVEL qs;         //Adresa struktury s levelem pro savovani
DISK_LEVEL dm;         //Adresa struktury s levelem pro ukladani dema

dword YFUK = 3;

int   x_kor,y_kor,   // smery posunu hrace
      sx,sy, // stare souradnice hrace na obrazovce
      slx,sly, // stare souradnice hrace v levelu
      nx,ny, // nove souradnice hrace na obrazovce
      nlx,nly, // nove souradnice hrace v levelu
      nnx,nny, // policko za novymi souradnicemi na obrazovce
      nnlx,nnly; // policko za novymi souradnicemi na levelu

hrac   p[HRACU];       //Definovani hracu
edit   e;
hrac   *p_p;       //pointer na aktivniho hrace
edit   *p_e;

hrac   s[HRACU];       //Definovani hracu pro save
hrac   *p_s;       //save pointer na hrace

dword volatile status = 0;
dword klicu_celkem = 0;
dword klicu_demo = 0;
dword s_klice = 0;

byte  txt_mod = 0x3;

dword lista[1000][3][3]; //-> zakomponovat do rovin

ed_rovina  rv[4];
int  hr_zav[100];
int  rovina;
int  rotace;
int  vypni_omezeni = 0;

int  anim[MAX_ANIM];
dword posledni_pohyb;   //handle na posledni pohyb posledniho hrace

/* levelsety */
byte  levelset[200][50];
/* prvni byte -> levelset[0][0] = cislo obtiznosti 1 - 5 */
/* levelset[0][1] = aktivni level */

int  DAT_FILE;

int     fn1;
int     fn2;
int     fn3;
int     ve_hre = 0;
int     z_editoru = 0;
int     uziv_level = 0;


#define MASKA_LV "*.lv3"
#define KONCOVKA ".lv3"
#define DEF_EDIT "default.lv3"
byte    ed_level[30] = "default.lv3";

volatile int hrany_mod = 0;

int  posledni_file = 0;
byte *p_spd;

#define KONCOVKA_DM ".dm"
byte jmeno_dm[30];
demo_str demo[MAX_DEMO_KROKU];
int  pos_demo = 0;
int  kroku = 0;


int  no_mouse = 0;

byte *p_nsp;
byte *p_pan;

int  load_limit = 10000;
int  d_kroku = 0;

int  low_mem = 0;

/*
 * Zacatek samotne parby berusky
 */

void main(dword argc,byte *argv[])
{
   printf("Berusky %s (C) Anakreon 1999, Berusky jsou ABSOLUTNE BEZ ZARUKY\nBlizsi informace ziskate v souboru copying\n\n",VERZE);

   if((argc > 1)&&((argv[1][1] == '?')||(argv[1][1] == 'h'))) {
      help:
      printf("\nPouzijte jeden z nasledujicich parametru:\n\n");
      printf("    e [jmeno.lv3]     -editor urovni (jmeno urovne zadavat nemusite)\n");
      printf("    u  jmeno.lv3      -spusteni uzivatelske urovne\n");
      printf("    d  jmeno.dm       -spusteni dema\n");
      printf("\nPriklad: berusky.exe u level2.lv3  -spusti level2.lv3 \n");
      printf("         berusky.exe e level2.lv3  -edituje level2.lv3 \n");
      printf("         berusky.exe d demo01.dm   -prehraje demo v demo01.dm\n\n");
      printf("\n Pri spusteni bez parametru se spusti menu.\n");
      exit(0);
   }

   samply = SAMPLY;
   music = HUDBA;

   nastav_pameti();

   atexit(uvolni_pamet);
   setbuf(stdout,NULL);
   setbuf(VYPIS,NULL);
   srandom(clock());

   smaz_paletu();
   if(prepni_do_grafiky(GRAF_MOD,LFB) != NULL) {
     exit(0);
   }
   vf(0);
   _a_x = X;
   _a_y = Y;

   ini_spr();
   if((DAT_FILE = otevri_soubor_lep("berusky.dat")) == NULL) {
     tiskni_chybu("Nemuzu otevrit berusky.dat...",__LINE__);
   }
   nahraj_paletu_lep(DAT_FILE,F_PALETA,F_PALET);
   inicializuj_menu();
   zapni_klavesnici();
   vypni_repeat();
   ini_fn();
   fn1 = nahraj_fonty_lep(DAT_FILE,FN1_FILE);
   fn2 = nahraj_fonty_lep(DAT_FILE,FN2_FILE);
   fn3 = nahraj_fonty_lep(DAT_FILE,FN3_FILE);

   /* kdyz je spusten bezparametru -> spust menu */
#ifdef __AMIGA__
   if(argc <= 1) {
#else
   if(argc == 1) {
#endif
      zobraz_logo_anak();
      zapni_midas();
      nahraj_data();
      
      p_mys = spr[PRVNI_OSTATNI+2];
      if(zapni_mys(mysi_info,p_mys) == 0) {
          fprintf(VYPIS,"Neni nainstalovana mys !\n");
          no_mouse = 1;
      }
      
      hraj_hudbu_intro();

      uvodni_uvitani();
      hraj_hudbu_menu();

      nahraj_hlavni_menu();
      nahraj_nastaveni_menu();

      vf(0);
      kresli_podklad_menu();
      nastav_paletu(PALETA_MNU);
      spust_oci();
      obsluz_menu((GENERIC_MENU *)&hlavni_menu,1,0,0);
      smaz_paletu();

      stop_modul();

      uvolni_hlavni_menu();
      uvolni_nastaveni_menu();

      vypni_mys();
      prepni_do_textu(0x3);
      posledni_sbohem();
      exit(0);
   }

   zobraz_logo_uziv();
   nahraj_data();

   /* pri parametru e spust editor */
   if(argv[1][0] == 'e') {
      p_mys = spr[PRVNI_OSTATNI+1];
      if(zapni_mys(mysi_info,p_mys) == 0) {
         tiskni_chybu("Neni nainstalovana mys !\n",__LINE__);
      }

      samply = 0;
      music = 0;
      zapni_midas();

      obnov_level_lep(EDITOR,DAT_FILE,&pl);

      if(argc > 2) {
         strcpy(ed_level,argv[2]);
         if(obnov_level(ed_level,&pl) == NULL)  {
           strcat(ed_level,KONCOVKA);
           if(obnov_level(ed_level,&pl) == NULL) {
             obnov_level_lep(EDITOR,DAT_FILE,&pl);
             strcpy(ed_level,DEF_EDIT);
           }
         }
      }

      vypln_ctverec(0,0,0,XRES,YRES);
      smaz_paletu();
      edituj();
      uvolni_pozadi();
   }
/* neni to editor -> spust standartni hru */
   else {
      p_mys = spr[PRVNI_OSTATNI+2];
      if(zapni_mys(mysi_info,p_mys) == 0) {
         tiskni_chybu("Neni nainstalovana mys !\n",__LINE__);
      }

      if((argc < 3)||((argv[1][0] != 'u')&&(argv[1][0] != 'd'))) {
         // Opravdovym programatorum goto nevadi
         // alespon v tomto se jim podobam :-)
         prepni_do_textu(0x3);
         goto help;
      }

//      nastav_paletu(PALETA);

      zapni_midas();

      vypln_ctverec(0,0,0,XRES,YRES);
      smaz_paletu();
/* pri u nahraj level z druheho parametru */
      strcpy(levelset[levelset[0][1] = 1],argv[2]);
      uziv_level = 1;

      if(argv[1][0] == 'u')
         if(obnov_level(argv[2],&pl) == NULL) {
           neni_file(argv[2]);
         }

      YFUK=2;

      if(argv[1][0] == 'u') {
           vykresli_pozadi();
           vykresli_level();
           vykresli_panel();
           hraj();
           stop_modul();
      }
      if(argv[1][0] == 'd') {
        hraj_demo(argv[2],0);
        stop_modul();
      }
   }

/*  konci */
   prepni_do_textu(0x3);
   exit(0);
}


int hraj_demo(byte *p_jmeno, int lep)
{
char   pstr[100];
int    natoceni = 0;
int    akt_demo = 0;
byte   *p_kroky = vyrob_sprit(320,20,8,OSTATNI_LOCK);
byte   *p_demo;
FILE   *f;
int    i;

 if(p_kroky == NULL) {
   tiskni_chybu("Malo pameti !\n",__LINE__);
 }

 if(lep) {
   if((p_demo = malloc(velikost_lepfile(DAT_FILE,p_jmeno)+10)) == NULL)
     tiskni_chybu("Nedostatek pameti !",__LINE__);
     
   nahraj_soubor_lep(0,p_demo,DAT_FILE,p_jmeno);

   if((f = fopen(p_jmeno,"wb")) == NULL) {
     tiskni_chybu("Nemohu zapisovat na disk !",__LINE__);
   }
   fwrite(p_demo,sizeof(byte),velikost_lepfile(DAT_FILE,p_jmeno),f);
   fclose(f);
   free(p_demo);
 }

 nahraj_demo(p_jmeno);
 vykresli_pozadi();
 vykresli_level_grf();

 p_p = p;
 for(i = 0; i < HRACU; i++) {
    vykresli_hrace(i);
    if((!natoceni)&&(p[i].aktivni == 1)) {
       p_p = &p[i];
       kresli_sprit_bar(spr[UKAZATEL_2],p_p->x_pozice*X,p_p->y_pozice*Y,BILA);
       natoceni = 1;
    }
 }

 if(klicu_celkem > 0)
    kresli_sprit_bar(spr[PRVNI_KLIC+klicu_celkem],X_KLICU_POZICE,Y_KLICU_POZICE,BILA);

 if(!lep)
   hraj_hudbu_levelu();

 rozsvit_paletu(PALETA);

 spr_vypln_ctverec(0,0,0,320,20,p_kroky);
 buf_printfn(10,0,fn2,"celkem krok#u: 0",p_kroky);
 kresli_sprit(p_kroky,0,460);
 prohledej_level();

 status = 0; // promena, podle ktere se ukoncuje level

 aktivuj_animace();
 aktualizuj_dolni_listu();

 kresli = demo[akt_demo].prodleva-70;
// Pockej 1 sekundu nez zacnes hrat demo

 do {
// Je to Escape,opust tu gamesu
    if(key[K_ESC]) {
      break;
    }
    updatuj_animace();
    updatuj_samply();

    if(status_anim(posledni_pohyb))
      continue;

    updatuj_animace();

    if(getkba() > 0x80) {
      clrkb();
      continue;
    }
    if(akt_demo == pos_demo)
      break;

    if(demo[akt_demo].prodleva <= kresli) {
      if(demo[akt_demo].hrac != p_p->cislo) {
        prepni();
        updatuj_samply();
        cekej(10);
        continue;
      }

      x_kor = demo[akt_demo].x_kor;
      y_kor = demo[akt_demo].y_kor;
      if(demo[akt_demo].alt)
        key[K_ALT] = 1;
      natoceni = demo[akt_demo].natoceni;
      akt_demo++;

      sy = p_p->y_pozice;
      slx = sx = p_p->x_pozice;
      sly = p_p->y_pozice-YFUK;

      nx = nlx = sx+x_kor;
      ny = sy+y_kor;
      nly = sly+y_kor;

      nnx = nx+x_kor;
      nny = ny+y_kor;

      nnlx = nlx+x_kor;
      nnly = nly+y_kor;

      status = muze_tahnout();

      if(status != 1) {
//        fprintf(stderr,"prvek na nx => %d, nlx = %d, nly = %d\n",pl.level[nly][nlx][PRVEK],nlx,nly);
        akt_demo--;
        continue;
      }

      if(status == 1) {
        p_p->x_pozice = nx;
        p_p->y_pozice = ny;
        pl.natoceni[p_p->cislo] = natoceni;

        kroku++;

        itoa(kroku, pstr, 10);
        spr_vypln_ctverec(0,220,0,100,20,p_kroky);
        buf_printfn(220,0,fn2,pstr,p_kroky);
        kresli_sprit(p_kroky,0,460);
      }
      x_kor = 0;
      y_kor = 0;
      continue;
    }

    if(key[K_N]) {  // Znak N, dalsi skladba
      key[K_N] = 0;
      hraj_hudbu_levelu();
      continue;
    }
    
    if(key[K_X]) {
       key[K_X] = 0;
       if(alt == 1) {
          prepni_do_textu(0x3);
          exit(10);
       }
       continue;
    }

    if(key[K_F10]) {
       key[K_F10] = 0;
       tipni_obrazek(posledni_file++);
       continue;
    }

    clrkb();

 }while(status != 2);

 nuluj_hrace();

 vypni_animace();
 uvolni_vsechny_animace();

 vypln_ctverec(0,0,460,300,20);
 sprintf(pstr,"Konec dema na %d krok#u",kroku);
 printfn(10,460,fn2,pstr);
 getkb();

 stmav_paletu(PALETA);

 zrus_sprit(p_kroky);

 return(status);
}

void posledni_sbohem(void)
{
  byte text[4000];

  memset(text,0,4000);
  nahraj_soubor_lep(0,text,DAT_FILE,"file_id.diz");
  puts(text);
}

/* Chybova procedura na spatne jmeno souboru */
void neni_file(byte *p_jmeno)
{
  vypni_midas();
  zavri_soubor_lep(DAT_FILE);
  vypni_mys();
  vypni_klavesnici();
  prepni_do_textu(0x3);
  printf("\nChyba : Nemuzu otevrit soubor '%s'",p_jmeno);
  getch();
  exit(CHYBA);
}

void tiskni_chybu(byte *p_hlaska, int line)
{
  vypni_midas();
  zavri_soubor_lep(DAT_FILE);
  vypni_mys();
  vypni_klavesnici();
  prepni_do_textu(0x3);
  printf("\n\nChyba na line %d : %s\n",line,p_hlaska);
  getch();
  exit(CHYBA);
}


void kresli_prvek_levelu(DISK_LEVEL *p_lev, int x, int y)
{
 int prvek;
 int sprit;
 int i;
 int x1,y1,sp1;
 int poradi;
 int varianta,v,pr;
 //pp_spr

/*
  word podlaha[YPOLI][XPOLI][10]   __attribute__ ((packed));   // -> podlaha
  word level[YPOLI][XPOLI][10]     __attribute__ ((packed));   //-> herni plocha (dnesni level)
  word hraci[YPOLI][XPOLI]     __attribute__ ((packed));   //-> mapa hracu (dneska sou hraci n levelu)
  [0] -> cislo prvku v poli
  [1] -> poradove cislo grafiky
  [3] -> rotace
*/
  /* Kresleni podlah
  */
  buf_uloz_sprit(pp_spr,x*X,y*Y,p_pozadi);


  prvek = p_lev->podlaha[y][x][PRVEK];
  if ((prvek != NENI_PODLAHA)&&(p_lev->podlaha[y][x][VARIANTA] != SLEPEJ_SPRIT)) {
     sprit = prv[prvek].varianty[p_lev->podlaha[y][x][VARIANTA]];
     buf_kresli_sprit_bar(spr[sprit],0,0,BILA,pp_spr);
  }
  kresli_sprit_bar(pp_spr,x*X,(y+YFUK)*Y,BILA);


  /* Kresleni Hernich prvku
  */
  prvek = p_lev->level[y][x][PRVEK];

  if(prvek >= HERNICH_PRVKU) return;

  if ((prvek != P_ZEME)&&(p_lev->level[y][x][VARIANTA] != SLEPEJ_SPRIT)) {
/*
     printf("\nPred kreslenim prvku %d na x = %d a y = %d\n",prvek,x,y);
     for(i = 0; i < 32; i++) {
        for(j = 0; j < 21; j++) {
           printf("%.2d ",p_lev->level[j][i][PRVEK]);
        }
        printf("\n");
     }
*/   v = p_lev->level[y][x][VARIANTA];
     sprit = prv[prvek].varianty[v];
     if (p_lev->level[y][x][ROTACE]) {
        rotuj_sprit(spr[sprit],pp_spr,p_lev->level[y][x][ROTACE]);
        kresli_sprit_bar(pp_spr,x*X,(y+YFUK)*Y,BILA);
     }
     else
        kresli_sprit_bar(spr[sprit],x*X,(y+YFUK)*Y,BILA);

    // Kresleni podprvku
     pr = v*prv[prvek].podprvku;
     for(i = 0; i < prv[prvek].podprvku; i++) {
        x1 = x+podprv[prv[prvek].podprvky[pr+i]].x_kor;
        y1 = y+podprv[prv[prvek].podprvky[pr+i]].y_kor;
        varianta = podprv[prv[prvek].podprvky[pr+i]].varianty[prv[prvek].pdp_vr[pr+i]];
        sp1 = prv[podprv[prv[prvek].podprvky[pr+i]].druh].varianty[varianta];
        kresli_sprit_bar(spr[sp1],X*x1,Y*(y1+YFUK), BILA);

        p_lev->level[y1][x1][PRVEK] = podprv[prv[prvek].podprvky[pr+i]].druh;
        p_lev->level[y1][x1][VARIANTA] = varianta;
     }
  }

  if(p_lev->hraci[y][x] != SLEPEJ_PRVEK) {
      poradi = p_lev->hraci[y][x];
      kresli_sprit_bar(spr[PRVNI_HRAC+poradi+(p_lev->natoceni[poradi]*HRACU)],x*X,(y+YFUK)*Y,BILA);
      /*
      poradi = pl.hraci[y][x];
      if(p_lev->natoceni[poradi] != 0) {
        rotuj_sprit(spr[PRVNI_HRAC+poradi],pp_spr,p_lev->natoceni[poradi]);
        kresli_sprit_bar(pp_spr,x*X,(y+YFUK)*Y,BILA);
      }
      else
      */
  }
}

void nuluj_hrace(void)
{
   int i;
   
   for(i = 0; i < 5; i++) {
      p[i].cislo = SLEPEJ_PRVEK;      //Cislo = 0 -> neni tady
      p[i].aktivni = 0;
      p[i].x_pozice = 0;
      p[i].y_pozice = 0;
      p[i].lopaty = 0;
      p[i].klice = 0;
      p[i].prvku = 0;
      p[i].pole_spr[0] = 0;
      p[i].pole_spr[1] = 0;
   }
   klicu_celkem = 0;
}

void nuluj_animace(void)
{
 int x,y;

 for(y = 0; y < YPOLI; y++) {
    for(x = 0; x < XPOLI; x++) {
      pl.level[y][x][ANIMACE] = NULL;
    }
 }
 
}

/* vykresleni levelu na obrazovku */
void vykresli_level(void)
{
dword x,y;

   nuluj_hrace();

   for (y = 0; y < YPOLI; y++) {
       for (x = 0; x < XPOLI; x++) {
           kresli_prvek_levelu(&pl,x,y);

           if (pl.hraci[y][x] != SLEPEJ_PRVEK) {
              p[pl.hraci[y][x]].aktivni = 1;
              p[pl.hraci[y][x]].cislo = pl.hraci[y][x];
              p[pl.hraci[y][x]].x_pozice = x;
              p[pl.hraci[y][x]].y_pozice = y+YFUK;
              p[pl.hraci[y][x]].lopaty = 0;
              p[pl.hraci[y][x]].klice = 0;
           }
       }
   }

   p_p = &p[0];
   for (x = 0;x < 5;x++) {
      if (p[x].aktivni == 1) {
         p_p = &p[x];
         kresli_sprit_bar(spr[UKAZATEL_2],p_p->x_pozice*X,p_p->y_pozice*Y,BILA);
         break;
      }
   }

}


void vykresli_level_grf(void)
{
 dword x,y;

 for (y = 0; y < YPOLI; y++) {
     for (x = 0; x < XPOLI; x++) {
         kresli_prvek_levelu(&pl,x,y);
     }
 }
}

void animuj_dvere(int x, int y)
{
 #define AD_DVERE 10

 dword pole_delay[30] = {18,18,18,18,18,18,18,18,18,18,18,18};
 dword pole_rotace[30];
 dword pole_spr[30];
 dword pole_korekci_x[30];
 dword pole_korekci_y[30];
 int i;
 int n_sx,n_sy,sprr;
 int handle,pr;

 if(pl.level[y][x][VARIANTA] != 1)
   return;
 if(pl.level[y][x][ANIMACE] != NULL)
   return;

  pr = pl.level[y][x][PRVEK];

  if(((pr >= P_DVERE1_V_Z)&&(pr <= P_DVERE5_V_Z))||
    ((pr >= P_ID_DVERE1_V_Z)&&(pr <= P_ID_DVERE5_V_Z))) {

    pole_spr[0] = PRVNI_KYBER_LEVEL+30;
    pole_spr[1] = PRVNI_KYBER_LEVEL+60;
  }
  else
    if(((pr >= P_DVERE1_H_Z)&&(pr <= P_DVERE5_H_Z))||
      ((pr >= P_ID_DVERE1_H_Z)&&(pr <= P_ID_DVERE5_H_Z))) {

      pole_spr[0] = PRVNI_KYBER_LEVEL+39;
      pole_spr[1] = PRVNI_KYBER_LEVEL+97;
    }
    else
      if(pr == P_DV_H_Z) {
        pole_spr[0] = PRVNI_KYBER_LEVEL+79;
        pole_spr[1] = PRVNI_KYBER_LEVEL+76;
      }
      else {
        if(pr == P_DV_V_Z) {
          pole_spr[0] = PRVNI_KYBER_LEVEL+73;
          pole_spr[1] = PRVNI_KYBER_LEVEL+70;
        }
        else
          return;
      }

  for(i = 2; i < 10; i+=2) {
     pole_spr[i] = pole_spr[i-2];
     pole_spr[i+1] = pole_spr[i-1];
  }

  n_sx = x*X; n_sy = (y+YFUK)*Y;

  handle = registruj_animaci(vyrob_animaci(AD_DVERE,1,0,1),X,Y,spr,&kresli_int, BILA);
  for(i = 0; i < AD_DVERE; i++) {
     pole_korekci_x[i] = 0;
     pole_korekci_y[i] = 0;
     pole_rotace[i] = 0;
     pole_delay[i] = random()%30+1;
  }
  
  dopln_framy(handle,pole_spr,pole_korekci_x,pole_korekci_y,pole_rotace,pole_delay);
  nastav_souradnice_anim(handle,n_sx,n_sy,0,0);
  
  smaz_pozadi(handle);
  pridej_pozadi(handle,A_POZADI,p_pozadi,n_sx,n_sy-YFUK*Y);
  if(pl.podlaha[y][x][PRVEK] != NENI_PODLAHA) {
    sprr = prv[P_ZEME].varianty[pl.podlaha[y][x][VARIANTA]];
    pridej_pozadi(handle,A_SPRIT,spr[sprr],0,0);
  }
  play_anim(handle);
  pl.level[y][x][ANIMACE] = handle;
}

void zrus_dvere(int x, int y)
{
  if(pl.level[y][x][VARIANTA] != 1)
    return;
 if(pl.level[y][x][ANIMACE] == NULL)
   return;

 stop_anim(pl.level[y][x][ANIMACE]);
 uvolni_animaci(pl.level[y][x][ANIMACE]);
 pl.level[y][x][ANIMACE] = NULL;
}

void prohledej_level(void)
{
 int x,y;

 vygeneruj_zakladni_animace();

 for(y = 0; y < YPOLI; y++) {
    for(x = 0; x < XPOLI; x++) {
       animuj_dvere(x,y);
    }
 }
}



/* samotna hra */
int hraj(void)
{
int    i;
int    demo_on = 0;
int    pom;
int    rychly_ulozeni = 0;
char   pstr[10];
char   file[100];
int    natoceni;
int    save_demo;
byte   *p_kroky = vyrob_sprit(320,20,8,OSTATNI_LOCK);
int    loadu = load_limit;
byte   *p_pom;
int    quick = 0;
int    uz_jel = 4;

 if(p_kroky == NULL) {
   tiskni_chybu("Malo pameti !\n",__LINE__);
 }

 kroku = 0;
 ve_hre = 1;

// testuj_pamet("Pred loadem gamemenu");
 nahraj_gamemenu();
// testuj_pamet("po loadu gamemenu");

 hraj_hudbu_levelu();
// testuj_pamet("po hrani hudby");

 spr_vypln_ctverec(0,0,0,320,20,p_kroky);
 buf_printfn(10,0,fn2,"celkem krok#u: 0",p_kroky);
 kresli_sprit(p_kroky,0,460);

 prohledej_level();
// testuj_pamet("po prohledani levelu");

 rozsvit_paletu(PALETA);

 status = 0; // promena, podle ktere se ukoncuje level
 posledni_pohyb = 0;

 aktivuj_animace();
 aktualizuj_dolni_listu();

// testuj_pamet("zac. smyc.");

 do {
    /* Vykresli dalsi frame animaci do animacnich bufferu
    ten se zobrazi v timeru */
    updatuj_animace();
    updatuj_samply();

    if(status_anim(posledni_pohyb) == 1)
      continue;

    updatuj_animace();
    
    if(getkba() > 0x80) {
      clrkb();
      continue;
    }

    pom = key[K_NAHORU]+key[K_DOLU]+key[K_DOPRAVA]+key[K_DOLEVA];

    if(pom > 1) {
      clrkb();
      continue;
    }

    if(pom == 1) {
      if(key[K_NAHORU]) {  //nahoru
         y_kor = -1;
         natoceni = 0;
      }
      else
        if(key[K_DOLEVA]) { // do leva
           x_kor = -1;
           natoceni = 3;
        }
        else
          if(key[K_DOPRAVA]) { // do prava
             x_kor = 1;
             natoceni = 1;
          }
          else {
             if(key[K_DOLU]) {
               y_kor = 1;
               natoceni = 2;
             }
             else
              continue;
          }

      sy = p_p->y_pozice;
      slx = sx = p_p->x_pozice;
      sly = p_p->y_pozice-YFUK;

      nx = nlx = sx+x_kor;
      ny = sy+y_kor;
      nly = sly+y_kor;

      nnx = nx+x_kor;
      nny = ny+y_kor;

      nnlx = nlx+x_kor;
      nnly = nly+y_kor;

      status = muze_tahnout();

      if(status == 1) {
        p_p->x_pozice = nx;
        p_p->y_pozice = ny;
        pl.natoceni[p_p->cislo] = natoceni;

        if((demo_on)&&(pos_demo < MAX_DEMO_KROKU)) {
          demo[pos_demo].x_kor = x_kor;
          demo[pos_demo].y_kor = y_kor;
          demo[pos_demo].alt = key[K_ALT];
          demo[pos_demo].hrac = p_p->cislo;
          demo[pos_demo].prodleva = kresli;
          demo[pos_demo].natoceni = natoceni;
          pos_demo++;
        }
        kroku++;

        itoa(kroku, pstr, 10);
        spr_vypln_ctverec(0,220,0,100,20,p_kroky);
        buf_printfn(220,0,fn2,pstr,p_kroky);
        kresli_sprit(p_kroky,0,460);
      }
      else {
        if(!status) {
          if(y_kor == -1)
            natoceni = 0;
          if(x_kor == -1)
            natoceni = 3;
          if(x_kor == 1)
            natoceni = 1;
          if(y_kor == 1)
            natoceni = 2;

          if(uz_jel != natoceni) {
            pl.natoceni[p_p->cislo] = natoceni;
            kresli_prvek_levelu(&pl,slx,sly);
            uz_jel = natoceni;
          }
        }
      }
      x_kor = 0;
      y_kor = 0;
      continue;
    }

    if(key[K_N]) {  // Znak N, dalsi skladba
      key[K_N] = 0;
      hraj_hudbu_levelu();
      continue;
    }
    
// Je to Escape,opust tu gamesu
    if(key[K_ESC]) {
       key[K_ESC] = 0;
       if(z_editoru)
          goto konec_cteni;

       if((i = dilema()) == 3) { // Konec
          status = 0;
          goto konec_cteni;
       }
       if(i == 0) {  // Restart
          stmav_paletu(PALETA);
          smaz_paletu();
          vypni_animace();
          uvolni_vsechny_animace();
          if(!uziv_level)
            obnov_level_lep(levelset[levelset[0][1]],DAT_FILE,&pl);
          else
            if(obnov_level(levelset[levelset[0][1]],&pl) == NULL) {
              tiskni_chybu("??? Proboha, jak se toto povedlo ? Interni chyba c.1, napis na komatovy !",__LINE__);
            }
          prohledej_level();
          posledni_pohyb = 0;
          kroku = 0;
          loadu = load_limit;
          if(demo_on)
            key[K_D] = 1;
          rychly_ulozeni = 0;
          aktivuj_animace();
          nuluj_hrace();
          vykresli_pozadi();
          vykresli_level();
          vykresli_panel();
          aktualizuj_dolni_listu();

          itoa(kroku, pstr, 10);
          spr_vypln_ctverec(0,220,0,100,20,p_kroky);
          buf_printfn(220,0,fn2,pstr,p_kroky);
          kresli_sprit(p_kroky,0,460);

          rozsvit_paletu(PALETA);
       }
       if(i == 1) { // Load
         key[K_F3] = 1;
       }
       if(i == 2) { // Save
         key[K_F2] = 1;
       }
    }

    // Prepnuti mezi beruskama
    if(key[K_TAB]) {
       key[K_TAB] = 0;
       prepni();
       uz_jel = pl.natoceni[p_p->cislo];
       continue;
    }
  
    if(key[K_X]) {
       key[K_X] = 0;
       if(alt == 1) {
          prepni_do_textu(0x3);
          if(demo_on) {
            uloz_demo(jmeno_dm);
          }

          exit(0);
       }
       else
         continue;
    }

    if(key[K_F1]) {
       key[K_F1] = 0;
       vypni_animace();
       stmav_paletu(PALETA);
       nahraj_menu_napoveda();
       spust_oci();
       help_ig();
       stop_oci();
       uvolni_menu_napoveda();
       rozsvit_paletu(PALETA);
       key[K_ESC] = 0;
       /* tady by mel byt help
       */
       aktivuj_animace();
       continue;
    }
    if(key[K_F2]) {
       key[K_F2] = 0;
       /* tady by mel byt rychly save
       */
       if(!status_anim(anim[A_BEDNA1])) {
         if(!status_anim(anim[A_BEDNA2])) {
           if(!status_anim(anim[A_BEDNA3])) {
             if(!status_anim(anim[A_BEDNA4])) {
               if(!status_anim(anim[A_BEDNA5])) {
                 if(!status_anim(anim[A_BEDNA6])) {
                   goto neni_bedna;
                 }
               }
             }
           }
         }
       }
       continue;

       neni_bedna:

       if(demo_on) {
         vypln_ctverec(0,0,460,400,20);
         printfn(10,460,fn2,"U dema nelze ukl$adat...");
         cekej(10);
         vypln_ctverec(0,0,460,400,20);
         kresli_sprit(p_kroky,0,460);
         continue;
       }

       if(!loadu) {
         vypln_ctverec(0,0,460,300,20);
         printfn(10,460,fn2,"U^z nelze ukl$adat...");
         cekej(10);
         kresli_sprit(p_kroky,0,460);
         continue;
       }
       else
         loadu--;

       vypln_ctverec(0,0,460,300,20);
       printfn(10,460,fn2,"Ukl$ad$am $urove^n...");
       qs = pl;
       s_klice = klicu_celkem;
       for(i = 0; i < 5; i++)
          s[i] = p[i];

       p_s = p_p;
       if(demo_on)
         save_demo = pos_demo;

       rychly_ulozeni = 1;
       cekej(10);
       kresli_sprit(p_kroky,0,460);
       continue;
    }

    if(key[K_F3]) {
       key[K_F3] = 0;
       /* tady by mel byt rychly load
       */
       if(demo_on) {
         vypln_ctverec(0,0,460,400,20);
         printfn(10,460,fn2,"U dema nelze nahr$avat...");
         cekej(10);
         vypln_ctverec(0,0,460,400,20);
         kresli_sprit(p_kroky,0,460);
         continue;
       }

       if(!loadu) {
         vypln_ctverec(0,0,460,300,20);
         printfn(10,460,fn2,"U^z nelze nahr$avat...");
         cekej(10);
         kresli_sprit(p_kroky,0,460);
         continue;
       }
       else
         loadu--;

       if(!rychly_ulozeni) {
          vypln_ctverec(0,0,460,300,20);
          printfn(10,460,fn2,"Nen$i co nahr$at...");
          cekej(10);
          kresli_sprit(p_kroky,0,460);
          continue;
       }
       vypln_ctverec(0,0,460,300,20);
       printfn(10,460,fn2,"Nahr$av$am $urove^n...");

       stmav_paletu(PALETA);
       vypni_animace();
       uvolni_vsechny_animace();

       nuluj_hrace();
       pl = qs;
       for(i = 0; i < 5; i++)
          p[i] = s[i];

       p_p = p_s;
       klicu_celkem = s_klice;

       nuluj_animace();
       prohledej_level();
       posledni_pohyb = 0;
       aktivuj_animace();
       vykresli_pozadi();
       vykresli_level_grf();
       vykresli_panel();
       aktualizuj_dolni_listu();

       if(demo_on)
         pos_demo = save_demo;
       kresli_sprit(p_kroky,0,460);
       rozsvit_paletu(PALETA);
       continue;
    }

    // Klavesa D -> spusti nahravani dema
    if(key[K_D]) {
       key[K_D] = 0;

       strcpy(jmeno_dm,levelset[levelset[0][1]]);
       if((p_pom = strchr(jmeno_dm,'.')) != NULL)
         *p_pom = 0;            

       strcat(jmeno_dm,KONCOVKA_DM);
          
       sprintf(file,"nahr$av$am demo do %s",jmeno_dm);
       vypln_ctverec(0,0,460,300,20);
       printfn(10,460,fn2,file);
       d_kroku = kroku;
       pos_demo = 0;
       dm = pl;

       s_klice = klicu_celkem;
       for(i = 0; i < 5; i++)
          s[i] = p[i];

       cekej(10);
       vypln_ctverec(0,0,460,450,20);
       kresli_sprit(p_kroky,0,460);
       demo_on = 1;
       continue;
    }

    if(key[K_F4]) {
       key[K_F4] = 0;
       vypni_animace();
       nastaveni_ig();
       aktivuj_animace();
       continue;
    }

    if(key[K_F10]) {
       key[K_F10] = 0;
       tipni_obrazek(posledni_file++);
       continue;
    }
/*
    if(key[K_C]) {
       key[K_C] = 0;
       status = 2;
       quick = 1;
    }
*/
    if(blue_death_on == 1)
      blue_death();

    clrkb();

 }while(status != 2);
 konec_cteni:

 nuluj_hrace();

 vypni_animace();
 uvolni_vsechny_animace();

 if(demo_on) {
   uloz_demo(jmeno_dm);
 }

 stmav_paletu(PALETA);

 uvolni_gamemenu();

// testuj_pamet("konec hraj");

 ve_hre = 0;
 zrus_sprit(p_kroky);
 if((!quick)&&(status == 2)&&(samply))
   cekej(30);
 return(status);
}


int posun_berusku(void)
{
  dword pole_delay[30] = {2,2,2,2,2,2,2,2,2,2,0};
  dword pole_delay_fast[30] = {1,1,1,1,1,1,1,1,1,1,0};
//  dword pole_delay[30] = {20,20,20,20,20,20,20,20,20,20,0};
//  dword pole_delay_fast[30] = {10,10,10,10,10,10,10,10,10,10,0};
  dword pole_rotace[30];
  dword pole_spr[30];
  dword pole_korekci_x[30];
  dword pole_korekci_y[30];
  int i;
  int handle;
  int n_sx,n_sy,n_nx,n_ny,sprr;
  int rotace;
  int x_zac,y_zac;
  int x_kon,y_kon;
  int delka_smp;

  if(alt)
    delka_smp = DELKA_KROKY_1;
  else
    delka_smp = DELKA_KROKY_2;
  
  if(pl.podlaha[nly][nlx][PRVEK] == NENI_PODLAHA)
    hraj_sampl(KROKY_POZADI,delka_smp,PRIORITA_KROKY);
  else {
     if(pl.podlaha[nly][nlx][VARIANTA] < 5)
       hraj_sampl(KROKY_BLATO,delka_smp,PRIORITA_KROKY);
     else
       hraj_sampl(KROKY_MRAMOR,delka_smp,PRIORITA_KROKY);
  }


  n_sx = sx*X; n_sy = sy*Y;
  n_nx = nx*X; n_ny = ny*Y;

  pole_spr[0] = (pl.hraci[nly][nlx]*10)+PRVNI_ANIMACE_HRACE;
  for(i = 0; i < HR_FRAMU; i++) {
     pole_spr[i+1] = pole_spr[i]+1;
  }
/*
  pole_spr[0] = 0;
  pole_spr[HR_FRAMU] = 1;
*/
  if(x_kor == 1) { // doprava
    rotace = A_DOPRAVA;
    x_zac = 0; y_zac = 0;
    x_kon = X; y_kon = 0;
  }
  if(x_kor == -1) { // doleva
    rotace = A_DOLEVA;
    n_nx -= X; n_sx -= X;
    x_zac = X; y_zac = 0;
    x_kon = 0; y_kon = 0;
  }
  if(y_kor == -1) { //nahoru
    rotace = A_NAHORU;
    n_ny -= Y; n_sy -= Y;
    x_zac = 0; y_zac = Y;
    x_kon = 0; y_kon = 0;
  }
  if(y_kor == 1) { // dolu
    rotace = A_DOLU;
    x_zac = 0; y_zac = 0;
    x_kon = 0; y_kon = Y;
  }

  pl.natoceni[p_p->cislo] = rotace;
  handle = anim[rotace];
  for(i = 0; i < HR_FRAMU; i++) {
     pole_korekci_x[i] = x_kor*2*(i+1);
     pole_korekci_y[i] = y_kor*2*(i+1);
     pole_rotace[i] = rotace;
  }

  if(key[K_ALT])
    dopln_framy(handle,pole_spr,pole_korekci_x,pole_korekci_y,pole_rotace,pole_delay_fast);
  else
    dopln_framy(handle,pole_spr,pole_korekci_x,pole_korekci_y,pole_rotace,pole_delay);

  nastav_souradnice_anim(handle,n_sx,n_sy,x_zac,y_zac);

  smaz_pozadi(handle);
  pridej_pozadi(handle,A_POZADI,p_pozadi,n_sx,n_sy-YFUK*Y);

  if(pl.podlaha[sly][slx][PRVEK] != NENI_PODLAHA) {
    sprr = prv[P_ZEME].varianty[pl.podlaha[sly][slx][VARIANTA]];
    pridej_pozadi(handle,A_SPRIT,spr[sprr],x_zac,y_zac);
  }
  if(pl.podlaha[nly][nlx][PRVEK] != NENI_PODLAHA) {
    sprr = prv[P_ZEME].varianty[pl.podlaha[nly][nlx][VARIANTA]];
    pridej_pozadi(handle,A_SPRIT,spr[sprr],x_kon,y_kon);

  }

  if(pl.level[sly][slx][PRVEK] != P_ZEME) {
    sprr = prv[pl.level[sly][slx][PRVEK]].varianty[pl.level[sly][slx][VARIANTA]];
    if (pl.level[sly][slx][ROTACE]) {
       rotuj_sprit(spr[sprr],pp_anim,pl.level[sly][slx][ROTACE]);
       pridej_pozadi(handle,A_SPRIT,pp_anim,x_zac,y_zac);
    }
    else
       pridej_pozadi(handle,A_SPRIT,spr[sprr],x_zac,y_zac);
  }

  if(pl.level[nly][nlx][PRVEK] != P_ZEME) {
    sprr = prv[pl.level[nly][nlx][PRVEK]].varianty[pl.level[nly][nlx][VARIANTA]];
    if (pl.level[nly][nlx][ROTACE]) {
       rotuj_sprit(spr[sprr],pp_anim,pl.level[nly][nlx][ROTACE]);
       pridej_pozadi(handle,A_SPRIT,pp_anim,x_kon,y_kon);
    }
    else
       pridej_pozadi(handle,A_SPRIT,spr[sprr],x_kon,y_kon);
  }

  play_anim(handle);
  return(handle);
}

int posun_berusku_a_bednu(void)
{
  dword pole_delay[30] = {0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0};
  dword pole_delay_fast[30] = {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0};
  dword pole_rotace[30];
  dword pole_spr[30];
  dword pole_korekci_x[30];
  dword pole_korekci_y[30];
  int i;
  int handle;
  int n_sx,n_sy,n_nx,n_ny;
  int rotace;
  int x_zac,y_zac,x_kon_1,y_kon_1,x_kon_2,y_kon_2;
  int sprr;
  int delka_smp;

  if(alt)
    delka_smp = DELKA_KROKY_1;
  else
    delka_smp = DELKA_KROKY_2;
  
  hraj_sampl(POSUN,delka_smp,PRIORITA_POSUN);
  
  if(pl.podlaha[nly][nlx][PRVEK] == NENI_PODLAHA)
    hraj_sampl(KROKY_POZADI,delka_smp,PRIORITA_KROKY);
  else {
     if(pl.podlaha[nly][nlx][VARIANTA] < 5)
       hraj_sampl(KROKY_BLATO,delka_smp,PRIORITA_KROKY);
     else
       hraj_sampl(KROKY_MRAMOR,delka_smp,PRIORITA_KROKY);
  }

  n_sx = sx*X; n_sy = sy*Y;
  n_nx = nx*X; n_ny = ny*Y;

  pole_spr[0] = (pl.hraci[nly][nlx]*10)+PRVNI_ANIMACE_HRACE;
  for(i = 0; i < HRB_FRAMU; i+=2)
     pole_spr[i+2] = pole_spr[i]+1;

  pole_spr[1] = prv[pl.level[nnly][nnlx][PRVEK]].varianty[pl.level[nnly][nnlx][VARIANTA]];
  for(i = 3; i < HRB_FRAMU; i+=2)
     pole_spr[i] = prv[pl.level[nnly][nnlx][PRVEK]].varianty[pl.level[nnly][nnlx][VARIANTA]];

  if(x_kor == 1) { // doprava
    rotace = A_DOPRAVA;
    x_zac = 0; y_zac = 0;
    x_kon_1 =   X; y_kon_1 = 0;
    x_kon_2 = 2*X; y_kon_2 = 0;
  }
  if(x_kor == -1) { // doleva
    rotace = A_DOLEVA;
    n_nx -= 2*X; n_sx -= 2*X;
    x_zac = 2*X; y_zac = 0;
    x_kon_1 = X; y_kon_1 = 0;
    x_kon_2 = 0; y_kon_2 = 0;
  }
  if(y_kor == -1) { //nahoru
    rotace = A_NAHORU;
    n_ny -= 2*Y; n_sy -= 2*Y;
    x_zac   = 0; y_zac = 2*Y;
    x_kon_1 = 0; y_kon_1 = Y;
    x_kon_2 = 0; y_kon_2 = 0;
  }
  if(y_kor == 1) { // dolu
    rotace = A_DOLU;
    x_zac = 0; y_zac = 0;
    x_kon_1 = 0; y_kon_1 = Y;
    x_kon_2 = 0; y_kon_2 = 2*Y;
  }

  handle = anim[rotace+4];
  pl.natoceni[p_p->cislo] = rotace;

  for(i = 0; i < HRB_FRAMU;) {
     pole_korekci_x[i] = x_kor*(i+2);
     pole_korekci_y[i] = y_kor*(i+2);
     pole_rotace[i++] = rotace;
     pole_korekci_x[i] = x_kor*(i+1)+x_kor*X;
     pole_korekci_y[i] = y_kor*(i+1)+y_kor*Y;
     pole_rotace[i++] = pl.level[nnly][nnlx][ROTACE];
  }

  if(key[K_ALT])
    dopln_framy(handle,pole_spr,pole_korekci_x,pole_korekci_y,pole_rotace,pole_delay_fast);
  else
    dopln_framy(handle,pole_spr,pole_korekci_x,pole_korekci_y,pole_rotace,pole_delay);

  nastav_souradnice_anim(handle,n_sx,n_sy,x_zac,y_zac);

  smaz_pozadi(handle);
  pridej_pozadi(handle,A_POZADI,p_pozadi,n_sx,n_sy-YFUK*Y);


  if(pl.podlaha[sly][slx][PRVEK] != NENI_PODLAHA) {
    sprr = prv[P_ZEME].varianty[pl.podlaha[sly][slx][VARIANTA]];
    pridej_pozadi(handle,A_SPRIT,spr[sprr],x_zac,y_zac);
  }
  if(pl.podlaha[nly][nlx][PRVEK] != NENI_PODLAHA) {
    sprr = prv[P_ZEME].varianty[pl.podlaha[nly][nlx][VARIANTA]];
    pridej_pozadi(handle,A_SPRIT,spr[sprr],x_kon_1,y_kon_1);
  }
  if(pl.podlaha[nnly][nnlx][PRVEK] != NENI_PODLAHA) {
    sprr = prv[P_ZEME].varianty[pl.podlaha[nnly][nnlx][VARIANTA]];
    pridej_pozadi(handle,A_SPRIT,spr[sprr],x_kon_2,y_kon_2);
  }


  if(pl.level[sly][slx][PRVEK] != P_ZEME) {
    sprr = prv[pl.level[sly][slx][PRVEK]].varianty[pl.level[sly][slx][VARIANTA]];
    if (pl.level[sly][slx][ROTACE]) {
       rotuj_sprit(spr[sprr],pp_anim,pl.level[sly][slx][ROTACE]);
       pridej_pozadi(handle,A_SPRIT,pp_anim,x_zac,y_zac);
    }
    else
       pridej_pozadi(handle,A_SPRIT,spr[sprr],x_zac,y_zac);
  }

  if(pl.level[nly][nlx][PRVEK] != P_ZEME) {
    sprr = prv[pl.level[nly][nlx][PRVEK]].varianty[pl.level[nly][nlx][VARIANTA]];
    if (pl.level[nly][nlx][ROTACE]) {
       rotuj_sprit(spr[sprr],pp_anim,pl.level[nly][nlx][ROTACE]);
       pridej_pozadi(handle,A_SPRIT,pp_anim,x_kon_1,y_kon_1);
    }
    else
       pridej_pozadi(handle,A_SPRIT,spr[sprr],x_kon_1,y_kon_1);
  }

/*
  if(pl.level[nnly][nnlx][PRVEK] != P_ZEME) {
    sprr = prv[pl.level[nnly][nnlx][PRVEK]].varianty[pl.level[nnly][nnlx][VARIANTA]];
    pridej_pozadi(handle,A_SPRIT,spr[sprr],x_kon_2,y_kon_2);
  }
*/
  play_anim(handle);

  return(handle);
}

int animuj_bednu(dword x, dword y)
{
 dword pole_delay[30] = {3,9,6,9,9,9,3,3,1,1,1};
 dword pole_rotace[30];
 dword pole_spr[30];
 dword pole_korekci_x[30];
 dword pole_korekci_y[30];
 int i;
 int n_sx,n_sy,sprr;
 int handle;

 hraj_sampl(VYBUCH,DELKA_VYBUCH,PRIORITA_VYBUCH);

 n_sx = x*X; n_sy = (y+YFUK)*Y;

 pole_spr[0] = PRVNI_GLOBAL_LEVEL+7;
 for(i = 0; i < AD_BEDNA; i++) {
    pole_spr[i+1] = pole_spr[i]+1;
 }

 handle = anim[A_BEDNA1];
 if(status_anim(anim[A_BEDNA1]) == 1) {
   handle = anim[A_BEDNA2];
   if(status_anim(anim[A_BEDNA2]) == 1) {
     handle = anim[A_BEDNA3];
     if(status_anim(anim[A_BEDNA3]) == 1) {
       handle = anim[A_BEDNA4];
       if(status_anim(anim[A_BEDNA4]) == 1) {
         handle = anim[A_BEDNA5];
         if(status_anim(anim[A_BEDNA5]) == 1) {
           handle = anim[A_BEDNA6];
           if(status_anim(anim[A_BEDNA6]) == 1) {
             handle = anim[A_BEDNA7];
           }
         }
       }
     }
   }
 }
 for(i = 0; i < AD_BEDNA; i++) {
    pole_korekci_x[i] = 0;
    pole_korekci_y[i] = 0;
    pole_rotace[i] = 0;/*random()%4;*/
 }

 dopln_framy(handle,pole_spr,pole_korekci_x,pole_korekci_y,pole_rotace,pole_delay);
 nastav_souradnice_anim(handle,n_sx,n_sy,0,0);
 
 smaz_pozadi(handle);
 pridej_pozadi(handle,A_POZADI,p_pozadi,n_sx,n_sy-YFUK*Y);
 
 if(pl.podlaha[y][x][PRVEK] != NENI_PODLAHA) {
   sprr = prv[P_ZEME].varianty[pl.podlaha[y][x][VARIANTA]];
   pridej_pozadi(handle,A_SPRIT,spr[sprr],0,0);
 }
 pridej_zmenu(handle,x,y,P_ZEME,0,0,0,0,0);
 play_anim(handle);

 return(handle);
}

int animuj_kamen(dword varianta, dword x, dword y)
{
 dword pole_delay[30] = {9,9,9,9,9,9,9,9,1,1,1};
 dword pole_rotace[30];
 dword pole_spr[30];
 dword pole_korekci_x[30];
 dword pole_korekci_y[30];
 int i;
 int n_sx,n_sy,sprr;
 int handle;

 n_sx = x*X; n_sy = (y+YFUK)*Y;

 if(varianta == 0) {
   pole_spr[0] = PRVNI_KLASIK_LEVEL+1;
   pole_spr[1] = PRVNI_KLASIK_LEVEL+2;
   pole_spr[2] = PRVNI_KLASIK_LEVEL+3;
   pole_spr[3] = PRVNI_GLOBAL_LEVEL+13;
 }

 if(varianta == 1) {
   pole_spr[0] = PRVNI_KYBER_LEVEL+47;
   pole_spr[1] = PRVNI_KYBER_LEVEL+48;
   pole_spr[2] = PRVNI_KYBER_LEVEL+49;
   pole_spr[3] = PRVNI_GLOBAL_LEVEL+13;
 }

 handle = anim[A_KAMEN];
 for(i = 0; i < AD_KAMEN; i++) {
    pole_korekci_x[i] = 0;
    pole_korekci_y[i] = 0;
    pole_rotace[i] = 0;
 }

 dopln_framy(handle,pole_spr,pole_korekci_x,pole_korekci_y,pole_rotace,pole_delay);
 nastav_souradnice_anim(handle,n_sx,n_sy,0,0);
 
 smaz_pozadi(handle);
 pridej_pozadi(handle,A_POZADI,p_pozadi,n_sx,n_sy-YFUK*Y);
 
 if(pl.podlaha[y][x][PRVEK] != NENI_PODLAHA) {
   sprr = prv[P_ZEME].varianty[pl.podlaha[y][x][VARIANTA]];
   pridej_pozadi(handle,A_SPRIT,spr[sprr],0,0);
 }
 play_anim(handle);
 return(handle);
}


int animuj_exit(dword x, dword y)
{
 dword pole_delay[30] = {15,15,15,15,15,15,15,15,15,15,15};
 dword pole_rotace[30];
 dword pole_spr[30];
 dword pole_korekci_x[30];
 dword pole_korekci_y[30];
 int i;
 int n_sx,n_sy;
 int handle;


 n_sx = x*X; n_sy = (y+YFUK)*Y;
/*
#define A_EXIT      10
#define AD_EXIT     6
*/


 pole_spr[0] = PRVNI_KLASIK_LEVEL+42;
 pole_spr[1] = PRVNI_KLASIK_LEVEL+43;
 pole_spr[2] = PRVNI_KLASIK_LEVEL+44;
 pole_spr[3] = PRVNI_KLASIK_LEVEL+45;
 pole_spr[4] = PRVNI_KLASIK_LEVEL+44;
 pole_spr[5] = PRVNI_KLASIK_LEVEL+43;


 handle = registruj_animaci(vyrob_animaci(AD_EXIT,1,0,1),X,Y,spr,&kresli_int, BILA);
 for(i = 0; i < AD_EXIT; i++) {
    pole_korekci_x[i] = 0;
    pole_korekci_y[i] = 0;
    pole_rotace[i] = 0;
 }

 dopln_framy(handle,pole_spr,pole_korekci_x,pole_korekci_y,pole_rotace,pole_delay);
 nastav_souradnice_anim(handle,n_sx,n_sy,0,0);
 
 smaz_pozadi(handle);

 play_anim(handle);
 pl.level[y][x][ANIMACE] = handle;
 return(handle);
}


void kopiruj_prvek(int sx, int sy, int nx, int ny)
{
  pl.level[ny][nx][PRVEK]    = pl.level[sy][sx][PRVEK];
  pl.level[ny][nx][VARIANTA] = pl.level[sy][sx][VARIANTA];
  pl.level[ny][nx][ROTACE]   = pl.level[sy][sx][ROTACE];
}

void soucastny_pole(int handle)
{
 dword varr = pl.level[sly][slx][VARIANTA];

   switch(pl.level[sly][slx][PRVEK]) {

      //Dvere spec pro barevnou berusku
      case P_ID_DVERE1_H_O :
      case P_ID_DVERE2_H_O :
      case P_ID_DVERE3_H_O :
      case P_ID_DVERE4_H_O :
      case P_ID_DVERE5_H_O :
      case P_ID_DVERE1_V_O :
      case P_ID_DVERE2_V_O :
      case P_ID_DVERE3_V_O :
      case P_ID_DVERE4_V_O :
      case P_ID_DVERE5_V_O :
         if(varr == 0)
            pridej_zmenu(handle,slx,sly,pl.level[sly][slx][PRVEK]+5,1,1,
                        START_SMP,ZAVRENI,DELKA_ZAVRENI);
         else
            pridej_zmenu(handle,slx,sly,pl.level[sly][slx][PRVEK]+5,1,1,
                        START_SMP,MODRY_BLESK,DELKA_MODRY_BLESK);
            return;

      case P_ID_DVERE1_H_Z :
      case P_ID_DVERE2_H_Z :
      case P_ID_DVERE3_H_Z :
      case P_ID_DVERE4_H_Z :
      case P_ID_DVERE5_H_Z :
      case P_ID_DVERE1_V_Z :
      case P_ID_DVERE2_V_Z :
      case P_ID_DVERE3_V_Z :
      case P_ID_DVERE4_V_Z :
      case P_ID_DVERE5_V_Z :
         if(varr == 0)
            pridej_zmenu(posledni_pohyb,slx,sly,pl.level[sly][slx][PRVEK]+5,1,1,
                        START_SMP,ZAVRENI,DELKA_ZAVRENI);
         else
            pridej_zmenu(posledni_pohyb,slx,sly,pl.level[sly][slx][PRVEK]+5,1,1,
                        START_SMP,MODRY_BLESK,DELKA_MODRY_BLESK);
         return;

      case P_DV_H_O:
      case P_DV_V_O:
         if(varr == 0)
           pridej_zmenu(posledni_pohyb,slx,sly,pl.level[sly][slx][PRVEK]+1,1,1,
                       START_SMP,ZAVRENI,DELKA_ZAVRENI);
         else
           pridej_zmenu(posledni_pohyb,slx,sly,pl.level[sly][slx][PRVEK]+1,1,1,
                       START_SMP,MODRY_BLESK,DELKA_MODRY_BLESK);
         return;
         
      default:
         return;
   }
}

dword muze_tahnout/* do haje ! */(void)
{
 byte  st;
 dword handle;
 dword varianta;
 int x,y;

   // Nestoji tam hrac ?
   if(pl.hraci[nly][nlx] != SLEPEJ_PRVEK) {
      return(0);
   }

   //Neni to mino oblast ?
   if((ny >= YPOLI+YFUK)||(nx >= XPOLI)||(nx < 0)||(ny < YFUK)) {
      return(0);
   }

   st = pl.level[nly][nlx][PRVEK];

   // Neni to mimo oblast pri tahnuti bedny
   if((st == BEDNA1)||(st == TNT1)) {
      if((nny >= YPOLI+YFUK)||(nnx >= XPOLI)||(nnx < 0)||(nny < YFUK)) {
         return(0);
      }
      if(pl.hraci[nnly][nnlx] != SLEPEJ_PRVEK) {
         return(0);
      }
   }

   switch(pl.level[nly][nlx][PRVEK]) {

      case P_ZEME :
         pl.hraci[nly][nlx] = pl.hraci[sly][slx];
         pl.hraci[sly][slx] = SLEPEJ_PRVEK;

         posledni_pohyb = posun_berusku();
         soucastny_pole(posledni_pohyb);

         return(1);

      case P_TNT :
         if(pl.level[nnly][nnlx][PRVEK] == P_ZEME) {
            kopiruj_prvek(nlx,nly,nnlx,nnly);
            pl.level[nly][nlx][PRVEK] = P_ZEME;
            pl.hraci[nly][nlx] = pl.hraci[sly][slx];
            pl.hraci[sly][slx] = SLEPEJ_PRVEK;
            posledni_pohyb = posun_berusku_a_bednu();
            soucastny_pole(posledni_pohyb);
            return(1);
         }

         if(pl.level[nnly][nnlx][PRVEK] == P_BEDNA) {
            pl.level[nly][nlx][PRVEK] = P_ZEME;
            pl.level[nnly][nnlx][PRVEK] = P_STENA;
            pl.hraci[nly][nlx] = pl.hraci[sly][slx];
            pl.hraci[sly][slx] = SLEPEJ_PRVEK;
            obnov_pozadi(nnlx,nnly);
            posledni_pohyb = posun_berusku();
            animuj_bednu(nnlx,nnly);
            soucastny_pole(posledni_pohyb);
            return(1);
         }
         else {
           return(0);
         }

      case P_BEDNA :  //obycejna bedna,je za ni podlaha
         if(pl.level[nnly][nnlx][PRVEK] == P_ZEME) {
            kopiruj_prvek(nlx,nly,nnlx,nnly);
            pl.level[nly][nlx][PRVEK] = P_ZEME;
            pl.hraci[nly][nlx] = pl.hraci[sly][slx];
            pl.hraci[sly][slx] = SLEPEJ_PRVEK;
            posledni_pohyb = posun_berusku_a_bednu();
            soucastny_pole(posledni_pohyb);
            return(1);
         }
         else {
            return(0);
         }

      case P_EXIT :   //je to exit
         if(klicu_celkem >= 5) {
            stop_modul();
            hraj_sampl(SAMPL_OK,DELKA_SAMPL_OK,PRIORITA_SAMPL_OK);
            updatuj_samply();
            return(2);
         }
         else {
            return(0);
         }

      case P_KLIC : //klic
         if(klicu_celkem >= 5) {
            return(0);
         }

         pl.hraci[nly][nlx] = pl.hraci[sly][slx];
         pl.hraci[sly][slx] = SLEPEJ_PRVEK;

         hraj_sampl(SEBRANI,DELKA_SEBRANI,PRIORITA_SEBRANI);
/*
  Pokud to bude posledni klic, tak jeste rozsviti exit
  a prehraje zvuk otevnirani exitu
  (animace exitu az po vziti 5-ti klicu -> otvirani a pod.)
*/
         posledni_pohyb = posun_berusku();
         soucastny_pole(posledni_pohyb);

         klicu_celkem += 1;
         pl.level[nly][nlx][PRVEK] = P_ZEME;

         while(status_anim(posledni_pohyb)) {
            updatuj_animace();
            updatuj_samply();
         }
         kresli_prvek_levelu(&pl,nlx,nly);

         if(klicu_celkem <= 5)
            kresli_sprit_bar(spr[PRVNI_KLIC+klicu_celkem],X_KLICU_POZICE,Y_KLICU_POZICE,BILA);

         if(klicu_celkem == 5) {
           hraj_sampl(OTEVRENI_EXITU,DELKA_OTEVRENI_EXITU,PRIORITA_OTEVRENI_EXITU);
           for(y = 0; y < YPOLI; y++) {
              for(x = 0; x < XPOLI; x++) {
                 if(pl.level[y][x][PRVEK] == P_EXIT) {
                   if(pl.level[y][x][VARIANTA] == 6)
                     animuj_exit(x,y);
                   else {
                     pl.level[y][x][VARIANTA] = pl.level[y][x][VARIANTA]+1;
                     kresli_prvek_levelu(&pl,x,y);
                   }
                 }
              }
           }
         }
         return(1);

      case P_KRUMPAC: //lopata
         if(p_p->lopaty >= 9) {
            return(0);
         }

         pl.hraci[nly][nlx] = pl.hraci[sly][slx];
         pl.hraci[sly][slx] = SLEPEJ_PRVEK;

         hraj_sampl(SEBRANI,DELKA_SEBRANI,PRIORITA_SEBRANI);
/*
  Tady se bude prepinat nahore pulka barusky
  jak drzi krumpac, pokud bude oznacena tak obtahnuta pulka
*/

// a tady se taky prepina ...
         p_p->a_ls = 1;
         vykresli_hrace(p_p->cislo);

         posledni_pohyb = posun_berusku();
         soucastny_pole(posledni_pohyb);

         pl.level[nly][nlx][PRVEK] = P_ZEME;
         while(status_anim(posledni_pohyb)) {
            updatuj_animace();
            updatuj_samply();
         }
         kresli_prvek_levelu(&pl,nlx,nly);

         p_p->lopaty += 1;
         aktualizuj_dolni_listu();

         return(1);

      case P_KAMEN:   //kamen
         if(!p_p->lopaty) {
            return(0);
         }
         else
            pl.level[nly][nlx][PRVEK] = P_ZEME;
            pl.hraci[nly][nlx] = pl.hraci[sly][slx];
            pl.hraci[sly][slx] = SLEPEJ_PRVEK;

/*
  Tady se bude prepinat nahore pulka berusky
  jak straci krumpac (pokud je to posledni krumpac),
  pokud bude oznacena tak obtahnuta pulka
*/          varianta = pl.level[nly][nlx][VARIANTA];
            if(varianta == 0)
              hraj_sampl(ZELEZNY_KAMEN,DELKA_ZELEZNY_KAMEN,DELKA_ZELEZNY_KAMEN);
            else
              hraj_sampl(MODRY_KAMEN,DELKA_MODRY_KAMEN,PRIORITA_MODRY_KAMEN);


            handle = animuj_kamen(varianta,nlx,nly);
            p_p->lopaty--;

            if(p_p->lopaty == 0) {
              p_p->a_ls = 3;
              vykresli_hrace(p_p->cislo);
            }

            while(status_anim(handle)) {
              updatuj_animace();
              updatuj_samply();
            }

            posledni_pohyb = posun_berusku();
            soucastny_pole(posledni_pohyb);
            aktualizuj_dolni_listu();
            return(1);

      case P_KLIC1: //barevne klice
      case P_KLIC2:
      case P_KLIC3:
      case P_KLIC4:
      case P_KLIC5:
         if(p_p->cislo+0xd == pl.level[nly][nlx][PRVEK])
            if(p_p->klice < MAX_BAR_KLICU) {
               p_p->klice++;
               pl.hraci[nly][nlx] = pl.hraci[sly][slx];
               pl.hraci[sly][slx] = SLEPEJ_PRVEK;
//               prehraj_zvuk(s_klic);
/*
  Tady se bude prepinat nahore pulka barusky
  jak drzi klic, pokud bude oznacena tak obtahnuta pulka
*/
               hraj_sampl(SEBRANI,DELKA_SEBRANI,PRIORITA_SEBRANI);

               p_p->a_ps = 1;
               vykresli_hrace(p_p->cislo);

               posledni_pohyb = posun_berusku();
               soucastny_pole(posledni_pohyb);
               pl.level[nly][nlx][PRVEK] = P_ZEME;
               while(status_anim(posledni_pohyb)) {
                 updatuj_animace();
                 updatuj_samply();
               }
               kresli_prvek_levelu(&pl,nlx,nly);
               return(1);
            }
         else {
           return(0);
         }

      case P_DVERE1_H_O:// barevne dvere
      case P_DVERE2_H_O:
      case P_DVERE3_H_O:
      case P_DVERE4_H_O:
      case P_DVERE5_H_O:
      case P_DVERE1_V_O:// barevne dvere
      case P_DVERE2_V_O:
      case P_DVERE3_V_O:
      case P_DVERE4_V_O:
      case P_DVERE5_V_O:
           if((p_p->cislo+P_DVERE1_H_O == pl.level[nly][nlx][PRVEK])||
              (p_p->cislo+P_DVERE1_V_O == pl.level[nly][nlx][PRVEK])) {
               posledni_pohyb = posun_berusku();
               pl.hraci[nly][nlx] = pl.hraci[sly][slx];
               pl.hraci[sly][slx] = SLEPEJ_PRVEK;
               posledni_pohyb = posun_berusku();
               soucastny_pole(posledni_pohyb);
               return(1);
           }
           else {
             return(0);
           }

      case P_DVERE1_H_Z:// barevne dvere
      case P_DVERE2_H_Z:
      case P_DVERE3_H_Z:
      case P_DVERE4_H_Z:
      case P_DVERE5_H_Z:
      case P_DVERE1_V_Z:// barevne dvere
      case P_DVERE2_V_Z:
      case P_DVERE3_V_Z:
      case P_DVERE4_V_Z:
      case P_DVERE5_V_Z:
         if((p_p->cislo+P_DVERE1_H_Z == pl.level[nly][nlx][PRVEK])||
            (p_p->cislo+P_DVERE1_V_Z == pl.level[nly][nlx][PRVEK])){

           if(p_p->klice > 0) {
              p_p->klice--;

              hraj_sampl(ODEMYK,DELKA_ODEMYK,PRIORITA_ODEMYK);

              if(pl.level[nly][nlx][VARIANTA] == 0)
                hraj_sampl(OTEVRENI_DVERI,DELKA_OTEVRENI_DVERI,PRIORITA_OTEVRENI_DVERI);

              zrus_dvere(nlx,nly);

              if(p_p->klice == 0) {
                p_p->a_ps = 3;
                vykresli_hrace(p_p->cislo);
              }

              if(p_p->cislo+P_DVERE1_H_Z == pl.level[nly][nlx][PRVEK])
                varianta = 0;
              else
                varianta = 1;

              if(pl.level[nly][nlx][VARIANTA] == 0) {
                pl.level[nly][nlx][PRVEK] -= 5;
                kresli_prvek_levelu(&pl,nlx,nly);

                pl.level[nly][nlx][PRVEK] = P_ZEME;
                pl.podlaha[nly][nlx][PRVEK] = P_ZEME;
                if(varianta) {// vertik. dvere
                  pl.level[nly][nlx-1][VARIANTA] = 50;
                  pl.podlaha[nly][nlx][VARIANTA] = 2;
                }
                else {       // horiz. dvere
                  pl.level[nly-1][nlx][VARIANTA] = 50;
                  pl.podlaha[nly][nlx][VARIANTA] = 3;
                }
                kresli_prvek_levelu(&pl,nlx,nly);
              }
              else {
                if(varianta) // vertik. dvere
                  pl.level[nly][nlx][PRVEK] = P_DV_V;
                else        // horiz. dvere
                  pl.level[nly][nlx][PRVEK] = P_DV_H;

                pl.level[nly][nlx][VARIANTA] = 0;
                kresli_prvek_levelu(&pl,nlx,nly);
              }

              pl.hraci[nly][nlx] = pl.hraci[sly][slx];
              pl.hraci[sly][slx] = SLEPEJ_PRVEK;

              posledni_pohyb = posun_berusku();
              soucastny_pole(posledni_pohyb);
              return(1);
           }
         }
         else {
           return(0);
         }

      //Dvere spec pro barevnou berusku
      case P_ID_DVERE1_H_O :
      case P_ID_DVERE2_H_O :
      case P_ID_DVERE3_H_O :
      case P_ID_DVERE4_H_O :
      case P_ID_DVERE5_H_O :
      case P_ID_DVERE1_V_O :
      case P_ID_DVERE2_V_O :
      case P_ID_DVERE3_V_O :
      case P_ID_DVERE4_V_O :
      case P_ID_DVERE5_V_O :
         if((p_p->cislo+P_ID_DVERE1_H_O ==  pl.level[nly][nlx][PRVEK])||
            (p_p->cislo+P_ID_DVERE1_V_O ==  pl.level[nly][nlx][PRVEK])) {

            pl.hraci[nly][nlx] = pl.hraci[sly][slx];
            pl.hraci[sly][slx] = SLEPEJ_PRVEK;

            posledni_pohyb = posun_berusku();
            soucastny_pole(posledni_pohyb);

            return(1);
         }
         else {
            return(0);
         }

      case P_ID_DVERE1_H_Z :
      case P_ID_DVERE2_H_Z :
      case P_ID_DVERE3_H_Z :
      case P_ID_DVERE4_H_Z :
      case P_ID_DVERE5_H_Z :
      case P_ID_DVERE1_V_Z :
      case P_ID_DVERE2_V_Z :
      case P_ID_DVERE3_V_Z :
      case P_ID_DVERE4_V_Z :
      case P_ID_DVERE5_V_Z :
         if((p_p->cislo+P_ID_DVERE1_H_Z ==  pl.level[nly][nlx][PRVEK])||
            (p_p->cislo+P_ID_DVERE1_V_Z ==  pl.level[nly][nlx][PRVEK])) {

            zrus_dvere(nlx,nly);

            if(pl.level[nly][nlx][VARIANTA] == 0)
              hraj_sampl(ZAVRENI,DELKA_ZAVRENI,PRIORITA_ZAVRENI);
//            hraj_sampl(OTEVRENI_DVERI,DELKA_OTEVRENI_DVERI,PRIORITA_OTEVRENI_DVERI);

            pl.hraci[nly][nlx] = pl.hraci[sly][slx];
            pl.hraci[sly][slx] = SLEPEJ_PRVEK;

            pl.level[nly][nlx][PRVEK] -= 5;
            posledni_pohyb = posun_berusku();
            soucastny_pole(posledni_pohyb);

            return(1);
         }
         else {
            return(0);
         }

      case P_DV_H_Z:   //Jsou to zavrene zaviraci dvere
      case P_DV_V_Z:
         return(0);

      case P_DV_H_O:
      case P_DV_V_O:
         pl.hraci[nly][nlx] = pl.hraci[sly][slx];
         pl.hraci[sly][slx] = SLEPEJ_PRVEK;
         posledni_pohyb = posun_berusku();
         soucastny_pole(posledni_pohyb);
         return(1);

      case P_DV_V:
      case P_DV_H:
         pl.hraci[nly][nlx] = pl.hraci[sly][slx];
         pl.hraci[sly][slx] = SLEPEJ_PRVEK;
         posledni_pohyb = posun_berusku();
         soucastny_pole(posledni_pohyb);
         return(1);

      default:
         break;
   }
   return(0);
}

void vykresli_hrace(int hrac)
{
 int x,s1,s2;

 x = VELKY_XFUK+(hrac*VELKY_SKOK);
 s1 = p[hrac].a_ls; s2 = p[hrac].a_ps;
 if(p_p->cislo == hrac) { s1--; s2--; }

 spr_vypln_ctverec(0,0,0,65,40,p_pan);
 buf_kresli_sprit_bar(spr[p[hrac].ls[s1]],0,0,BILA,p_pan);
 buf_kresli_sprit_bar(spr[p[hrac].ps[s2]],VELKY_X,0,BILA,p_pan);

 kresli_sprit_bar(p_pan,x-VELKY_X,0,BILA);

 if(p[hrac].aktivni == 0)
   kresli_sprit_bar(spr[MASKA],x-23,2,BILA);
}

// Rutinka,ktera prepne mezi hraci
void prepni(void)
{
 dword i,hrac,zal;

 if(status_anim(posledni_pohyb))
   return;

 hraj_sampl(PREPNI,DELKA_PREPNI,PRIORITA_PREPNI);

 obnov_pozadi(p_p->x_pozice,p_p->y_pozice-YFUK);
 kresli_sprit_bar(pp_spr,p_p->x_pozice*X,p_p->y_pozice*Y,BILA);
 kresli_prvek_levelu(&pl,p_p->x_pozice,p_p->y_pozice-YFUK);

 zal = hrac = p_p->cislo;

 for(i = 0;i < 5;i++,hrac++) {
   if(hrac > 4)
      hrac = 0;
   if(p[hrac].aktivni == 1) {
      p_p = &p[hrac];
      if(zal != p_p->cislo) {
        vykresli_hrace(zal);
        vykresli_hrace(p_p->cislo);
        break;
      }
   }
 }
 kresli_sprit_bar(spr[UKAZATEL_2],p_p->x_pozice*X,p_p->y_pozice*Y,BILA);
 aktualizuj_dolni_listu();
}


void aktualizuj_dolni_listu(void)
{
 int  i;
 int  x;
 static int p = 0xffff;

 spr_vypln_ctverec(0,0,0,180,20,p_spd);

 if(p_p->aktivni != 1)
   return;

 if(p != p_p->cislo)
   p = p_p->cislo;
 else {
   if(p_p->lopaty < p_p->prvku)
     p_p->prvku = p_p->lopaty;
   else
     if(p_p->lopaty > p_p->prvku) {
       p_p->pole_spr[p_p->prvku] = prv[P_KRUMPAC].varianty[pl.level[nly][nlx][VARIANTA]];
       p_p->prvku = p_p->lopaty;
   }
 }

 x = 180-p_p->lopaty*X;

 for(i = 0; i < p_p->lopaty; i++) {
     buf_kresli_sprit_bar(spr[p_p->pole_spr[p_p->lopaty-i-1]],x+i*X,0,BILA,p_spd);
 }

 kresli_sprit(p_spd,460,460);
}


void vykresli_panel(void)
{
// Vykresli horni panel
/*
#define VELKY_X      46
#define VELKY_Y      36
#define ZAC_ZLATA_VELKY 64800
#define ZAC_VELKY    (ZAC_ZLATA_VELKY + (VELKY_X*VELKY_Y)
#define ZAC_VELKY_MASKA (ZAC_VELKY + (VELKY_X*VELKY_Y)
#define VELKY_SKOK   100
#define VELKY_YFUK   5
*/
/*
#define VELKY_X           29
#define VELKY_SKOK        100
#define VELKY_YFUK        2
#define VELKY_XFUK        40+(46/2)
#define MASKA             56
*/
/*
  Vypocet jednotlive berusky:

  x_stred = cislo_ber*velky_skok+VELKY_XFUK
  y_stred = VELKY_YFUK

*/
 int i;

 for(i = 0; i < HRACU; i++) {
    p[i].a_ls = 3;
    p[i].a_ps = 3;
    vykresli_hrace(i);
 }
// tady se updatne pocet klicu
 vypln_ctverec(0,X_KLICU_POZICE,Y_KLICU_POZICE,60,40);
 if((klicu_celkem <= 5)&&(klicu_celkem > 0 ))
   kresli_sprit_bar(spr[PRVNI_KLIC+klicu_celkem],X_KLICU_POZICE,Y_KLICU_POZICE,BILA);
}

void uvolni_pozadi(void)
{
 zrus_sprit(spr[PRVNI_POZADI]);
}

void nahraj_pozadi(int pozadi)
{
 uvolni_pozadi();
 nahraj_sadu_spritu("poz_%.3d.spr",PRVNI_POZADI,pozadi,1,OSTATNI_LOCK);
 p_pozadi = spr[PRVNI_POZADI];
}

void obnov_pozadi(dword x,dword y)
{
dword  prvek,sprit;

  buf_uloz_sprit(pp_spr,x*X,y*Y,p_pozadi);

  prvek = pl.podlaha[y][x][PRVEK];
  if ((prvek != NENI_PODLAHA)&&(pl.podlaha[y][x][VARIANTA] != SLEPEJ_SPRIT)) {
     buf_kresli_sprit_bar(spr[prv[prvek].varianty[pl.podlaha[y][x][VARIANTA]]],0,0,BILA,pp_spr);
  }

  prvek = pl.level[y][x][PRVEK];
  if ((prvek != P_ZEME)&&(pl.level[y][x][VARIANTA] != SLEPEJ_SPRIT)) {
     sprit = prv[prvek].varianty[pl.level[y][x][VARIANTA]];
     if (pl.level[y][x][ROTACE]) {
        rotuj_sprit(spr[sprit],pp_spr_2,pl.level[y][x][ROTACE]);
        buf_kresli_sprit_bar(pp_spr_2,0,0,BILA,pp_spr);
     }
     else
        buf_kresli_sprit_bar(spr[sprit],0,0,BILA,pp_spr);
  }
}


//***********************************************************************
//Konec jadra hry
//************************************************************************

//---------------------------------------------------------------------------
//Editace void edituj(void)
//---------------------------------------------------------------------------
/* Napady k editoru:

   F9 -> hraje editovany level podobne jako u jmeno_lev
         mohlo  by tam byt nejake kompilovani

   Zruseni editace klavesama
   udelat na posun nejaky ikony -> pri stisku je promacknout
   < > -> posun editacni grafiky
   n m -> posun kresleneho prvku na liste

*/
/*
  funkce pouzite v editoru


*/
/* prvni parametr je kreslena rovina
   druhy parametr je kreslit/nekreslit podlahu
   (tj. prazdne casti zaplnit cernou a nebo nechat pruhledne)

   roviny:
   0 - pozadi
   1 - podlaha
   2 - steny & prvky
   3 - berusky

   podlaha: 0 - nulova podlaha se nekresli
            1 - nulova podlaha se kresli

*/
/*

*/

void vykresli_edit_level(int rovina,int podlaha)
{
dword x,y,sprr,prvek,x1,y1,varianta,sp1,i,pr;

   if(!rovina) {
     if(!podlaha)
        kresli_sprit(p_pozadi,0,60);
      else
         for(y = YFUK*Y; y < (YPOLI+YFUK)*Y; y+=Y) {
            for(x = 0; x < XPOLI*X; x+=X)
                kresli_sprit_bar(spr[EDIT_ZEME],x,y,BILA);
         }
   }
   if(rovina == 1) {
      for(y = 0; y < YPOLI; y++) {
         for(x = 0; x < XPOLI; x++) {
            if(podlaha)
              kresli_sprit_bar(spr[EDIT_ZEME],x*X,(y+YFUK)*Y,BILA);
            if(pl.podlaha[y][x][PRVEK] != NENI_PODLAHA) {
              sprr = prv[P_ZEME].varianty[pl.podlaha[y][x][VARIANTA]];
              kresli_sprit_bar(spr[sprr],x*X,(y+YFUK)*Y,BILA);
            }
         }
      }
   }
   if(rovina == 2) {
      for(y = 0; y < YPOLI; y++) {
         for(x = 0; x < XPOLI; x++) {
            if(podlaha)
              kresli_sprit_bar(spr[EDIT_ZEME],x*X,(y+YFUK)*Y,BILA);

            prvek = pl.level[y][x][PRVEK];
            if(prvek != P_ZEME) {
              sprr = prv[prvek].varianty[pl.level[y][x][VARIANTA]];
              if (pl.level[y][x][ROTACE]) {
                 rotuj_sprit(spr[sprr],pp_spr,pl.level[y][x][ROTACE]);
                 kresli_sprit_bar(pp_spr,x*X,(y+YFUK)*Y,BILA);
              }
              else
                 kresli_sprit_bar(spr[sprr],x*X,(y+YFUK)*Y,BILA);

              pr = pl.level[y][x][VARIANTA]*prv[prvek].podprvku;
              for(i = 0; i < prv[prvek].podprvku; i++) {
                 x1 = x+podprv[prv[prvek].podprvky[pr+i]].x_kor;
                 y1 = y+podprv[prv[prvek].podprvky[pr+i]].y_kor;
                 varianta = podprv[prv[prvek].podprvky[pr+i]].varianty[prv[prvek].pdp_vr[pr+i]];
                 sp1 = prv[podprv[prv[prvek].podprvky[pr+i]].druh].varianty[varianta];
                 kresli_sprit_bar(spr[sp1],X*x1,Y*(y1+YFUK), BILA);
           
                 pl.level[y1][x1][PRVEK] = podprv[prv[prvek].podprvky[pr+i]].druh;
                 pl.level[y1][x1][VARIANTA] = varianta;
              }
            }
         }
      }
   }

   if(rovina == 3) {
      for(y = 0; y < YPOLI; y++) {
         for(x = 0; x < XPOLI; x++) {
            if(pl.hraci[y][x] != SLEPEJ_PRVEK) {
                i = pl.hraci[y][x];
                kresli_sprit_bar(spr[PRVNI_HRAC+i+(pl.natoceni[i]*HRACU)],x*X,(y+YFUK)*Y,BILA);
                /*
                i = pl.hraci[y][x];
                if(pl.natoceni[i] != 0) {
                  rotuj_sprit(spr[PRVNI_HRAC+i],pp_spr,pl.natoceni[i]);
                  kresli_sprit_bar(pp_spr,x*X,(y+YFUK)*Y,BILA);
                }
                else
                  kresli_sprit_bar(spr[PRVNI_HRAC+i],x*X,(y+YFUK)*Y,BILA);
                */
            }
         }
      }
   }
}

    
void prekresli_roviny(void)
{
/*  rovina 1,2,,,

    = 0 -> neaktivni
    = | 1 -> aktivni
    = | 2 -> nepruhledna
*/
  vypni_kurzor(mysi_info);

  vykresli_edit_level(0,rv[0].stav & 2);

  if(rv[1].stav & 1)
     vykresli_edit_level(1,rv[1].stav & 2);

  if(rv[2].stav & 1)
     vykresli_edit_level(2,rv[2].stav & 2);

  if(rv[3].stav & 1)
     vykresli_edit_level(3,0);

  zapni_kurzor(mysi_info);
}

#define vypln_sloupec(i,prvek,varianta) \
{                            \
  lista[i][0][0] = prvek;    \
  lista[i][0][1] = varianta; \
  lista[i][1][0] = prvek;    \
  lista[i][1][1] = varianta; \
  lista[i][2][0] = prvek;    \
  lista[i][2][1] = varianta; \
}


int vygeneruj_listu(int rovina)
{
 int posledni = 0,
     i,j;

 if(rovina == 1) { // jsou to podlahy
   for(i = 0; i < prv[P_ZEME].variant; i++) {
      vypln_sloupec(i,P_ZEME,i)
   }
   return(i);
 }
/*
 int  minus_x;       //kolik ctverecku doprava navic
 int  plus_x;        //kolik ctverecku doprava
*/
 if(rovina == 2) {
   for(i = P_BEDNA; i < HERNICH_PRVKU; i++) {
      for(j = 0; j < prv[i].variant; j++) {

         if((i == P_STENA)&&(j > 29)&&(j < 52)&&(!vypni_omezeni)) {
           posledni--;
           continue;
         }
         if((i == P_STENA)&&(j > 65)&&(!vypni_omezeni)) {
           posledni--;
           continue;
         }
         if((i == P_EXIT)&&(!vypni_omezeni)) {
           if((j == 1)||(j == 3)||(j == 5)||(j == 8)) {
             posledni--;
             continue;
           }
         }

         if(prv[i].minus_x) {
           vypln_sloupec(posledni+j,i,j)
           posledni++;
         }
         vypln_sloupec(posledni+j,i,j)
         if(prv[i].plus_x) {
           posledni++;
           vypln_sloupec(posledni+j,i,j)
         }
      }
      posledni += j;
   }
   return(posledni);
 }

 if(rovina == 3) {
   for(i = 1; i < HRACU+1; i++) {
      for(j = 0; j < prv[i].variant; j++)
         vypln_sloupec(posledni+j,i,j)
      posledni += j;
   }
   return(posledni);
 }
 return(0);
}

#define PRVKU_NA_LISTE 25
#define NENI_BARVA     0xff
#define SPODNI_BARVA   86
#define sirka_prvku(z) (1+prv[(z)].plus_x+prv[(z)].minus_x)
#define PRVNI_X        1

void kresli_prvek_v_liste(int poz_l, int pozice_x, int barva, int prvni_x, int posledni_x)
//-> nakresli jiden prvek listy na urcity misto
{
 /*
   pozice x je ve ctvercich vzdalenost
   Zada prvni ctverec v liste a souradnice tohoto ctverce
 */

 int p,v,s,i,x1,y1,v1,pr;
 int zac,posl,prvni;

 p = lista[poz_l][1][PRVEK];
 v = v1 = lista[poz_l][1][VARIANTA];

 zac = pozice_x*X;
 posl = posledni_x*X;
 prvni = prvni_x*X;

 if(prv[p].minus_x) {
   zac += X;
 }

 s = prv[p].varianty[v];
 if((zac <= posl)&&(zac >= prvni))  {
   if(barva) vypln_ctverec(barva,zac,0,X,3*Y);
   kresli_sprit_bar(spr[s],zac,Y,BILA);
 }
 pr = v1*prv[p].podprvku;
 for(i = 0; i < prv[p].podprvku; i++) {
    x1 = zac+(podprv[prv[p].podprvky[pr+i]].x_kor*X);
    y1 = Y+(podprv[prv[p].podprvky[pr+i]].y_kor*Y);
    v = podprv[prv[p].podprvky[pr+i]].varianty[prv[p].pdp_vr[pr+i]];
    s = prv[podprv[prv[p].podprvky[pr+i]].druh].varianty[v];
    if((prvni <= x1)&&(x1 <= posl)) {
      if(barva) vypln_ctverec(barva,x1,0,X,3*Y);
      kresli_sprit_bar(spr[s],x1,y1, BILA);
    }
 }
}

void kresli_vzorek_prvku(int barva)
{
 int p,v,s,i,x1,y1,z,v1,pr;

 #define X_VZORKU (28*X)
 #define Y_VZORKU (1*X)

 z = rv[rovina].prvni+rv[rovina].akt;
 p = lista[z][1][PRVEK];
 v = v1 = lista[z][1][VARIANTA];

 vypln_ctverec(barva,X_VZORKU-X,0,3*X,3*Y);
 s = prv[p].varianty[v];
 if(rotace) {
   rotuj_sprit(spr[s],pp_spr,rotace);
   kresli_sprit_bar(pp_spr,X_VZORKU,Y_VZORKU,BILA);
 }
 else
   kresli_sprit_bar(spr[s],X_VZORKU,Y_VZORKU,BILA);

 pr = v1*prv[p].podprvku;
 for(i = 0; i < prv[p].podprvku; i++) {
    x1 = X_VZORKU+(podprv[prv[p].podprvky[pr+i]].x_kor*X);
    y1 = Y_VZORKU+(podprv[prv[p].podprvky[pr+i]].y_kor*Y);
    v = podprv[prv[p].podprvky[pr+i]].varianty[prv[p].pdp_vr[pr+i]];
    s = prv[podprv[prv[p].podprvky[pr+i]].druh].varianty[v];
    kresli_sprit_bar(spr[s],x1,y1, BILA);
 }
}

void kresli_prvek(int p, int v, int rot, int x, int y)
{
 int s,i,x1,y1,v1,pr;

 v1 = v;

 s = prv[p].varianty[v];
 if(rot) {
   rotuj_sprit(spr[s],pp_spr,rotace);
   kresli_sprit_bar(pp_spr,x,y,BILA);
 }
 else
   kresli_sprit_bar(spr[s],x,y,BILA);

 pr = v1*prv[p].podprvku;
 for(i = 0; i < prv[p].podprvku; i++) {
    x1 = x+(podprv[prv[p].podprvky[pr+i]].x_kor*X);
    y1 = y+(podprv[prv[p].podprvky[pr+i]].y_kor*Y);
    v = podprv[prv[p].podprvky[pr+i]].varianty[prv[p].pdp_vr[pr+i]];
    s = prv[podprv[prv[p].podprvky[pr+i]].druh].varianty[v];
    kresli_sprit_bar(spr[s],x1,y1, BILA);
 }
}

void vykresli_horni_listu(int prvku, int prvni)
{
 int i,odecist;
 int p,v;


 kresli_sprit_bar(spr[LEVA_SIPKA],0,Y,BILA);
 kresli_sprit_bar(spr[PRAVA_SIPKA],(PRVNI_X+PRVKU_NA_LISTE)*X,Y,BILA);

 vypln_ctverec(0,X,0,(PRVKU_NA_LISTE)*X,3*Y);

 for(i = 0; i < prvku;) {
    odecist = 0;
    if(prvni+i > 10) {
      p = lista[prvni+i][1][PRVEK]; v = lista[prvni+i][1][VARIANTA];
      if((p == lista[prvni+i-1][1][PRVEK])&&(v == lista[prvni+i-1][1][VARIANTA]))
        odecist++;
      if((p == lista[prvni+i-2][1][PRVEK])&&(v == lista[prvni+i-2][1][VARIANTA]))
        odecist++;
    }
    kresli_prvek_v_liste(prvni+i,i+PRVNI_X-odecist,0,PRVNI_X,prvku+PRVNI_X-1);
    i += (sirka_prvku(lista[prvni+i][1][PRVEK])-odecist);
 }
 rotace = 0;
 kresli_vzorek_prvku(0);
}

void listu_doleva(void)
{
  int prvku;

  if(rv[rovina].prvni == 0)
    return;

  rv[rovina].prvni--;
  prvku = rv[rovina].prvku-rv[rovina].prvni;
  // prvku je ted celkovej pocet prvku co zbyva do konce
  if(prvku > PRVKU_NA_LISTE) prvku = PRVKU_NA_LISTE;
  vypni_kurzor(mysi_info);
  vykresli_horni_listu(prvku,rv[rovina].prvni);
  kresli_sprit_bar(spr[UKAZATEL_3],(PRVNI_X+rv[rovina].akt)*X,0,BILA);
  zapni_kurzor(mysi_info);
}

void listu_doprava(void)
{
 int prvku;

 if((rv[rovina].prvku-rv[rovina].prvni) <= PRVKU_NA_LISTE)
   return;

 rv[rovina].prvni++;
 prvku = rv[rovina].prvku-rv[rovina].prvni;
 // prvku je ted celkovej pocet prvku co zbyva do konce
 if(prvku > PRVKU_NA_LISTE) prvku = PRVKU_NA_LISTE;
 vypni_kurzor(mysi_info);
 vykresli_horni_listu(prvku,rv[rovina].prvni);
 kresli_sprit_bar(spr[UKAZATEL_3],(PRVNI_X+rv[rovina].akt)*X,0,BILA);
 zapni_kurzor(mysi_info);
}

#define zmen_podlahu(x,y,nove)    \
pl.podlaha[y][x][VARIANTA] = (pl.podlaha[y][x][VARIANTA] < 5) ? nove : nove+5



void stinuj_level(void)
{
 int x,y;

 for(y = 1; y < YPOLI; y++) {
    for(x = 1; x < XPOLI; x++) {
      if(pl.podlaha[y][x][PRVEK] != NENI_PODLAHA) {

        if((pl.level[y][x-1][PRVEK] == P_STENA)&&
           (pl.level[y-1][x][PRVEK] == P_STENA)) {
           zmen_podlahu(x,y,0);
        }
        if((pl.level[y][x-1][PRVEK] != P_STENA)&&
           (pl.level[y-1][x][PRVEK] != P_STENA)&&
           (pl.level[y-1][x-1][PRVEK] == P_STENA)) {
           zmen_podlahu(x,y,1);
        }
        if((pl.level[y-1][x][PRVEK] != P_STENA)&&
           (pl.level[y][x-1][PRVEK] == P_STENA)) {
           zmen_podlahu(x,y,2);
        }
        if((pl.level[y-1][x][PRVEK] == P_STENA)&&
           (pl.level[y][x-1][PRVEK] != P_STENA)) {
           zmen_podlahu(x,y,3);
        }
      }
    }
 }

 for(x = 0, y = 1; y < YPOLI; y++) {
    if(pl.podlaha[y][x][PRVEK] != NENI_PODLAHA) {
        if(pl.level[y-1][x][PRVEK] == P_STENA)
           zmen_podlahu(x,y,3);
    }
 }

 for(x = 1, y = 0; x < XPOLI; x++) {
    if(pl.podlaha[y][x][PRVEK] != NENI_PODLAHA) {
      if(pl.level[y][x-1][PRVEK] == P_STENA)
        zmen_podlahu(x,y,2);
    }
 }
}

#define obnov_listu()                                                 \
{                                                                     \
  vypln_ctverec(0,0,0,640,60);                                        \
  prvku = (rv[rovina].prvku-rv[rovina].prvni);                        \
  if(prvku > PRVKU_NA_LISTE) prvku = PRVKU_NA_LISTE;                  \
  vypni_kurzor(mysi_info);                                            \
  vykresli_horni_listu(prvku,rv[rovina].prvni);                       \
  kresli_sprit_bar(spr[UKAZATEL_3],(PRVNI_X+rv[rovina].akt)*X,0,BILA);\
  zapni_kurzor(mysi_info);                                            \
  ini = 2;                                                            \
}

void edituj(void)
{
 dword j,i;
 int   x1 = 0,y1 = 0,x2 = 0,y2 = 0,poradi,tem;
 int   nevyp = 0,vypln = 0,prvku = 0;
 int   ini = 1;
 int   prvek,varianta;
 byte  pom[100];
 byte  pom_file[100];
 int   rychlosave = 1;

 // 2 = zeme
 // 3 = herni prvky
 // 4 = hraci

 if(!strcmp(ed_level,EDITOR))
   rychlosave = 0;

 nahraj_fileselector();
 nastav_paletu(PALETA);
 zapni_kurzor(mysi_info);

 rv[0].stav = 1;
 rv[0].prvni = 0;
 rv[1].stav = 1;
 rv[1].prvni = 0;
 rv[2].stav = 1;
 rv[2].prvni = 0;
 rv[3].stav = 1;
 rv[3].prvni = 0;

 do {
    // aktivacni sekvence pri zapnuti
    if(ini == 1) { _a_klavesa[0] = K_3;}
    if(ini == 2) { _a_klavesa[0] = K_R;}
    ini++;

    test:
    while((!getkba())&&(mysi_info[0] == 0));

    if(mysi_info[0] != 0) {
      obsluz_mys(rovina);
      goto test;
    }

   /* Inicializace editoru */

   switch (getkba()) {
// Je to Escape,opust tu gamesu
      case K_ESC:
         goto konec_cteni;

      // q,w,e,r -> zapne rovinu
      // Alt + q,w,e,r -> zapne pruhlednost roviny
      case K_Q:
      case K_W:
      case K_E:
      case K_R:
         if((getkba() == 0x13)&&(key[K_CTRL])) {
           rotace = (rotace+1)%4;
           kresli_vzorek_prvku(0);
           break;
         }
         pp:
         j = getkba() - 0x10;

         rv[j].stav = 1; // pouze se zobrazi
         
         prekresli_roviny();
         break;

       //   a,s,d,f -> vypne rovinu
      case K_A:
      case K_S:
      case K_D:
      case K_F:
         if((getkba() == K_S)&&(key[K_CTRL])) {
           stinuj_level();
           _a_klavesa[0] = 0x11;
           goto pp;
         }

         j = getkba() - 30;

         if(j != 0)
           rv[j].stav = 0; // vymaze se uplne
         else
           rv[j].stav = 2;

         prekresli_roviny();
         break;

      case K_2: //  klavesa 2 -> prepnuti na zemi
         rv[1].prvku = vygeneruj_listu(1);
         rovina = 1;
         prvku = (rv[1].prvku-rv[1].prvni);
         if(prvku > PRVKU_NA_LISTE) prvku = PRVKU_NA_LISTE;
         vypni_kurzor(mysi_info);
         vykresli_horni_listu(prvku,rv[1].prvni);
         kresli_sprit_bar(spr[UKAZATEL_3],(PRVNI_X+rv[1].akt)*X,0,BILA);
         zapni_kurzor(mysi_info);
         break;

      case K_3: //  klavesa 3 -> prepnuti na hraci prvky
         rv[2].prvku = vygeneruj_listu(2);
         rovina = 2;
         prvku = (rv[2].prvku-rv[2].prvni);
         // prvku je ted celkovej pocet prvku co zbyva do konce
         if(prvku > PRVKU_NA_LISTE) prvku = PRVKU_NA_LISTE;
         vypni_kurzor(mysi_info);
         vykresli_horni_listu(prvku,rv[2].prvni);
         kresli_sprit_bar(spr[UKAZATEL_3],(PRVNI_X+rv[2].akt)*X,0,BILA);
         zapni_kurzor(mysi_info);
         break;


      case K_4: //  klavesa 4 -> prenuti na hrace
         rv[3].prvku = vygeneruj_listu(3);
         rovina = 3;
         prvku = (rv[3].prvku-rv[3].prvni);
         // prvku je ted celkovej pocet prvku co zbyva do konce
         if(prvku > PRVKU_NA_LISTE) prvku = PRVKU_NA_LISTE;
         vypni_kurzor(mysi_info);
         vykresli_horni_listu(prvku,rv[3].prvni);
         kresli_sprit_bar(spr[UKAZATEL_3],(PRVNI_X+rv[3].akt)*X,0,BILA);
         zapni_kurzor(mysi_info);
         break;

// Je to posun na dalsi pole
      case K_K:  // k posun <-
         listu_doleva();
         break;

      case K_L:  // l posun ->
         listu_doprava();
         break;

// Posun kresliciho kurzoru
      case 0x33:  //  '<'
         if(!rv[rovina].akt) {
           listu_doleva();
           break;
         }

         rv[rovina].akt--;
         prvku = rv[rovina].prvku-rv[rovina].prvni;
         if(prvku > PRVKU_NA_LISTE) prvku = PRVKU_NA_LISTE;
         vypni_kurzor(mysi_info);
         vykresli_horni_listu(prvku,rv[rovina].prvni);
         kresli_sprit_bar(spr[UKAZATEL_3],(PRVNI_X+rv[rovina].akt)*X,0,BILA);
         zapni_kurzor(mysi_info);
         break;

      case 0x34:  //  '>'
         if(rv[rovina].akt >= PRVKU_NA_LISTE-1) {
           listu_doprava();
           break;
         }

         if(rv[rovina].akt+rv[rovina].prvni == rv[rovina].prvku-1)
             break;

         rv[rovina].akt++;
         prvku = (rv[rovina].prvku-rv[rovina].prvni);
         if(prvku > PRVKU_NA_LISTE) prvku = PRVKU_NA_LISTE;
         vypni_kurzor(mysi_info);
         vykresli_horni_listu(prvku,rv[rovina].prvni);
         kresli_sprit_bar(spr[UKAZATEL_3],(PRVNI_X+rv[rovina].akt)*X,0,BILA);
         zapni_kurzor(mysi_info);
         break;


      //Kresleni ctvercu -> nevyplnenych
      case K_Z: //z -> zacatek
         nevyp = 1;
         zacatek:
         x1 = mysi_info[1]/X;
         y1 = mysi_info[2]/Y;

         if(y1 < YFUK) {
           vypln = 0;
           nevyp = 0;
           break;
         }

         poradi = rv[rovina].prvni+rv[rovina].akt;
         prvek = lista[poradi][1][PRVEK];
         varianta = lista[poradi][1][VARIANTA];
         rotace = lista[poradi][1][ROTACE];
         vypni_kurzor(mysi_info);
         zapis_ctverecek(x1,y1,rovina,prvek,varianta,rotace);
         zapni_kurzor(mysi_info);
         break;

      case K_X: //x -> konec
         prazdny:
         if(nevyp != 1)
            break;
         else
            nevyp = 0;

         x2 = mysi_info[1]/X;
         y2 = mysi_info[2]/Y;
         if(y2 < YFUK) break;

         vypni_kurzor(mysi_info);
         zapis_ctverecek(x2,y2,rovina,prvek,varianta,rotace);

         if(x2 < x1) { tem = x2; x2 = x1; x1 = tem;}
         if(y2 < y1) { tem = y2; y2 = y1; y1 = tem;}

         for(i = x1; i <= x2; i++)
            zapis_ctverecek(i,y1,rovina,prvek,varianta,rotace);
         for(i = y1; i <= y2; i++) {
            zapis_ctverecek(x1,i,rovina,prvek,varianta,rotace);
            zapis_ctverecek(x2,i,rovina,prvek,varianta,rotace);
         }
         for(i = x1; i <= x2; i++)
            zapis_ctverecek(i,y2,rovina,prvek,varianta,rotace);
         zapni_kurzor(mysi_info);
         break;

      case K_C:
         if(rovina == 1)
           prvek = NENI_PODLAHA;
         if(rovina == 2)
           prvek = P_ZEME;
          goto   prazdny;


      case K_V: // -> zacatek
         vypln = 1;
         goto zacatek;
         break;

      case K_B: // -> konec
         vyplneny:
         if(vypln != 1)
            break;
         else
            vypln = 0;

         x2 = mysi_info[1]/X;
         y2 = mysi_info[2]/Y;
         if(y2 < YFUK) break;

         vypni_kurzor(mysi_info);
         zapis_ctverecek(x2,y2,rovina,prvek,varianta,rotace);

         if(x2 < x1) { tem = x2; x2 = x1; x1 = tem;}
         if(y2 < y1) { tem = y2; y2 = y1; y1 = tem;}

         for(i = y1; i <= y2; i++) {
             for(j = x1; j <= x2; j++)
                zapis_ctverecek(j,i,rovina,prvek,varianta,rotace);
         }
         zapni_kurzor(mysi_info);
         break;

      case K_N:
         if(rovina == 1)
           prvek = NENI_PODLAHA;
         if(rovina == 2)
           prvek = P_ZEME;
         goto vyplneny;

      case K_F1:  // F1
         edit_help();
         break;

      case K_F5:    // F2 ulozeni ...
         vypln_ctverec(0,0,0,640,60);
         if(!rychlosave) {
           printfn(10,20,fn2,"Nen$i co ukl$adat...");
           cekej_s_kb(2*18);
         }
         else {
           sprintf(pom,"Ukl$ad$am $urove^n %s",ed_level);
           printfn(10,20,fn2,pom);
           uloz_level(ed_level,PREPISUJ);
           cekej_s_kb(2*18);
         }
         obnov_listu();
         break;

      case K_F6:    // F3 nahrani ...
         vypln_ctverec(0,0,0,640,60);
         if(!rychlosave) {
           printfn(10,20,fn2,"Nen$i co na^c$ist...");
           cekej_s_kb(2*18);
         }
         else {
           sprintf(pom,"nahr$av$am $urove^n %s",ed_level);
           printfn(10,20,fn2,pom);

           if(load_level(ed_level) == NULL) {
             sprintf(pom,"nemohu na^c$ist $urove^n %s",ed_level);
             vypln_ctverec(0,0,0,640,60);
             printfn(10,20,fn2,pom);
             cekej_s_kb(2*18);
           }
           else
             cekej_s_kb(2*18);
         }
         obnov_listu();
         break;

      case K_F4: // napise jmeno levelu ...
         vypln_ctverec(0,0,0,640,60);
         sprintf(pom,"pracujete s $urovn$i %s",ed_level);
         printfn(10,20,fn2,pom);
         cekej_s_kb(2*18);
         obnov_listu();
         break;


      case K_F2:    //  ulozeni jako ...
         if(filesector(pom_file,1,ed_level,"Ulo^zit $urove^n do...")) {

           vypln_ctverec(0,0,0,640,60);
           sprintf(pom,"Ukl$ad$am $urove^n %s",pom_file);
           printfn(10,20,fn2,pom);

           if(uloz_level(pom_file,NEPREPISUJ) != NULL) {
              strcpy(ed_level,pom_file);
              rychlosave = 1;
           }
           obnov_listu();
         }
         break;

      case K_F3:    // nahrani jako ...
         if(filesector(pom_file,0,ed_level,"Nahr$at $urove^n z...")) {

           vypln_ctverec(0,0,0,640,60);
           sprintf(pom,"Nahr$av$am $urove^n %s...",pom_file);
           printfn(10,20,fn2,pom);

           if(load_level(pom_file) == NULL) {
             vypln_ctverec(0,0,0,640,60);
             sprintf(pom,"nemohu na^c$ist %s...",pom_file);
             printfn(10,20,fn2,pom);
             getkb();

             if(load_level(ed_level) == NULL) {
               vypln_ctverec(0,0,0,640,60);
               sprintf(pom,"nemohu na^c$ist %s...",ed_level);
               printfn(10,20,fn2,pom);
               getkb();
             }
           }
           else {
             strcpy(ed_level,pom_file);
             rychlosave = 1;
           }
         }
         obnov_listu();
         break;

      case K_F8:   // F8 -> prekresleni levelu
         ini = 1;
         break;

      case K_F9:   // F9 -> hrani levelu
         hraj_level();
         ini = 1;
         break;

      case K_1:
      case K_P:
         if(++pl.pozadi == POZADI_SPRITU)
           pl.pozadi = 0;
         nahraj_pozadi(pl.pozadi);
         ini = 2;
         break;

      case K_O:
         if(key[K_CTRL])
           vypni_omezeni = 1;
         else
           vypni_omezeni = 0;
         ini = 1;
         break;

      default:
         break;
   }
   _a_klavesa[0] = 0;

 } while(1);

 konec_cteni:
 vypni_kurzor(mysi_info);
 uvolni_pozadi();
 uvolni_fileselector();
}


int filesector(byte *p_final_buffer, int mod, byte *p_aktualni_soubor, byte *p_nadpis)
{
 /* mod = 0 -> load mod -> podle filesel. se updatuje vybrany soubor
    mod = 1 -> save mod, implicidne je zapla
    zapisovaci lista a po enter to zustava
    sipka hore - 281,48
    sipka dole - 282,269
    ok         - 336,43
    zpet       - 323,82

    setridit dle jmena a ne podle dosu
    
 */

#define MAX_FILE       1000
#define MAX_ZOBRAZENO  12
#define BARVA_POZADI1  9
#define BARVA_POZADI2  0

#define X_BAZE         (320-213)
#define Y_BAZE         (240-367/2+20)

#define X_RADEK        26
#define Y_RADEK        305

#define X_RADEK_B      (26+X_BAZE)
#define Y_RADEK_B      (305+Y_BAZE)

#define X_FILE         427
#define Y_FILE         367

#define X_SEL          240
#define Y_SEL          (20*MAX_ZOBRAZENO)

#define X_OKRAJ        27
#define Y_OKRAJ        48

#define X_SIPKY        281
#define Y_SIPKY1       48
#define Y_SIPKY2       269

#define X_PISMENA      323  //OK
#define Y_PISMENA1     43
#define Y_PISMENA2     82


#define akt_fileselector()  \
{                           \
   for(i = Y_OKRAJ, j = 0; j < MAX_ZOBRAZENO; j++, i+=20) {\
      if(zac+j < souboru) { \
        if(akt == j) {                                  \
          spr_vypln_ctverec(BARVA_POZADI1,X_OKRAJ,i,X_SEL,20,p_file);     \
          if(prepis)\
            strcpy(vybr_soubor,soubory[zac+j]);           \
        }                                               \
        else                                            \
          spr_vypln_ctverec(BARVA_POZADI2,X_OKRAJ,i,X_SEL,20,p_file);     \
                                                        \
        if(zac+j < souboru)\
          buf_printfn(X_OKRAJ,i,fn2,soubory[zac+j],p_file);    \
      }                     \
   }                                                  \
   spr_vypln_ctverec(BARVA_POZADI2,X_RADEK,Y_RADEK,X_SEL,20,p_file);       \
   buf_printfn(X_RADEK,Y_RADEK,fn2,vybr_soubor,p_file);\
   vypni_kurzor(mysi_info);                           \
   kresli_sprit(p_file,X_BAZE,Y_BAZE);                \
   zapni_kurzor(mysi_info);                           \
}

 byte *p_scr  = vyrob_sprit(X_FILE,Y_FILE,8,OSTATNI_LOCK);
 byte *p_file = vyrob_sprit(X_FILE,Y_FILE,8,OSTATNI_LOCK);
 byte vybr_soubor[16] = "";
 byte dolni_buffer[100] = "";
 byte soubory[MAX_FILE][16];
 struct ffblk srch;
 int dale = 0;
 int i,j,zac = 0,akt = 0;
 int status = 1,vybran = 0;
 int souboru = 0;
 int prepis = 1;


 if((p_scr == NULL)||(p_file == NULL))
   tiskni_chybu("Malo pameti !",__LINE__);

 buf_kresli_sprit(spr[PRVNI_MENU+80],0,0,p_file);
 if(mod)
   buf_kresli_sprit(spr[PRVNI_MENU+81],0,0,p_file);
 else
   buf_kresli_sprit(spr[PRVNI_MENU+82],0,0,p_file);
   
 buf_kresli_sprit(spr[PRVNI_MENU+83],281,48,p_file);
 buf_kresli_sprit(spr[PRVNI_MENU+84],282,269,p_file);
 buf_kresli_sprit(spr[PRVNI_MENU+87],336,43,p_file);
 buf_kresli_sprit(spr[PRVNI_MENU+88],323,82,p_file);

 vypni_kurzor(mysi_info);
 uloz_sprit(p_scr,X_BAZE,Y_BAZE);

 zapni_kurzor(mysi_info);

 dale = findfirst(MASKA_LV,&srch,NULL);
 while(!dale) {
   if(souboru < MAX_FILE) {
     strcpy(soubory[souboru],srch.ff_name);
     if(!strcasecmp(srch.ff_name,p_aktualni_soubor))
       vybran = souboru;
     souboru++;
   }
   dale = findnext(&srch);
 }

 if(souboru > 0) {
   if(vybran+MAX_ZOBRAZENO < souboru) {
     zac = vybran;
   }
   else {
     if(souboru >= MAX_ZOBRAZENO) {
       zac = souboru - MAX_ZOBRAZENO;
       akt = vybran - zac;
     }
     else {
       zac = 0;
       akt = vybran;
     }
   }
 }
 
 if(mod&&!souboru) {
   strcpy(soubory[souboru],p_aktualni_soubor);
   souboru++;
 }

 dolni_buffer[0] = 0;
  
 akt_fileselector();
 while(status) {

   if(mysi_info[0]) {

     if((M_X > X_BAZE + X_SIPKY)&&(M_X < X_BAZE + X_SIPKY + 20)) {
       if((M_Y > Y_BAZE + Y_SIPKY1)&&(M_Y < Y_BAZE + Y_SIPKY1 + 20)) {
         key[K_NAHORU] = 1;
         mysi_info[0] = 0;
         goto klavesy;
       }

       if((M_Y > Y_BAZE + Y_SIPKY2)&&(M_Y < Y_BAZE + Y_SIPKY2 + 20)) {
         key[K_DOLU] = 1;
         mysi_info[0] = 0;
         goto klavesy;
       }
     }

     if((M_X > X_BAZE + X_PISMENA)&&(M_X < X_BAZE + X_PISMENA  + 90)) {
       if((M_Y > Y_BAZE + Y_PISMENA1)&&(M_Y < Y_BAZE + Y_PISMENA1 +33)) {
         key[K_ENTER] = 1;
         mysi_info[0] = 0;
         goto klavesy;
       }
       if((M_Y > Y_BAZE + Y_PISMENA2)&&(M_Y < Y_BAZE + Y_PISMENA2 + 39)) {
         key[K_ESC] = 1;
         mysi_info[0] = 0;
         goto klavesy;
       }
       
     }

     if((M_X > X_BAZE + X_OKRAJ)&&(M_X < X_BAZE + X_OKRAJ + X_SEL)) {
       if((M_Y > Y_BAZE + X_OKRAJ)&&(M_Y < Y_BAZE + Y_OKRAJ+ Y_SEL)) {
         akt = (M_Y - Y_BAZE - Y_OKRAJ)/20;
         akt_fileselector();
         mysi_info[0] = 0;
         goto klavesy;
       }
     }

     if((M_X > X_BAZE + X_OKRAJ)&&(M_X < X_BAZE + X_OKRAJ + X_SEL)) {
       if((M_Y > Y_BAZE+Y_OKRAJ+10+Y_SEL)&&(M_Y < Y_BAZE+Y_OKRAJ+10+Y_SEL+20)) {
         key[K_TAB] = 1;
         mysi_info[0] = 0;
         goto klavesy;
       }
     }
   }
   klavesy:
   
   if(key[K_ESC]) {
     key[K_ESC] = 0;
     status = 0;
     break;
   }
 
   if(key[K_ENTER]) {
     key[K_ENTER] = 0;
     break;
   }
 
   if(key[K_TAB]) {
     vypni_kurzor(mysi_info);
     vypln_ctverec(0,X_RADEK_B,Y_RADEK_B,X_SEL,20);
     
     if(il(X_RADEK_B,Y_RADEK_B,fn2,12,dolni_buffer,0)) {
       strcpy(vybr_soubor,dolni_buffer);
     }

     prepis = 0;
     akt_fileselector();
     prepis = 1;
     zapni_kurzor(mysi_info);
     key[K_TAB] = 0;
   }
   
   if(key[K_NAHORU]) {
     if(akt == 0) {
       if(zac != 0) {
         zac--;
         akt_fileselector();
       }
     }
     else {
       akt--;
       akt_fileselector();
     }
     key[K_NAHORU] = 0;
   }
 
   if(key[K_DOLU]) {
     key[K_DOLU] = 0;

     if(zac+akt+1 >= souboru)
       continue;
       
     if(akt+1 >= MAX_ZOBRAZENO) {
       if(zac+akt+1 < souboru) {
         zac++;
         akt_fileselector();
       }
     }
     else {
       akt++;
       akt_fileselector();
     }
   }
 
   if(key[K_PGUP]) {
     if(zac >= MAX_ZOBRAZENO)
       zac -= MAX_ZOBRAZENO;
     else {
       if(zac != 0)
         zac = 0;
       else
         akt = 0;
     }
     akt_fileselector();
     key[K_PGUP] = 0;
   }
 
   if(key[K_PGDN]) {
     if(zac+MAX_ZOBRAZENO >= souboru)
       continue;

     if(zac+MAX_ZOBRAZENO+MAX_ZOBRAZENO < souboru) {
       zac += MAX_ZOBRAZENO;
     }
     else {
       if(zac+MAX_ZOBRAZENO < souboru)
         zac = souboru-MAX_ZOBRAZENO;
       else {
         zac = souboru-MAX_ZOBRAZENO;
         akt = MAX_ZOBRAZENO-1;
       }
     }
     akt_fileselector();
     key[K_PGDN] = 0;
   }

 }


 if(status)
   strcpy(p_final_buffer,vybr_soubor);

// fprintf(stderr,"vybr_soubor = %s, final_buf = %s\n",vybr_soubor,p_final_buffer);
/*
 if(!strlen(p_final_buffer))
   strcpy(p_final_buffer,p_aktualni_soubor);
*/

 vypni_kurzor(mysi_info);
 kresli_sprit(p_scr,X_BAZE,Y_BAZE);
 zapni_kurzor(mysi_info);
 zrus_sprit(p_scr);
 zrus_sprit(p_file);
 return(status);
}


void obsluz_mys(int rovina)
{

int x,y,lim,prvek,varianta,prvku;

    x = mysi_info[1]/X;
    y = (mysi_info[2]/Y);

   if(mysi_info[2] < 60) {;
       if(x < 1) {
         listu_doleva();
         mysi_info[0] = 0;
         return;
       }

       if((x == PRVNI_X+PRVKU_NA_LISTE)) {
         listu_doprava();
         mysi_info[0] = 0;
         return;
       }

       if((x >= PRVNI_X)&&(x < PRVNI_X+
         ((rv[rovina].prvku < PRVKU_NA_LISTE)?rv[rovina].prvku:PRVKU_NA_LISTE))) {
          rv[rovina].akt = x-PRVNI_X;
          prvku = rv[rovina].prvku-rv[rovina].prvni;
          if(prvku > PRVKU_NA_LISTE) prvku = PRVKU_NA_LISTE;
          vypni_kurzor(mysi_info);
          vykresli_horni_listu(prvku,rv[rovina].prvni);
          kresli_sprit_bar(spr[UKAZATEL_3],(PRVNI_X+rv[rovina].akt)*X,0,BILA);
          zapni_kurzor(mysi_info);
          mysi_info[0] = 0;
          return;
       }
   }
   else {
   /* kresleni vybraneho prvku */
       if(!(rv[rovina].stav & 1)) {
          mysi_info[0] = 0;
          return;
       }

       if(mysi_info[0] == 1) {  // Kresleni vybraneho ctverce
         lim = rv[rovina].prvni+rv[rovina].akt;
         prvek = lista[lim][1][PRVEK];
         varianta = lista[lim][1][VARIANTA];
       }
       if(mysi_info[0] == 2) { // mazani vybraneho ctverecku
         if(rovina == 1)
           prvek = NENI_PODLAHA;
         if(rovina == 2)
           prvek = P_ZEME;
         if(rovina == 3)
           prvek = SLEPEJ_PRVEK;
       }

       vypni_kurzor(mysi_info);
       zapis_ctverecek(x,y,rovina,prvek,varianta,rotace);
       zapni_kurzor(mysi_info);
       mysi_info[0] = 0;
   }
}

void zapis_ctverecek(int x, int y, int rovina, int prvek, int varianta, int rotace)
{

   if(!(rv[rovina].stav & 1)) {
      return;
   }
   y -= YFUK;

   if(rovina == 1) {
     pl.podlaha[y][x][PRVEK] = prvek;
     pl.podlaha[y][x][VARIANTA] = varianta;
     pl.podlaha[y][x][ROTACE] = rotace;
   }
   if(rovina == 2) {
     if(pl.hraci[y][x] == SLEPEJ_PRVEK) {
        pl.level[y][x][PRVEK] = prvek;
        pl.level[y][x][VARIANTA] = varianta;
        pl.level[y][x][ROTACE] = rotace;
     }
   }
   if(rovina == 3) {
     if(prvek == SLEPEJ_PRVEK) {
       pl.hraci[y][x] = SLEPEJ_PRVEK;
     }
     else {
       pl.hraci[y][x] = prvek-1;
       pl.natoceni[prvek-1] = varianta;
     }
   }
   obnov_ctverecek(x,y);
}

void obnov_ctverecek(int x, int y)
{
  dword s,prvek,pom,x1,y1,i,pr,x2,y2;

  x1 = x*X;
  y1 = (y+YFUK)*Y;

  if(!(rv[0].stav&2)) {;
    buf_uloz_sprit(pp_spr,x*X,y*Y,p_pozadi);
  }
  else
    buf_kresli_sprit_bar(spr[EDIT_ZEME],0,0,BILA,pp_spr);

  if(rv[1].stav & 1) {
      if(rv[1].stav&2)
        buf_kresli_sprit_bar(spr[EDIT_ZEME],0,0,BILA,pp_spr);
      if(pl.podlaha[y][x][PRVEK] != NENI_PODLAHA) {
        s = prv[P_ZEME].varianty[pl.podlaha[y][x][VARIANTA]];
        buf_kresli_sprit_bar(spr[s],0,0,BILA,pp_spr);
      }
  }

  if(rv[2].stav&1) {
    if(rv[2].stav&2)
      buf_kresli_sprit_bar(spr[EDIT_ZEME],0,0,BILA,pp_spr);

    prvek = pl.level[y][x][PRVEK];
    if(prvek != P_ZEME) {
      s = prv[prvek].varianty[pl.level[y][x][VARIANTA]];
      if (pl.level[y][x][ROTACE]) {
         rotuj_sprit(spr[s],pp_spr,pl.level[y][x][ROTACE]);
      }
      else
         buf_kresli_sprit_bar(spr[s],0,0,BILA,pp_spr);
   
      pr = pl.level[y][x][VARIANTA]*prv[prvek].podprvku;
      for(i = 0; i < prv[prvek].podprvku; i++) {
         x2 = x+podprv[prv[prvek].podprvky[pr+i]].x_kor;
         y2 = y+podprv[prv[prvek].podprvky[pr+i]].y_kor;
         pom = podprv[prv[prvek].podprvky[pr+i]].varianty[prv[prvek].pdp_vr[pr+i]];
         s = prv[podprv[prv[prvek].podprvky[pr+i]].druh].varianty[pom];
         kresli_sprit_bar(spr[s],X*x2,Y*(y2+YFUK),BILA);
   
         pl.level[y2][x2][PRVEK] = podprv[prv[prvek].podprvky[pr+i]].druh;
         pl.level[y2][x2][VARIANTA] = pom;
      }
    }
 }

 if(rv[3].stav&1) {
   if(pl.hraci[y][x] != SLEPEJ_PRVEK) {
       i = pl.hraci[y][x];
       buf_kresli_sprit_bar(spr[PRVNI_HRAC+i+(pl.natoceni[i]*HRACU)],0,0,BILA,pp_spr);
       /*
       if(pl.natoceni[i] != 0) {
         rotuj_sprit(spr[PRVNI_HRAC+i],pp_spr,pl.natoceni[i]);
       }
       else
         buf_kresli_sprit_bar(spr[PRVNI_HRAC+i],0,0,BILA,pp_spr);
       */
   }
 }
 kresli_sprit_bar(pp_spr,x1,y1,BILA);
}

int uloz_level(byte *p_file, int kontrola)
{
 FILE *f;
 byte bak[100];
 byte *p_tecka;
 byte pom[100];

 strcpy(bak,p_file);
 if((p_tecka = strchr(bak,'.')) == NULL) {
   strcat(bak,KONCOVKA);
   strcat(p_file,KONCOVKA);
 }
 if((p_tecka = strchr(bak,'.')) == NULL) {
   /* sakra ? co ted ? asi konec... */
   tiskni_chybu("Interni chyba c.1 (asi jedina mozna :). Napiste mi na kom@atlas.cz.",__LINE__);
 }
 *p_tecka = NULL;
 strcat(bak,".bak");

 if((kontrola)&&((f = fopen(p_file,"rb")) != NULL)) {
   fclose(f);
   vypln_ctverec(0,0,0,640,60);
   sprintf(pom,"%s ji^z existuje. p^repsat? (A/N)",p_file);
   printfn(10,20,fn2,pom);
   clrkb();
   if(getkb() != K_A)
     return(NULL);
 }

 rename(p_file,bak);
 if((f = fopen(p_file,"wb")) == NULL) {
   vypln_ctverec(0,0,0,640,60);
   sprintf(pom,"nemohu zapsat $urove^n %s",p_file);
   printfn(10,20,fn2,pom);
   getkb();
   return(NULL);
 }

 fwrite(&pl,sizeof(pl),1,f);
 fclose(f);
 return(1);
}

int load_level(byte *p_file)
{
 byte *p_tecka;

 if(obnov_level(p_file,&pl) == NULL) {
   if(strstr(p_file,KONCOVKA) == NULL) {
     if((p_tecka = strchr(p_file,'.')) != NULL)
        *p_tecka = NULL;
     strcat(p_file,KONCOVKA);
     return(obnov_level(p_file,&pl));
   }
   else {
//    fprintf(stderr,"podivny navrat ?\n");
    return(NULL);
   }
 }
 return(1);
}


void hraj_level(void)
{
 DISK_LEVEL zal;         //Adresa struktury s levelem

 zal = pl;

 smaz_paletu();
 vypni_kurzor(mysi_info);
 YFUK = 2;

 z_editoru = 1;
 uziv_level = 1;

 strcpy(levelset[levelset[0][1] = 1],ed_level);
 vypln_ctverec(0,0,0,_a_xres,_a_yres);
 vykresli_pozadi();
 vykresli_level();
 vykresli_panel();

 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+3]);
 hraj();
 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+1]);

 vypln_ctverec(0,0,0,_a_xres,_a_yres);

 z_editoru = 0;
 uziv_level = 0;

 YFUK = 3;
 pl = zal;
 zapni_kurzor(mysi_info);
 nastav_paletu(PALETA);
}

/*-------------------------------------------------------------------------------
 * Konec EDITORU
 * -------------------------------------------------------------------------------
 */
void uvolni_pamet(void)
{
 vypni_midas();
 zavri_soubor_lep(DAT_FILE);
 vypni_mys();
 vypni_klavesnici();
}



/* vypise hlasku o pameti */
void testuj_pamet(byte *p_text)
{
#ifndef __AMIGA__
 __dpmi_free_mem_info      fr;
 __dpmi_memory_info        mi;

 __dpmi_get_free_memory_information(&fr);
 __dpmi_get_memory_information(&mi);
#endif

// printf("**** %s ****\n",p_text);
 printf("%s.....................%ld KB\n",p_text,_go32_dpmi_remaining_physical_memory()/1000);
 /*
 printf("Zbyvajivi virtualni...................%ld KB\n",_go32_dpmi_remaining_virtual_memory()/1000);

// printf("Get free memory .....\n");
// printf("Nejvetsi volny blok..%ld KB\n",fr.largest_available_free_block_in_bytes/1000);
 printf("Max odemknute pameti..................%ld KB\n",fr.maximum_unlocked_page_allocation_in_pages*4096/1000);
 printf("Max zamknute..........................%ld KB\n",fr.maximum_locked_page_allocation_in_pages*4096/1000);
 printf("Celkem oddemknute.....................%ld KB\n",fr.total_number_of_unlocked_pages*4096/1000);
// printf("Celkem volne ve strankach.............%ld KB\n",fr.total_number_of_free_pages*4096/1000);
// printf("Celkem fyzicke.......%ld KB\n",fr.total_number_of_physical_pages*4096/1000);
// printf("Pameti na disku......%ld KB\n\n",fr.size_of_paging_file_partition_in_pages*4096/1000);

// printf("Get memory .....\n");
 printf("Celkem alokovane pameti fyzicke host..%ld KB\n",mi.total_allocated_bytes_of_physical_memory_host/1000);
 printf("Celkem alokovane virtualni host.......%ld KB\n",mi.total_allocated_bytes_of_virtual_memory_host/1000);
 printf("Celkem volne virtualni host...........%ld KB\n",mi.total_available_bytes_of_virtual_memory_host/1000);

// printf("Celkem alokovane virtualni vcpu.......%ld KB\n",mi.total_allocated_bytes_of_virtual_memory_vcpu/1000);
// printf("Celkem volne virtualni vcpu...........%ld KB\n",mi.total_available_bytes_of_virtual_memory_vcpu/1000);
 printf("Celkem alokovane virtualni client.....%ld KB\n",mi.total_allocated_bytes_of_virtual_memory_client/1000);
// printf("Celkem volne virtualni client.........%ld KB\n\n",mi.total_available_bytes_of_virtual_memory_client/1000);

 printf("Celkem zamcene client.................%ld KB\n",mi.total_locked_bytes_of_memory_client/1000);
 printf("Max locked client.....................%ld KB\n\n",mi.max_locked_bytes_of_memory_client/1000);
// printf("Nejvetsi lin. adresa vol. pro client..%ld KB\n",mi.highest_linear_address_available_to_client/1000);
// printf("V nejvetsim volnem bloku .............%ld KB\n",mi.size_in_bytes_of_largest_free_memory_block/1000);
// printf("Nejmensi alokovatalna jednotka........%ld KB\n",mi.size_of_minimum_allocation_unit_in_bytes/1000);
// printf("Alokacni zarovnani....................%ld KB\n",mi.size_of_allocation_alignment_unit_in_bytes/1000);
*/

}


void spust_editor(void)
{
 int zal_music, zal_sound;

 vypni_kurzor(mysi_info);
 smaz_paletu();
 vf(0);
 YFUK = 3;

 uvolni_hlavni_menu();
 uvolni_nastaveni_menu();

 stop_modul();
 stop_oci();

 zal_sound = samply;
 zal_music = music;

 samply = 0;
 music = 0;

 obnov_level_lep(EDITOR,DAT_FILE,&pl);

 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+1]);
 edituj();
 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+2]);

 music = zal_music;
 samply = zal_sound;

 spust_oci();
 hraj_hudbu_menu();
 navrat_do_menu();
}

inline void navrat_do_menu(void)
{
 smaz_paletu();
 vypln_ctverec(0,0,0,_a_xres,_a_yres);

 nahraj_hlavni_menu();
 nahraj_nastaveni_menu();

 kresli_podklad_menu();
 nastav_paletu(PALETA_MNU);
 p_smenu = (GENERIC_MENU *)&hlavni_menu;
 spust_oci();
}

inline void navrat(void)
{
 smaz_paletu();
 vypni_kurzor(mysi_info);
 vypln_ctverec(0,0,0,_a_xres,_a_yres);
 kresli_podklad_menu();
 nastav_paletu(PALETA_MNU);
}

void _1_sada(void)
{
 spust_levely(1,-1);
}
void _2_sada(void)
{
 spust_levely(2,-1);
}
void _3_sada(void)
{
 spust_levely(3,-1);
}
void _4_sada(void)
{
 spust_levely(4,-1);
}
void _5_sada(void)
{
 spust_levely(5,-1);
}

void spust_levely(int sada,int ak_level)
{
 #define END_SIGN "konec"

 int stav;

 stop_oci();
 vypni_kurzor(mysi_info);
 smaz_paletu();
 vypln_ctverec(0,0,0,_a_xres,_a_yres);

 uvolni_hlavni_menu();
 uvolni_nastaveni_menu();

 nahraj_levelset_lep(sada,DAT_FILE);
 YFUK = 2;

 do  {
     levelset[0][1] = (ak_level+=2);
     if(uvod_do_levelu()) {
       obnov_level_lep(levelset[ak_level],DAT_FILE,&pl);
       vypln_ctverec(0,0,0,_a_xres,_a_yres);
       vykresli_pozadi();
       vykresli_level();
       vykresli_panel();
       stav = hraj();
       uvolni_pozadi();
/*
       if((levelset[level+2][0] != 0)&&(stav == 2)) {
          vyhodnoceni_levelu();
       }
*/
//       fprintf(stderr,"level = %s\n",levelset[ak_level+2]);
       if((!strcmp(levelset[ak_level+2],END_SIGN))&&(stav == 2)) {
          konec_epizody(levelset[0][0]);
          stav = 0;
       }
       hraj_hudbu_menu();

       if(_a_klavesa[0] == K_ESC)
         break;
     }
     else stav = 0;
 } while(stav);

 navrat_do_menu();
}

#define uvolni_mem()       \
{                          \
  for(j = 0; j < i; j++) { \
     zrus_sprit(p_maz[j]); \
  }                        \
  zrus_sprit(p_maz[MAZOL]);\
}                          \

int il(int x, int y, int font, int maxzn, char *p_str, int maz)
{
 #define MAZOL 259
 #define KURZOR '_'

 int z,i = 0,j;
 byte *p_maz[260];
 dword x_vel[256];

 if((p_maz[MAZOL] = vyrob_sprit(szn(font,KURZOR),vzn(font,KURZOR),8,OSTATNI_LOCK)) == NULL)
   tiskni_chybu("Nedostatek pameti !",__LINE__);

 if(maxzn > 255) // Moc znaku
   return(0);

 clrkb();

 uloz_sprit(p_maz[MAZOL],x,y);
 putfn(x,y,font,'_');

 while(1) {
   kresli_oci();
   z = getasa();

   if(z == 0)
     continue;

   kresli_sprit(p_maz[MAZOL],x,y);
 
   if((32 < z)&&(z < 127)) {
     if(i < maxzn) {

       if((p_maz[i] = vyrob_sprit(szn(font,z),vzn(font,z),8,OSTATNI_LOCK)) == NULL) {
          goto chyba_mem;
       }

       uloz_sprit(p_maz[i],x,y);
       x += x_vel[i] = putfn(x,y,font,z);
       p_str[i] = z;
       i++;
     }
     if(i == maxzn) {
       p_str[i] = 0;
       uvolni_mem();
       return(1);
     }
   }
   z = getkba();
   if(z == K_BKSP) {
     if(!i) continue;
     --i;
     x -= x_vel[i];
     kresli_sprit(p_maz[i],x,y);
     zrus_sprit(p_maz[i]);
     p_str[i] = 0;
   }
   if(z == K_ESC) {
     uvolni_mem();
     key[K_ESC] = 0;
     return(0);
   }
   if(z == K_ENTER) {
     p_str[i] = 0;
     uvolni_mem();
     key[K_ENTER] = 0;
     return(1);
   }
   clrkb();
   uloz_sprit(p_maz[MAZOL],x,y);
   putfn(x,y,font,'_');
 }
 uvolni_mem();
 return(0);

chyba_mem:
 tiskni_chybu("Nedostatek pameti !",__LINE__);
 return(0);
}

void zadej_heslo(void)
{
 byte heslo[20];
 int i,h;

 vypni_kurzor(mysi_info);
 kresli_sprit(spr[PRVNI_MENU+16],113+127,HMF+99);

 if(il(280,245,fn1,5,heslo,0)) {
   // Nahraje postupne jednotlive levelsety a prohleda na hesla
   for(i = 1; i < 6; i++) {
      nahraj_levelset_lep(i,DAT_FILE);
      h = 2;
      do  {
          if(strcmp(levelset[h],heslo) == NULL) {
            spust_levely(i,h-3);
            return;
          }
      } while(levelset[h+=2][0] != 0);
   }
   // spatne heslo a nebo dohrani berusek -> navrat do menu
 }
}


void nahraj_levelset(int set)
{
 char *sety[] = {"s_tren.txt","s_lehky.txt","s_stred.txt","s_tezky.txt","s_nemoz.txt"};
 FILE *f;
 int   i = 1;

 if((f = fopen(sety[set-1],"r")) == NULL)
   neni_file(sety[set-1]);

 for(i = 0; i < 100; i++)
   memset(levelset[i],0,50);
 i = 1;
 levelset[0][0] = (byte) set;
 while(fgets(levelset[i],50,f)) {
    if((levelset[i][0] == ';')||(levelset[i][0] == '\n')||(levelset[i][0] == ' '))
      continue;
    levelset[i][strlen(levelset[i++])-1] = 0;
   while(fgets(levelset[i],50,f)&&((levelset[i][0] == ';')||(levelset[i][0] == '\n')||(levelset[i][0] == ' ')));
    levelset[i][strlen(levelset[i++])-1] = 0;
 }
 fclose(f);
}

void nahraj_levelset_lep(int set,int dat)
{
 char *sety[20] = {"s_tren.txt","s_lehky.txt","s_stred.txt","s_tezky.txt","s_nemoz.txt"};
 char *p_jmeno,
     *p_buffer,
     *p_pom;
 int i = 1;


 if((p_jmeno = p_buffer = malloc(VEL_LEVELSETU)) == NULL) {
    tiskni_chybu("Nedostatek pameti.\n",__LINE__);
 }

 if(nahraj_soubor_lep(0,p_buffer,dat,sety[set-1]) == 0)
   neni_file(sety[set-1]);

 levelset[0][0] = (byte) set;
 levelset[1][0] = 0;

 while(1) {
    levelset[i][0] = 0;
    levelset[i+1][0] = 0;

    if((p_pom = strchr(p_buffer,0xd)) == NULL)
      break;
    p_pom[0] = 0;

    if((p_buffer[0] == ';')||(p_buffer[0] == '\n')||(p_buffer[0] == ' ')) {
      p_buffer = p_pom+2;
      continue;
    }
    strcpy(levelset[i],p_buffer);
    p_buffer = p_pom+2;
    i++;
 }

 free(p_jmeno);
}

/* dilema po stisku ESC ve hre */
int dilema(void)
{
 int ret;
 byte *p_0, *p_01;
 byte *p_1, *p_11;
 byte *p_2, *p_21;
 byte *p_3, *p_31;
 byte *p_4, *p_41;
 int i;

 #define P_0 (PRVNI_MENU+49)
 #define P_1 (PRVNI_MENU+50)
 #define P_2 (PRVNI_MENU+51)
 #define P_3 (PRVNI_MENU+52)
 #define P_4 (PRVNI_MENU+53)

 #define X_GR 237
 #define Y_GR 54

 #define D0 (X_GR*(Y_GAME+Y_GR)+VH_SPRITU)
 #define D1 ((X_GAME+X_GR)*Y_GR+VH_SPRITU)
 #define D2 ((X_GAME+X_GR)*Y_GR+VH_SPRITU)
 #define D3 ((X_GAME+X_GR)*Y_GR+VH_SPRITU)
 #define D4 (X_GR*(Y_GAME+Y_GR)+VH_SPRITU)

/*
#define X_GAME (320-(237/2))
#define Y_GAME (240-(249/2))
*/

 vypni_animace();

 p_0 = vyrob_sprit(X_GR,Y_GAME+Y_GR,8,OSTATNI_LOCK);
 p_01 = vyrob_sprit(X_GR,Y_GAME+Y_GR,8,OSTATNI_LOCK);

 p_1 = vyrob_sprit(X_GAME+X_GR,Y_GR,8,OSTATNI_LOCK);
 p_11 = vyrob_sprit(X_GAME+X_GR,Y_GR,8,OSTATNI_LOCK);

 p_2 = vyrob_sprit(X_GAME+X_GR,Y_GR,8,OSTATNI_LOCK);
 p_21 = vyrob_sprit(X_GAME+X_GR,Y_GR,8,OSTATNI_LOCK);

 p_3 = vyrob_sprit(X_GAME+X_GR,Y_GR,8,OSTATNI_LOCK);
 p_31 = vyrob_sprit(X_GAME+X_GR,Y_GR,8,OSTATNI_LOCK);

 p_4 = vyrob_sprit(X_GR,Y_GAME+Y_GR,8,OSTATNI_LOCK);
 p_41 = vyrob_sprit(X_GR,Y_GAME+Y_GR,8,OSTATNI_LOCK);

 uloz_sprit(p_01,X_GAME,0);
 uloz_sprit(p_11,0,Y_GAME+51);
 uloz_sprit(p_21,X_GAME,Y_GAME+106);
 uloz_sprit(p_31,0,Y_GAME+156);
 uloz_sprit(p_41,X_GAME,Y_GAME+208);

#ifndef __AMIGA__
 for(i = 0; i <= 100; i += 10) {
    memcpy(p_0,p_01,D0);
    memcpy(p_1,p_11,D1);
    memcpy(p_2,p_21,D2);
    memcpy(p_3,p_31,D3);
    memcpy(p_4,p_41,D4);
    buf_kresli_sprit_bar(spr[P_0],0,17+i,16,p_0);
    buf_kresli_sprit_bar(spr[P_1],4+(i*2),0,16,p_1);
    buf_kresli_sprit_bar(spr[P_2],200-(i*2),0,16,p_2);
    buf_kresli_sprit_bar(spr[P_3],4+(i*2),0,16,p_3);
    buf_kresli_sprit_bar(spr[P_4],0,100-i,16,p_4);
    cekej_na_paprsek();
    kresli_sprit(p_0,X_GAME,0);
    kresli_sprit(p_1,0,Y_GAME+51);
    kresli_sprit(p_2,X_GAME,Y_GAME+106);
    kresli_sprit(p_3,0,Y_GAME+156);
    kresli_sprit(p_4,X_GAME,Y_GAME+208);
//    getkb();
 }
#endif

 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+3]);
 ret = obsluz_menu((GENERIC_MENU *)&dilema_menu,0,0,0);
 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+2]);


#ifndef __AMIGA__
 for(i = 100; i >= 0; i -= 10) {
    memcpy(p_0,p_01,D0);
    memcpy(p_1,p_11,D1);
    memcpy(p_2,p_21,D2);
    memcpy(p_3,p_31,D3);
    memcpy(p_4,p_41,D4);
    buf_kresli_sprit_bar(spr[P_0],0,17+i,16,p_0);
    buf_kresli_sprit_bar(spr[P_1],4+(i*2),0,16,p_1);
    buf_kresli_sprit_bar(spr[P_2],200-(i*2),0,16,p_2);
    buf_kresli_sprit_bar(spr[P_3],4+(i*2),0,16,p_3);
    buf_kresli_sprit_bar(spr[P_4],0,100-i,16,p_4);
    cekej_na_paprsek();
    kresli_sprit(p_0,X_GAME,0);
    kresli_sprit(p_1,0,Y_GAME+51);
    kresli_sprit(p_2,X_GAME,Y_GAME+106);
    kresli_sprit(p_3,0,Y_GAME+156);
    kresli_sprit(p_4,X_GAME,Y_GAME+208);
//    getkb();
 }
#endif

 kresli_sprit(p_01,X_GAME,0);
 kresli_sprit(p_11,0,Y_GAME+51);
 kresli_sprit(p_21,X_GAME,Y_GAME+106);
 kresli_sprit(p_31,0,Y_GAME+156);
 kresli_sprit(p_41,X_GAME,Y_GAME+208);

 zrus_sprit(p_0);
 zrus_sprit(p_01);
 zrus_sprit(p_1);
 zrus_sprit(p_11);
 zrus_sprit(p_2);
 zrus_sprit(p_21);
 zrus_sprit(p_3);
 zrus_sprit(p_31);
 zrus_sprit(p_4);
 zrus_sprit(p_41);

 aktivuj_animace();

 return(ret);
}

void help_ig(void)
{
 byte *p_spr;

 if((p_spr = vyrob_sprit(640,480,8,OSTATNI_LOCK)) == NULL) {
   tiskni_chybu("Nedostatek pameti !",__LINE__);
 }
 uloz_sprit(p_spr,0,0);

 napoveda.pol[3].p_menu = (GENERIC_MENU *)NULL;

 vf(0);

 kresli_podklad_menu();
 nastav_paletu(PALETA_MNU);
 obsluz_menu((GENERIC_MENU *)&napoveda,0,0,0);
 napoveda.pol[3].p_menu = (GENERIC_MENU *)&hlavni_menu;
 smaz_paletu();

 kresli_sprit(p_spr,0,0);
 zrus_sprit(p_spr);

 clrkb();
 return;
}

void nastaveni_ig(void)
{
 byte *p_spr;

 stmav_paletu(PALETA);
 nahraj_podklad_menu();
 nahraj_nastaveni_menu();
 spust_oci();

 if((p_spr = vyrob_sprit(640,480,8,OSTATNI_LOCK)) == NULL) {
   tiskni_chybu("Nedostatek pameti !",__LINE__);
 }

 uloz_sprit(p_spr,0,0);

 vf(0);
 kresli_podklad_menu();
 nastav_paletu(PALETA_MNU);

 menu_nastaveni();

 smaz_paletu();
 kresli_sprit(p_spr,0,0);

 zrus_sprit(p_spr);

 stop_oci();
 uvolni_nastaveni_menu();
 uvolni_podklad_menu();
 rozsvit_paletu(PALETA);

 clrkb();
 key[K_ESC] = 0;
 return;
}

void menu_nastaveni(void)
{
 #define X_B 131
 #define Y_B (HMF+104)

 int sp,x,y,kr = 0,stat;
 int x1 = vol_sound*3, x2 = vol_music*3;
 int z_s;
 byte *p_zal1,*p_zal2;
// byte text[100];
 FILE *f;

 vypni_kurzor(mysi_info);

 if((p_zal1 = vyrob_sprit(12,28,8,OSTATNI_LOCK)) == NULL)
   exit(CHYBA);
 if((p_zal2 = vyrob_sprit(12,28,8,OSTATNI_LOCK)) == NULL)
   exit(CHYBA);

 kresli_sprit(spr[PRVNI_MENU+59],113+75,HMF+99);

 uloz_sprit(p_zal1,x1+100+X_B-5,78+Y_B);
 uloz_sprit(p_zal2,x2+100+X_B-5,155+Y_B);

 kresli_sprit_bar(spr[PRVNI_MENU+58],x1+X_B+100-5,Y_B+78,BILA);
 kresli_sprit_bar(spr[PRVNI_MENU+58],x2+X_B+100-5,Y_B+155,BILA);

 sp = 63; x = X_B+149; y = Y_B+43;
 kresli_sprit(spr[PRVNI_MENU+60],x,y);

 zapni_kurzor(mysi_info);
 nastav_paletu(PALETA_MNU);

 clrkb();
 mysi_info[0] = 0;
 key[K_ESC] = 0;
 key[K_ENTER] = 0;

 z_s = vol_sound;

#ifdef __AMIGA__
 // how come the compiler didn't complain about this?
 stat = 0;
#endif

 do {
   kresli_oci();
   updatuj_samply();

   if((kr != 0)&&(M_Y > Y_B+43)&&(M_Y < Y_B+125)&&
      (M_X > X_B+100)&&(M_X < X_B+290)) {
      hraj_sampl(MENU_SKOK,DELKA_MENU_SKOK,PRIORITA_MENU_SKOK);
      vypni_kurzor(mysi_info);
      kresli_sprit(spr[PRVNI_MENU+sp],x,y);
      sp = 63; x = X_B+149; y = Y_B+43;
      kresli_sprit(spr[PRVNI_MENU+60],x,y);
      zapni_kurzor(mysi_info);
      kr = 0;
   }

   if((kr != 1)&&(M_Y > Y_B+125)&&(M_Y < Y_B+186)&&
      (M_X > X_B+100)&&(M_X < X_B+290)) {
      hraj_sampl(MENU_SKOK,DELKA_MENU_SKOK,PRIORITA_MENU_SKOK);
      vypni_kurzor(mysi_info);
      kresli_sprit(spr[PRVNI_MENU+sp],x,y);
      sp = 64; x = X_B+149; y = Y_B+125;
      kresli_sprit(spr[PRVNI_MENU+61],x,y);
      zapni_kurzor(mysi_info);
      kr = 1;
   }

   if((kr != 2)&&(M_Y > Y_B+221)&&(M_Y < Y_B+261)&&
      (M_X > X_B+154)&&(M_X < X_B+228)) {
      hraj_sampl(MENU_SKOK,DELKA_MENU_SKOK,PRIORITA_MENU_SKOK);
      vypni_kurzor(mysi_info);
      kresli_sprit(spr[PRVNI_MENU+sp],x,y);
      sp = 65; x = X_B+154; y = Y_B+221;
      kresli_sprit(spr[PRVNI_MENU+62],x,y);
      zapni_kurzor(mysi_info);
      kr = 2;
   }

   if((mysi_info[0])&&(kr == 2)) {
      stat = K_ENTER;
      break;
   }


   if((mysi_info[0])&&(M_X > 100+X_B)&&(M_X < 290+X_B)&&
      (M_Y > 78+Y_B)&&(M_Y < 78+Y_B+28)) {
      vypni_kurzor(mysi_info);
      kresli_sprit(p_zal1,x1+100+X_B-5,78+Y_B);
      uloz_sprit(p_zal1,M_X-5,78+Y_B);
      kresli_sprit_bar(spr[PRVNI_MENU+58],M_X-5,78+Y_B,BILA);
      x1 = M_X-(100+X_B);
      zapni_kurzor(mysi_info);
      mysi_info[0] = 0;

      vol_sound = x1/3;
/*
      vypln_ctverec(0,0,460,400,20);
      sprintf(text,"Hlasitost samplu je %d",x1/3);
      printfn(0,460,fn1,text);
*/
      play_sampl(PREPNI,0);
   }

   if((mysi_info[0])&&(M_X > 100+X_B)&&(M_X < 290+X_B)&&
      (M_Y > 155+Y_B)&&(M_Y < 155+Y_B+28)) {
      vypni_kurzor(mysi_info);
      kresli_sprit(p_zal2,x2+100+X_B-5,155+Y_B);
      uloz_sprit(p_zal2,M_X-5,155+Y_B);
      kresli_sprit_bar(spr[PRVNI_MENU+58],M_X-5,155+Y_B,BILA);
      x2 = M_X-(100+X_B);
      zapni_kurzor(mysi_info);
      mysi_info[0] = 0;
/*
      vypln_ctverec(0,0,460,400,20);
      sprintf(text,"Hlasitost hudby je %d",x2/3);
      printfn(0,460,fn1,text);
*/
      MIDASsetMusicVolume(a_mod_play_handle,x2/3);
   }

   if(key[K_ESC])
     stat = K_ESC;

   if(key[K_ENTER])
     stat = K_ENTER;
 
#ifdef __AMIGA__
   cekej_na_paprsek();

   // makeshift keyboard controls for the options menu
   int mousepos[3][2] =
   {
      {(Y_B+43 + Y_B+125) / 2 + 5, (X_B+100 + X_B+290) / 2},
      {(Y_B+125 + Y_B+186) / 2 + 10, (X_B+100 + X_B+290) / 2},
      {(Y_B+221 + Y_B+261) / 2, (X_B+154 + X_B+228) / 2}
   };

   // poll joystick
   getkba();

   if(key[K_NAHORU]) {
      int newkr = (kr <= 0) ? 2 : kr-1;
      souradnice_mysi(mousepos[newkr][1], mousepos[newkr][0]);
      key[K_NAHORU] = 0;
   }
   else if(key[K_DOLU]) {
      int newkr = (kr+1) % 3;
      souradnice_mysi(mousepos[newkr][1], mousepos[newkr][0]);
      key[K_DOLU] = 0;
   }
   else if (kr < 2 && kr >= 0)
   {
      int sliderx = (kr == 0) ? x1 : x2;
      if(key[K_DOLEVA] && sliderx >= 3) {
         sliderx -= 3;
         souradnice_mysi(X_B+100+sliderx, mousepos[kr][0]);
         mysi_info[0] = 1;
      }
      else if(key[K_DOPRAVA] && sliderx <= 189-3) {
         sliderx += 3;
         souradnice_mysi(X_B+100+sliderx, mousepos[kr][0]);
         mysi_info[0] = 1;
      }
   }
#endif

 } while(!((stat == K_ESC)||(stat == K_ENTER)));

 hraj_sampl(MENU_KLIK,DELKA_MENU_KLIK,PRIORITA_MENU_KLIK);
 updatuj_samply();

 /*
   Jestlize ESC -> zrus nastaveni
   ENTER -> potvrd a nastav volbu
 */

 if(stat == K_ENTER) {

   vol_sound = x1/3;
   vol_music = x2/3;

   if((f = fopen(F_CONFIG,"rb+")) != NULL) {
     fseek(f,28,SEEK_SET);
#ifdef __AMIGA__
     vol_sound = SWAP32LE(vol_sound);
     vol_music = SWAP32LE(vol_music);
#endif
     fwrite(&vol_sound,sizeof(int),1,f);
     fwrite(&vol_music,sizeof(int),1,f);
#ifdef __AMIGA__
     vol_sound = SWAP32LE(vol_sound);
     vol_music = SWAP32LE(vol_music);
#endif
     fclose(f);
   }
 }
 else {
   vol_sound = z_s;
 }

 zrus_sprit(p_zal1);
 zrus_sprit(p_zal2);

 vypni_kurzor(mysi_info);
}


void prvni_napoveda(void)
{
 vypni_kurzor(mysi_info);
 kresli_sprit(spr[PRVNI_MENU+56],0,0);

 printfn(220,100,fn1,"Ovl$ad$an$i beru^sek");
 printfn(20,140,fn1,"K dispozici m#u^zete m$it a^z p^et beru^sek,");
 printfn(20,170,fn1,"kter$e lze ovl$adat t^emito kl$avesami:");

 printfn(20,230,fn1,"^sipky");
 printfn(20,260,fn1,"Alt+^sipky");
 printfn(20,290,fn1,"Tab");
 printfn(20,320,fn1,"N");
 printfn(20,350,fn1,"D");
 printfn(20,380,fn1,"ALT+X");

 printfn(69,230,fn1," . . . . . . . pohyb beru^sky");
 printfn(158,260,fn1,". . . . b^eh");
 printfn(86,290,fn1,". . . . . . . p^rep$in$an$i mezi beru^skami");
 printfn(60,320,fn1,". . . . . . . . zm^ena hudby");
 printfn(60,350,fn1,". . . . . . . . demo");
 printfn(110,380,fn1,". . . . . . Rychl$e ukon^cen$i");

 vykresli_menu((GENERIC_MENU *)&ovl2);
}

void druha_napoveda(void)
{
 vypni_kurzor(mysi_info);
 kresli_sprit(spr[PRVNI_MENU+56],0,0);

 printfn(220,100,fn1,"Ovlad$an$i beru^sek");
 printfn(20,140,fn1,"Dal^s$i u^zite^cn$e kl$avesy:");

 printfn(20,200,fn1,"ESC");
 printfn(20,230,fn1,"F1");
 printfn(20,260,fn1,"F2");
 printfn(20,290,fn1,"F3");
 printfn(20,320,fn1,"F4");
 printfn(20,350,fn1,"F10");

 printfn(60,200,fn1,". . . . . . . . Menu");
 printfn(60,230,fn1,". . . . . . . . N$apov^eda");
 printfn(60,260,fn1,". . . . . . . . Ulo^zen$i pozice");
 printfn(60,290,fn1,". . . . . . . . Nahr$an$i pozice");
 printfn(60,320,fn1,". . . . . . . . Nastaven$i");
 printfn(60,350,fn1,". . . . . . . . \"T$ipnut$i\" obr$azku");

 vykresli_menu((GENERIC_MENU *)&ovl2);
}

#define DELAY_HELP   0

void napoveda_ovladani(void)
{

  int stav = 0;
  int menu = 0;
  GENERIC_MENU *p_menu;

  stop_oci();

  nahraj_napoveda_menu();
  nahraj_menu_poz();

  p_menu = (GENERIC_MENU *)&ovl1;

  ovl1.pozadi.p_fn_vstup = prvni_napoveda;
  ovl3.pozadi.p_fn_vstup = druha_napoveda;

  do {
    switch(menu) {
      case 0:
       stav = obsluz_menu(p_menu,0,0xff,0);
       if(stav == 0)
         stav = 2;
       if(stav == 1) {
         p_menu = (GENERIC_MENU *)&ovl3;
         menu = 1;
       }
       break;

      case 1:
       stav = obsluz_menu(p_menu,0,0xff,0);
       if(stav == 0) {
         p_menu = (GENERIC_MENU *)&ovl1;
         menu = 0;
       }
       if(stav == 1)
         stav = 2;
       break;

      default:
       break;
    }

  } while(stav != 2);

  uvolni_napoveda_menu();
  uvolni_menu_poz();

  spust_oci();
  p_smenu = (GENERIC_MENU *)&napoveda;
  navrat();
}

void pravidla_1(void)
{
 int i,x,y;

 vypni_kurzor(mysi_info);
 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+3]);

 kresli_sprit(spr[PRVNI_MENU+57],0,0);
 printfn(80,95,fn2,"Z$akladn$i pravidla a hern$i prvky");

 printfn(20,140,fn2,"K  opu^st^en$i  ka^zd$e  $urovn^e  je  t^reba");
 printfn(20,160,fn2,"vlastnit p^et kl$i^c#u  a nav$ic m$it  volnou");
 printfn(20,180,fn2,"cestu k v$ychodu. P^ri pln^en$i jednotliv$ych");
 printfn(20,200,fn2,"mis$i  se budete  setk$avat  s rozli^cn$ymi");
 printfn(20,220,fn2,"hern$imi prvky,  jejich^z  v$yznam  se v$am");
 printfn(20,240,fn2,"nyn$i pokus$ime ve stru^cnosti  p^ribl$i^zit.");


 printfn(20,280,fn2,"Bedny - lze je tla^cit p^red sebou.");
 x = 20; y = 310;
 for(i = 1; i < prv[P_BEDNA].variant; i++) {
    kresli_prvek(P_BEDNA,i,0,x,y);
    x+= 30;
 }

 printfn(20,350,fn2,"v$ybu^snina - d$a se s n$i zni^cit bedna.");
 x = 20; y = 380;
 for(i = 0; i < prv[P_TNT].variant; i++) {
    kresli_prvek(P_TNT,i,0,x,y);
    x+= 30;
 }
 vykresli_menu((GENERIC_MENU *)&ovl2);
 zapni_kurzor(mysi_info);
}
void pravidla_2(void)
{
 int i,x,y;

 vypni_kurzor(mysi_info);
 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+3]);
 kresli_sprit(spr[PRVNI_MENU+57],0,0);
 printfn(160,95,fn2,"Aktivn$i prvky ve h^re");

 printfn(20,140,fn2,"kl$i^c - pot^rebujete jich rovn$ych p^et.");
 kresli_prvek(P_KLIC,0,0,20,165);

 printfn(20,200,fn2,"v$ychod - br$ana do dal^s$i $urovn^e.");
 x = 20; y = 230;
 for(i = 0; i < prv[P_EXIT].variant-1; i += 2) {
    kresli_prvek(P_EXIT,i,0,x,y);
    x+= 30;
 }
 kresli_prvek(P_EXIT,i-1,0,x,y);

 printfn(20,270,fn2,"k$amen - je mo^zn$e jej rozb$it kromp$a^cem.");
 x = 20; y = 300;
 for(i = 0; i < prv[P_KAMEN].variant; i++) {
    kresli_prvek(P_KAMEN,i,0,x,y);
    x+= 30;
 }

 printfn(20,340,fn2,"kromp$a^c - n$astroj pro eliminaci kamen#u.");
 x = 20; y = 370;
 for(i = 0; i < prv[P_KRUMPAC].variant; i++) {
    kresli_prvek(P_KRUMPAC,i,0,x,y);
    x+= 30;
 }

 vykresli_menu((GENERIC_MENU *)&ovl2);
 zapni_kurzor(mysi_info);
}

void pravidla_3(void)
{
 int i,j,x,y;

 vypni_kurzor(mysi_info);
 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+3]);
 kresli_sprit(spr[PRVNI_MENU+57],0,0);
 printfn(160,95,fn2,"Aktivn$i prvky ve h^re");

 printfn(20,140,fn2,"barevn$e  kl$i^ce -  slou^z$i pro odemyk$an$i");
 printfn(20,160,fn2,"barevn$ych  dve^r$i,  vz$it jej m#u^ze pouze");
 printfn(20,180,fn2,"beru^ska identick$e barvy.");
 x = 20; y = 210;
 for(j = 0; j < 5; j++) {
    for(i = 0; i < prv[P_KLIC1+j].variant; i++) {
       kresli_prvek(P_KLIC1+j,i,0,x,y);
       x+= 30;
    }
 }

 printfn(20,250,fn2,"Barevn$e dve^re - otev^r$it je lze v$yhrad-");
 printfn(20,270,fn2,"n^e kl$i^cem p^r$islu^sn$e barvy.");
 x = 40; y = 300;
 for(i = 0; i < prv[P_DVERE1_V_Z].variant; i++) {
    kresli_prvek(P_DVERE1_V_Z,i,0,x,y);
    x+= 70;
 }
// printfn(150,300,fn2," - dve^re pro ^cervenou beru^sku");

 printfn(20,340,fn2,"Barevn$e pr#uchody -  propust$i  p^res sv$e");
 printfn(20,360,fn2,"v^e^reje jen beru^sku stejn$e barvy. Pr#ucho-");
 printfn(20,380,fn2,"dem nelze protla^cit bednu.");

 x = 40; y = 410;
 for(i = 0; i < prv[P_ID_DVERE1_V_Z].variant; i++) {
    kresli_prvek(P_ID_DVERE1_V_Z,i,0,x,y);
    x+= 70;
 }
// printfn(150,410,fn2," - dve^re pro ^cervenou beru^sku");

 vykresli_menu((GENERIC_MENU *)&ovl2);
 zapni_kurzor(mysi_info);
}

void pravidla_4(void)
{
 int i,x,y;

 vypni_kurzor(mysi_info);

 nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+3]);

 kresli_sprit(spr[PRVNI_MENU+57],0,0);
 printfn(160,95,fn2,"Aktivn$i prvky ve h^re");

 printfn(20,140,fn2,"Jednopr#uchodov$e dve^re - lze jimi proj$it");
 printfn(20,160,fn2,"maxim$aln^e jednou,  potom se nav^zdy uza-");
 printfn(20,180,fn2,"v^rou a ji^z nen$i cesty, jak je otev^r$it.");
 x = 40; y = 220;
 for(i = 0; i < prv[P_DV_V_O].variant; i++) {
    kresli_prvek(P_DV_V_O,i,0,x,y);
    x+= 70;
 }
// printfn(150,220,fn2," - otev^ren$e");
 x = 40; y = 260;
 for(i = 0; i < prv[P_DV_V_Z].variant; i++) {
    kresli_prvek(P_DV_V_Z,i,0,x,y);
    x+= 70;
 }
// printfn(150,260,fn2," - zav^ren$e");

 printfn(15,320,fn2,"Ostatn$i prvky, tedy ty, kter$e zde nejsou");
 printfn(15,340,fn2,"uvedeny, jsou pouh$ymi zdmi, kter$e nemaj$i");
 printfn(15,360,fn2,"^z$adn$e zaj$imav$e vlastnosti.  Nelze je od-");
 printfn(15,380,fn2,"tla^cit  a ani je nen$i mo^zno ^z$adn$ym zp#u-");
 printfn(15,400,fn2,"sobem rozb$it.");

 vykresli_menu((GENERIC_MENU *)&ovl2);
 zapni_kurzor(mysi_info);
}

#define menu_do_gamepal()               \
{                                       \
  ovl1.pol[0].ne_akt = PRVNI_MENU+66+6; \
  ovl1.pol[0].akt = PRVNI_MENU+67+6;    \
  ovl1.pol[1].ne_akt = PRVNI_MENU+70+6; \
  ovl1.pol[1].akt = PRVNI_MENU+71+6;    \
  ovl2.pol[0].ne_akt = PRVNI_MENU+68+6; \
  ovl2.pol[0].akt = PRVNI_MENU+69+6;    \
  ovl2.pol[1].ne_akt = PRVNI_MENU+66+6; \
  ovl2.pol[1].akt = PRVNI_MENU+67+6;    \
  ovl2.pol[2].ne_akt = PRVNI_MENU+70+6; \
  ovl2.pol[2].akt = PRVNI_MENU+71+6;    \
  ovl3.pol[0].ne_akt = PRVNI_MENU+68+6; \
  ovl3.pol[0].akt = PRVNI_MENU+69+6;    \
  ovl3.pol[1].ne_akt = PRVNI_MENU+66+6; \
  ovl3.pol[1].akt = PRVNI_MENU+67+6;    \
  ovl1.pozadi.bila = 255;               \
  ovl2.pozadi.bila = 255;               \
  ovl3.pozadi.bila = 255;               \
}                                       \

#define menu_do_menupal()             \
{                                     \
  ovl1.pol[0].ne_akt = PRVNI_MENU+66; \
  ovl1.pol[0].akt = PRVNI_MENU+67;    \
  ovl1.pol[1].ne_akt = PRVNI_MENU+70; \
  ovl1.pol[1].akt = PRVNI_MENU+71;    \
  ovl2.pol[0].ne_akt = PRVNI_MENU+68; \
  ovl2.pol[0].akt = PRVNI_MENU+69;    \
  ovl2.pol[1].ne_akt = PRVNI_MENU+66; \
  ovl2.pol[1].akt = PRVNI_MENU+67;    \
  ovl2.pol[2].ne_akt = PRVNI_MENU+70; \
  ovl2.pol[2].akt = PRVNI_MENU+71;    \
  ovl3.pol[0].ne_akt = PRVNI_MENU+68; \
  ovl3.pol[0].akt = PRVNI_MENU+69;    \
  ovl3.pol[1].ne_akt = PRVNI_MENU+66; \
  ovl3.pol[1].akt = PRVNI_MENU+67;    \
  ovl1.pozadi.bila = 235;             \
  ovl2.pozadi.bila = 235;             \
  ovl3.pozadi.bila = 235;             \
}                                     \

void napoveda_pravidla(void)
{
  int stav = 0;
  int menu = 0;
  GENERIC_MENU *p_menu;

  stop_oci();
  p_menu = (GENERIC_MENU *)&ovl1;

  vypni_kurzor(mysi_info);
  vf(0);

  nahraj_napoveda_menu();
  nahraj_herni_poz();

  ovl1.pozadi.p_fn_vstup = pravidla_1;
  ovl2.pozadi.p_fn_vstup = pravidla_2;
  ovl3.pozadi.p_fn_vstup = pravidla_4;
  menu_do_gamepal();
  nastav_paletu(PALETA);

  do {
    switch(menu) {
      case 0:
       stav = obsluz_menu(p_menu,0,0xff,0);
       if(stav == 0)
         stav = 5;
       if(stav == 1) {
         p_menu = (GENERIC_MENU *)&ovl2;
         menu = 1;
       }
       break;

      case 1:
       stav = obsluz_menu(p_menu,0,0xff,0);
       if(stav == 0) {
         p_menu = (GENERIC_MENU *)&ovl1;
         menu = 0;
       }
       if(stav == 1)
         stav = 5;

       if(stav == 2) {
         ovl2.pozadi.p_fn_vstup = pravidla_3;
         menu = 2;
       }
       break;

      case 2:
       stav = obsluz_menu(p_menu,0,0xff,DELAY_HELP);
       if(stav == 0) {
         ovl2.pozadi.p_fn_vstup = pravidla_2;
         menu = 1;
       }
       if(stav == 1)
         stav = 5;

       if(stav == 2) {
         ovl2.pozadi.p_fn_vstup = pravidla_4;
         p_menu = (GENERIC_MENU *)&ovl3;
         menu = 3;
       }
       break;

      case 3:
       stav = obsluz_menu(p_menu,0,0xff,DELAY_HELP);
       if(stav == 0) {
         ovl2.pozadi.p_fn_vstup = pravidla_3;
         p_menu = (GENERIC_MENU *)&ovl2;
         menu = 2;
       }
       if(stav == 1)
         stav = 5;
       break;

      default:
       break;
    }

  } while(stav != 5);

  smaz_paletu();
  vf(0);
  menu_do_menupal();
  nastav_paletu(PALETA_MNU);

  uvolni_napoveda_menu();
  uvolni_herni_poz();

  spust_oci();
  p_smenu = (GENERIC_MENU *)&napoveda;
  navrat();

  nastav_kurzor_sprit(mysi_info,spr[PRVNI_OSTATNI+2]);
}


void edit_help(void)
{

}


void napoveda_autori(void)
{
 #define DELKA_ROLOVANI 1340

 byte *p_text[200];
 byte zumpa[4010];
 byte *p_sp[200];
 byte *p_scr;
 int  pozice[200];
 int  krok;
 int  i,t,j,q,v;

 vypni_kurzor(mysi_info);
 stop_oci();
 vf(0);

 if(!ve_hre)
   hraj_hudbu_credits();

 nahraj_ber_logo();
 if((p_scr = vyrob_sprit(640,480,8,OSTATNI_LOCK)) == NULL) {
   tiskni_chybu("Nedostatek pameti ...",__LINE__);
 }


 nastav_paletu(PALETA_LOGO2);

 memset(zumpa,0,4001);
 nahraj_soubor_lep(0,zumpa,DAT_FILE,"credit.txb");
// nahraj_soubor(4000,zumpa,"credit.txb",0);

 spr_vypln_ctverec(0,0,0,640,480,p_scr);

 i = 0; t = 0;
 while(zumpa[i] != 0) {
   if(zumpa[i] == 0xd) {
     zumpa[i] = 0;
     i += 2;
     p_text[t++] = zumpa+i;
     continue;
   }
   else
     i++;
 }

 for(i = 0; i < t; i++) {
    if(p_text[i][0] == 0) {
       p_text[i] = 0;
       p_sp[i] = 0;
    }
    else {
      if((p_sp[i] = vyrob_sprit(dlfn(p_text[i],fn1)+20,20,8,OSTATNI_LOCK)) == NULL)
        tiskni_chybu("Nedostatek pameti...",__LINE__);
      spr_vypln_ctverec(0,0,0,dlfn(p_text[i],fn1)+20,20,p_sp[i]);
      buf_printfn(10,0,fn1,p_text[i],p_sp[i]);
      pozice[i] = XRES/2-x_sprit(p_sp[i])/2;
    }
 }
 clrkb();
 v = 0;
 krok = kresli;
 for(j = 0/*DELKA_ROLOVANI-1*/; j < DELKA_ROLOVANI; j++) {

    if((getkba() == K_ESC)||(getkba() == K_ENTER))
      goto pokr;
/*
    spr_vypln_ctverec(0,0,v+20,10,172,p_scr);
    spr_vypln_ctverec(0,628,v+20,12,172,p_scr);
*/
//    spr_vypln_ctverec(0,0,v,640,20,p_scr);
//    spr_vypln_ctverec(0,0,v+20+172,640,20,p_scr);


    for(i = 0; i < t; i++) {
       if(p_sp[i] != 0) {
         q = (i*20)-j;
         if((q < 460)&&(q > 130)) {
           buf_kresli_sprit(p_sp[i],pozice[i],q,p_scr);
         }
       }
    }
    buf_kresli_sprit_bar(spr[PRVNI_MENU+54],10,v,0,p_scr);
    spr_vypln_ctverec(0,0,460,640,20,p_scr);

    krok+=1;
    while(krok > kresli);

    kresli_sprit(p_scr,0,0);
 }

 cnr();

 pokr:
 for(i = 0; i < t; i++) {
    if(p_sp[i] != 0)
      zrus_sprit(p_sp[i]);

 }

 zrus_sprit(p_scr);

 uvolni_ber_logo();

 if(!ve_hre)
   hraj_hudbu_menu();
 spust_oci();
 navrat();
}

/*--------------------------------------------------------------------------*/
/* konecne funkce */
/*--------------------------------------------------------------------------*/
/* konec levelu */
#define odchod()                            \
{                                           \
  vypni_kurzor(mysi_info);                  \
  smaz_paletu();                            \
  vypln_ctverec(0,0,0,_a_xres,_a_yres);     \
}                                           \

int uvod_do_levelu(void)
{
  byte pom[100] = "obt$i^znost: ";
  byte *p_text[100];
  byte buf[4000];
  int  i,j,ay;
  int  level;
  dword limity[10] = {4*2,4*2,3*2,2*2,2*2,800};
//  dword limity[5] = {800,600,400,200,0};
/*
#define _HACEK    '^'
#define _CARKA    '$'
#define _KROUZEK  '#'
*/

  smaz_paletu();
  nahraj_menu_poz();

  kresli_sprit(spr[PRVNI_MENU+56],0,0);

  load_limit = limity[levelset[0][0]];

  switch(levelset[0][0]) {
      case 1: strcat(pom,"Tr$enink"); break;
      case 2: strcat(pom,"Lehk$a"); break;
      case 3: strcat(pom,"st^redn$i"); break;
      case 4: strcat(pom,"T^e^zk$a"); break;
      case 5: strcat(pom,"Nemo^zn$a"); break;
  }
  printfn(350,130,fn1,pom);
  level = levelset[0][1];
  sprintf(pom,"$Urove^n ^c. %d",(level/2)+1);
  printfn(230,100,fn1,pom);

  sprintf(pom,"Heslo: %s",levelset[level+1]);
  printfn(30,130,fn1,pom);

  printfn(50,450,fn1,"Hlavn$i menu");
  printfn(400,450,fn1,"start levelu");

  sprintf(pom,"~%d_%d",levelset[0][0],(level/2)+1);

  p_text[0] = strstr(spr[TEXT],pom);
  p_text[0] = strchr(p_text[0],0xd)+2;
  p_text[1] = strchr(p_text[0],'~');
  *p_text[1] = NULL;
  strcpy(buf,p_text[0]);
  *p_text[1] = '~';
  p_text[0] = buf;

  i = 0;
  while((p_text[i+1] = strchr(p_text[i],0xD)) != NULL) {
    *p_text[++i] = NULL;
    p_text[i] += 2;
  }
  p_text[i] = NULL;

  ay = 160;
  for(j = 0; j < i; j++) {
    printfn(20,ay,fn1,p_text[j]);
    ay += 23;
  }

  uvolni_menu_poz();
  
  nastav_paletu(PALETA_MNU);
  zapni_kurzor(mysi_info);

  while(1) {
    clrkb();
    mysi_info[0] = 0;
    while((mysi_info[0] == 0)&&((getkba() == 0)||(getkba() > 0x80)));
   
    if(getkba() == K_ESC) {
    // Skoc do hlavniho menu
      odchod();
      return(0);
    }
    if(getkba() == K_ENTER) {
    // Vstup do tohoto levelu
      odchod();
      return(1);
    }
    if((mysi_info[0] != 0)&&(mysi_info[2] > 450)) {
      if(mysi_info[1] < 320) {
        // konec -> skok do menu
        odchod();
        return(0);
      }
      else { // Pokracuj v dalsim levelu
        odchod();
        return(1);
      }
    }
  }
}

void vyhodnoceni_levelu(void)
{

}

void roluj_text(byte *p_file,int od_y, int do_y, int font, int kroku, int paleta)
{
 #define MAX_DELKA_TEXTU 10000
 #define MAX_RADKU       500

 byte *p_text[MAX_RADKU];
 byte zumpa[MAX_DELKA_TEXTU];
 byte *p_sp[MAX_RADKU];
 byte *p_scr;
 int  pozice[MAX_RADKU];
 int  krok;
 int  i,t,j,q,vyska;

 vyska = do_y-od_y;

 if((p_scr = vyrob_sprit(640,vyska,8,OSTATNI_LOCK)) == NULL) {
   tiskni_chybu("Nedostatek pameti ...",__LINE__);
 }

 memset(zumpa,0,MAX_DELKA_TEXTU);
 nahraj_soubor_lep(0,zumpa,DAT_FILE,p_file);
// nahraj_soubor(MAX_DELKA_TEXTU,zumpa,p_file,0);

 spr_vypln_ctverec(0,0,0,640,vyska,p_scr);

 i = 0; t = 0;
 while(zumpa[i] != 0) {
   if(zumpa[i] == 0xd) {
     zumpa[i] = 0;
     i += 2;
     p_text[t++] = zumpa+i;
     continue;
   }
   else
     i++;
 }

 for(i = 0; i < t; i++) {
    if(p_text[i][0] == 0) {
       p_text[i] = 0;
       p_sp[i] = 0;
    }
    else {
      if((p_sp[i] = vyrob_sprit(dlfn(p_text[i],font)+20,20,8,OSTATNI_LOCK)) == NULL)
        tiskni_chybu("Nedostatek pameti...",__LINE__);
      spr_vypln_ctverec(0,0,0,dlfn(p_text[i],font)+20,20,p_sp[i]);
      buf_printfn(10,0,font,p_text[i],p_sp[i]);
      pozice[i] = XRES/2-x_sprit(p_sp[i])/2;
    }
 }


 clrkb();
 krok = kresli;
 nastav_paletu(paleta);

 for(j = 0; j < kroku ; j++) {

    if((getkba() == K_ESC)||(getkba() == K_ENTER))
      goto pokr;

//    spr_vypln_ctverec(0,0,v+20,10,172,p_scr);
//    spr_vypln_ctverec(0,628,v+20,12,172,p_scr);
//    spr_vypln_ctverec(0,0,v+20+172,640,20,p_scr);


    spr_vypln_ctverec(0,0,0,640,vyska,p_scr);
    for(i = 0; i < t; i++) {
       if(p_sp[i] != 0) {
         q = (i*20)-j;
          if((q < vyska-20)&&(q > 0)) {
            buf_kresli_sprit(p_sp[i],pozice[i],q,p_scr);
         }
       }
    }

    spr_vypln_ctverec(0,0,0,640,20,p_scr);
    spr_vypln_ctverec(0,0,vyska-20,640,20,p_scr);

    krok+=3;
    while(krok > kresli);

    cekej_na_paprsek();
    kresli_sprit(p_scr,0,od_y);
 }

 cnr();

 pokr:
 for(i = 0; i < t; i++) {
    if(p_sp[i] != 0) {
      zrus_sprit(p_sp[i]);
    }
 }
 zrus_sprit(p_scr);
}

void roluj_text_s_obr(byte *p_file,int od_y, int do_y, int font, int kroku, byte *p_obr, int paleta)
{
 #define MAX_DELKA_TEXTU 10000
 #define MAX_RADKU       500

 byte *p_text[MAX_RADKU];
 byte zumpa[MAX_DELKA_TEXTU];
 byte *p_sp[MAX_RADKU];
 byte *p_scr;
 int  pozice[MAX_RADKU];
 int  krok;
 int  i,t,j,q,vyska;

 vyska = do_y-od_y;

 if((p_scr = vyrob_sprit(640,vyska,8,OSTATNI_LOCK)) == NULL) {
   tiskni_chybu("Nedostatek pameti ...",__LINE__);
 }

 memset(zumpa,0,MAX_DELKA_TEXTU);
 nahraj_soubor_lep(0,zumpa,DAT_FILE,p_file);
// nahraj_soubor(MAX_DELKA_TEXTU,zumpa,p_file,0);

 i = 0; t = 0;
 while(zumpa[i] != 0) {
   if(zumpa[i] == 0xd) {
     zumpa[i] = 0;
     i += 2;
     p_text[t++] = zumpa+i;
     continue;
   }
   else
     i++;
 }

 for(i = 0; i < t; i++) {
    if(p_text[i][0] == 0) {
       p_text[i] = 0;
       p_sp[i] = 0;
    }
    else {
      if((p_sp[i] = vyrob_sprit(dlfn(p_text[i],font)+20,20,8,OSTATNI_LOCK)) == NULL)
        tiskni_chybu("Nedostatek pameti...",__LINE__);
      spr_vypln_ctverec(0,0,0,dlfn(p_text[i],font)+20,20,p_sp[i]);
      buf_printfn(10,0,font,p_text[i],p_sp[i]);
      pozice[i] = XRES/2-x_sprit(p_sp[i])/2;
    }
 }


 clrkb();
 krok = kresli;
 nastav_paletu(paleta);

 for(j = 0; j < kroku ; j++) {

    if((getkba() == K_ESC)||(getkba() == K_ENTER))
      goto pokr;

    buf_kresli_sprit(p_obr,0,0,p_scr);
    for(i = 0; i < t; i++) {
       if(p_sp[i] != 0) {
         q = (i*20)-j;
          if((q < vyska-20)&&(q > 0)) {
            buf_kresli_sprit_bar(p_sp[i],pozice[i],q,0,p_scr);
         }
       }
    }

    spr_vypln_ctverec(0,0,0,640,20,p_scr);
    spr_vypln_ctverec(0,0,vyska-20,640,20,p_scr);

    krok+=4;
    while(krok > kresli);

    cekej_na_paprsek();
    kresli_sprit(p_scr,0,od_y);
 }

 cnr();

 pokr:
 for(i = 0; i < t; i++) {
    if(p_sp[i] != 0) {
      zrus_sprit(p_sp[i]);
    }
 }
 zrus_sprit(p_scr);
}

#define HUDBA_OUTRO_1  27
#define HUDBA_OUTRO_2  28
#define HUDBA_OUTRO_3  27
#define HUDBA_OUTRO_4  28
#define HUDBA_OUTRO_5  29


void epizoda_1(void)
{
 smaz_paletu();
 hraj_hudbu_outro(HUDBA_OUTRO_1);
 spr[END_SPRIT] = load_sprit_lep(DAT_FILE,"end_003.spr",OSTATNI_LOCK);
 kresli_sprit(spr[END_SPRIT],0,0);
 roluj_text_s_obr("epizoda1.txb",0,480,fn1,1350,spr[END_SPRIT],PALETA_MNU);
 zrus_sprit(spr[END_SPRIT]);
}

void epizoda_2(void)
{
 smaz_paletu();
 hraj_hudbu_outro(HUDBA_OUTRO_2);
 spr[END_SPRIT] = load_sprit_lep(DAT_FILE,"end_001.spr",OSTATNI_LOCK);
 kresli_sprit(spr[END_SPRIT],0,0);
 roluj_text_s_obr("epizoda2.txb",0,480,fn1,160,spr[END_SPRIT],PALETA_MNU);
 zrus_sprit(spr[END_SPRIT]);
}

void epizoda_3(void)
{
 smaz_paletu();
 hraj_hudbu_outro(HUDBA_OUTRO_3);
 spr[END_SPRIT] = load_sprit_lep(DAT_FILE,"end_002.spr",OSTATNI_LOCK);
 kresli_sprit(spr[END_SPRIT],0,0);
 roluj_text_s_obr("epizoda3.txb",0,480,fn1,180,spr[END_SPRIT],PALETA_MNU);
 zrus_sprit(spr[END_SPRIT]);
}

void epizoda_4(void)
{
 smaz_paletu();
 hraj_hudbu_outro(HUDBA_OUTRO_4);
 spr[END_SPRIT] = load_sprit_lep(DAT_FILE,"end_000.spr",OSTATNI_LOCK);
 kresli_sprit(spr[END_SPRIT],0,0);
 roluj_text_s_obr("epizoda4.txb",0,480,fn3,1800,spr[END_SPRIT],PALETA_END);
 zrus_sprit(spr[END_SPRIT]);
}

void epizoda_5(void)
{
 vf(0);
 hraj_hudbu_outro(HUDBA_OUTRO_5);
 roluj_text("epizoda5.txb",0,480,fn1,1800,PALETA_MNU);
}

/* konec cele epizody (dohrani epizody) */
void konec_epizody(int epizoda)
{
   switch(epizoda) {

     case 1:
       epizoda_1();
       break;

     case 2:
       epizoda_2();
       break;

     case 3:
       epizoda_3();
       break;

     case 4:
       epizoda_4();
       break;

     case 5:
       epizoda_5();
       break;

     default:
       break;
   }
}


/* --------------------------------------------------------------------------*/
/* pomocne a ostatni dulezite fce */
/* --------------------------------------------------------------------------*/
/* Blue Death */
void blue_death(void)
{
//   byte *p_b;

   prepni_do_textu(0x3);
   exit(0);

   blue_death_on = 0;
   return;
/*
   vypni_animace();

   if((p_b = vyrob_sprit(_a_xres,_a_yres,8)) == NULL) {
     tiskni_chybu("Nedostatek pameti !",__LINE__);
   }

   uloz_sprit(p_b,0,0);

   blue_death_on = 0;

   vypni_klavesnici();
   prepni_do_textu(0x3);
   textbackground(1);
   textcolor(15);
   window(1,1,80,25);
   clrscr();
   textbackground(7);
   textcolor(1);
   gotoxy(40-(28/2),9);
   cputs(" System neni zaneprazdnen ! ");

   textbackground(1);
   textcolor(15);
   gotoxy(6,11);
   cputs("System neni zaneprazdnen ani neni v nestabilnim stavu. Nemusite");
   gotoxy(6,12);
   cputs("cekat, dokud zacne reagovat. Zvolte si z nasledujicich");
   gotoxy(6,13);
   cputs("moznosti:");
   gotoxy(6,15);
   cputs("*  Stisk libovolne klavesy znamena navrat do Berusek.");
   gotoxy(6,16);
   cputs("*  Stisk CTRL+ALT+DEL restartuje pocitac. Vsechna neulozena data");
   gotoxy(6,17);
   cputs("   budou ztracena.");
   gotoxy(40-(31/2),19);
   cputs("  Pokracujte libovolnou klavesou  ");

   while(!kbhit());
   getch();

   zapni_klavesnici();
   prepni_do_grafiky(GRAF_MOD,LFB);
   kresli_sprit(p_b,0,0);
   nastav_paletu(PALETA);

   zrus_sprit(p_b);

   aktivuj_animace();
*/
}

void nahraj_sadu_spritu(char *p_file, int prvni_sprit, int prvni_file, int pocet, int lock)
{
   int i;
   byte file[50];

   for(i = prvni_file; i < prvni_file+pocet; i++) {
     sprintf(file,p_file,i);
     spr[prvni_sprit++] = load_sprit_lep(DAT_FILE,file,lock);

     if(spr[prvni_sprit-1] == NULL) fprintf(stderr,"\nnemuzu nacist sprit %s ...",file);
   }
}

void nahraj_sadu_spritu_komp(char *p_file, int prvni_sprit, int prvni_file, int pocet, int lock)
{
   int  i;
   byte file[50];
   int  velikost = 0;
   int  prvni = 0;

   prvni = prvni_sprit++;

   for(i = prvni_file; i < prvni_file+pocet; i++) {
     sprintf(file,p_file,i);
     velikost += (dword) (spr[prvni_sprit++] = (byte *) velikost_lepfile(DAT_FILE,file));
   }

   if((spr[prvni_sprit = prvni] = malloc(velikost+10)) == NULL) {
     printf("\n\nNedostatek pameti !\n");
     exit(CHYBA);
   }

   if(lock)
     _go32_dpmi_lock_data(spr[prvni_sprit],velikost+10);

   for(i = prvni_file; i < prvni_file+pocet; i++) {
     sprintf(file,p_file,i);
     nahraj_soubor_lep(0,spr[prvni_sprit++],DAT_FILE,file);

#ifdef __AMIGA__
     SPRIT_HEAD *p_spr;

     p_spr = (SPRIT_HEAD *)spr[prvni_sprit-1];

     p_spr->x = SWAP32LE(p_spr->x);
     p_spr->y = SWAP32LE(p_spr->y);
     p_spr->barev = SWAP32LE(p_spr->barev);
#endif

     velikost = (dword) spr[prvni_sprit];
     velikost += (dword) spr[prvni_sprit-1];
     spr[prvni_sprit] = (byte *) velikost;

     if(spr[prvni_sprit-1] == NULL) fprintf(stderr,"\nnemuzu nacist sprit %s ...",file);
   }
}

void zobraz_logo_anak(void)
{
 nahraj_sadu_spritu("menu_%.3d.spr",PRVNI_MENU+55,55,1,OSTATNI_LOCK);
 nastav_paletu(PALETA_LOGO);
 kresli_sprit(spr[PRVNI_MENU+55],320-x_sprit(spr[PRVNI_MENU+55])/2,
                                 240-y_sprit(spr[PRVNI_MENU+55])/2);
 zrus_sprit(spr[PRVNI_MENU+55]);
}

void zobraz_logo_uziv(void)
{
 byte text1[] = "Logick$a hra Beru^sky";
 byte text2[] = "V roce 1999 vyrobil AnakreoN";

 nahraj_sadu_spritu("menu_%.3d.spr",PRVNI_MENU+54,54,1,OSTATNI_LOCK);
 nastav_paletu(PALETA_LOGO2);
 kresli_sprit(spr[PRVNI_MENU+54],10,104);
 printfn(320-dlfn(text1,fn1)/2,290,fn1,text1);
 printfn(320-dlfn(text2,fn1)/2,310,fn1,text2);
 zrus_sprit(spr[PRVNI_MENU+54]);
}

int roluj_textik(byte *p_text, int x, int y, int centr, int maz, int klikej)
{
 int diakr = 0,i;

 mysi_info[0] = 0;

 if(centr)
   x = 320-dlfn(p_text,fn1)/2;
 
 putfn(x,y,fn1,'_');
 for(i = 0; i < strlen(p_text); i++) {
    if((p_text[i] == _HACEK)||(p_text[i] == _CARKA)||(p_text[i] == _KROUZEK)) {
       diakr = p_text[i];
       continue;
    }
    vypln_ctverec(0,x,y,20,20);
    if(diakr) {
      putfn(x+(szn(fn1,p_text[i])/2)-3,y,fn1,diakr);
      diakr = 0;
    }
/*
#define MENU_SKOK       7
#define MENU_KLIK       8
*/
    if(klikej) {
      if(random()%2)
        play_sampl(MENU_KLIK,5);
      else
        play_sampl(MENU_SKOK,5);
    }

    x+=putfn(x,y,fn1,p_text[i]);
    putfn(x,y,fn1,'_');
    cekej(2);
    if(getkba()||mysi_info[0])
      return(1);
 }
 if(maz)
   vypln_ctverec(0,x,y,20,20);

 return(0);
}

void uvodni_uvitani(void)
{
 #define KR_DELAY 1
 #define KLIK     1
 #define NE_KLIK  0
 

 byte text_1[100] = "uv$ad$i";
 byte text_2[100] = "logickou hru";
 byte text_3[100] = "Beru^sky";
 byte text_5[100] = "Chr$an^eno GNU licenc$i";
 byte text_6[100] = "info v souboru copying";
 byte text_7[100] = "(C) Anakreon 1999";
 int i;
 byte *p_lg;

 mysi_info[0] = 0;

 if(cekej_s_kb(18*1)) {
   vf(0);
   return;
 }

 if((p_lg = vyrob_sprit(618,172,8,OSTATNI_LOCK)) == NULL)
   tiskni_chybu("Nedostatek pameti!",__LINE__);

 nahraj_sadu_spritu("menu_%.3d.spr",PRVNI_MENU+79,79,1,OSTATNI_LOCK);
 nahraj_sadu_spritu("menu_%.3d.spr",PRVNI_MENU+78,78,1,OSTATNI_LOCK);

 stmav_paletu(PALETA_LOGO);
 vf(0);
 nastav_paletu(PALETA_MNU);

 if(roluj_textik(text_1,0,180,1,1,KLIK))
   goto konec_proc;

 if(cekej_s_kb(10))
   goto konec_proc;

 if(roluj_textik(text_2,0,210,1,1,KLIK))
   goto konec_proc;

#ifdef __AMIGA__
 kresli_sprit(spr[PRVNI_MENU+79],10,120);
#else
 for(i = 0; i < 618/2; i+=10) {
    buf_kresli_sprit(spr[PRVNI_MENU+79],0,0,p_lg);
    spr_vypln_ctverec(BILA,0,0,618,84,p_lg);
    spr_vypln_ctverec(BILA,0,87,618,84,p_lg);

    spr_vypln_ctverec(BILA,0,84,309-i,4,p_lg);
    spr_vypln_ctverec(BILA,309+i,84,309-i,4,p_lg);
    cekej_s_kb(KR_DELAY);
    cekej_na_paprsek();
    if(getkba()||mysi_info[0])
      goto konec_proc;
    kresli_sprit_bar(p_lg,10,120,BILA);
 }

 for(i = 0; i < 84; i+=5) {
    buf_kresli_sprit(spr[PRVNI_MENU+79],0,0,p_lg);
    spr_vypln_ctverec(BILA,0,0,618,84-i,p_lg);
    spr_vypln_ctverec(BILA,0,87+i,618,84-i,p_lg);

    cekej_s_kb(KR_DELAY);
    cekej_na_paprsek();
    if(getkba()||mysi_info[0])
      goto konec_proc;
    kresli_sprit_bar(p_lg,10,120,BILA);
 }
#endif
 if(cekej_s_kb(10))
   goto konec_proc;

 if(roluj_textik(text_3,0,300,1,1,KLIK))
   goto konec_proc;

 if(cekej_s_kb(20))
   goto konec_proc;
 
 zrus_sprit(p_lg);
 if((p_lg = vyrob_sprit(640,480,8,OSTATNI_LOCK)) == NULL)
   tiskni_chybu("Nedostatek pameti !",__LINE__);
 
#ifdef __AMIGA__
 kresli_sprit(spr[PRVNI_MENU+78],0,0);
#else
 for(i = 0; i < 618/2; i+=10) {
    buf_kresli_sprit(spr[PRVNI_MENU+78],0,0,p_lg);
    spr_vypln_ctverec(BILA,0,0,618,240,p_lg);
    spr_vypln_ctverec(BILA,0,244,618,200,p_lg);

    spr_vypln_ctverec(BILA,0,240,309-i,4,p_lg);
    spr_vypln_ctverec(BILA,309+i,240,309-i,4,p_lg);
    cekej_s_kb(KR_DELAY);
    cekej_na_paprsek();
    if(getkba()||mysi_info[0])
      goto konec_proc;
    kresli_sprit_bar(p_lg,0,0,BILA);
 }
 
 for(i = 0; i < 200; i+=10) {
    buf_kresli_sprit(spr[PRVNI_MENU+78],0,0,p_lg);
    spr_vypln_ctverec(BILA,0,0,618,240-i,p_lg);
    spr_vypln_ctverec(BILA,0,244+i,618,244-i,p_lg);

    cekej_s_kb(KR_DELAY);
    cekej_na_paprsek();
    if(getkba()||mysi_info[0])
      goto konec_proc;
    kresli_sprit_bar(p_lg,0,0,BILA);
 }
#endif
 if(roluj_textik(text_5,0,20,0,1,KLIK))
   goto konec_proc;
 if(roluj_textik(text_6,0,40,0,1,KLIK))
   goto konec_proc;
 if(roluj_textik(text_7,360,440,0,1,KLIK))
   goto konec_proc;

 if(cekej_s_kb(30))
    goto konec_proc;

 for(i = 0; i < 260; i+=10) {
    cekej_s_kb(KR_DELAY);
    cekej_na_paprsek();
    vypln_ctverec(0,0,i,640,10);
    vypln_ctverec(0,0,480-i,640,10);
    if(getkba()||mysi_info[0])
      goto konec_proc;
 }

konec_proc:
 vf(0);
 zrus_sprit(spr[PRVNI_MENU+78]);
 zrus_sprit(spr[PRVNI_MENU+79]);
 zrus_sprit(p_lg);
}


void nahraj_data(void)
{

 nahraj_sadu_spritu_komp("glob_%.3d.spr",PRVNI_GLOBAL_LEVEL,0,GLOBALNICH_SPRITU,LOCK);
 nahraj_sadu_spritu_komp("klas_%.3d.spr",PRVNI_KLASIK_LEVEL,0,KLASICKYCH_SPRITU,LOCK);
 nahraj_sadu_spritu_komp("kyb_%.3d.spr",PRVNI_KYBER_LEVEL,0,KYBER_SPRITU,LOCK);
 nahraj_sadu_spritu_komp("hrni_%.3d.spr",PRVNI_OSTATNI,0,HERNICH_SPRITU,LOCK);
 p_mys = spr[PRVNI_OSTATNI+3];

 nahraj_sadu_spritu("hrac_%.3d.spr",PRVNI_ANIMACE_HRACE,   101,HRACU_SPRITU,LOCK);
 nahraj_sadu_spritu("hrac_%.3d.spr",PRVNI_ANIMACE_HRACE+10,201,HRACU_SPRITU,LOCK);
 nahraj_sadu_spritu("hrac_%.3d.spr",PRVNI_ANIMACE_HRACE+20,301,HRACU_SPRITU,LOCK);
 nahraj_sadu_spritu("hrac_%.3d.spr",PRVNI_ANIMACE_HRACE+30,401,HRACU_SPRITU,LOCK);
 nahraj_sadu_spritu("hrac_%.3d.spr",PRVNI_ANIMACE_HRACE+40,501,HRACU_SPRITU,LOCK);

 spr[PRVNI_HRAC]   = spr[PRVNI_ANIMACE_HRACE];
 spr[PRVNI_HRAC+1] = spr[PRVNI_ANIMACE_HRACE+10];
 spr[PRVNI_HRAC+2] = spr[PRVNI_ANIMACE_HRACE+20];
 spr[PRVNI_HRAC+3] = spr[PRVNI_ANIMACE_HRACE+30];
 spr[PRVNI_HRAC+4] = spr[PRVNI_ANIMACE_HRACE+40];

 rotuj_sprit(spr[PRVNI_HRAC],  spr[PRVNI_HRAC+5] = vyrob_sprit(X,Y,8,LOCK),1);
 rotuj_sprit(spr[PRVNI_HRAC+1],spr[PRVNI_HRAC+6] = vyrob_sprit(X,Y,8,LOCK),1);
 rotuj_sprit(spr[PRVNI_HRAC+2],spr[PRVNI_HRAC+7] = vyrob_sprit(X,Y,8,LOCK),1);
 rotuj_sprit(spr[PRVNI_HRAC+3],spr[PRVNI_HRAC+8] = vyrob_sprit(X,Y,8,LOCK),1);
 rotuj_sprit(spr[PRVNI_HRAC+4],spr[PRVNI_HRAC+9] = vyrob_sprit(X,Y,8,LOCK),1);

 rotuj_sprit(spr[PRVNI_HRAC],  spr[PRVNI_HRAC+10] = vyrob_sprit(X,Y,8,LOCK),2);
 rotuj_sprit(spr[PRVNI_HRAC+1],spr[PRVNI_HRAC+11] = vyrob_sprit(X,Y,8,LOCK),2);
 rotuj_sprit(spr[PRVNI_HRAC+2],spr[PRVNI_HRAC+12] = vyrob_sprit(X,Y,8,LOCK),2);
 rotuj_sprit(spr[PRVNI_HRAC+3],spr[PRVNI_HRAC+13] = vyrob_sprit(X,Y,8,LOCK),2);
 rotuj_sprit(spr[PRVNI_HRAC+4],spr[PRVNI_HRAC+14] = vyrob_sprit(X,Y,8,LOCK),2);

 rotuj_sprit(spr[PRVNI_HRAC],  spr[PRVNI_HRAC+15] = vyrob_sprit(X,Y,8,LOCK),3);
 rotuj_sprit(spr[PRVNI_HRAC+1],spr[PRVNI_HRAC+16] = vyrob_sprit(X,Y,8,LOCK),3);
 rotuj_sprit(spr[PRVNI_HRAC+2],spr[PRVNI_HRAC+17] = vyrob_sprit(X,Y,8,LOCK),3);
 rotuj_sprit(spr[PRVNI_HRAC+3],spr[PRVNI_HRAC+18] = vyrob_sprit(X,Y,8,LOCK),3);
 rotuj_sprit(spr[PRVNI_HRAC+4],spr[PRVNI_HRAC+19] = vyrob_sprit(X,Y,8,LOCK),3);

 if((spr[TEXT] = malloc(DELKA_TEXTU)) == NULL) {
    tiskni_chybu("Malo pameti...",__LINE__);
 }

 nahraj_soubor_lep(0,spr[TEXT],DAT_FILE,"texty.txb");
// nahraj_soubor(60000,spr[TEXT],"texty.txb",0);

 napln_zavislosti();

 init_anim();

 if((pp_spr = vyrob_sprit(X,Y,8,LOCK)) == NULL)
   tiskni_chybu("Malo pameti...",__LINE__);

 if((pp_spr_2 = vyrob_sprit(X,Y,8,LOCK)) == NULL)
   tiskni_chybu("Malo pameti...",__LINE__);

 if((pp_anim = vyrob_sprit(X,Y,8,LOCK)) == NULL)
   tiskni_chybu("Malo pameti...",__LINE__);

 if((p_spd = vyrob_sprit(180,20,8,OSTATNI_LOCK)) == NULL)
   tiskni_chybu("Malo pameti...",__LINE__);

 if((p_pan = vyrob_sprit(65,40,8,OSTATNI_LOCK)) == NULL)
   tiskni_chybu("Malo pameti...",__LINE__);

 p[0].ls[0] = PRVNI_OSTATNI+48; p[0].ls[1] = PRVNI_OSTATNI+49;
 p[0].ls[2] = PRVNI_OSTATNI+52; p[0].ls[3] = PRVNI_OSTATNI+53;
 p[0].ps[0] = PRVNI_OSTATNI+50; p[0].ps[1] = PRVNI_OSTATNI+51;
 p[0].ps[2] = PRVNI_OSTATNI+54; p[0].ps[3] = PRVNI_OSTATNI+55;

 p[1].ls[0] = PRVNI_OSTATNI+16; p[1].ls[1] = PRVNI_OSTATNI+17;
 p[1].ls[2] = PRVNI_OSTATNI+44; p[1].ls[3] = PRVNI_OSTATNI+45;
 p[1].ps[0] = PRVNI_OSTATNI+24; p[1].ps[1] = PRVNI_OSTATNI+25;
 p[1].ps[2] = PRVNI_OSTATNI+46; p[1].ps[3] = PRVNI_OSTATNI+47;

 p[2].ls[0] = PRVNI_OSTATNI+22; p[2].ls[1] = PRVNI_OSTATNI+23;
 p[2].ls[2] = PRVNI_OSTATNI+20; p[2].ls[3] = PRVNI_OSTATNI+21;
 p[2].ps[0] = PRVNI_OSTATNI+18; p[2].ps[1] = PRVNI_OSTATNI+19;
 p[2].ps[2] = PRVNI_OSTATNI+26; p[2].ps[3] = PRVNI_OSTATNI+27;

 p[3].ls[0] = PRVNI_OSTATNI+36; p[3].ls[1] = PRVNI_OSTATNI+37;
 p[3].ls[2] = PRVNI_OSTATNI+40; p[3].ls[3] = PRVNI_OSTATNI+41;
 p[3].ps[0] = PRVNI_OSTATNI+38; p[3].ps[1] = PRVNI_OSTATNI+39;
 p[3].ps[2] = PRVNI_OSTATNI+42; p[3].ps[3] = PRVNI_OSTATNI+43;


 p[4].ls[0] = PRVNI_OSTATNI+28; p[4].ls[1] = PRVNI_OSTATNI+29;
 p[4].ls[2] = PRVNI_OSTATNI+32; p[4].ls[3] = PRVNI_OSTATNI+33;
 p[4].ps[0] = PRVNI_OSTATNI+30; p[4].ps[1] = PRVNI_OSTATNI+31;
 p[4].ps[2] = PRVNI_OSTATNI+34; p[4].ps[3] = PRVNI_OSTATNI+35;

}



void vygeneruj_zakladni_animace(void)
{
/* Vygeneruje zakladni animace */
/*
ANIM_FILE * vyrob_animaci(int framu, int loop, int wr, int buffer)
int registruj_animaci(ANIM_FILE *p_anim, byte *p_buffer, byte **p_sprt, volatile int *p_casovac, byte barva)
*/
/* Vyroba animaci hracu a dalsich prvku
*/

 anim[A_NAHORU] = registruj_animaci(vyrob_animaci(HR_FRAMU,0,0,1),X,2*Y,spr,&kresli_int, BILA);
 anim[A_DOLU] = registruj_animaci(vyrob_animaci(HR_FRAMU,0,0,1),X,2*Y,spr,&kresli_int, BILA);
 anim[A_DOPRAVA] = registruj_animaci(vyrob_animaci(HR_FRAMU,0,0,1),2*X,Y,spr,&kresli_int, BILA);
 anim[A_DOLEVA] = registruj_animaci(vyrob_animaci(HR_FRAMU,0,0,1),2*X,Y,spr,&kresli_int, BILA);

 anim[A_B_NAHORU] = registruj_animaci(vyrob_animaci(HRB_FRAMU,0,0,1),X,3*Y,spr,&kresli_int, BILA);
 anim[A_B_DOLU] = registruj_animaci(vyrob_animaci(HRB_FRAMU,0,0,1),X,3*Y,spr,&kresli_int, BILA);
 anim[A_B_DOPRAVA] = registruj_animaci(vyrob_animaci(HRB_FRAMU,0,0,1),3*X,Y,spr,&kresli_int, BILA);
 anim[A_B_DOLEVA] = registruj_animaci(vyrob_animaci(HRB_FRAMU,0,0,1),3*X,Y,spr,&kresli_int, BILA);

 anim[A_KAMEN] = registruj_animaci(vyrob_animaci(AD_KAMEN,0,0,1),X,Y,spr,&kresli_int, BILA);
 anim[A_BEDNA1] = registruj_animaci(vyrob_animaci(AD_BEDNA,0,0,1),X,Y,spr,&kresli_int, BILA);
 anim[A_BEDNA2] = registruj_animaci(vyrob_animaci(AD_BEDNA,0,0,1),X,Y,spr,&kresli_int, BILA);
 anim[A_BEDNA3] = registruj_animaci(vyrob_animaci(AD_BEDNA,0,0,1),X,Y,spr,&kresli_int, BILA);
 anim[A_BEDNA4] = registruj_animaci(vyrob_animaci(AD_BEDNA,0,0,1),X,Y,spr,&kresli_int, BILA);
 anim[A_BEDNA5] = registruj_animaci(vyrob_animaci(AD_BEDNA,0,0,1),X,Y,spr,&kresli_int, BILA);
 anim[A_BEDNA6] = registruj_animaci(vyrob_animaci(AD_BEDNA,0,0,1),X,Y,spr,&kresli_int, BILA);
 anim[A_BEDNA7] = registruj_animaci(vyrob_animaci(AD_BEDNA,0,0,1),X,Y,spr,&kresli_int, BILA);

}

void tik_30(void)
{
 if(!mezicas) {
   dos_time++;
   mezicas = mezi_back;
 }
 else
   mezicas--;

 kresli++;
 kresli_animace();
}

void getms(void)
{
 mysi_info[0] = 0;
 while(mysi_info[0] == 0);
}

void cekej_na_reakci(void)
{
 clrkb();
 mysi_info[0] = 0;
 while((mysi_info[0] == 0)&&((getkba() == 0)||(getkba() > 0x80)));
}

void cekej(int tiku)
{
 int ted;

 ted = dos_time;
 while(dos_time < ted+tiku);
}

void cekej_proc(int tiku,void (*p_fnc)())
{
 int ted;

 ted = dos_time;
 while(dos_time < ted+tiku)
   p_fnc();
}

int cekej_s_kb(int tiku)
{
 int ted;

 clrkb();
 mysi_info[0] = 0;

 ted = dos_time;
 while(dos_time < ted+tiku) {
   if(((getkba() != 0)&&(getkba() < 0x80))||(mysi_info[0] != 0))
     return(1);
 }
 return(0);
}

inline void ini_spr(void)
{
 int i;

 for(i = 0; i < MAX_SPR; i++)
    spr[i] = NULL;
}

void nastav_pameti(void)
{
 dword volne;
/*
 #define  LOW_MEM_MUSIC     8000
 #define  LOW_MEM_RUN       3000
*/
 #define  ZERE_PROGRAM      1241
 
 printf("Volne pameti %d KB\n",ZERE_PROGRAM+(volne = _go32_dpmi_remaining_physical_memory()/1000));
 printf("Pro data     %d KB\n\n",volne);

 if(volne < LOW_MEM_RUN) {
   printf("\nBerusky potrebuji ke svemu behu kolem %d KB volne pameti.\n\
Pokud je volne pameti mene, vypnete midas (v setupu nastavne No Sound\n\
a ve hre prilis nepouzivejte napovedu. Presto mohou Berusky pri kritickem\n\
nedostatku pameti spadnout.\n\n",(dword)(LOW_MEM_RUN+ZERE_PROGRAM));
   low_mem = 1;
   getch();
 }
 else {
   if(volne < LOW_MEM_MUSIC) {
     printf("\nMate mene nez %d KB volne pameti. Vyrazuji ze seznamu\n\
pametove narocnejsi skladby.\n\n",LOW_MEM_MUSIC+ZERE_PROGRAM);
     low_mem = 1;
   }
   else
     low_mem = 0;
 }
}
