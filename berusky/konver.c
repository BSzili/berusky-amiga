/*
         .���� �    � ����.     �    � �����.  ����� .����. �    �
        .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
        �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
      .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
     .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
     �.      � �    � �      .� �   .� �    .� ����� .����. �    �
     (c) 1998 AnakreoN Freeware

   Berusky (C) Komat za Anakreon fwp. 1997-1998-1999-?

   Soubor:     konver.c   
   Autor:      Komat za Anakreon
   Datum:      furt se opravuje
   Verze:      release 0.0
   Prekladac:  djgpp -> gcc + nasm
   Nakladac:   Zetor BS-564 Mk III C

*/
#include "berusky.h"

#define  HLAVICKA_DEMA "DEMOFILE K BERUSKAM V.1"

HERNI_OBJEKT   prv[HERNICH_PRVKU];
PODPRVEK       podprv[PODPRVKU];
/*
  int  druh;          // funkcni zarazeni prvku (dvere, podlaha, ...)
  int  variant;       // druhu jeho grafickych variant
  int  varianty[100]; // pole variant, kazdy prvek muze mit az 100 ruznych spritu
*/
/*
  Funkce pouze pro zavisle osoby
*/
void napln_zavislosti(void)
{
  int i;

// Definice futer -> podprvku

  podprv[PP_LEVY_FUTRO_O].druh = P_STENA;
  podprv[PP_LEVY_FUTRO_O].x_kor = -1;
  podprv[PP_LEVY_FUTRO_O].y_kor = 0;
  podprv[PP_LEVY_FUTRO_O].variant = 10;
  podprv[PP_LEVY_FUTRO_O].varianty[0] = 35;//PRVNI_KLASIK_LEVEL+61;
  podprv[PP_LEVY_FUTRO_O].varianty[1] = 50;//PRVNI_KLASIK_LEVEL+48;
  podprv[PP_LEVY_FUTRO_O].varianty[2] = 46;//PRVNI_KLASIK_LEVEL+75;
  podprv[PP_LEVY_FUTRO_O].varianty[3] = 66;// kyber level
  podprv[PP_LEVY_FUTRO_O].varianty[4] = 67;
  podprv[PP_LEVY_FUTRO_O].varianty[5] = 68;
  podprv[PP_LEVY_FUTRO_O].varianty[6] = 69;
  podprv[PP_LEVY_FUTRO_O].varianty[7] = 70;
  podprv[PP_LEVY_FUTRO_O].varianty[8] = 71;
  podprv[PP_LEVY_FUTRO_O].varianty[9] = 72;

  podprv[PP_LEVY_FUTRO_Z] = podprv[PP_LEVY_FUTRO_O];
  podprv[PP_LEVY_FUTRO_Z].variant = 7;
  podprv[PP_LEVY_FUTRO_Z].varianty[0] = 51;//PRVNI_KLASIK_LEVEL+50;
  podprv[PP_LEVY_FUTRO_Z].varianty[1] = 73;
  podprv[PP_LEVY_FUTRO_Z].varianty[2] = 74;
  podprv[PP_LEVY_FUTRO_Z].varianty[3] = 75;
  podprv[PP_LEVY_FUTRO_Z].varianty[4] = 76;
  podprv[PP_LEVY_FUTRO_Z].varianty[5] = 77;
  podprv[PP_LEVY_FUTRO_Z].varianty[6] = 78;

// Toto je HORNI futro
  podprv[PP_HORNI_FUTRO_O].druh = P_STENA;
  podprv[PP_HORNI_FUTRO_O].x_kor = 0;
  podprv[PP_HORNI_FUTRO_O].y_kor = -1;
  podprv[PP_HORNI_FUTRO_O].variant = 10;
  podprv[PP_HORNI_FUTRO_O].varianty[0] = 35;
  podprv[PP_HORNI_FUTRO_O].varianty[1] = 50;
  podprv[PP_HORNI_FUTRO_O].varianty[2] = 48;

  podprv[PP_HORNI_FUTRO_O].varianty[3] = 86; // kyber
  podprv[PP_HORNI_FUTRO_O].varianty[4] = 87;
  podprv[PP_HORNI_FUTRO_O].varianty[5] = 88;
  podprv[PP_HORNI_FUTRO_O].varianty[6] = 89;
  podprv[PP_HORNI_FUTRO_O].varianty[7] = 90;
  podprv[PP_HORNI_FUTRO_O].varianty[8] = 91;
  podprv[PP_HORNI_FUTRO_O].varianty[9] = 92;

  podprv[PP_HORNI_FUTRO_Z] = podprv[PP_HORNI_FUTRO_O];
  podprv[PP_HORNI_FUTRO_Z].variant = 7;
  podprv[PP_HORNI_FUTRO_Z].varianty[0] = 51;
  podprv[PP_HORNI_FUTRO_Z].varianty[1] = 93; // kyber
  podprv[PP_HORNI_FUTRO_Z].varianty[2] = 94;
  podprv[PP_HORNI_FUTRO_Z].varianty[3] = 95;
  podprv[PP_HORNI_FUTRO_Z].varianty[4] = 96;
  podprv[PP_HORNI_FUTRO_Z].varianty[5] = 97;
  podprv[PP_HORNI_FUTRO_Z].varianty[6] = 98;

/*
  *****************************************************************************
  Ted prijde na radu to tezsi, a sice definice futer ktery sou na opacny strane
  *****************************************************************************
*/

  podprv[PP_PRAVY_FUTRO_O].druh = P_STENA;
  podprv[PP_PRAVY_FUTRO_O].x_kor = 1;
  podprv[PP_PRAVY_FUTRO_O].y_kor = 0;
  podprv[PP_PRAVY_FUTRO_O].variant = 18;
  podprv[PP_PRAVY_FUTRO_O].varianty[0] = 36;
  podprv[PP_PRAVY_FUTRO_O].varianty[1] = 37;
  podprv[PP_PRAVY_FUTRO_O].varianty[2] = 38;
  podprv[PP_PRAVY_FUTRO_O].varianty[3] = 39;
  podprv[PP_PRAVY_FUTRO_O].varianty[4] = 40;
  podprv[PP_PRAVY_FUTRO_O].varianty[5] = 30;
  podprv[PP_PRAVY_FUTRO_O].varianty[6] = 31;
  podprv[PP_PRAVY_FUTRO_O].varianty[7] = 32;
  podprv[PP_PRAVY_FUTRO_O].varianty[8] = 33;
  podprv[PP_PRAVY_FUTRO_O].varianty[9] = 34;//PRVNI_KLASIK_LEVEL+63;
  podprv[PP_PRAVY_FUTRO_O].varianty[10] = 47;

  podprv[PP_PRAVY_FUTRO_O].varianty[11] = 79; // kyber level
  podprv[PP_PRAVY_FUTRO_O].varianty[12] = 80;
  podprv[PP_PRAVY_FUTRO_O].varianty[13] = 81;
  podprv[PP_PRAVY_FUTRO_O].varianty[14] = 82;
  podprv[PP_PRAVY_FUTRO_O].varianty[15] = 83;
  podprv[PP_PRAVY_FUTRO_O].varianty[16] = 84;
  podprv[PP_PRAVY_FUTRO_O].varianty[17] = 85;


  podprv[PP_PRAVY_FUTRO_Z].druh = P_STENA;
  podprv[PP_PRAVY_FUTRO_Z].x_kor = 1;
  podprv[PP_PRAVY_FUTRO_Z].y_kor = 0;
  podprv[PP_PRAVY_FUTRO_Z].variant = 1;
  podprv[PP_PRAVY_FUTRO_Z].varianty[0] = 49;//PRVNI_KLASIK_LEVEL+80;

/*
  Spodni futra
*/
  podprv[PP_DOLNI_FUTRO_O].druh = P_STENA;
  podprv[PP_DOLNI_FUTRO_O].x_kor = 0;
  podprv[PP_DOLNI_FUTRO_O].y_kor = 1;
  podprv[PP_DOLNI_FUTRO_O].variant = 18;
  podprv[PP_DOLNI_FUTRO_O].varianty[0] = 41;//PRVNI_KLASIK_LEVEL+70;
  podprv[PP_DOLNI_FUTRO_O].varianty[1] = 42;//PRVNI_KLASIK_LEVEL+71;
  podprv[PP_DOLNI_FUTRO_O].varianty[2] = 43;//PRVNI_KLASIK_LEVEL+72;
  podprv[PP_DOLNI_FUTRO_O].varianty[3] = 44;//PRVNI_KLASIK_LEVEL+73;
  podprv[PP_DOLNI_FUTRO_O].varianty[4] = 45;//PRVNI_KLASIK_LEVEL+74;
  podprv[PP_DOLNI_FUTRO_O].varianty[5] = 30;//PRVNI_KLASIK_LEVEL+52;
  podprv[PP_DOLNI_FUTRO_O].varianty[6] = 31;//PRVNI_KLASIK_LEVEL+55;
  podprv[PP_DOLNI_FUTRO_O].varianty[7] = 32;//PRVNI_KLASIK_LEVEL+53;
  podprv[PP_DOLNI_FUTRO_O].varianty[8] = 33;//PRVNI_KLASIK_LEVEL+54;
  podprv[PP_DOLNI_FUTRO_O].varianty[9] = 34;//PRVNI_KLASIK_LEVEL+56;
  podprv[PP_DOLNI_FUTRO_O].varianty[10] = 47;

  podprv[PP_DOLNI_FUTRO_O].varianty[11] = 99; // Kyber level
  podprv[PP_DOLNI_FUTRO_O].varianty[12] = 100;
  podprv[PP_DOLNI_FUTRO_O].varianty[13] = 101;
  podprv[PP_DOLNI_FUTRO_O].varianty[14] = 102;
  podprv[PP_DOLNI_FUTRO_O].varianty[15] = 103;
  podprv[PP_DOLNI_FUTRO_O].varianty[16] = 104;
  podprv[PP_DOLNI_FUTRO_O].varianty[17] = 105;


  podprv[PP_DOLNI_FUTRO_Z].druh = P_STENA;
  podprv[PP_DOLNI_FUTRO_Z].x_kor = 0;
  podprv[PP_DOLNI_FUTRO_Z].y_kor = 1;
  podprv[PP_DOLNI_FUTRO_Z].variant = 1;
  podprv[PP_DOLNI_FUTRO_Z].varianty[0] = 49;//PRVNI_KLASIK_LEVEL+80;


  // Pokud varianty[cislo] = 0xffffffff -> tento prvek se nekresli,
  // je slepej tj. rovna se SLEPEJ_PRVEK

/*  Definice Zeme  */
  prv[P_ZEME].druh = P_ZEME;
  prv[P_ZEME].variant = 10;
  prv[P_ZEME].podprvku = 0;
  prv[P_ZEME].varianty[0] = PRVNI_KLASIK_LEVEL+81;
  prv[P_ZEME].varianty[1] = PRVNI_KLASIK_LEVEL+92;
  prv[P_ZEME].varianty[2] = PRVNI_KLASIK_LEVEL+83;
  prv[P_ZEME].varianty[3] = PRVNI_KLASIK_LEVEL+82;
  prv[P_ZEME].varianty[4] = PRVNI_KLASIK_LEVEL+84;
  // kyber level
  prv[P_ZEME].varianty[5] = PRVNI_KYBER_LEVEL+27;
  prv[P_ZEME].varianty[6] = PRVNI_KYBER_LEVEL;
  prv[P_ZEME].varianty[7] = PRVNI_KYBER_LEVEL+26;
  prv[P_ZEME].varianty[8] = PRVNI_KYBER_LEVEL+25;
  prv[P_ZEME].varianty[9] = PRVNI_KYBER_LEVEL+28;

  prv[P_HRAC_1].variant = 4;
  prv[P_HRAC_1].podprvku = 0;
  prv[P_HRAC_1].varianty[0] = PRVNI_HRAC;
  prv[P_HRAC_1].varianty[1] = PRVNI_HRAC+5;
  prv[P_HRAC_1].varianty[2] = PRVNI_HRAC+10;
  prv[P_HRAC_1].varianty[3] = PRVNI_HRAC+15;

  prv[P_HRAC_2].variant = 4;
  prv[P_HRAC_2].podprvku = 0;
  prv[P_HRAC_2].varianty[0] = PRVNI_HRAC+1;
  prv[P_HRAC_2].varianty[1] = PRVNI_HRAC+6;
  prv[P_HRAC_2].varianty[2] = PRVNI_HRAC+11;
  prv[P_HRAC_2].varianty[3] = PRVNI_HRAC+16;

  prv[P_HRAC_3].variant = 4;
  prv[P_HRAC_3].podprvku = 0;
  prv[P_HRAC_3].varianty[0] = PRVNI_HRAC+2;
  prv[P_HRAC_3].varianty[1] = PRVNI_HRAC+7;
  prv[P_HRAC_3].varianty[2] = PRVNI_HRAC+12;
  prv[P_HRAC_3].varianty[3] = PRVNI_HRAC+17;

  prv[P_HRAC_4].variant = 4;
  prv[P_HRAC_4].podprvku = 0;
  prv[P_HRAC_4].varianty[0] = PRVNI_HRAC+3;
  prv[P_HRAC_4].varianty[1] = PRVNI_HRAC+8;
  prv[P_HRAC_4].varianty[2] = PRVNI_HRAC+13;
  prv[P_HRAC_4].varianty[3] = PRVNI_HRAC+18;

  prv[P_HRAC_5].variant = 4;
  prv[P_HRAC_5].podprvku = 0;
  prv[P_HRAC_5].varianty[0] = PRVNI_HRAC+4;
  prv[P_HRAC_5].varianty[1] = PRVNI_HRAC+9;
  prv[P_HRAC_5].varianty[2] = PRVNI_HRAC+14;
  prv[P_HRAC_5].varianty[3] = PRVNI_HRAC+19;


/*  Definice Beden  */
  prv[P_BEDNA].druh = P_BEDNA;
  prv[P_BEDNA].variant = 8;
  prv[P_BEDNA].podprvku = 0;
  prv[P_BEDNA].varianty[0] = PRVNI_KLASIK_LEVEL+4;
  prv[P_BEDNA].varianty[1] = PRVNI_KLASIK_LEVEL+90;
  prv[P_BEDNA].varianty[2] = PRVNI_KLASIK_LEVEL+5;
  prv[P_BEDNA].varianty[3] = PRVNI_GLOBAL_LEVEL+2;
  prv[P_BEDNA].varianty[4] = PRVNI_KLASIK_LEVEL+6;
  prv[P_BEDNA].varianty[5] = PRVNI_KLASIK_LEVEL+7;
  prv[P_BEDNA].varianty[6] = PRVNI_KYBER_LEVEL+5;
  prv[P_BEDNA].varianty[7] = PRVNI_KYBER_LEVEL+6;

/*  Definice TNT Bedny  */
  prv[P_TNT].druh = P_TNT;
  prv[P_TNT].podprvku = 0;
  prv[P_TNT].variant = 5;
  prv[P_TNT].varianty[0] = PRVNI_GLOBAL_LEVEL+0;
  prv[P_TNT].varianty[1] = PRVNI_KLASIK_LEVEL+91;
  prv[P_TNT].varianty[2] = PRVNI_GLOBAL_LEVEL+3;
  prv[P_TNT].varianty[3] = PRVNI_GLOBAL_LEVEL+4;
  prv[P_TNT].varianty[4] = PRVNI_GLOBAL_LEVEL+1;

/*  Definice sten  */
  prv[P_STENA].druh = P_STENA;
  prv[P_STENA].podprvku = 0;
  prv[P_STENA].variant = 106;
  prv[P_STENA].varianty[0] = PRVNI_KLASIK_LEVEL+23;
  prv[P_STENA].varianty[1] = PRVNI_KLASIK_LEVEL+25;
  prv[P_STENA].varianty[2] = PRVNI_KLASIK_LEVEL+26;
  prv[P_STENA].varianty[3] = PRVNI_KLASIK_LEVEL+27;
  prv[P_STENA].varianty[4] = PRVNI_KLASIK_LEVEL+13;
  prv[P_STENA].varianty[5] = PRVNI_KLASIK_LEVEL+14;
  prv[P_STENA].varianty[6] = PRVNI_KLASIK_LEVEL+15;
  prv[P_STENA].varianty[7] = PRVNI_KLASIK_LEVEL+16;
  prv[P_STENA].varianty[8] = PRVNI_KLASIK_LEVEL+17;
  prv[P_STENA].varianty[9] = PRVNI_KLASIK_LEVEL+18;
  prv[P_STENA].varianty[10] = PRVNI_KLASIK_LEVEL+19;
  prv[P_STENA].varianty[11] = PRVNI_KLASIK_LEVEL+20;
  prv[P_STENA].varianty[12] = PRVNI_KLASIK_LEVEL+24;
  prv[P_STENA].varianty[13] = PRVNI_KLASIK_LEVEL+31;
  prv[P_STENA].varianty[14] = PRVNI_KLASIK_LEVEL+33;
  prv[P_STENA].varianty[15] = PRVNI_KLASIK_LEVEL+21;
  prv[P_STENA].varianty[16] = PRVNI_KLASIK_LEVEL+22;
  prv[P_STENA].varianty[17] = PRVNI_KLASIK_LEVEL+28;
  prv[P_STENA].varianty[18] = PRVNI_KLASIK_LEVEL+29;
  prv[P_STENA].varianty[19] = PRVNI_KLASIK_LEVEL+30;
  prv[P_STENA].varianty[20] = PRVNI_KLASIK_LEVEL+32;
  prv[P_STENA].varianty[21] = PRVNI_KLASIK_LEVEL+34;
  prv[P_STENA].varianty[22] = PRVNI_KLASIK_LEVEL+35;

  prv[P_STENA].varianty[23] = PRVNI_KLASIK_LEVEL+46;
  prv[P_STENA].varianty[24] = PRVNI_KLASIK_LEVEL+47;
  prv[P_STENA].varianty[25] = PRVNI_KLASIK_LEVEL+85;
  prv[P_STENA].varianty[26] = PRVNI_KLASIK_LEVEL+86;
  prv[P_STENA].varianty[27] = PRVNI_KLASIK_LEVEL+87;
  prv[P_STENA].varianty[28] = PRVNI_KLASIK_LEVEL+88;
  prv[P_STENA].varianty[29] = PRVNI_KLASIK_LEVEL+89;

// futra dveri a podobne blbosti
  prv[P_STENA].varianty[30] = PRVNI_KLASIK_LEVEL+52;
  prv[P_STENA].varianty[31] = PRVNI_KLASIK_LEVEL+55;
  prv[P_STENA].varianty[32] = PRVNI_KLASIK_LEVEL+53;
  prv[P_STENA].varianty[33] = PRVNI_KLASIK_LEVEL+54;
  prv[P_STENA].varianty[34] = PRVNI_KLASIK_LEVEL+56;

// futra dveri a podobne blbosti
  prv[P_STENA].varianty[35] = PRVNI_KLASIK_LEVEL+61;

  prv[P_STENA].varianty[36] = PRVNI_KLASIK_LEVEL+63;
  prv[P_STENA].varianty[37] = PRVNI_KLASIK_LEVEL+64;
  prv[P_STENA].varianty[38] = PRVNI_KLASIK_LEVEL+65;
  prv[P_STENA].varianty[39] = PRVNI_KLASIK_LEVEL+66;
  prv[P_STENA].varianty[40] = PRVNI_KLASIK_LEVEL+67;

  prv[P_STENA].varianty[41] = PRVNI_KLASIK_LEVEL+70;
  prv[P_STENA].varianty[42] = PRVNI_KLASIK_LEVEL+71;
  prv[P_STENA].varianty[43] = PRVNI_KLASIK_LEVEL+72;
  prv[P_STENA].varianty[44] = PRVNI_KLASIK_LEVEL+73;
  prv[P_STENA].varianty[45] = PRVNI_KLASIK_LEVEL+74;

  // posledni prvky
  prv[P_STENA].varianty[46] = PRVNI_KLASIK_LEVEL+75;
  prv[P_STENA].varianty[47] = PRVNI_KLASIK_LEVEL+77;
  prv[P_STENA].varianty[48] = PRVNI_KLASIK_LEVEL+78;
  prv[P_STENA].varianty[49] = PRVNI_KLASIK_LEVEL+80;

  prv[P_STENA].varianty[50] = PRVNI_KLASIK_LEVEL+48;
  prv[P_STENA].varianty[51] = PRVNI_KLASIK_LEVEL+50;

  // Kyber level
  prv[P_STENA].varianty[52] = PRVNI_KYBER_LEVEL+2;
  prv[P_STENA].varianty[53] = PRVNI_KYBER_LEVEL+3;
  prv[P_STENA].varianty[54] = PRVNI_KYBER_LEVEL+4;
  prv[P_STENA].varianty[55] = PRVNI_KYBER_LEVEL+14;
  prv[P_STENA].varianty[56] = PRVNI_KYBER_LEVEL+15;
  prv[P_STENA].varianty[57] = PRVNI_KYBER_LEVEL+16;
  prv[P_STENA].varianty[58] = PRVNI_KYBER_LEVEL+17;
  prv[P_STENA].varianty[59] = PRVNI_KYBER_LEVEL+18;
  prv[P_STENA].varianty[60] = PRVNI_KYBER_LEVEL+19;
  prv[P_STENA].varianty[61] = PRVNI_KYBER_LEVEL+20;
  prv[P_STENA].varianty[62] = PRVNI_KYBER_LEVEL+21;
  prv[P_STENA].varianty[63] = PRVNI_KYBER_LEVEL+22;
  prv[P_STENA].varianty[64] = PRVNI_KYBER_LEVEL+23;
  prv[P_STENA].varianty[65] = PRVNI_KYBER_LEVEL+24;

  /* Kyber futra */
  /* Leve otevrene futra */
  prv[P_STENA].varianty[66] = PRVNI_KYBER_LEVEL+37;
  prv[P_STENA].varianty[67] = PRVNI_KYBER_LEVEL+50;
  prv[P_STENA].varianty[68] = PRVNI_KYBER_LEVEL+81;
  prv[P_STENA].varianty[69] = PRVNI_KYBER_LEVEL+83;
  prv[P_STENA].varianty[70] = PRVNI_KYBER_LEVEL+85;
  prv[P_STENA].varianty[71] = PRVNI_KYBER_LEVEL+55;
  prv[P_STENA].varianty[72] = PRVNI_KYBER_LEVEL+69;

  /* Leve futra zavrene */
  prv[P_STENA].varianty[73] = PRVNI_KYBER_LEVEL+29;
  prv[P_STENA].varianty[74] = PRVNI_KYBER_LEVEL+33;
  prv[P_STENA].varianty[75] = PRVNI_KYBER_LEVEL+34;
  prv[P_STENA].varianty[76] = PRVNI_KYBER_LEVEL+35;
  prv[P_STENA].varianty[77] = PRVNI_KYBER_LEVEL+36;
  prv[P_STENA].varianty[78] = PRVNI_KYBER_LEVEL+72;

  /* prave futra otevrene */
  prv[P_STENA].varianty[79] = PRVNI_KYBER_LEVEL+32;
  prv[P_STENA].varianty[80] = PRVNI_KYBER_LEVEL+53;
  prv[P_STENA].varianty[81] = PRVNI_KYBER_LEVEL+82;
  prv[P_STENA].varianty[82] = PRVNI_KYBER_LEVEL+84;
  prv[P_STENA].varianty[83] = PRVNI_KYBER_LEVEL+54;
  prv[P_STENA].varianty[84] = PRVNI_KYBER_LEVEL+56;
  prv[P_STENA].varianty[85] = PRVNI_KYBER_LEVEL+71;

  /* prave futra zavrene */
  //-> jsou stejny jako otevreny

  /* Horni a dolni futra */
  /* Horni futra otevr */
  prv[P_STENA].varianty[86] = PRVNI_KYBER_LEVEL+45;
  prv[P_STENA].varianty[87] = PRVNI_KYBER_LEVEL+68;
  prv[P_STENA].varianty[88] = PRVNI_KYBER_LEVEL+61;
  prv[P_STENA].varianty[89] = PRVNI_KYBER_LEVEL+63;
  prv[P_STENA].varianty[90] = PRVNI_KYBER_LEVEL+65;
  prv[P_STENA].varianty[91] = PRVNI_KYBER_LEVEL+57;
  prv[P_STENA].varianty[92] = PRVNI_KYBER_LEVEL+75;

  /* Horni futra zavreny */
  prv[P_STENA].varianty[93] = PRVNI_KYBER_LEVEL+41;
  prv[P_STENA].varianty[94] = PRVNI_KYBER_LEVEL+38;
  prv[P_STENA].varianty[95] = PRVNI_KYBER_LEVEL+42;
  prv[P_STENA].varianty[96] = PRVNI_KYBER_LEVEL+43;
  prv[P_STENA].varianty[97] = PRVNI_KYBER_LEVEL+44;
  prv[P_STENA].varianty[98] = PRVNI_KYBER_LEVEL+78;

  /* dolni futra vsechny */
  prv[P_STENA].varianty[99] = PRVNI_KYBER_LEVEL+40;
  prv[P_STENA].varianty[100] = PRVNI_KYBER_LEVEL+66;
  prv[P_STENA].varianty[101] = PRVNI_KYBER_LEVEL+67;
  prv[P_STENA].varianty[102] = PRVNI_KYBER_LEVEL+64;
  prv[P_STENA].varianty[103] = PRVNI_KYBER_LEVEL+62;
  prv[P_STENA].varianty[104] = PRVNI_KYBER_LEVEL+59;
  prv[P_STENA].varianty[105] = PRVNI_KYBER_LEVEL+80;

  /*  Definice exitu  */
  prv[P_EXIT].druh = P_EXIT;
  prv[P_EXIT].podprvku = 0;
  prv[P_EXIT].variant = 9;
  for(i = 0; i < 7; i++)
     prv[P_EXIT].varianty[i] = PRVNI_KLASIK_LEVEL+36+i;
  prv[P_EXIT].varianty[7] = PRVNI_KYBER_LEVEL+13;
  prv[P_EXIT].varianty[8] = PRVNI_KYBER_LEVEL+12;

/*  Definice kamenu  */
  prv[P_KAMEN].druh = P_KAMEN;
  prv[P_KAMEN].podprvku = 0;
  prv[P_KAMEN].variant = 2;
  prv[P_KAMEN].varianty[0] = PRVNI_KLASIK_LEVEL+0;
  prv[P_KAMEN].varianty[1] = PRVNI_KYBER_LEVEL+46;

/*  Definice klicu  */
  prv[P_KLIC].druh = P_KLIC;
  prv[P_KLIC].podprvku = 0;
  prv[P_KLIC].variant = 1;
  prv[P_KLIC].varianty[0] = PRVNI_GLOBAL_LEVEL+5;
  
/*  Definice krumpacu  */
  prv[P_KRUMPAC].druh = P_KRUMPAC;
  prv[P_KRUMPAC].podprvku = 0;
  prv[P_KRUMPAC].variant = 2;
  prv[P_KRUMPAC].varianty[0] = PRVNI_GLOBAL_LEVEL+6;
  prv[P_KRUMPAC].varianty[1] = PRVNI_KYBER_LEVEL+1;

/* Definice barevnych individualnich klicu 1 */
  prv[P_KLIC1].druh = P_KLIC1;
  prv[P_KLIC1].podprvku = 0;
  prv[P_KLIC1].variant = 2;
  prv[P_KLIC1].varianty[0] = PRVNI_KLASIK_LEVEL+8;
  prv[P_KLIC1].varianty[1] = PRVNI_KYBER_LEVEL+8;

/* Definice barevnych individualnich klicu 2 */
  prv[P_KLIC2].druh = P_KLIC2;
  prv[P_KLIC2].podprvku = 0;
  prv[P_KLIC2].variant = 2;
  prv[P_KLIC2].varianty[0] = PRVNI_KLASIK_LEVEL+9;
  prv[P_KLIC2].varianty[1] = PRVNI_KYBER_LEVEL+7;

/* Definice barevnych individualnich klicu 3 */
  prv[P_KLIC3].druh = P_KLIC3;
  prv[P_KLIC3].podprvku = 0;
  prv[P_KLIC3].variant = 2;
  prv[P_KLIC3].varianty[0] = PRVNI_KLASIK_LEVEL+10;
  prv[P_KLIC3].varianty[1] = PRVNI_KYBER_LEVEL+9;

/* Definice barevnych individualnich klicu 4 */
  prv[P_KLIC4].druh = P_KLIC4;
  prv[P_KLIC4].podprvku = 0;
  prv[P_KLIC4].variant = 2;
  prv[P_KLIC4].varianty[0] = PRVNI_KLASIK_LEVEL+11;
  prv[P_KLIC4].varianty[1] = PRVNI_KYBER_LEVEL+10;

/* Definice barevnych individualnich klicu 5 */
  prv[P_KLIC5].druh = P_KLIC5;
  prv[P_KLIC5].podprvku = 0;
  prv[P_KLIC5].variant = 2;
  prv[P_KLIC5].varianty[0] = PRVNI_KLASIK_LEVEL+12;
  prv[P_KLIC5].varianty[1] = PRVNI_KYBER_LEVEL+11;

/* Definice dveri pro tyto barevne klice */
  prv[P_DVERE1_H_O].druh = P_DVERE1_H_O; // h -> horizontalni -> vodorovny
  prv[P_DVERE1_H_O].variant = 1;
  prv[P_DVERE1_H_O].varianty[0] = PRVNI_KLASIK_LEVEL+60;
  prv[P_DVERE1_H_O].podprvku = 2;
  prv[P_DVERE1_H_O].podprvky[0] = PP_HORNI_FUTRO_O;
  prv[P_DVERE1_H_O].pdp_vr[0] = 1;
  prv[P_DVERE1_H_O].podprvky[1] = PP_DOLNI_FUTRO_O;
  prv[P_DVERE1_H_O].pdp_vr[1] = 5;
  
  prv[P_DVERE2_H_O] = prv[P_DVERE1_H_O];
  prv[P_DVERE3_H_O] = prv[P_DVERE1_H_O];
  prv[P_DVERE4_H_O] = prv[P_DVERE1_H_O];
  prv[P_DVERE5_H_O] = prv[P_DVERE1_H_O];

/* Definice dveri pro hrace cislo 2 */

  prv[P_DVERE2_H_O].druh = P_DVERE2_H_O;
  prv[P_DVERE2_H_O].pdp_vr[1] = 6;

/* Definice dveri pro hrace cislo 3 */
  prv[P_DVERE3_H_O].druh = P_DVERE3_H_O;
  prv[P_DVERE3_H_O].pdp_vr[1] = 7;

/* Definice dveri pro hrace cislo 4 */
  prv[P_DVERE4_H_O].druh = P_DVERE4_H_O;
  prv[P_DVERE4_H_O].pdp_vr[1] = 8;

/* Definice dveri pro hrace cislo 5 */
  prv[P_DVERE5_H_O].druh = P_DVERE5_H_O;
  prv[P_DVERE5_H_O].pdp_vr[1] = 9;

/* Defince tech samych dveri akorat zavrenych */
  prv[P_DVERE1_H_Z].druh = P_DVERE1_H_Z; // h -> horizontalni -> vodorovny
  prv[P_DVERE1_H_Z].variant = 2;
  prv[P_DVERE1_H_Z].varianty[0] = PRVNI_KLASIK_LEVEL+58;
  prv[P_DVERE1_H_Z].varianty[1] = PRVNI_KYBER_LEVEL+39;
  prv[P_DVERE1_H_Z].podprvku = 2;
  prv[P_DVERE1_H_Z].podprvky[0] = PP_HORNI_FUTRO_Z;
  prv[P_DVERE1_H_Z].pdp_vr[0] = 0;
  prv[P_DVERE1_H_Z].podprvky[1] = PP_DOLNI_FUTRO_O;
  prv[P_DVERE1_H_Z].pdp_vr[1] = 5;
  prv[P_DVERE1_H_Z].podprvky[2] = PP_HORNI_FUTRO_Z;
  prv[P_DVERE1_H_Z].pdp_vr[2] = 1;
  prv[P_DVERE1_H_Z].podprvky[3] = PP_DOLNI_FUTRO_O;
  prv[P_DVERE1_H_Z].pdp_vr[3] = 11;

  prv[P_DVERE2_H_Z] = prv[P_DVERE1_H_Z];
  prv[P_DVERE3_H_Z] = prv[P_DVERE1_H_Z];
  prv[P_DVERE4_H_Z] = prv[P_DVERE1_H_Z];
  prv[P_DVERE5_H_Z] = prv[P_DVERE1_H_Z];
  
/* Zmeny po urcitych prvcich */
  prv[P_DVERE2_H_Z].druh = P_DVERE2_H_Z;
  prv[P_DVERE2_H_Z].pdp_vr[1] = 6;
  prv[P_DVERE2_H_Z].pdp_vr[2] = 2;

/* Definice dveri pro hrace cislo 3 */
  prv[P_DVERE3_H_Z].druh = P_DVERE3_H_Z;
  prv[P_DVERE3_H_Z].pdp_vr[1] = 7;
  prv[P_DVERE3_H_Z].pdp_vr[2] = 3;

/* Definice dveri pro hrace cislo 4 */
  prv[P_DVERE4_H_Z].druh = P_DVERE4_H_Z;
  prv[P_DVERE4_H_Z].pdp_vr[1] = 8;
  prv[P_DVERE4_H_Z].pdp_vr[2] = 4;

/* Definice dveri pro hrace cislo 5 */
  prv[P_DVERE5_H_Z].druh = P_DVERE5_H_Z;
  prv[P_DVERE5_H_Z].pdp_vr[1] = 9;
  prv[P_DVERE5_H_Z].pdp_vr[2] = 5;


/* Definice dveri pro tyto barevne klice */
/* Toto jsou vertikalni klice            */
  prv[P_DVERE1_V_O].druh = P_DVERE1_V_O; // h -> horizontalni -> vodorovny
  prv[P_DVERE1_V_O].variant = 1;
  prv[P_DVERE1_V_O].varianty[0] = PRVNI_KLASIK_LEVEL+49;
  prv[P_DVERE1_V_O].podprvku = 2;
  prv[P_DVERE1_V_O].podprvky[0] = PP_LEVY_FUTRO_O;
  prv[P_DVERE1_V_O].pdp_vr[0] = 1;
  prv[P_DVERE1_V_O].podprvky[1] = PP_PRAVY_FUTRO_O;
  prv[P_DVERE1_V_O].pdp_vr[1] = 5;
  
  prv[P_DVERE2_V_O] = prv[P_DVERE1_V_O];
  prv[P_DVERE3_V_O] = prv[P_DVERE1_V_O];
  prv[P_DVERE4_V_O] = prv[P_DVERE1_V_O];
  prv[P_DVERE5_V_O] = prv[P_DVERE1_V_O];

/* Definice dveri pro hrace cislo 2 */

  prv[P_DVERE2_V_O].druh = P_DVERE2_V_O;
  prv[P_DVERE2_V_O].pdp_vr[1] = 6;

/* Definice dveri pro hrace cislo 3 */
  prv[P_DVERE3_V_O].druh = P_DVERE3_V_O;
  prv[P_DVERE3_V_O].pdp_vr[1] = 7;

/* Definice dveri pro hrace cislo 4 */
  prv[P_DVERE4_V_O].druh = P_DVERE4_V_O;
  prv[P_DVERE4_V_O].pdp_vr[1] = 8;

/* Definice dveri pro hrace cislo 5 */
  prv[P_DVERE5_V_O].druh = P_DVERE5_V_O;
  prv[P_DVERE5_V_O].pdp_vr[1] = 9;

/* Defince tech samych dveri akorat zavrenych */
  prv[P_DVERE1_V_Z].druh = P_DVERE1_V_Z; // h -> horizontalni -> vodorovny
  prv[P_DVERE1_V_Z].variant = 2;
  prv[P_DVERE1_V_Z].varianty[0] = PRVNI_KLASIK_LEVEL+51;
  prv[P_DVERE1_V_Z].varianty[1] = PRVNI_KYBER_LEVEL+31;
  prv[P_DVERE1_V_Z].podprvku = 2;
  prv[P_DVERE1_V_Z].podprvky[0] = PP_LEVY_FUTRO_Z;
  prv[P_DVERE1_V_Z].pdp_vr[0] = 0;
  prv[P_DVERE1_V_Z].podprvky[1] = PP_PRAVY_FUTRO_O;
  prv[P_DVERE1_V_Z].pdp_vr[1] = 5;
  prv[P_DVERE1_V_Z].podprvky[2] = PP_LEVY_FUTRO_Z;
  prv[P_DVERE1_V_Z].pdp_vr[2] = 1;
  prv[P_DVERE1_V_Z].podprvky[3] = PP_PRAVY_FUTRO_O;
  prv[P_DVERE1_V_Z].pdp_vr[3] = 11;

  prv[P_DVERE2_V_Z] = prv[P_DVERE1_V_Z];
  prv[P_DVERE3_V_Z] = prv[P_DVERE1_V_Z];
  prv[P_DVERE4_V_Z] = prv[P_DVERE1_V_Z];
  prv[P_DVERE5_V_Z] = prv[P_DVERE1_V_Z];
  
/* Zmeny po urcitych prvcich */
  prv[P_DVERE2_V_Z].druh = P_DVERE2_V_Z;
  prv[P_DVERE2_V_Z].pdp_vr[1] = 6;
  prv[P_DVERE2_V_Z].pdp_vr[2] = 2;

/* Definice dveri pro hrace cislo 3 */
  prv[P_DVERE3_V_Z].druh = P_DVERE3_V_Z;
  prv[P_DVERE3_V_Z].pdp_vr[1] = 7;
  prv[P_DVERE3_V_Z].pdp_vr[2] = 3;

/* Definice dveri pro hrace cislo 4 */
  prv[P_DVERE4_V_Z].druh = P_DVERE4_V_Z;
  prv[P_DVERE4_V_Z].pdp_vr[1] = 8;
  prv[P_DVERE4_V_Z].pdp_vr[2] = 4;

/* Definice dveri pro hrace cislo 5 */
  prv[P_DVERE5_V_Z].druh = P_DVERE5_V_Z;
  prv[P_DVERE5_V_Z].pdp_vr[1] = 9;
  prv[P_DVERE5_V_Z].pdp_vr[2] = 5;


/* Definice individualnich dveri */
/* Ktery muzou projit jen berusky stejne barvy */

  prv[P_ID_DVERE1_H_O].druh = P_ID_DVERE1_H_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE1_H_O].variant = 2;
  prv[P_ID_DVERE1_H_O].varianty[0] = PRVNI_KLASIK_LEVEL+60;
  prv[P_ID_DVERE1_H_O].varianty[1] = PRVNI_KYBER_LEVEL+97;
  prv[P_ID_DVERE1_H_O].podprvku = 2;
  prv[P_ID_DVERE1_H_O].podprvky[0] = PP_HORNI_FUTRO_O;
  prv[P_ID_DVERE1_H_O].pdp_vr[0] = 0;
  prv[P_ID_DVERE1_H_O].podprvky[1] = PP_DOLNI_FUTRO_O;
  prv[P_ID_DVERE1_H_O].pdp_vr[1] = 0;
  prv[P_ID_DVERE1_H_O].podprvky[2] = PP_HORNI_FUTRO_O;
  prv[P_ID_DVERE1_H_O].pdp_vr[2] = 4;
  prv[P_ID_DVERE1_H_O].podprvky[3] = PP_DOLNI_FUTRO_O;
  prv[P_ID_DVERE1_H_O].pdp_vr[3] = 12;

  prv[P_ID_DVERE2_H_O] = prv[P_ID_DVERE1_H_O];
  prv[P_ID_DVERE3_H_O] = prv[P_ID_DVERE1_H_O];
  prv[P_ID_DVERE4_H_O] = prv[P_ID_DVERE1_H_O];
  prv[P_ID_DVERE5_H_O] = prv[P_ID_DVERE1_H_O];

  /* ted to tam je nakopirovany, ted to jenom opravim */
  prv[P_ID_DVERE2_H_O].druh = P_ID_DVERE2_H_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE2_H_O].pdp_vr[1] = 1;
  prv[P_ID_DVERE2_H_O].pdp_vr[2] = 5;
  prv[P_ID_DVERE2_H_O].pdp_vr[3] = 13;

  prv[P_ID_DVERE3_H_O].druh = P_ID_DVERE3_H_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE3_H_O].pdp_vr[1] = 2;
  prv[P_ID_DVERE3_H_O].pdp_vr[2] = 6;
  prv[P_ID_DVERE3_H_O].pdp_vr[3] = 14;
  
  prv[P_ID_DVERE4_H_O].druh = P_ID_DVERE4_H_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE4_H_O].pdp_vr[1] = 3;
  prv[P_ID_DVERE4_H_O].pdp_vr[2] = 7;
  prv[P_ID_DVERE4_H_O].pdp_vr[3] = 15;

  prv[P_ID_DVERE5_H_O].druh = P_ID_DVERE5_H_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE5_H_O].pdp_vr[1] = 4;
  prv[P_ID_DVERE5_H_O].pdp_vr[2] = 8;
  prv[P_ID_DVERE5_H_O].pdp_vr[3] = 16;

/* Definice individualnich dveri */
/* Ktery muzou projit jen berusky stejne barvy */
/* Toto jsou zavrene dvere */

  prv[P_ID_DVERE1_H_Z].druh = P_ID_DVERE1_H_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE1_H_Z].variant = 2;
  prv[P_ID_DVERE1_H_Z].varianty[0] = PRVNI_KLASIK_LEVEL+58;
  prv[P_ID_DVERE1_H_Z].varianty[1] = PRVNI_KYBER_LEVEL+39;
  prv[P_ID_DVERE1_H_Z].podprvku = 2;
  prv[P_ID_DVERE1_H_Z].podprvky[0] = PP_HORNI_FUTRO_O;
  prv[P_ID_DVERE1_H_Z].pdp_vr[0] = 0;
  prv[P_ID_DVERE1_H_Z].podprvky[1] = PP_DOLNI_FUTRO_O;
  prv[P_ID_DVERE1_H_Z].pdp_vr[1] = 0;
  prv[P_ID_DVERE1_H_Z].podprvky[2] = PP_HORNI_FUTRO_O;
  prv[P_ID_DVERE1_H_Z].pdp_vr[2] = 4;
  prv[P_ID_DVERE1_H_Z].podprvky[3] = PP_DOLNI_FUTRO_O;
  prv[P_ID_DVERE1_H_Z].pdp_vr[3] = 12;

  prv[P_ID_DVERE2_H_Z] = prv[P_ID_DVERE1_H_Z];
  prv[P_ID_DVERE3_H_Z] = prv[P_ID_DVERE1_H_Z];
  prv[P_ID_DVERE4_H_Z] = prv[P_ID_DVERE1_H_Z];
  prv[P_ID_DVERE5_H_Z] = prv[P_ID_DVERE1_H_Z];

  /* ted to tam je nakopirovany, ted to jenom opravim */
  prv[P_ID_DVERE2_H_Z].druh = P_ID_DVERE2_H_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE2_H_Z].pdp_vr[1] = 1;
  prv[P_ID_DVERE2_H_Z].pdp_vr[2] = 5;
  prv[P_ID_DVERE2_H_Z].pdp_vr[3] = 13;

  prv[P_ID_DVERE3_H_Z].druh = P_ID_DVERE3_H_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE3_H_Z].pdp_vr[1] = 2;
  prv[P_ID_DVERE3_H_Z].pdp_vr[2] = 6;
  prv[P_ID_DVERE3_H_Z].pdp_vr[3] = 14;
  
  prv[P_ID_DVERE4_H_Z].druh = P_ID_DVERE4_H_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE4_H_Z].pdp_vr[1] = 3;
  prv[P_ID_DVERE4_H_Z].pdp_vr[2] = 7;
  prv[P_ID_DVERE4_H_Z].pdp_vr[3] = 15;

  prv[P_ID_DVERE5_H_Z].druh = P_ID_DVERE5_H_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE5_H_Z].pdp_vr[1] = 4;
  prv[P_ID_DVERE5_H_Z].pdp_vr[2] = 8;
  prv[P_ID_DVERE5_H_Z].pdp_vr[3] = 16;

/* Definice individualnich dveri */
/* Ktery muzou projit jen berusky stejne barvy */
/* Toto je sekce ve ktere definuju dvere,
ktere jsou vertikalni */

  prv[P_ID_DVERE1_V_O].druh = P_ID_DVERE1_V_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE1_V_O].variant = 2;
  prv[P_ID_DVERE1_V_O].varianty[0] = PRVNI_KLASIK_LEVEL+49;
  prv[P_ID_DVERE1_V_O].varianty[1] = PRVNI_KYBER_LEVEL+60;
  prv[P_ID_DVERE1_V_O].podprvku = 2;
  prv[P_ID_DVERE1_V_O].podprvky[0] = PP_LEVY_FUTRO_O;
  prv[P_ID_DVERE1_V_O].pdp_vr[0] = 0;
  prv[P_ID_DVERE1_V_O].podprvky[1] = PP_PRAVY_FUTRO_O;
  prv[P_ID_DVERE1_V_O].pdp_vr[1] = 0;
  prv[P_ID_DVERE1_V_O].podprvky[2] = PP_LEVY_FUTRO_O;
  prv[P_ID_DVERE1_V_O].pdp_vr[2] = 4;
  prv[P_ID_DVERE1_V_O].podprvky[3] = PP_PRAVY_FUTRO_O;
  prv[P_ID_DVERE1_V_O].pdp_vr[3] = 12;

  prv[P_ID_DVERE2_V_O] = prv[P_ID_DVERE1_V_O];
  prv[P_ID_DVERE3_V_O] = prv[P_ID_DVERE1_V_O];
  prv[P_ID_DVERE4_V_O] = prv[P_ID_DVERE1_V_O];
  prv[P_ID_DVERE5_V_O] = prv[P_ID_DVERE1_V_O];

  /* ted to tam je nakopirovany, ted to jenom opravim */
  prv[P_ID_DVERE2_V_O].druh = P_ID_DVERE2_V_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE2_V_O].pdp_vr[1] = 1;
  prv[P_ID_DVERE2_V_O].pdp_vr[2] = 5;
  prv[P_ID_DVERE2_V_O].pdp_vr[3] = 13;

  prv[P_ID_DVERE3_V_O].druh = P_ID_DVERE3_V_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE3_V_O].pdp_vr[1] = 2;
  prv[P_ID_DVERE3_V_O].pdp_vr[2] = 6;
  prv[P_ID_DVERE3_V_O].pdp_vr[3] = 14;
  
  prv[P_ID_DVERE4_V_O].druh = P_ID_DVERE4_V_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE4_V_O].pdp_vr[1] = 3;
  prv[P_ID_DVERE4_V_O].pdp_vr[2] = 7;
  prv[P_ID_DVERE4_V_O].pdp_vr[3] = 15;

  prv[P_ID_DVERE5_V_O].druh = P_ID_DVERE5_V_O; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE5_V_O].pdp_vr[1] = 4;
  prv[P_ID_DVERE5_V_O].pdp_vr[2] = 8;
  prv[P_ID_DVERE5_V_O].pdp_vr[3] = 16;

/* Definice individualnich dveri */
/* Ktery muzou projit jen berusky stejne barvy */
/* Toto jsou zavrene dvere */

  prv[P_ID_DVERE1_V_Z].druh = P_ID_DVERE1_V_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE1_V_Z].variant = 2;
  prv[P_ID_DVERE1_V_Z].varianty[0] = PRVNI_KLASIK_LEVEL+51;
  prv[P_ID_DVERE1_V_Z].varianty[1] = PRVNI_KYBER_LEVEL+51;
  prv[P_ID_DVERE1_V_Z].podprvku = 2;
  prv[P_ID_DVERE1_V_Z].podprvky[0] = PP_LEVY_FUTRO_O;
  prv[P_ID_DVERE1_V_Z].pdp_vr[0] = 0;
  prv[P_ID_DVERE1_V_Z].podprvky[1] = PP_PRAVY_FUTRO_O;
  prv[P_ID_DVERE1_V_Z].pdp_vr[1] = 0;
  prv[P_ID_DVERE1_V_Z].podprvky[2] = PP_LEVY_FUTRO_O;
  prv[P_ID_DVERE1_V_Z].pdp_vr[2] = 4;
  prv[P_ID_DVERE1_V_Z].podprvky[3] = PP_PRAVY_FUTRO_O;
  prv[P_ID_DVERE1_V_Z].pdp_vr[3] = 12;

  prv[P_ID_DVERE2_V_Z] = prv[P_ID_DVERE1_V_Z];
  prv[P_ID_DVERE3_V_Z] = prv[P_ID_DVERE1_V_Z];
  prv[P_ID_DVERE4_V_Z] = prv[P_ID_DVERE1_V_Z];
  prv[P_ID_DVERE5_V_Z] = prv[P_ID_DVERE1_V_Z];

  /* ted to tam je nakopirovany, ted to jenom opravim */
  prv[P_ID_DVERE2_V_Z].druh = P_ID_DVERE2_V_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE2_V_Z].pdp_vr[1] = 1;
  prv[P_ID_DVERE2_V_Z].pdp_vr[2] = 5;
  prv[P_ID_DVERE2_V_Z].pdp_vr[3] = 13;

  prv[P_ID_DVERE3_V_Z].druh = P_ID_DVERE3_V_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE3_V_Z].pdp_vr[1] = 2;
  prv[P_ID_DVERE3_V_Z].pdp_vr[2] = 6;
  prv[P_ID_DVERE3_V_Z].pdp_vr[3] = 14;
  
  prv[P_ID_DVERE4_V_Z].druh = P_ID_DVERE4_V_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE4_V_Z].pdp_vr[1] = 3;
  prv[P_ID_DVERE4_V_Z].pdp_vr[2] = 7;
  prv[P_ID_DVERE4_V_Z].pdp_vr[3] = 15;

  prv[P_ID_DVERE5_V_Z].druh = P_ID_DVERE5_V_Z; // h -> horizontalni -> vodorovny
  prv[P_ID_DVERE5_V_Z].pdp_vr[1] = 4;
  prv[P_ID_DVERE5_V_Z].pdp_vr[2] = 8;
  prv[P_ID_DVERE5_V_Z].pdp_vr[3] = 16;


/* Prace na dverich na jeden pruchod */
  prv[P_DV_H_O].druh = P_DV_H_O;
  prv[P_DV_H_O].variant = 2;
  prv[P_DV_H_O].varianty[0] = PRVNI_KLASIK_LEVEL+60;
  prv[P_DV_H_O].varianty[1] = PRVNI_KYBER_LEVEL+76;
  prv[P_DV_H_O].podprvku = 2;
  prv[P_DV_H_O].podprvky[0] = PP_HORNI_FUTRO_O;
  prv[P_DV_H_O].pdp_vr[0] = 2;
  prv[P_DV_H_O].podprvky[1] = PP_DOLNI_FUTRO_O;
  prv[P_DV_H_O].pdp_vr[1] = 10;
  prv[P_DV_H_O].podprvky[2] = PP_HORNI_FUTRO_O;
  prv[P_DV_H_O].pdp_vr[2] = 9;
  prv[P_DV_H_O].podprvky[3] = PP_DOLNI_FUTRO_O;
  prv[P_DV_H_O].pdp_vr[3] = 17;

  prv[P_DV_H_Z] = prv[P_DV_H_O];
  prv[P_DV_H_Z].druh = P_DV_H_Z;
  prv[P_DV_H_Z].varianty[0] = PRVNI_KLASIK_LEVEL+58;
  prv[P_DV_H_Z].varianty[1] = PRVNI_KYBER_LEVEL+79;

  prv[P_DV_H_Z].podprvky[1] = PP_DOLNI_FUTRO_Z;
  prv[P_DV_H_Z].pdp_vr[1] = 0;
  prv[P_DV_H_Z].podprvky[2] = PP_HORNI_FUTRO_Z;
  prv[P_DV_H_Z].pdp_vr[2] = 6;


/* Ty same dvere akorat na vertikalni pruchod */
  prv[P_DV_V_O].druh = P_DV_V_O;
  prv[P_DV_V_O].variant = 2;
  prv[P_DV_V_O].varianty[0] = PRVNI_KLASIK_LEVEL+49;
  prv[P_DV_V_O].varianty[1] = PRVNI_KYBER_LEVEL+70;
  prv[P_DV_V_O].podprvku = 2;
  prv[P_DV_V_O].podprvky[0] = PP_LEVY_FUTRO_O;
  prv[P_DV_V_O].pdp_vr[0] = 2;
  prv[P_DV_V_O].podprvky[1] = PP_PRAVY_FUTRO_O;
  prv[P_DV_V_O].pdp_vr[1] = 10;
  prv[P_DV_V_O].podprvky[2] = PP_LEVY_FUTRO_O;
  prv[P_DV_V_O].pdp_vr[2] = 9;
  prv[P_DV_V_O].podprvky[3] = PP_PRAVY_FUTRO_O;
  prv[P_DV_V_O].pdp_vr[3] = 17;

  prv[P_DV_V_Z] = prv[P_DV_V_O];
  prv[P_DV_V_Z].druh = P_DV_V_Z;
  prv[P_DV_V_Z].varianty[0] = PRVNI_KLASIK_LEVEL+51;
  prv[P_DV_V_Z].varianty[1] = PRVNI_KYBER_LEVEL+73;
  prv[P_DV_V_Z].podprvky[1] = PP_PRAVY_FUTRO_Z;
  prv[P_DV_V_Z].pdp_vr[1] = 0;
  prv[P_DV_V_Z].podprvky[2] = PP_LEVY_FUTRO_Z;
  prv[P_DV_V_Z].pdp_vr[2] = 6;


  prv[P_DV_H].druh = P_DV_H;
  prv[P_DV_H].variant = 1;
  prv[P_DV_H].varianty[0] = PRVNI_KYBER_LEVEL+97;
  prv[P_DV_H].podprvku = 2;
  prv[P_DV_H].podprvky[0] = PP_HORNI_FUTRO_O;
  prv[P_DV_H].pdp_vr[0] = 3;
  prv[P_DV_H].podprvky[1] = PP_DOLNI_FUTRO_O;
  prv[P_DV_H].pdp_vr[1] = 11;

  prv[P_DV_V].druh = P_DV_V;
  prv[P_DV_V].variant = 1;
  prv[P_DV_V].varianty[0] = PRVNI_KYBER_LEVEL+60;
  prv[P_DV_V].podprvku = 2;
  prv[P_DV_V].podprvky[0] = PP_LEVY_FUTRO_O;
  prv[P_DV_V].pdp_vr[0] = 3;
  prv[P_DV_V].podprvky[1] = PP_PRAVY_FUTRO_O;
  prv[P_DV_V].pdp_vr[1] = 11;

  /* Editacni prvky a ostatni
  */
  prv[P_ZEME].minus_x = 0; prv[P_ZEME].plus_x = 0;
  prv[P_BEDNA].minus_x = 0; prv[P_BEDNA].plus_x = 0;
  prv[P_TNT].minus_x = 0; prv[P_TNT].plus_x = 0;
  prv[P_STENA].minus_x = 0; prv[P_STENA].plus_x = 0;
  prv[P_EXIT].minus_x = 0; prv[P_EXIT].plus_x = 0;
  prv[P_KAMEN].minus_x = 0; prv[P_KAMEN].plus_x = 0;
  prv[P_KLIC].minus_x = 0; prv[P_KLIC].plus_x = 0;
  prv[P_KRUMPAC].minus_x = 0; prv[P_KRUMPAC].plus_x = 0;
  prv[P_KLIC1].minus_x = 0; prv[P_KLIC1].plus_x = 0;
  prv[P_KLIC2].minus_x = 0; prv[P_KLIC2].plus_x = 0;
  prv[P_KLIC3].minus_x = 0; prv[P_KLIC3].plus_x = 0;
  prv[P_KLIC4].minus_x = 0; prv[P_KLIC4].plus_x = 0;
  prv[P_KLIC5].minus_x = 0; prv[P_KLIC5].plus_x = 0;

  prv[P_DVERE1_H_O].minus_x = 0; prv[P_DVERE1_H_O].plus_x = 0;
  prv[P_DVERE2_H_O].minus_x = 0; prv[P_DVERE2_H_O].plus_x = 0;
  prv[P_DVERE3_H_O].minus_x = 0; prv[P_DVERE3_H_O].plus_x = 0;
  prv[P_DVERE4_H_O].minus_x = 0; prv[P_DVERE4_H_O].plus_x = 0;
  prv[P_DVERE5_H_O].minus_x = 0; prv[P_DVERE5_H_O].plus_x = 0;

  prv[P_DVERE1_H_Z].minus_x = 0; prv[P_DVERE1_H_Z].plus_x = 0;
  prv[P_DVERE2_H_Z].minus_x = 0; prv[P_DVERE2_H_Z].plus_x = 0;
  prv[P_DVERE3_H_Z].minus_x = 0; prv[P_DVERE3_H_Z].plus_x = 0;
  prv[P_DVERE4_H_Z].minus_x = 0; prv[P_DVERE4_H_Z].plus_x = 0;
  prv[P_DVERE5_H_Z].minus_x = 0; prv[P_DVERE5_H_Z].plus_x = 0;

  prv[P_DVERE1_V_O].minus_x = 1; prv[P_DVERE1_V_O].plus_x = 1;
  prv[P_DVERE2_V_O].minus_x = 1; prv[P_DVERE2_V_O].plus_x = 1;
  prv[P_DVERE3_V_O].minus_x = 1; prv[P_DVERE3_V_O].plus_x = 1;
  prv[P_DVERE4_V_O].minus_x = 1; prv[P_DVERE4_V_O].plus_x = 1;
  prv[P_DVERE5_V_O].minus_x = 1; prv[P_DVERE5_V_O].plus_x = 1;

  prv[P_DVERE1_V_Z].minus_x = 1; prv[P_DVERE1_V_Z].plus_x = 1;
  prv[P_DVERE2_V_Z].minus_x = 1; prv[P_DVERE2_V_Z].plus_x = 1;
  prv[P_DVERE3_V_Z].minus_x = 1; prv[P_DVERE3_V_Z].plus_x = 1;
  prv[P_DVERE4_V_Z].minus_x = 1; prv[P_DVERE4_V_Z].plus_x = 1;
  prv[P_DVERE5_V_Z].minus_x = 1; prv[P_DVERE5_V_Z].plus_x = 1;

  prv[P_ID_DVERE1_H_O].minus_x = 0; prv[P_ID_DVERE1_H_O].plus_x = 0;
  prv[P_ID_DVERE2_H_O].minus_x = 0; prv[P_ID_DVERE2_H_O].plus_x = 0;
  prv[P_ID_DVERE3_H_O].minus_x = 0; prv[P_ID_DVERE3_H_O].plus_x = 0;
  prv[P_ID_DVERE4_H_O].minus_x = 0; prv[P_ID_DVERE4_H_O].plus_x = 0;
  prv[P_ID_DVERE5_H_O].minus_x = 0; prv[P_ID_DVERE5_H_O].plus_x = 0;

  prv[P_ID_DVERE1_H_Z].minus_x = 0; prv[P_ID_DVERE1_H_Z].plus_x = 0;
  prv[P_ID_DVERE2_H_Z].minus_x = 0; prv[P_ID_DVERE2_H_Z].plus_x = 0;
  prv[P_ID_DVERE3_H_Z].minus_x = 0; prv[P_ID_DVERE3_H_Z].plus_x = 0;
  prv[P_ID_DVERE4_H_Z].minus_x = 0; prv[P_ID_DVERE4_H_Z].plus_x = 0;
  prv[P_ID_DVERE5_H_Z].minus_x = 0; prv[P_ID_DVERE5_H_Z].plus_x = 0;

  prv[P_ID_DVERE1_V_O].minus_x = 1; prv[P_ID_DVERE1_V_O].plus_x = 1;
  prv[P_ID_DVERE2_V_O].minus_x = 1; prv[P_ID_DVERE2_V_O].plus_x = 1;
  prv[P_ID_DVERE3_V_O].minus_x = 1; prv[P_ID_DVERE3_V_O].plus_x = 1;
  prv[P_ID_DVERE4_V_O].minus_x = 1; prv[P_ID_DVERE4_V_O].plus_x = 1;
  prv[P_ID_DVERE5_V_O].minus_x = 1; prv[P_ID_DVERE5_V_O].plus_x = 1;

  prv[P_ID_DVERE1_V_Z].minus_x = 1; prv[P_ID_DVERE1_V_Z].plus_x = 1;
  prv[P_ID_DVERE2_V_Z].minus_x = 1; prv[P_ID_DVERE2_V_Z].plus_x = 1;
  prv[P_ID_DVERE3_V_Z].minus_x = 1; prv[P_ID_DVERE3_V_Z].plus_x = 1;
  prv[P_ID_DVERE4_V_Z].minus_x = 1; prv[P_ID_DVERE4_V_Z].plus_x = 1;
  prv[P_ID_DVERE5_V_Z].minus_x = 1; prv[P_ID_DVERE5_V_Z].plus_x = 1;

  prv[P_DV_H_O].minus_x = 0; prv[P_DV_H_O].plus_x = 0;
  prv[P_DV_H_Z].minus_x = 0; prv[P_DV_H_Z].plus_x = 0;
  prv[P_DV_V_O].minus_x = 1; prv[P_DV_V_O].plus_x = 1;
  prv[P_DV_V_Z].minus_x = 1; prv[P_DV_V_Z].plus_x = 1;

  prv[P_DV_H].minus_x = 0; prv[P_DV_H].plus_x = 0;
  prv[P_DV_V].minus_x = 1; prv[P_DV_V].plus_x = 1;

  /*
    A to je zatim konec, pratele ...
  */
}



int obnov_level_lep(byte *p_level_file,int dat, DISK_LEVEL *pl)
{
 int x,y;

 if(nahraj_soubor_lep(0,(byte *)pl,dat,p_level_file) == 0)
    neni_file(p_level_file);

 if(strcmp(pl->signum,h_levelu)) {
   return(NULL);
 }
 for(y = 0; y < YPOLI; y++) {
    for(x = 0; x < XPOLI; x++) {
#ifdef __AMIGA__
       int z;
       for(z = 0; z < 10; z++) {
          pl->podlaha[y][x][z] = SWAP16LE(pl->podlaha[y][x][z]);
          pl->level[y][x][z] = SWAP16LE(pl->level[y][x][z]);
       }
       pl->hraci[y][x] = SWAP16LE(pl->hraci[y][x]);
#endif
       pl->level[y][x][ANIMACE] = 0;
    }
 }
 nahraj_pozadi(pl->pozadi);

 return(1);
}


int obnov_level(byte *p_level_file, DISK_LEVEL *pl)
{
 int x,y;

 if(nahraj_soubor(sizeof(DISK_LEVEL),(byte *)pl,p_level_file,0) == 0)
   return(NULL);

 if(strcmp(pl->signum,h_levelu)) {
   return(NULL);
 }
 nahraj_pozadi(pl->pozadi);
 for(y = 0; y < YPOLI; y++) {
    for(x = 0; x < XPOLI; x++) {
#ifdef __AMIGA__
       int z;
       for(z = 0; z < 10; z++) {
          pl->podlaha[y][x][z] = SWAP16LE(pl->podlaha[y][x][z]);
          pl->level[y][x][z] = SWAP16LE(pl->level[y][x][z]);
       }
       pl->hraci[y][x] = SWAP16LE(pl->hraci[y][x]);
#endif
       pl->level[y][x][ANIMACE] = 0;
    }
 }

 return(1);
}

/* Nahraje pozadi do pameti na pointer p_pozadi */

void obr_do_bmp(dword cislo_p, byte *p_ctverec, dword x_res, dword y_res,byte *p_b)
{

typedef struct {
   BITMAPFILEHEADER  bf __attribute__ ((packed));
   BITMAPINFOHEADER  bi __attribute__ ((packed));
} BMPHEAD __attribute__ ((packed));

BMPHEAD  *p_bmp;
byte     *p_barvy;
int      i;

  p_bmp = (BMPHEAD *)p_b;

  p_bmp->bf.btType = 0x4d42;
  p_bmp->bf.bfSize = (x_res*y_res)+1078;
  p_bmp->bf.bfReserved1 = 0;
  p_bmp->bf.bfReserved2 = 0;
  p_bmp->bf.bfOffBits = 1078;		// Posun na zacatek pixelu od zacatku souboru
#ifdef __AMIGA__
  p_bmp->bf.btType = SWAP16LE(p_bmp->bf.btType);
  p_bmp->bf.bfSize = SWAP32LE(p_bmp->bf.bfSize);
  p_bmp->bf.bfOffBits = SWAP32LE(p_bmp->bf.bfOffBits);
#endif

  p_bmp->bi.biSize = sizeof(p_bmp->bi);//sizeof(p_bmp->bi);			//velikost struktury bi
  p_bmp->bi.biWidth = x_res;		//sirka v pixelech
  p_bmp->bi.biHeight = y_res;		//vyska v pixelech
  p_bmp->bi.biPlanes = 1;  		//Pocet rovin - musi byt 1
  p_bmp->bi.biBitCount = 8;		//Bitu na pixel 1,4,8,24
  p_bmp->bi.biCompression = 0;	//Typ komprese 0-BI_RGB,1-BI_RLE8,2-BI_RLE4
  p_bmp->bi.biSizeImage = _a_xres*_a_yres;	//Velikost obrazu v bitech (pri BI_RGB muze byt 1)
  p_bmp->bi.biXPelsPerMeter = x_res;	//velikost vystupniho zarizeni x v pix/m
  p_bmp->bi.biYPelsPerMeter = y_res;	// y v pix/m
  p_bmp->bi.biClrUsed = 0;		//pocet pouzitych barev (0 = vsechny mozne barvy)
  p_bmp->bi.biClrImportant = 0;	//pocet dulezitych barev (0 = vsechny sou pouzite)
#ifdef __AMIGA__
  p_bmp->bi.biSize = SWAP32LE(p_bmp->bi.biSize);
  p_bmp->bi.biWidth = SWAP32LE( p_bmp->bi.biWidth);
  p_bmp->bi.biHeight = SWAP32LE(p_bmp->bi.biHeight);
  p_bmp->bi.biPlanes = SWAP16LE(p_bmp->bi.biPlanes);
  p_bmp->bi.biBitCount = SWAP16LE(p_bmp->bi.biBitCount);
  p_bmp->bi.biSizeImage = SWAP32LE(p_bmp->bi.biSizeImage);
  p_bmp->bi.biXPelsPerMeter = SWAP32LE(p_bmp->bi.biXPelsPerMeter);
  p_bmp->bi.biYPelsPerMeter = SWAP32LE(p_bmp->bi.biYPelsPerMeter);
#endif

  p_barvy = p_b+54;
  /*
  memset(p_barvy,0xff,1024);
  for(i = 0; i < 768; i++)
     p_barvy[i] = _a_palety[0][i];
*/
  for(i = 0; i < 256*3; i+=3) {
     p_barvy[0] = (_a_palety[cislo_p][i+2] << 2);
     p_barvy[1] = (_a_palety[cislo_p][i+1] << 2);
     p_barvy[2] = (_a_palety[cislo_p][i] << 2);
     p_barvy+=4;
  }
  for(i = 0; i < y_res; i++)
     memcpy(p_b+1078+i*x_res,p_ctverec+(y_res-1)*x_res-i*x_res,x_res);
}

int nahraj_demo(byte *p_file)
{
 /*
   hlavicka ->

   dw velikost levelu
   byte hlavicka

   dw pocet kroku na zacatku
   dw pocet kroku dema

   dw delka dema
   byte data dema

 */
 byte  *p_buffer;
 byte  hlav[50];
 dword vdelka;
 dword delka;
 dword delka_levelu;
 dword delka_dema;
 dword delka_hraci;
 dword x,y,err,i;
 FILE  *d;


 if((d = fopen(p_file,"rb")) != NULL) {
   /* Nahrani uvodniho infa */
   fread(hlav,sizeof(byte),strlen(HLAVICKA_DEMA),d);

   if(strcmp(hlav,HLAVICKA_DEMA))
     tiskni_chybu("Spatna hlavicka dema !",__LINE__);

   fread(&vdelka,sizeof(vdelka),1,d);
   fread(&klicu_celkem,sizeof(klicu_celkem),1,d);
   fread(&kroku,sizeof(dword),1,d);
   fread(&pos_demo,sizeof(dword),1,d);

   if((p_buffer = malloc(vdelka)) == NULL)
     tiskni_chybu("Nedostatek pameti !",__LINE__);

   fread(&delka_hraci,sizeof(dword),1,d);
   fread(p_buffer,sizeof(byte),delka_hraci,d);
   delka = HRACU*sizeof(p);
   err = uncompress(p, &delka, p_buffer, delka_hraci);
   CHECK_ERR2(err, "Chyba odpakovani ",__LINE__);


   fread(&delka_levelu,sizeof(dword),1,d);
   fread(p_buffer,sizeof(byte),delka_levelu,d);
   delka = sizeof(dm);
   err = uncompress(&pl, &delka, p_buffer, delka);
   CHECK_ERR2(err, "Chyba odpakovani ",__LINE__);


   fread(&delka_dema,sizeof(dword),1,d);
   fread(p_buffer,sizeof(byte),delka_dema,d);
   vdelka = sizeof(demo_str)*(pos_demo+1);
   err = uncompress(demo, &vdelka, p_buffer, delka_dema);
   CHECK_ERR2(err, "Chyba odpakovani ",__LINE__);


   fclose(d);
   free(p_buffer);

   if(strcmp(pl.signum,h_levelu)) {
     tiskni_chybu("Vadna hlavicka dema !",__LINE__);
   }

   for(y = 0; y < YPOLI; y++) {
      for(x = 0; x < XPOLI; x++) {
         pl.level[y][x][ANIMACE] = 0;
      }
   }
   
   nahraj_pozadi(pl.pozadi);
   return(A_OK);
   
 }
 else {
   free(p_buffer);
   tiskni_chybu("Nelze otevrit demosoubor !",__LINE__);
   return(CHYBA);
 }
}

int uloz_demo(byte *p_file)
{
 /*
   hlavicka ->

   velikost hracu
   hraci
   pocet klicu

   dw velikost levelu
   byte level

   dw pocet kroku na zacatku
   dw pocet kroku dema

   dw delka dema
   byte data dema
 */
 byte  *p_buffer;
 dword delka = 2*sizeof(dm),
       vdemo = (pos_demo+1)*sizeof(demo_str),
       err;
 FILE  *d;
 int   i;

 if((p_buffer = malloc(vdemo = (delka > vdemo ? delka+10 : vdemo+10))) == NULL)
    tiskni_chybu("Nedostatek pameti !",__LINE__);

 strcpy(p_buffer,HLAVICKA_DEMA);

 if((d = fopen(p_file,"wb")) != NULL) {
   /* ulozeni uvodniho infa */
   delka = vdemo;
   fwrite(p_buffer,sizeof(byte),strlen(HLAVICKA_DEMA),d);
   fwrite(&delka,sizeof(delka),1,d);
   fwrite(&s_klice,sizeof(s_klice),1,d);
   fwrite(&d_kroku,sizeof(dword),1,d);
   fwrite(&pos_demo,sizeof(pos_demo),1,d);

   err = compress(p_buffer, &delka, s, sizeof(s)*HRACU);
   CHECK_ERR2(err, "Chyba pri kompresi ",__LINE__);
   fwrite(&delka,sizeof(delka),1,d);
   fwrite(p_buffer,delka,1,d);

   delka = vdemo;
   err = compress(p_buffer, &delka, &dm, sizeof(dm));
   CHECK_ERR2(err, "Chyba pri kompresi ",__LINE__);
   fwrite(&delka,sizeof(delka),1,d);
   fwrite(p_buffer,delka,1,d);

   delka = vdemo;
   err = compress(p_buffer, &delka, demo, sizeof(demo_str)*(pos_demo+1));
   CHECK_ERR2(err, "Chyba pri kompresi ",__LINE__);
   fwrite(&delka,sizeof(delka),1,d);
   fwrite(p_buffer,delka,1,d);

   fclose(d);
   free(p_buffer);
   return(A_OK);
 }
 else {
   free(p_buffer);
   tiskni_chybu("Chyba pri ulozeni dema...",__LINE__);
   exit(CHYBA);
 }
}

inline void nahraj_podklad_menu(void)
{
 spr[PRVNI_MENU+1] = load_sprit_lep(DAT_FILE,"menu_001.spr",OSTATNI_LOCK);
}

inline void uvolni_podklad_menu(void)
{
 zrus_sprit(spr[PRVNI_MENU+1]);
}

inline void nahraj_menu_napoveda(void)
{
 spr[PRVNI_MENU+1] = load_sprit_lep(DAT_FILE,"menu_001.spr",OSTATNI_LOCK);
 spr[PRVNI_MENU+17] = load_sprit_lep(DAT_FILE,"menu_017.spr",OSTATNI_LOCK);
 spr[PRVNI_MENU+18] = load_sprit_lep(DAT_FILE,"menu_018.spr",OSTATNI_LOCK);
 spr[PRVNI_MENU+19] = load_sprit_lep(DAT_FILE,"menu_019.spr",OSTATNI_LOCK);
 spr[PRVNI_MENU+20] = load_sprit_lep(DAT_FILE,"menu_020.spr",OSTATNI_LOCK);
 spr[PRVNI_MENU+21] = load_sprit_lep(DAT_FILE,"menu_021.spr",OSTATNI_LOCK);
 spr[PRVNI_MENU+37] = load_sprit_lep(DAT_FILE,"menu_037.spr",OSTATNI_LOCK);
 spr[PRVNI_MENU+38] = load_sprit_lep(DAT_FILE,"menu_038.spr",OSTATNI_LOCK);
 spr[PRVNI_MENU+39] = load_sprit_lep(DAT_FILE,"menu_039.spr",OSTATNI_LOCK);
 spr[PRVNI_MENU+40] = load_sprit_lep(DAT_FILE,"menu_040.spr",OSTATNI_LOCK);
}

inline void nahraj_ber_logo(void)
{
if(spr[PRVNI_MENU+54] == NULL)
  spr[PRVNI_MENU+54] = load_sprit_lep(DAT_FILE,"menu_054.spr",OSTATNI_LOCK);
}

inline void nahraj_menu_poz(void)
{
 if(spr[PRVNI_MENU+56] == NULL)
   spr[PRVNI_MENU+56] = load_sprit_lep(DAT_FILE,"menu_056.spr",OSTATNI_LOCK);
}

inline void nahraj_gamemenu(void)
{
 nahraj_sadu_spritu("menu_%.3d.spr",PRVNI_MENU+44,44,10,OSTATNI_LOCK);
}

inline void nahraj_herni_poz(void)
{
 if(spr[PRVNI_MENU+57] == NULL)
   spr[PRVNI_MENU+57] = load_sprit_lep(DAT_FILE,"menu_057.spr",OSTATNI_LOCK);
}

inline void nahraj_nastaveni_menu(void)
{
 nahraj_sadu_spritu("menu_%.3d.spr",PRVNI_MENU+58,58,8,OSTATNI_LOCK);
}

inline void nahraj_napoveda_menu(void)
{
 nahraj_sadu_spritu("menu_%.3d.spr",PRVNI_MENU+66,66,12,OSTATNI_LOCK);
}

inline void nahraj_hlavni_menu(void)
{
 nahraj_sadu_spritu("menu_%.3d.spr",PRVNI_MENU+1,1,42,OSTATNI_LOCK);
}

inline void nahraj_fileselector(void)
{
 nahraj_sadu_spritu("menu_%.3d.spr",PRVNI_MENU+80,80,11,OSTATNI_LOCK);
}

inline void uvolni_fileselector(void)
{
 int i;

 for(i = 80; i < 91; i++)
    zrus_sprit(spr[PRVNI_MENU+i]);
}

inline void uvolni_nastaveni_menu(void)
{
 int i;

 for(i = 58; i < (58+8); i++)
    zrus_sprit(spr[PRVNI_MENU+i]);

}

inline void uvolni_napoveda_menu(void)
{
 int i;

 for(i = 66; i < (66+12); i++) {
    zrus_sprit(spr[PRVNI_MENU+i]);
 }
}


inline void uvolni_hlavni_menu(void)
{
 int i;

 for(i = 1; i < 43; i++) {
    zrus_sprit(spr[PRVNI_MENU+i]);
 }
}

inline void uvolni_gamemenu(void)
{
 int i;

 for(i = 44; i < 54; i++) {
    zrus_sprit(spr[PRVNI_MENU+i]);
 }
}

inline void uvolni_ber_logo(void)
{
  zrus_sprit(spr[PRVNI_MENU+54]);
}


inline void uvolni_menu_poz(void)
{
  zrus_sprit(spr[PRVNI_MENU+56]);
}


inline void uvolni_herni_poz(void)
{
  zrus_sprit(spr[PRVNI_MENU+57]);
}

inline void uvolni_menu_napoveda(void)
{
 zrus_sprit(spr[PRVNI_MENU+1]);
 zrus_sprit(spr[PRVNI_MENU+17]);
 zrus_sprit(spr[PRVNI_MENU+18]);
 zrus_sprit(spr[PRVNI_MENU+19]);
 zrus_sprit(spr[PRVNI_MENU+20]);
 zrus_sprit(spr[PRVNI_MENU+21]);
 zrus_sprit(spr[PRVNI_MENU+37]);
 zrus_sprit(spr[PRVNI_MENU+38]);
 zrus_sprit(spr[PRVNI_MENU+39]);
 zrus_sprit(spr[PRVNI_MENU+40]);
}

void tipni_obrazek(int cislo)
{
 byte *p_obr;
 byte *p_bmp;
 byte file[20];
 FILE *d;
 int  vl;

 p_obr = p_bmp = NULL;

 sprintf(file,"ber%.5d.bmp",cislo);

 vl = (_a_xres*_a_yres)+100;

 if(((p_obr = malloc(vl)) != NULL)&&
    ((p_bmp = malloc(vl+1078)) != NULL)) {
     _a_x = _a_xres; _a_y = _a_yres;
/*
     _go32_dpmi_lock_data(p_obr,vl);
     _go32_dpmi_lock_data(p_bmp,vl+1078);
*/
     uloz_ctverec(p_obr,0,0);
     obr_do_bmp(PALETA,p_obr,_a_xres,_a_yres,p_bmp);

     if((d = fopen(file,"wb")) != NULL) {
       fwrite(p_bmp,1,(_a_xres*_a_yres)+1078,d);
       fclose(d);
     }

 }
 free(p_obr);
 free(p_bmp);
}

