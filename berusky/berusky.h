/*
         .���� �    � ����.     �    � �����.  ����� .����. �    �
        .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
        �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
      .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
     .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
     �.      � �    � �      .� �   .� �    .� ����� .����. �    �
     (c) 1998 AnakreoN Freeware

   Berusky (C) Komat za Anakreon fwp. 1997-1998-1999-?

   Soubor:     berusky.h  - define pro cely berusky
   Autor:      Komat za Anakreon
   Datum:      furt se opravuje
   Verze:      release 0.0
   Prekladac:  djgpp -> gcc + nasm
   Nakladac:   Zetor BS-564 Mk III C
*/

#ifndef __BERUSKY_H
#define __BERUSKY_H

#include "libanak.h"
#include "zakl_typ.h"
#include "animace.h"
#include "konver.h"
#include "menu.h"
#include "sound.h"


#define OSTATNI_LOCK   LOCK

#define MAX_SPR     2000

#define ZVUKY     "berusky.snd"
#define INI_FILE  "berusky.ini"

#define VYPIS     stdout
#define MENU_FN   0

#define XRES      640
#define YRES      480
#define X         20
#define Y         20

#define GRAF_MOD    0x101
#define VGA_MOD     0x13
#define PALETA      0
#define PALETA_MNU  1
#define PALETA_LOGO 2
#define PALETA_LOGO2 3
#define PALETA_END  4
#define F_PALETA    "berusky.pal"
#define F_PALET     5
#define ZLEVEL      "level2.dat"
#define EDITOR      "level1.dat"
#define BILA        0xff
#define F_CONFIG    "berusky.cfg"

#define FN1_FILE     "font1.fn"
#define FN2_FILE     "font2.fn"
#define FN3_FILE     "font3.fn"


#define X_VELIKOST_KURZORU 13
#define Y_VELIKOST_KURZORU 19

#define HRACU              5
#define MAX_BAR_KLICU      1

#define MAX_DEMO_KROKU     10000

/* definice ini souboru */
#define X_KLICU_POZICE  550
#define Y_KLICU_POZICE  0

/* definice horni listy */
#define VELKY_X           29
#define VELKY_SKOK        100
#define VELKY_YFUK        0
#define VELKY_XFUK        (40+(46/2))
#define MASKA             (PRVNI_OSTATNI+56)

/* definice pouzitych klaves */
#define VEL_LEVELSETU 2000

#define HMF 20

#define X_GAME (320-(237/2))
#define Y_GAME (240-(249/2))

#define cnr() cekej_na_reakci()

#define A_NAHORU    0
#define A_DOPRAVA   1
#define A_DOLU      2
#define A_DOLEVA    3
#define HR_FRAMU    10

#define A_B_NAHORU  4
#define A_B_DOPRAVA 5
#define A_B_DOLU    6
#define A_B_DOLEVA  7
#define HRB_FRAMU   20

#define A_POZADI    1
#define A_SPRIT     0

#define A_KAMEN     8
#define AD_KAMEN    3
#define A_EXIT      9
#define AD_EXIT     6
#define A_BEDNA1    10
#define A_BEDNA2    11
#define A_BEDNA3    12
#define A_BEDNA4    13
#define A_BEDNA5    14
#define A_BEDNA6    15
#define A_BEDNA7    16
#define AD_BEDNA    7

// struktura,ktera popisuje 1 hrace
typedef struct {
   byte  aktivni;     // 0 - znamena neni aktivni,1 je aktivni
   dword cislo;
   dword x_pozice;
   dword y_pozice;
   dword rotace;
   dword lopaty;
   dword klice;

   dword a_ls;
   dword a_ps;

   dword ls[4];   //sprity na leve strane pulky horni listy
   dword ps[4];  //sprity na prave strane pulky

   dword pole_spr[10];
   dword prvku;

} hrac;

// Editacni struktura
typedef struct {
   dword x_pozice;
   dword y_pozice;
} edit;

/* definice editacnich rovin */
typedef struct {
   int  prvku;
   int  prvni;
   int  stav;
   int  akt;
} ed_rovina;

/* struktura pro ukladani dema */
typedef struct {
  int  x_kor;
  int  y_kor;
  int  alt;
  int  hrac;
  int  prodleva;
  int  natoceni;
} demo_str;

#define BRUTALWARE_STRING "BERUSKY BRUTAL WARE V.0.0 (C) ANAKREON 1999"

extern  int     DAT_FILE;
extern  int     LFB;
extern  int     fn1;
extern  int     fn2;
extern  byte    *p_pozadi;        //Pointer na buffer s pozadim
extern  byte    *spr[2000];
extern  DISK_LEVEL pl;         //Adresa struktury s levelem
extern  DISK_LEVEL dm;         //Adresa struktury s levelem pro ukladani dema

extern  hrac   p[HRACU];       //Definovani hracu
extern  hrac   s[HRACU];       //Definovani hracu
extern  dword  klicu_celkem;
extern  dword  s_klice;


#define KONCOVKA_DM ".dm"
extern  byte jmeno_dm[30];
extern  demo_str demo[MAX_DEMO_KROKU];
extern  int  pos_demo;
extern  int  kroku;
extern  int  d_kroku;

extern volatile dword kresli;
extern volatile dword kresli_int;
extern volatile dword dos_time;
extern volatile dword mezicas;
extern volatile dword mezi_back;

extern int low_mem;

void nahraj_pozadi(int pozadi);
void uvolni_pozadi(void);
void music_update(void);
void neni_file(byte *p_jmeno);
void vykresli_level(void);
int  hraj(void);
void prepni(void);
dword muze_tahnout();
void nahraj_edit_grafiku(void);
void edituj(void);
void vykresli_panel(void);
void posun_edit(void);
dword  muze_edit(dword ex,dword ey,dword mx,dword my);
int  uloz_level(byte *p_file, int kontrola);
int  load_level(byte *p_file);
void edit_help(void);
void obsluz_mys(int rovina);
void obnov_pozadi(dword x_p,dword y_p);
void uvolni_pamet(void);
int  posun_berusku();
int  posun_berusku_a_bednu(void);
void testuj_pamet(byte *p_text);
void aktualizuj_animace(void);
byte * pricti(byte  *p_mem,long delka);
void alokuj_pamet_na_pozadi(void);
void obnov_pozadi_do_bufferu(byte  *p_buffer,dword x_p,dword y_p,dword x_v,dword y_v);
void nastav_casovac(byte  *p_promena,dword cas);
void resetuj_casovac(void);
void aktualizuj_dolni_listu(void);
int  animuj_prvek(dword prvek, dword x, dword y);
int  animuj_bednu(dword x, dword y);
void nahrej_konfiguraci(char *p_file);
void pis_text_fn(int x, int y, int font, char *p_text);
int  nahraj_fonty(char *p_fn_file);
void blue_death(void);
void obnov_pozadi_menu(int x_p, int y_p,int x_v,int y_v);
void spust_editor(void);
void _1_sada(void);
void _2_sada(void);
void _3_sada(void);
void _4_sada(void);
void _5_sada(void);
void spust_levely(int sada,int level);
void obnov_grafiku(byte *p_char_file);
void nahraj_levelset(int set);
void vyhodnoceni_levelu(void);
void konec_epizody(int epizoda);
void zadej_heslo(void);
void napoveda_ovladani(void);
void uvodni_uvitani(void);
void napoveda_pravidla(void);
void napoveda_autori(void);
void menu_nastaveni(void);
void nahraj_levelset_lep(int set, int p_dat);
void tiskni_chybu(byte *p_hlaska, int line);
void nahraj_data(void);
void vykresli_edit_level(int rovina,int podlaha);
int  dilema(void);
void vykresli_level_grf(void);
void tik_30(void);

void nahraj_sadu_spritu(char *p_file, int prvni_sprit, int prvni_file, int pocet, int lock);
void nahraj_sadu_spritu_komp(char *p_file, int prvni_sprit, int prvni_file, int pocet, int lock);

void zapis_ctverecek(int x, int y, int rovina, int prvek, int varianta, int rotace);
void obnov_ctverecek(int x, int y);
void zobraz_logo_anak(void);
void zobraz_logo_uziv(void);
void hraj_level(void);
void getms(void);
void cekej_na_reakci(void);
void vykresli_hrace(int hrac);
int  uvod_do_levelu(void);
void kresli_prvek(int p, int v, int rot, int x, int y);
void kresli_prvek_levelu(DISK_LEVEL *p_lev, int x, int y);
void help_ig(void);
void prohledej_level(void);
void animuj_dvere(int x, int y);
void zrus_dvere(int x, int y);
void prohledej_level(void);
void posledni_sbohem(void);
void cekej(int tiku);
int  cekej_s_kb(int tiku);
void cekej_proc(int tiku,void (*p_fnc)());
void tipni_obrazek(int cislo);
void kopiruj_prvek(int sx, int sy, int nx, int ny);
void prvni_napoveda(void);
void druha_napoveda(void);
void nuluj_hrace(void);
int  hraj_demo(byte *p_jmeno, int lep);
void spust_oci(void);
void kresli_oci(void);
void stop_oci(void);
void nastaveni_ig(void);
inline void navrat_do_menu(void);
inline void navrat(void);
inline void ini_spr(void);
int il(int x, int y, int font, int maxzn, char *p_str, int maz);
int filesector(byte *p_final_buffer, int mod, byte *p_aktualni_soubor, byte *p_nadpis);
int getch(void);
void nuluj_animace(void);
void nastav_pameti(void);



/* Vykresli pozadi levelu ze souboru POZADI */
#define vykresli_pozadi()     kresli_sprit(p_pozadi,0,YFUK*Y)
#define kresli_podklad_menu() kresli_sprit(spr[PRVNI_MENU+1],113,HMF)

#define GLOBALNICH_SPRITU  22
#define KLASICKYCH_SPRITU  93
#define KYBER_SPRITU       98
#define HERNICH_SPRITU     59
#define POZADI_SPRITU      2
#define HRACU_SPRITU       10
#define MENU_SPRITU        58
#define DELKA_TEXTU        70000

#define NENI_POZADI        0xffffffff

#define NEPREPISUJ         1
#define PREPISUJ           0

#define M_X mysi_info[1]
#define M_Y mysi_info[2]

#define CHECK_ERR2(err, msg, line) { \
    if (err != Z_OK) { \
        prepni_do_textu(0x3); \
        fprintf(VYPIS, "\n\n%s error: %d na line %d\n", msg, err, line); \
        exit(1); \
    } \
}

#define NULA               0

#endif
