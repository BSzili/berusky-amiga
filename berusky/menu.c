/*
         .���� �    � ����.     �    � �����.  ����� .����. �    �
        .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
        �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
      .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
     .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
     �.      � �    � �      .� �   .� �    .� ����� .����. �    �
     (c) 1998 AnakreoN Freeware

   Berusky (C) Komat za Anakreon fwp. 1997-1998-1999-?

   Soubor:     menu.c - menusystem pro berusky

   Autor:      Komat za Anakreon
   Datum:      furt se opravuje
   Verze:      release 0.0
   Prekladac:  djgpp -> gcc + nasm
   Nakladac:   Zetor BS-564 Mk III C

*/

/*
  Dulezite cisla o menu:

  Menu      ->    (113,0  ) -> (518,402) = (405,402)
  Nova hra  ->    (256,119) -> (390,150) = (134,31)
  Heslo     ->    (256,159) -> (380,189) = (134,31)
  Napoveda  ->    (256,198) -> (390,229) = (134,31)
  Nastaveni ->    (256,239) -> (390,270) = (134,31)
  Editor    ->    (256,279) -> (390,310) = (134,31)
  Konec     ->    (256,319) -> (390,350) = (134,31)

  Obtiznost:
  Trenink   ->    (275,154) -> (377,176) = (102,22)
  Lehka     ->    (275,185) -> (377,206) = (102,21)
  Stredni   ->    (275,214) -> (377,235) = (102,21)
  Tezka     ->    (275,243) -> (377,265) = (102,22)
  Nemozna   ->    (275,276) -> (377,297) = (102,21)
  Zpet      ->    (275,320) -> (377,351) = (102,31)

  Napoveda:
  Ovladani  ->    (272,167) -> (373,189) = (101,22)
  Pravidla  ->    (272,201) -> (373,223) = (101,22)
  Autori    ->    (272,236) -> (373,257) = (101,21)
  Zpet      ->    (272,320) -> (373,351) = (101,31)

  Konec:
  Ano       ->    (268,221) -> (321,246) = (53,25)
  Ne        ->    (348,221) -> (384,246) = (36,25)


  Dilema menu:
  Restart   ->    (0,0)   ->  (237, 52) = (237,52)
  Nahrat    ->    (0,51)  ->  (237,105) = (237,54)
  Ulozit    ->    (0,106) ->  (237,158) = (237,52)
  Konec     ->    (0,156) ->  (237,209) = (237,53)
  Zpet      ->    (0,208) ->  (237,260) = (237,52)

*/
/*
  Druha verze menu, jeste jedna a ja te mega zakousnu !!!
  Dulezite cisla o menu:

  Menu      ->    (122,12  ) ->  = (405,402)
  Nova hra  ->    (265,131) ->  = (134,31)
  Heslo     ->    (265,171) ->  = (134,31)
  Napoveda  ->    (265,210) ->  = (134,31)
  Nastaveni ->    (265,251) ->  = (134,31)
  Editor    ->    (265,291) ->  = (134,31)
  Konec     ->    (265,331) ->  = (134,31)

  Obtiznost:
  Trenink   ->    (284,166) ->  = (102,22)
  Lehka     ->    (284,197) ->  = (102,21)
  Stredni   ->    (284,226) ->  = (102,21)
  Tezka     ->    (284,255) ->  = (102,22)
  Nemozna   ->    (284,288) ->  = (102,21)
  Zpet      ->    (284,332) ->  = (102,31)

  Napoveda:
  Ovladani  ->    (281,179) ->  = (101,22)
  Pravidla  ->    (281,213) ->  = (101,22)
  Autori    ->    (281,248) ->  = (101,21)
  Zpet      ->    (281,332) ->  = (101,31)

  Konec:
  Ano       ->    (277,233) ->  = (53,25)
  Ne        ->    (357,233) ->  = (36,25)

*/
/*
 Doplnujic informace:

 09    - (135,99) -> (284,358) = (149,259)
 heslo - (127,99) -> (298,358) = (171,259)
 konec - (92, 99) -> (340,358) = (248,259)
 hlavni- (75, 99) -> (340,363) = (265,264)
 nastav. (75, 99) -> (340,363) = (265,264)
 Zvuky - (94, 50) -> (173, 82) = (79,32)
 hudba - (94,131) -> (174,159) = (80,28)
 zpet -  (100,229) ->(167,263) = (67,34)
*/

#include "berusky.h"

//#define  SPORIC_TIME  (20*18)

//int HMF = 20;
int  x_o[300], y_o[300], akt_frame_o_1,akt_frame_o_2,akt_frame_o_3,
     akt_x_1, akt_x_2, akt_x_3;
int  oci_frame[] = {14,15,16,17,18,19,20,15,14,21};
int  oci_akt = 0;
static int kresl = 0;

void inicializuj_menu(void)
{
int i;

//  HMF = 20;//cti_polozku_int(INI_FILE,"general","vrsek_menu");

  /* uvodni menu */
  hlavni_menu.pozadi.podklad = PRVNI_MENU+2;
  hlavni_menu.pozadi.x_pozice = 113+75;
  hlavni_menu.pozadi.y_pozice = HMF+99;
  hlavni_menu.pozadi.polozek = 6;
  hlavni_menu.pozadi.maz = 1;
  hlavni_menu.pozadi.bila = BILA;
  hlavni_menu.pozadi.p_fn_vstup = NULL;


/* parametry stmivani
  0 - nic nedelej
& 1 - rozsvecuj
& 2 - rozsvecuj pri prvnim vstupu
& 4 - stmivej
& 8 - nastavuj paletu
& 16 - nastaveno, potom je prvni vstup
& 32 -
*/
//  hlavni_menu.pozadi.stmivej = 2|16;
  hlavni_menu.pozadi.stmivej = 0;
  
  hlavni_menu.pozadi.dolu = K_DOLU;
  hlavni_menu.pozadi.nahoru = K_NAHORU;
  hlavni_menu.pozadi.enter = K_ENTER;
  hlavni_menu.pozadi.esc = 5;

  for(i = 0; i < hlavni_menu.pozadi.polozek; i++) {
     hlavni_menu.pol[i].x_res = 134;
     hlavni_menu.pol[i].y_res = 31;
     hlavni_menu.pol[i].x_pozice = 257;
     hlavni_menu.pol[i].y_pozice = HMF+119;
     hlavni_menu.pol[i].ne_akt = PRVNI_MENU+30+i;
     hlavni_menu.pol[i].akt = PRVNI_MENU+3+i;
     hlavni_menu.pol[i].p_menu = (GENERIC_MENU *)NULL;
     hlavni_menu.pol[i].p_fce = (void( *)(void))NULL;// exit;
  }

  hlavni_menu.pol[1].y_pozice = HMF+159;
  hlavni_menu.pol[2].y_pozice = HMF+199;
  hlavni_menu.pol[3].y_pozice = HMF+239;
  hlavni_menu.pol[4].y_pozice = HMF+279;
  hlavni_menu.pol[5].y_pozice = HMF+319;

  hlavni_menu.pol[0].p_menu = (GENERIC_MENU *)&obtiznost;
  hlavni_menu.pol[1].p_fce = (void( *)(void))zadej_heslo;
  hlavni_menu.pol[2].p_menu = (GENERIC_MENU *)&napoveda;
  hlavni_menu.pol[3].p_fce = (void( *)(void))menu_nastaveni;
  hlavni_menu.pol[4].p_fce = (void( *)(void))spust_editor;
  hlavni_menu.pol[5].p_menu = (GENERIC_MENU *)&konec;
  
  /* obtiznost */
  obtiznost.pozadi.podklad = PRVNI_MENU+9;
  obtiznost.pozadi.x_pozice = 113+135;
  obtiznost.pozadi.y_pozice = HMF+99;
  obtiznost.pozadi.polozek = 6;
  obtiznost.pozadi.maz = 1;
  obtiznost.pozadi.bila = BILA;
  obtiznost.pozadi.p_fn_vstup = NULL;

  obtiznost.pozadi.dolu =   K_DOLU;
  obtiznost.pozadi.nahoru = K_NAHORU;
  obtiznost.pozadi.enter =  K_ENTER;
  obtiznost.pozadi.esc =  5;
/* parametry stmivani
  0 - nic nedelej
& 1 - rozsvecuj
& 2 - rozsvecuj pri prvnim vstupu
& 4 - stmivej
& 8 - nastavuj paletu
& 16 - nastaveno, potom je prvni vstup
& 32 -
*/
  obtiznost.pozadi.stmivej = 0;

  for(i = 0; i < obtiznost.pozadi.polozek; i++) {
     obtiznost.pol[i].x_res = 102;
     obtiznost.pol[i].y_res = 22;
     obtiznost.pol[i].x_pozice = 275;
     obtiznost.pol[i].y_pozice = HMF+154;
     obtiznost.pol[i].ne_akt = PRVNI_MENU+24+i;
     obtiznost.pol[i].akt = PRVNI_MENU+10+i;
     obtiznost.pol[i].p_menu = (GENERIC_MENU *)NULL;
     obtiznost.pol[i].p_fce = (void( *)(void))NULL;// exit;
  }

  obtiznost.pol[0].ne_akt = PRVNI_MENU+36;
  obtiznost.pol[1].y_pozice = HMF+185;
  obtiznost.pol[2].y_pozice = HMF+214;
  obtiznost.pol[3].y_pozice = HMF+244;
  obtiznost.pol[4].y_pozice = HMF+276;
  obtiznost.pol[5].y_pozice = HMF+320;

  obtiznost.pol[5].y_res = 31;

  obtiznost.pol[0].p_fce = (void( *)(void))_1_sada;// exit;
  obtiznost.pol[1].p_fce = (void( *)(void))_2_sada;// exit;
  obtiznost.pol[2].p_fce = (void( *)(void))_3_sada;// exit;
  obtiznost.pol[3].p_fce = (void( *)(void))_4_sada;// exit;
  obtiznost.pol[4].p_fce = (void( *)(void))_5_sada;// exit;
  obtiznost.pol[5].p_menu= (GENERIC_MENU *)&hlavni_menu;
  
  /* odchodova obrazovka */
  konec.pozadi.podklad = PRVNI_MENU+22;
  konec.pozadi.x_pozice = 113+92;
  konec.pozadi.y_pozice = HMF+99;
  konec.pozadi.polozek = 2;
  konec.pozadi.maz = 1;
  konec.pozadi.bila = BILA;
  konec.pozadi.p_fn_vstup = NULL;

  konec.pozadi.dolu  = K_DOPRAVA;
  konec.pozadi.nahoru= K_DOLEVA;
  konec.pozadi.enter = K_ENTER;
  konec.pozadi.esc = 1;

/* parametry stmivani
  0 - nic nedelej
& 1 - rozsvecuj
& 2 - rozsvecuj pri prvnim vstupu
& 4 - stmivej
& 8 - nastavuj paletu
& 16 - nastaveno, potom je prvni vstup
& 32 -
*/
  konec.pozadi.stmivej = 0;

  konec.pol[0].x_res = 53;
  konec.pol[0].y_res = 25;
  konec.pol[0].x_pozice = 268;
  konec.pol[0].y_pozice = HMF+221;
  konec.pol[0].ne_akt = PRVNI_MENU+41;
  konec.pol[0].akt = PRVNI_MENU+23;
  konec.pol[0].p_menu = (GENERIC_MENU *)&hlavni_menu;
  konec.pol[0].p_fce = (void( *)(void))NULL;// exit;
  
  konec.pol[1].x_res = 36;
  konec.pol[1].y_res = 25;
  konec.pol[1].x_pozice = 348;
  konec.pol[1].y_pozice = HMF+221;
  konec.pol[1].ne_akt = PRVNI_MENU+42;
  konec.pol[1].akt = PRVNI_MENU+24;
  konec.pol[1].p_menu = (GENERIC_MENU *)&hlavni_menu;
  konec.pol[1].p_fce = (void( *)(void))NULL;// exit;

  konec.pol[0].p_menu = (GENERIC_MENU *)NULL;

  /* definice napovedy */
  napoveda.pozadi.podklad = PRVNI_MENU+17;
  napoveda.pozadi.x_pozice = 113+135;
  napoveda.pozadi.y_pozice = HMF+99;
  napoveda.pozadi.polozek = 4;
  napoveda.pozadi.maz = 1;
  napoveda.pozadi.bila = BILA;
  napoveda.pozadi.p_fn_vstup = NULL;

  napoveda.pozadi.dolu =   K_DOLU;
  napoveda.pozadi.nahoru = K_NAHORU;
  napoveda.pozadi.enter =  K_ENTER;
  napoveda.pozadi.esc = 3;
  
  for(i = 0; i < napoveda.pozadi.polozek; i++) {
     napoveda.pol[i].x_res = 101;
     napoveda.pol[i].y_res = 22;
     napoveda.pol[i].x_pozice = 272;
     napoveda.pol[i].y_pozice = HMF+167;
     napoveda.pol[i].ne_akt = PRVNI_MENU+37+i;
     napoveda.pol[i].akt = PRVNI_MENU+18+i;
     napoveda.pol[i].p_menu = (GENERIC_MENU *)NULL;
     napoveda.pol[i].p_fce = (void( *)(void))NULL;// exit;
  }
  napoveda.pol[1].y_pozice = HMF+201;
  napoveda.pol[2].y_pozice = HMF+236;
  napoveda.pol[3].y_pozice = HMF+320;

  napoveda.pol[3].y_res = 31;

  napoveda.pol[0].p_fce = (void( *)(void))napoveda_ovladani;// exit;
  napoveda.pol[1].p_fce = (void( *)(void))napoveda_pravidla;// exit;
  napoveda.pol[2].p_fce = (void( *)(void))napoveda_autori;// exit;
  napoveda.pol[3].p_menu = (GENERIC_MENU *)&hlavni_menu;


  /* dilema */
/* Souradnice pro herni menu
  Dilema menu:
  Restart   ->    (0,0)   ->  (237, 52) = (237,52)
  Nahrat    ->    (0,51)  ->  (237,105) = (237,54)
  Ulozit    ->    (0,106) ->  (237,158) = (237,52)
  Konec     ->    (0,156) ->  (237,209) = (237,53)
  Zpet      ->    (0,208) ->  (237,260) = (237,52)

  #define X_GAME (320-(237/2))
  #define Y_GAME (240-(249/2))
*/

  dilema_menu.pozadi.podklad = NENI_POZADI;
  dilema_menu.pozadi.x_pozice = X_GAME;
  dilema_menu.pozadi.y_pozice = Y_GAME;
  dilema_menu.pozadi.polozek = 5;
  dilema_menu.pozadi.maz = 1;
  dilema_menu.pozadi.bila = 16;
  dilema_menu.pozadi.p_fn_vstup = NULL;

  dilema_menu.pozadi.dolu =   K_DOLU;
  dilema_menu.pozadi.nahoru = K_NAHORU;
  dilema_menu.pozadi.enter =  K_ENTER;
  dilema_menu.pozadi.esc = 4;
  
  for(i = 0; i < dilema_menu.pozadi.polozek; i++) {
     dilema_menu.pol[i].x_res = 237;
     dilema_menu.pol[i].y_res = 52;
     dilema_menu.pol[i].x_pozice = X_GAME;
     dilema_menu.pol[i].y_pozice = Y_GAME;
     dilema_menu.pol[i].ne_akt = PRVNI_MENU+49+i;
     dilema_menu.pol[i].akt = PRVNI_MENU+44+i;
     dilema_menu.pol[i].p_menu = (GENERIC_MENU *)NULL;
     dilema_menu.pol[i].p_fce = (void( *)(void))NULL;// exit;
  }

  dilema_menu.pol[1].y_pozice = Y_GAME+51;
  dilema_menu.pol[2].y_pozice = Y_GAME+106;
  dilema_menu.pol[3].y_pozice = Y_GAME+156;
  dilema_menu.pol[4].y_pozice = Y_GAME+208;

/*
  menu v napovede
*/
  ovl1.pozadi.podklad = NENI_POZADI;
  ovl1.pozadi.x_pozice = 20;
  ovl1.pozadi.y_pozice = 20;
  ovl1.pozadi.polozek = 2;
  ovl1.pozadi.maz = 0;
  ovl1.pozadi.bila = 235;
  ovl1.pozadi.p_fn_vstup = NULL;

  ovl1.pozadi.dolu =   K_DOPRAVA;
  ovl1.pozadi.nahoru = K_DOLEVA;
  ovl1.pozadi.enter =  K_ENTER;
  ovl1.pozadi.esc = 0;
  
  ovl1.pol[0].x_res = 131;
  ovl1.pol[0].y_res = 33;
  ovl1.pol[0].x_pozice = 320-65;
  ovl1.pol[0].y_pozice = 447;
  ovl1.pol[0].ne_akt = PRVNI_MENU+66;
  ovl1.pol[0].akt = PRVNI_MENU+67;
  ovl1.pol[0].p_menu = (GENERIC_MENU *)NULL;
  ovl1.pol[0].p_fce = (void( *)(void))NULL;// exit;

  ovl1.pol[1].x_res = 17;
  ovl1.pol[1].y_res = 34;
  ovl1.pol[1].x_pozice = 320+7+66;
  ovl1.pol[1].y_pozice = 447;
  ovl1.pol[1].ne_akt = PRVNI_MENU+70;
  ovl1.pol[1].akt = PRVNI_MENU+71;
  ovl1.pol[1].p_menu = (GENERIC_MENU *)NULL;
  ovl1.pol[1].p_fce = (void( *)(void))NULL;// exit;

  ovl2.pozadi.podklad = NENI_POZADI;
  ovl2.pozadi.x_pozice = 20;
  ovl2.pozadi.y_pozice = 20;
  ovl2.pozadi.polozek = 3;
  ovl2.pozadi.maz = 0;
  ovl2.pozadi.bila = 235;
  ovl2.pozadi.p_fn_vstup = NULL;

  ovl2.pozadi.dolu =   K_DOPRAVA;
  ovl2.pozadi.nahoru = K_DOLEVA;
  ovl2.pozadi.enter =  K_ENTER;
  ovl2.pozadi.esc = 2;
  
  ovl2.pol[0].x_res = 17;
  ovl2.pol[0].y_res = 34;
  ovl2.pol[0].x_pozice = 231;
  ovl2.pol[0].y_pozice = 447;
  ovl2.pol[0].ne_akt = PRVNI_MENU+68;
  ovl2.pol[0].akt = PRVNI_MENU+69;
  ovl2.pol[0].p_menu = (GENERIC_MENU *)NULL;
  ovl2.pol[0].p_fce = (void( *)(void))NULL;// exit;

  ovl2.pol[1].x_res = 131;
  ovl2.pol[1].y_res = 33;
  ovl2.pol[1].x_pozice = 320-65;
  ovl2.pol[1].y_pozice = 447;
  ovl2.pol[1].ne_akt = PRVNI_MENU+66;
  ovl2.pol[1].akt = PRVNI_MENU+67;
  ovl2.pol[1].p_menu = (GENERIC_MENU *)NULL;
  ovl2.pol[1].p_fce = (void( *)(void))NULL;// exit;

  ovl2.pol[2].x_res = 17;
  ovl2.pol[2].y_res = 34;
  ovl2.pol[2].x_pozice = 320+7+66;
  ovl2.pol[2].y_pozice = 447;
  ovl2.pol[2].ne_akt = PRVNI_MENU+70;
  ovl2.pol[2].akt = PRVNI_MENU+71;
  ovl2.pol[2].p_menu = (GENERIC_MENU *)NULL;
  ovl2.pol[2].p_fce = (void( *)(void))NULL;// exit;

  ovl3.pozadi.podklad = NENI_POZADI;
  ovl3.pozadi.x_pozice = 20;
  ovl3.pozadi.y_pozice = 20;
  ovl3.pozadi.polozek = 2;
  ovl3.pozadi.maz = 0;
  ovl3.pozadi.bila = 235;
  ovl3.pozadi.p_fn_vstup = NULL;

  ovl3.pozadi.dolu =   K_DOPRAVA;
  ovl3.pozadi.nahoru = K_DOLEVA;
  ovl3.pozadi.enter =  K_ENTER;
  ovl3.pozadi.esc = 1;
  
  ovl3.pol[0].x_res = 17;
  ovl3.pol[0].y_res = 34;
  ovl3.pol[0].x_pozice = 231;
  ovl3.pol[0].y_pozice = 447;
  ovl3.pol[0].ne_akt = PRVNI_MENU+68;
  ovl3.pol[0].akt = PRVNI_MENU+69;
  ovl3.pol[0].p_menu = (GENERIC_MENU *)NULL;
  ovl3.pol[0].p_fce = (void( *)(void))NULL;// exit;

  ovl3.pol[1].x_res = 131;
  ovl3.pol[1].y_res = 33;
  ovl3.pol[1].x_pozice = 320-65;
  ovl3.pol[1].y_pozice = 447;
  ovl3.pol[1].ne_akt = PRVNI_MENU+66;
  ovl3.pol[1].akt = PRVNI_MENU+67;
  ovl3.pol[1].p_menu = (GENERIC_MENU *)NULL;
  ovl3.pol[1].p_fce = (void( *)(void))NULL;// exit;
}

void vykresli_menu(GENERIC_MENU * p_s)
{
    int i;

/* parametry stmivani
  0 - nic nedelej
& 1 - rozsvecuj
& 2 - rozsvecuj pri prvnim vstupu
& 4 - stmivej
& 8 - nastavuj paletu
& 16 - nastaveno, potom je prvni vstup
& 32 -
*/
    if((p_s->pozadi.stmivej & 1)||((p_s->pozadi.stmivej & 2)&&(p_s->pozadi.stmivej & 16))||
      (p_s->pozadi.stmivej & 8))
      smaz_paletu();

    if(p_s->pozadi.podklad != NENI_POZADI)
      kresli_sprit_bar(spr[p_s->pozadi.podklad],p_s->pozadi.x_pozice,p_s->pozadi.y_pozice,p_s->pozadi.bila);
    else {
      for(i = 0; i < p_s->pozadi.polozek; i++) {
         kresli_sprit_bar(spr[p_s->pol[i].ne_akt],p_s->pol[i].x_pozice,p_s->pol[i].y_pozice,p_s->pozadi.bila);
      }
    }

// Vykresleni aktivni polozky
// kresli_sprit_bar(spr[p_s->pol[0].akt],p_s->pol[0].x_pozice,p_s->pol[0].y_pozice,BILA);

/* parametry stmivani
  0 - nic nedelej
& 1 - rozsvecuj
& 2 - rozsvecuj pri prvnim vstupu
& 4 - stmivej
& 8 - nastavuj paletu
& 16 - nastaveno, potom je prvni vstup
& 32 -
*/
   if(p_s->pozadi.stmivej & 8)
      nastav_paletu(PALETA_MNU);
   if(p_s->pozadi.stmivej & 1)
      rozsvit_paletu(PALETA_MNU);

   if((p_s->pozadi.stmivej & 2)&&(p_s->pozadi.stmivej & 16)) {
      rozsvit_paletu(PALETA_MNU);
        p_s->pozadi.stmivej &= 0xef;
    }
}

int obsluz_menu(GENERIC_MENU *p_gen_menu, int pozadi, int akt_pol, int delay)
{
   dword ret_code;

   p_smenu = (GENERIC_MENU *)p_gen_menu;
   if((p_smenu->pozadi.stmivej & 8)||(p_smenu->pozadi.stmivej & 1)||(p_smenu->pozadi.stmivej & 2))
      smaz_paletu();

   if(akt_pol < p_smenu->pozadi.polozek) {
     mysi_info[1] = p_smenu->pol[akt_pol].x_pozice+(p_smenu->pol[akt_pol].x_res>>1);
     mysi_info[2] = p_smenu->pol[akt_pol].y_pozice+(p_smenu->pol[akt_pol].y_res>>1);
     souradnice_mysi(mysi_info[1],mysi_info[2]);
   }

   do {
     vypni_kurzor(mysi_info);
     vykresli_menu(p_smenu);
     if(delay)
       cekej(delay);
     zapni_kurzor(mysi_info);
     mysi_info[0] = 0;
   } while((ret_code = cekej_na_stisk()) < 10);
   vypni_kurzor(mysi_info);

   return(ret_code - 10);
}

dword cekej_na_stisk(void)
{
 int  i = 0;
 int  akt_polozka = 0;
 int  je_tam = 0;
 int  stary_x = 0, stary_y = 0;
// int  cas = dos_time+SPORIC_TIME;
 
 
/*
 if((p_smenu->pol[0].x_pozice < mysi_info[1])&&(p_smenu->pol[0].y_pozice < mysi_info[2])&&
    (p_smenu->pol[0].x_pozice+p_smenu->pol[0].x_res > mysi_info[1])&&
    (p_smenu->pol[0].y_pozice+p_smenu->pol[0].y_res > mysi_info[2])) {
*/
 if(p_smenu->pozadi.p_fn_vstup != NULL)
   p_smenu->pozadi.p_fn_vstup();

 vypni_kurzor(mysi_info);
 kresli_sprit_bar(spr[p_smenu->pol[0].akt],p_smenu->pol[0].x_pozice,p_smenu->pol[0].y_pozice,p_smenu->pozadi.bila);
 zapni_kurzor(mysi_info);
// }

// nastav_barvu_fn(MENU_FN,druha_menu_barva);

 do {
 clrkb();
 while(1) {
  /* semka prijde zobrazovani polozek pri prejizdeni bez stisku */
     je_tam = 0;
     kresli_oci();
     updatuj_samply();
/*
     if(cas < dos_time)
       hraj_demo("dem_000.dm",0);
*/
     for(i = 0; i < p_smenu->pozadi.polozek; i++) {
         if((p_smenu->pol[i].x_pozice < mysi_info[1])&&(p_smenu->pol[i].y_pozice < mysi_info[2])&&
            (p_smenu->pol[i].x_pozice+p_smenu->pol[i].x_res > mysi_info[1])&&
            (p_smenu->pol[i].y_pozice+p_smenu->pol[i].y_res > mysi_info[2])) {

            if((mysi_info[1] != stary_x)||(mysi_info[2] != stary_y)) {
                stary_x = mysi_info[1];
                stary_y = mysi_info[2];
                je_tam = 1;
            }
            break;
         }
     }

     if((je_tam != 1)&&(_a_klavesa[0] != 0)) {
          je_tam  = 0;
          i = akt_polozka;
          if(p_smenu->pozadi.dolu == _a_klavesa[0]) {
            if(++i == p_smenu->pozadi.polozek)
                 i = 0;
            _a_klavesa[0] = 0;
            je_tam = 1;
          }
          if(p_smenu->pozadi.nahoru == _a_klavesa[0]) {
             if(--i < 0)
                  i = p_smenu->pozadi.polozek-1;
             _a_klavesa[0] = 0;
             je_tam = 1;
          }
     }

     if((je_tam == 1)&&(i != akt_polozka)) {
          vypni_kurzor(mysi_info);

          hraj_sampl(MENU_SKOK,TPS,PRIORITA_MENU_SKOK);

          kresli_sprit_bar(spr[p_smenu->pol[akt_polozka].ne_akt],p_smenu->pol[akt_polozka].x_pozice,p_smenu->pol[akt_polozka].y_pozice,p_smenu->pozadi.bila);
          kresli_sprit_bar(spr[p_smenu->pol[i].akt],p_smenu->pol[i].x_pozice,p_smenu->pol[i].y_pozice,p_smenu->pozadi.bila);

          zapni_kurzor(mysi_info);
          akt_polozka = i;
      }

     if((getkba() == p_smenu->pozadi.enter)||(mysi_info[0] != 0))
         if(i < p_smenu->pozadi.polozek)
              break;

     if(getkba() == K_ESC) {
       i = p_smenu->pozadi.esc;
       break;
     }
  }
 /************************************************************************
  * Bod zlomu
  ************************************************************************/
 hraj_sampl(MENU_KLIK,TPS,PRIORITA_MENU_KLIK);
 updatuj_samply();

 // [1] = x, [2] = y
 mysi_info[0] = 0;


    if(p_smenu->pol[i].p_menu != NULL) {
       if(p_smenu->pozadi.stmivej & 4) {
          smaz_paletu();
       }
      p_smenu = p_smenu->pol[i].p_menu;
      return(0);
   }
   else
      if(p_smenu->pol[i].p_fce != NULL) {
          if(p_smenu->pozadi.stmivej & 4) {
            smaz_paletu();
          }
         p_smenu->pol[i].p_fce();
         return(0);
      }
      else  {
         if(p_smenu->pozadi.stmivej & 4) {
            smaz_paletu();
         }
        return(10+i);
      }
/*     }
 }*/
 zapni_kurzor(mysi_info);
 }while(1);
}

void spust_oci(void)
{
 int i;

 for(i = 0; i < 300; i++) {
     y_o[i] = random()%470;

    if(y_o[i] > 409+20)
      x_o[i] = random()%(640-12);
    else {
      x_o[i] = random()%113;
      if(random()%2)
        x_o[i] += 405+110;
    }
 }
 akt_frame_o_1 = 0;
 akt_x_1 = 0;
 akt_frame_o_2 = 3;
 akt_x_2 = 1;
 akt_frame_o_3 = 6;
 akt_x_3 = 3;
 oci_akt = 1;
 kresl = dos_time;
}

void stop_oci(void)
{
 oci_akt = NULL;
}

#define oci_kres(akt_frame_o,akt_x)                                        \
{                                                                          \
 if(akt_frame_o > 9) {                                                     \
   if((mysi_info[1] > x_o[akt_x]-X_MYSI)&&(mysi_info[1] < x_o[akt_x]+12)&& \
      (mysi_info[2] > y_o[akt_x]-Y_MYSI)&&(mysi_info[2] < y_o[akt_x]+10)) {\
      vypni_kurzor(mysi_info);                                             \
      t = 1;                                                               \
   }                                                                       \
   vypln_ctverec(0,x_o[akt_x],y_o[akt_x],12,10);                           \
   if(t)                                                                   \
     zapni_kurzor(mysi_info);                                              \
                                                                           \
   akt_frame_o = 0;                                                        \
   akt_x += 3;                                                             \
 }                                                                         \
                                                                           \
 if(akt_x > 299) {                                                         \
   akt_x = 0;                                                              \
 }                                                                         \
                                                                           \
 if((mysi_info[1] > x_o[akt_x]-X_MYSI)&&(mysi_info[1] < x_o[akt_x]+12)&&   \
    (mysi_info[2] > y_o[akt_x]-Y_MYSI)&&(mysi_info[2] < y_o[akt_x]+10)) {  \
                                                                           \
    vypni_kurzor(mysi_info);                                               \
    t = 1;                                                                 \
 }                                                                         \
 kresli_sprit(spr[oci_frame[akt_frame_o++]],x_o[akt_x],y_o[akt_x]);        \
                                                                           \
 if(t)                                                                     \
   zapni_kurzor(mysi_info);                                                \
}                                                                          \


void kresli_oci(void)
{
 #define X_MYSI 18
 #define Y_MYSI 18
 int t = 0;

 if(oci_akt == NULL)
   return;

 if(kresl > dos_time)
   return;

 oci_kres(akt_frame_o_1,akt_x_1);
 oci_kres(akt_frame_o_2,akt_x_2);
 oci_kres(akt_frame_o_3,akt_x_3);

 kresl += 3;
}

