/*
         .���� �    � ����.     �    � �����.  ����� .����. �    �
        .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
        �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
      .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
     .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
     �.      � �    � �      .� �   .� �    .� ����� .����. �    �
     (c) 1998 AnakreoN Freeware

   Berusky (C) Komat za Anakreon fwp. 1997-1998-1999-?

   Soubor:     sound.c  - zvukovy modul
   Autor:      Komat za Anakreon
   Datum:      furt se opravuje
   Verze:      release 0.0
   Prekladac:  djgpp -> gcc + nasm
   Nakladac:   Zetor BS-564 Mk III C

*/
#include "berusky.h"

#define  DEF_REFRESH 60

#ifndef VYPIS
#define VYPIS stdout
#endif

#define NO_SOUND     0x4

MIDASmodule           a_mod_handle = NULL;
MIDASmodulePlayHandle a_mod_play_handle = NULA;
dword FPS;

dword music  = 0;
dword samply = 0;

int  vol_music = 15;
int  vol_sound = 55;

static MIDASsample  smp[100];

volatile dword kresli     = 0;
volatile dword kresli_int = 0;
volatile dword dos_time   = 0;
volatile dword mezicas    = 0;
volatile dword mezi_back  = 0;

dword refresh;

static _ZVUKY tbas[MAX_AKT_SAMPLU];

void zapni_midas(void)
{
 dword *p_int,i;
 byte buffer[501];

 if(nahraj_soubor(500,buffer,F_CONFIG,0) == NULL) {
   music = 0;
   samply = 0;
 }
 else {
   if(buffer[0] == NO_SOUND) { // NoSound
     music = 0;
     samply = 0;
   }
   else {
     p_int = (dword *)(buffer+28);
     vol_sound = *p_int;
     vol_music = *(++p_int);
#ifdef __AMIGA__
     vol_sound = SWAP32LE(vol_sound);
     vol_music = SWAP32LE(vol_music);
#endif
   }
 }

 if(!MIDASstartup())
   MIDASerror();

 if(!MIDASloadConfig("berusky.cfg")) {
//   fprintf(stderr,"Nemuzu nacist berusky.cfg, hudba & zvuky vypnuty...\n");
   music = 0;
   samply = 0;
 }

 refresh = MIDASgetDisplayRefreshRate();

 if(refresh > 119000)
   refresh /= 2;
 if(refresh > 119000)
   refresh /= 2;

 if(refresh == NULL) {
   refresh = DEF_REFRESH*1000;
//   fprintf(stderr,"Neni refresh...\n");
 }


 if(!MIDASinit())
   MIDASerror();
 if(!MIDASstartBackgroundPlay(0))
   MIDASerror();

 if(MIDASsetTimerCallbacks(refresh,TRUE,NULL,NULL,tik_30) == FALSE) {
   tiskni_chybu("Nemohu instalovat timer !",__LINE__);
 }
 mezi_back = mezicas = refresh/(TPS*1000);
 FPS = refresh/1000;
 
 uprav_kanaly();

 if(samply)
   nahraj_samply();

 for(i = 0; i < MAX_AKT_SAMPLU; i++) {
   tbas[i].aktivni = 0;
   tbas[i].akce = 0;
   tbas[i].hlasitost = 0;
   tbas[i].cislo_samplu = 0;   // pro play sampl
   tbas[i].play_handle = NULL;    // pro stop sampl
   tbas[i].do_stop = 0;
 }
}


void vypni_midas(void)
{
 if(!MIDASstopBackgroundPlay())
   MIDASerror();
 if(!MIDASclose())
   MIDASerror();
}


void hraj_modul(int mod, int loop)
{
 byte pom[100];

 if(!music)
   return;

 if(a_mod_play_handle != NULL) {
   stop_modul();
 }

// testuj_pamet("pred loadem modulu");
// printf("nahravam modul c. %d\n",mod);

 sprintf(pom,"mod\\x_%.3d.xm",mod);
 if((a_mod_handle = MIDASloadModule(pom)) == NULL) {
   MIDASerror();
 }

 uprav_kanaly();

 if((a_mod_play_handle = MIDASplayModule(a_mod_handle,TRUE)) == NULL)
    MIDASerror();

// testuj_pamet("uz hraje");
 
 MIDASsetMusicVolume(a_mod_play_handle,vol_music);
}

void stop_modul(void)
{
 if(!music)
   return;

 if(a_mod_play_handle == NULL) {
//   printf("Chyba uvolneni...\n");
   return;
 }
// testuj_pamet("pred stop+uvolneni");

 if(!MIDASstopModule(a_mod_play_handle))
   MIDASerror();

 if(!MIDASfreeModule(a_mod_handle))
   MIDASerror();

// testuj_pamet("po uvolneni");

 a_mod_play_handle = NULL;
 a_mod_handle = NULL;
}

void hraj_hudbu_levelu(void)
{
 #define LEVEL_HUDEB_OK  26
 #define LEVEL_HUDEB_LOW 22

 static int ok_hudby[60] = {0,1,2,4,3,5,6,7,8,9,10,11,12,13,14,15,
                            16,17,18,19,20,21,22,23,24,25,0,0,0,0};
 static int low_hudby[60] = {0,1,2,4,3,5,6,7,8,11,12,14,16,17,18,
                             19,20,21,22,23,24,25,0,0,0,0};
 static int hrali[60] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
 static int *p_hudby;
 int i,j = 0,
     skladeb;


 stop_modul();

 if(low_mem) {
   p_hudby = low_hudby;
   skladeb = LEVEL_HUDEB_LOW;
 }
 else {
   p_hudby = ok_hudby;
   skladeb = LEVEL_HUDEB_OK;
 }

 i = random()%skladeb;

 while(hrali[i] != 0) {
   if(++i >= skladeb)
     i = 0;
   j++;
   if(j >= skladeb) {
     i = 0;
     for(j = 0; j < skladeb; j++)
        hrali[j] = 0;
   }
 }

 hrali[i] = 1;
 hraj_modul(p_hudby[i],TRUE);
}

void hraj_hudbu_menu(void)
{
 /* 1,11,13,14,16,19,28
 */
 #define MENU_HUDEB 5

 static int hudby[30] = {4,5,6,7,8};
 static int hrali[30] = {0,0,0,0,0};
 int i,j = 0;

 stop_modul();

 i = random()%MENU_HUDEB;
 while(hrali[i] != 0) {
   if(++i >= MENU_HUDEB)
     i = 0;
   j++;
   if(j >= MENU_HUDEB) {
     i = 0;
     for(j = 0; j < MENU_HUDEB; j++)
        hrali[j] = 0;
   }
 }

 hrali[i] = 1;
 hraj_modul(hudby[i],TRUE);

}

void hraj_hudbu_credits(void)
{
 /* 8,17,27
 */
 #define CREDIT_HUDEB 4

 static int hudby[30] = {0,1,2,3};
 static int hrali[30] = {0,0,0,0};
 int i,j = 0;

 stop_modul();

 i = random()%CREDIT_HUDEB;
 while(hrali[i] != 0) {
   if(++i >= CREDIT_HUDEB)
     i = 0;
   j++;
   if(j >= CREDIT_HUDEB) {
     i = 0;
     for(j = 0; j < CREDIT_HUDEB; j++)
        hrali[j] = 0;
   }
 }

 hrali[i] = 1;
 hraj_modul(hudby[i],TRUE);

}

void hraj_hudbu_intro(void)
{
 #define INTRO_MOD 26

 hraj_modul(INTRO_MOD,FALSE);
}

void hraj_hudbu_outro(int cislo)
{
 hraj_modul(cislo,TRUE);
}

void uprav_kanaly(void)
{
 MIDASmoduleInfo m_info;
 int kanalu = 0;

 if(!music&&!samply)
   return;

 if((a_mod_handle != NULL)&&(music)) {
   if(!MIDASgetModuleInfo(a_mod_handle,&m_info))
     MIDASerror();
   kanalu = m_info.numChannels;
 }

 if(samply)
   MIDASfreeAutoEffectChannels();

 MIDAScloseChannels();

 if(!MIDASopenChannels(kanalu + SAMPL_KANALU))
    MIDASerror();

 if(samply) {
   if(!MIDASallocAutoEffectChannels(SAMPL_KANALU))
      MIDASerror();
 }
}



MIDASsamplePlayHandle play_sampl(int cislo, int prior)
{
  MIDASsamplePlayHandle hand;

  if(!samply)
    return(NULL);

  if((hand = MIDASplaySample(smp[cislo],MIDAS_CHANNEL_AUTO,
     prior, RATE,vol_sound, MIDAS_PAN_MIDDLE)) == 0) {
     MIDASerror();
  }
  return(hand);
}

void nahraj_samply(void)
{
 #define MAX_DELKA_SAMPLU 200000

 int i;
 byte file[100];

 for(i = 0; i < SAMPLU; i++) {
   sprintf(file,"smp\\smp_%.3d.raw",i);

   if((smp[i] = MIDASloadRawSample(file,MIDAS_SAMPLE_16BIT_MONO,MIDAS_LOOP_NO)) == 0)
     MIDASerror();
 }
}


void MIDASerror(void)
{
  int error;

  prepni_do_textu(0x3);

  error = MIDASgetLastError();

  printf("\n\nMIDAS error: %s\n", MIDASgetErrorMessage(error));
  if ( !MIDASclose() )
  {
      printf("\nBIG PANIC! MIDASclose Failed: %s\n",
             MIDASgetErrorMessage(MIDASgetLastError()));
  }
  exit(0);
}
/*
typedef struct __zvuky {

  dword aktivni;   // 0 - nic nedelej, 1 - proved akci
  dword akce;      // 0 - stop samplu,
                   // 1 - hraj sampl,
                   // 2 - uprav hlasitost
  dword hlasitost;

  dword                 cislo_samplu;   // pro play sampl
  MIDASsamplePlayHandle play_handle;    // pro stop sampl

} _ZVUKY;
*/

dword hraj_sampl(dword cislo, int do_stop, int prior) // cislo prehravaneho samplu
{ // tato rutina automaticky zaregistruje sampl do struct.
 int i;

 for(i = 0; i < MAX_AKT_SAMPLU; i++) {
    if(!tbas[i].aktivni) {
      tbas[i].akce = START_SMP;
      tbas[i].cislo_samplu = cislo;
      tbas[i].aktivni = 1;
      tbas[i].prior = prior;
      if(do_stop != NULL)
        tbas[i].do_stop = do_stop+kresli;
      return(i);
    }
 }
 return(CHYBA);
}

void stop_sampl(dword handle) // handle ve strukture samplu
{
 tbas[handle].akce = STOP_SMP;
 tbas[handle].aktivni = 1;
 tbas[handle].do_stop = NULL;
}

void updatuj_samply(void)
{
 int i;

 for(i = 0; i < MAX_AKT_SAMPLU; i++) {
    if(tbas[i].aktivni) {
      switch(tbas[i].akce) {
        case STOP_SMP:
          if(tbas[i].do_stop == NULL) {
            MIDASstopSample(tbas[i].play_handle);
            tbas[i].aktivni = 0;
            tbas[i].akce = NULL;
          }
          else {
            if(tbas[i].do_stop <= kresli)
              tbas[i].do_stop = NULL;
          }
          break;

        case START_SMP:
          tbas[i].play_handle = play_sampl(tbas[i].cislo_samplu,tbas[i].prior);

          if(tbas[i].do_stop == NULL)
            tbas[i].akce = NULL;
          else
            tbas[i].akce = STOP_SMP;
          break;

        case HLAS_SMP:        
          break;

        default:
          break;
      }
    }
 }
}


