/*
         .���� �    � ����.     �    � �����.  ����� .����. �    �
        .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
        �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
      .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
     .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
     �.      � �    � �      .� �   .� �    .� ����� .����. �    �
     (c) 1998 AnakreoN Freeware

   Berusky (C) Komat za Anakreon fwp. 1997-1998-1999-?

   Soubor:     animace.c  - animacni modul
   Autor:      Komat za Anakreon
   Datum:      furt se opravuje
   Verze:      release 0.0
   Prekladac:  djgpp -> gcc + nasm
   Nakladac:   Zetor BS-564 Mk III C

*/
/*
   C k animacim
*/
#include "libanak.h"
#include "berusky.h"
#include "animace.h"
#ifndef __AMIGA__
#include <pc.h>
#endif

extern byte *spr[2000];
extern volatile dword kresli;
extern volatile dword kresli_int;

volatile ANIM_FILE *p_an[MAX_ANIM];
volatile byte      *p_rotacni;
volatile int posl_anim = 0;
volatile int animace_aktivni = 0;

ZMENY pole_zmen[CELKEM_ZMEN];

void init_anim(void)
{
 int i;

 for(i = 0; i < MAX_ANIM; i++)
    p_an[i] = NULL;

 nastav_res_animaci(X,Y);
}

byte * nastav_res_animaci(int x_res, int y_res)
{

 if(!p_rotacni)
   zrus_sprit(p_rotacni);

 return(p_rotacni = vyrob_sprit(x_res,y_res,8,LOCK));
}


void smaz_pozadi(int handle)
{
 if(p_an[handle] != NULL)
   p_an[handle]->pozadi = 0;
}

void nastav_souradnice_anim(int handle, int x, int y, int x_baze, int y_baze)
{
 if(p_an[handle] != NULL) {
   p_an[handle]->x = x;
   p_an[handle]->y = y;
   p_an[handle]->x_baze = x_baze;
   p_an[handle]->y_baze = y_baze;
 }
}

//vrati pointer na animaci
ANIM_FILE * vyrob_animaci(int framu, int loop, int wr, int buffer)
{

 ANIMACNI_FRAME *p_frame;
 ANIM_FILE      *p_anim;
 int            i;

 if ((p_frame = malloc(framu*sizeof(ANIMACNI_FRAME))) == NULL)
    return(NULL);

 _go32_dpmi_lock_data(p_frame,framu*sizeof(ANIMACNI_FRAME));


 if ((p_anim = malloc(sizeof(ANIM_FILE))) == NULL) {
    free(p_frame);
    return(NULL);
 }
 _go32_dpmi_lock_data(p_anim,sizeof(ANIM_FILE));


 p_anim->celkem_framu = framu;
 p_anim->loop = loop;
 p_anim->wr = wr;
 p_anim->p_frame = p_frame;

 for ( i = 0; i < framu; i++) {
     p_anim->p_frame[i].cs = 0;
     p_anim->p_frame[i].x_kor = 0;
     p_anim->p_frame[i].y_kor = 0;
     p_anim->p_frame[i].rotace = 0;
     p_anim->p_frame[i].delay = 0;
 }

 return(p_anim);
}

/* ID animace -> pokud je p_anim == NULL, pak pouziju handle

*/
void dopln_framy(int handle, dword *p_sprity, int *p_x_kor, int *p_y_kor, int *p_rotace, dword *p_delay)
{
 int i;

 if(p_an[handle] == NULL)
   return;

 for(i = 0; i < p_an[handle]->celkem_framu; i++) {
     p_an[handle]->p_frame[i].cs = p_sprity[i];
     p_an[handle]->p_frame[i].x_kor = p_x_kor[i];
     p_an[handle]->p_frame[i].y_kor = p_y_kor[i];
     p_an[handle]->p_frame[i].rotace = p_rotace[i];
     p_an[handle]->p_frame[i].delay = p_delay[i];
 }
}

int pridej_pozadi(int handle, int pozadi, byte *p_sprit, int x, int y)
{
 int cp;

 if(p_an[handle] == NULL)
   return(0);

 if(p_an[handle]->pozadi >= MAX_ANIM_POZADI)
   return(0);

 cp = p_an[handle]->pozadi++;

 p_an[handle]->poz[cp].pozadi = pozadi;
 p_an[handle]->poz[cp].p_sprit = p_sprit;
 p_an[handle]->poz[cp].x = x;
 p_an[handle]->poz[cp].y = y;

 return(cp+1);
}

int registruj_animaci(ANIM_FILE *p_anim,int x_res,int y_res, byte **p_sprt, volatile int *p_casovac, byte barva)
{
 int handle;

 /* Najde prvni volny slot pro animaci */
 for(handle = 0; handle < MAX_ANIM; handle++) {
    if(p_an[handle] == NULL)
      break;
 }

 if(handle == MAX_ANIM)
   return(CHYBA);

 p_an[handle] = p_anim;

 p_an[handle]->pozadi = 0;
 if((p_an[handle]->p_buffer = vyrob_sprit(x_res,y_res,8,LOCK)) == NULL) {
   exit(ANIM_CHYBA+1);
 }

 p_an[handle]->p_spr = p_sprt;
 p_an[handle]->p_cas = p_casovac;
 p_an[handle]->barva = barva;

 p_an[handle]->akt_frame = 0;
 p_an[handle]->cas = 0;
 p_an[handle]->play = 0;
 p_an[handle]->update = 1;
 p_an[handle]->zmena = 0xffff;
 p_an[handle]->cas_zmeny = 0;

 if(handle > posl_anim)
   posl_anim += handle-posl_anim;

 if(handle == posl_anim)
   posl_anim++;

 return(handle);
}

void updatuj_animace(void)
{
 /* proskenuje pole animaci a updatuje animace
    delay a pod veci zabudovat do predchozich funkci.
 */
 static int i,j;
 static int a_frame;
 static ANIMACNI_FRAME *p_frm;
 static STR_POZADI *p_poz;
 static ANIM_FILE *p_akt;


 if(!animace_aktivni)
   return;

 for(i = 0; i < posl_anim; i++) {

    if((p_akt = p_an[i]) == NULL)
      continue;

    if(p_akt->play == 2) {
      p_akt->play = 0;
      proved_zmenu(p_akt->zmena);
      continue;
    }

    if((p_akt->play)&&(p_akt->update)) {

      p_frm = p_akt->p_frame;
      a_frame = p_akt->akt_frame;

      konec_anim:
      if(a_frame == p_akt->celkem_framu)
         continue;

      for(j = 0; j < p_akt->pozadi; j++) {
         p_poz = p_akt->poz+j;
         if(p_poz->pozadi)   // je-li to pozadi, updatuj ho
            buf_uloz_sprit(p_akt->p_buffer,p_poz->x,p_poz->y,p_poz->p_sprit);
         else  // je-li to sprit, nakresli jej na urcenou pozici
            buf_kresli_sprit_bar(p_poz->p_sprit,p_poz->x,p_poz->y,p_akt->barva,p_akt->p_buffer);
      }

      dalsi_fram:
//      fprintf(stderr,"kreslim fram %d\n",a_frame);
      if(!p_frm[a_frame].rotace) {
        buf_kresli_sprit_bar(p_akt->p_spr[p_frm[a_frame].cs],
                             p_akt->x_baze+p_frm[a_frame].x_kor,
                             p_akt->y_baze+p_frm[a_frame].y_kor,
                             p_akt->barva,
                             p_akt->p_buffer);
      }
      else {
        rotuj_sprit(p_akt->p_spr[p_frm[a_frame].cs],
                    p_rotacni,
                    p_frm[a_frame].rotace);

        buf_kresli_sprit_bar(p_rotacni,
                             p_akt->x_baze+p_frm[a_frame].x_kor,
                             p_akt->y_baze+p_frm[a_frame].y_kor,
                             p_akt->barva,
                             p_akt->p_buffer);
      }


      if(p_frm[a_frame].delay == 0) {
        p_akt->akt_frame = ++a_frame;
        if(a_frame >= p_akt->celkem_framu)
          goto konec_anim;
        else
          goto dalsi_fram;
      }
      p_akt->update = 0;
   }
 }
}

/*
typedef struct { // Hlavicka jednotliveho framu

  int cs;     // cislo spritu
  int x_kor;  // relativni pozice x k predchozimu framu
  int y_kor;  // relativni poloha y k poslednimu framu
  int rotace; // rotace tohoto framu
  int delay;  // delka cekani do dalsiho framu, 0 - hned na dalsi

} ANIMACNI_FRAME;

typedef struct {

  //----------------------------------------------------------

  int  celkem_framu;    // pocet framu
  int  loop;            // opakovat animaci
  int  wr;              // cekani na retrace ?

  int  posl_x;
  int  posl_y;

  int  x_baze;   // defaultni souradnice animace ve spritu/obrazovce
  int  y_baze;


  ANIMACNI_FRAME  *p_frame;

  //--------------------------------------------------------

  int pozadi;// pocet pouzitych pozadi
  STR_POZADI  poz[MAX_ANIM_POZADI];

  //---------------------------------------------------------

  byte barva;
  byte *p_buffer;// sprit do ktereho se bude kreslit animace
                 // animacni engine si tento sprite bude delat na zacatku
                 // zalozeni animace a pri skonceni jej uvolni

  byte **p_spr;  // Ukazatel na prvni sprit

  int  akt_frame;// Frame ktery se ma ted prehrat
  int  x;        // x - souradnice animace
  int  y;        // y - souradnice animace

  //-------------------------------------------------------------------

  int  cas;      // tato p_cas - promena se bude porovnavat s delay
  volatile int *p_cas;

  int  play;     // 0 - nehraje, 1 - hraje
  int  update;   // pri kazdym animacnim okne se nastavi na 1 a kresleni to smaze

  int  samply_akt; // NULL -> neni sampl
  int  start_sampl; // NENI_SAMPL -> neni sampl
  int  handle;
  int  stop_sampl; // NENI_SAMPL -> neni sampl
  int  handle;

} ANIM_FILE;
*/

/*
         mov  dx,0x3da   ;dx <- 3dah

.cekej_na_konec:  ; Pokud je prave retrace tak pockej na jeho konec
         in   al,dx     ;al <- port[3dah]                                     
         and  al,0x08    ;tesuje bit 8 v al
         jnz  .cekej_na_konec ;cekej na 0

.cekej_na_beh:   ;  Cekej na zacatek behu
         in   al,dx     ;al <- port[3dah]                                     
         and  al,0x08    ;testuje bit 8 v al
         jz   .cekej_na_beh ;cekej na 1
*/
/*
 cekej_na_paprsek();
 if((!konec_ref)&&(inportb(0x3da)&0x80))
   konec_ref = 1;
*/

void kresli_animace(void)
{
 static int i;
 static int a_frame;
 static ANIMACNI_FRAME *p_frm;
 static ANIM_FILE      *p_akt;

 if(!animace_aktivni)
   return;

 kresli_int++;

 for(i = 0; i < posl_anim; i++) {
    if((p_akt = p_an[i]) == NULL)
      continue;

    if((p_akt->play)&&(!p_akt->update)) {
   
      p_frm = p_akt->p_frame;
      a_frame = p_akt->akt_frame;
   
      if(*p_akt->p_cas - p_akt->cas >= p_frm[a_frame].delay) {
        p_akt->cas += p_frm[a_frame].delay;
   
        kresli_sprit_bar(p_akt->p_buffer,p_akt->x,p_akt->y,p_akt->barva);
   
        p_akt->akt_frame = ++a_frame;
   
        if(a_frame == p_akt->celkem_framu) {
           p_akt->akt_frame = a_frame = 0;
           if(p_akt->loop) {
             p_akt->play = 1;
           }
           else {
             if(p_akt->zmena != 0xffff)
               p_akt->play = 2;
             else
               p_akt->play = 0;
           }
        }
        p_akt->update = 1;
      }
    }
 }
}

/*
// Player

//  Zpusob prace s pozadim:
//  pozadi = 1;

//  buf_uloz_sprit(z pozadi,x,y, do animace);

//  pozadi = 0;
//  buf_kresli_sprit_bar( z pozadi do animace na souradnice v animaci x,y);

// Provozni struktura pri prehravani animace
// pole techto struktur a vracet od nich handle na jednotlivou animaci
// toto bude plnit nejaka fce a nebo rovnou pred pred prehranim...

Kresleni animace -> kresli_sprit p_buffer na pozici x,y

Kreslit se to bude pravidelne vsechny animace
a budou se jen updatovat,
a nebo tam bude flag -> update
*/



int play_anim(int handle)
{
 if(handle >= MAX_ANIM)
   return(ANIM_CHYBA);

 if(p_an[handle] == NULL)
   return(ANIM_CHYBA);

 // fprintf(stderr,"-> nastavuji cas %d\n",*p_an[handle]->p_cas);
 p_an[handle]->cas = *p_an[handle]->p_cas;
 p_an[handle]->play = 1;
 p_an[handle]->update = 1;
 p_an[handle]->akt_frame = 0;
 return(1);
}

int stop_anim(int handle)
{
 if(handle >= MAX_ANIM)
   return(ANIM_CHYBA);
 if(p_an[handle] == NULL)
   return(ANIM_CHYBA);
 p_an[handle]->play = 0;
 return(A_OK);
}

int status_anim(int handle)
{
 if(handle >= MAX_ANIM)
   return(ANIM_CHYBA);

 if(p_an[handle] == NULL)
   return(ANIM_CHYBA+1);

 return(p_an[handle]->play);
}

int stop_vsechno(void)
{
 int i;

 for(i = 0; i < MAX_ANIM; i++)
    stop_anim(i);

 return(A_OK);
}


int uvolni_animaci(int handle)
{
// fprintf(stderr,"uvolnuju animaci %d, posledni %d, max %d\n",handle,posl_anim,MAX_ANIM);
 if(p_an[handle] == NULL)
   return(ANIM_CHYBA);

 zrus_sprit(p_an[handle]->p_buffer);
 free(p_an[handle]->p_frame);
 free(p_an[handle]);
 p_an[handle] = NULL;
 return(A_OK);
}

int uvolni_vsechny_animace(void)
{
 int i;

 animace_aktivni = 0;
 stop_vsechno();
 for(i = 0; i < MAX_ANIM; i++)
    uvolni_animaci(i);

 return(0);
}

void aktivuj_animace(void)
{
// kresli = zal_kresli;
 animace_aktivni = 1;
}

void vypni_animace(void)
{
// zal_kresli = kresli;
 animace_aktivni = 0;
}

int pridej_zmenu(dword handle, dword x, dword y, dword prvek, dword prekresli, dword dvere, dword akce, dword cislo1, dword cislo2)
{
 int i;

 if(p_an[handle] == NULL)
   return(ANIM_CHYBA);

 for(i = 0; i < CELKEM_ZMEN; i++) {
    if(!pole_zmen[i].aktivni) {
      pole_zmen[i].aktivni = 1;
      pole_zmen[i].x = x;
      pole_zmen[i].y = y;
      pole_zmen[i].prvek = prvek;
      pole_zmen[i].handle = handle;
      pole_zmen[i].prekresli = prekresli;
      pole_zmen[i].dvere = dvere;
      pole_zmen[i].akce = akce;
      pole_zmen[i].cislo1 = cislo1;
      pole_zmen[i].cislo2 = cislo2;
      p_an[handle]->zmena = i;
      p_an[handle]->cas_zmeny = 0;
      return(i);
    }
 }
 return(CHYBA);
}

int proved_zmenu(int i)
{
  if(pole_zmen[i].aktivni) {
    pole_zmen[i].aktivni = 0;
    p_an[pole_zmen[i].handle]->zmena = 0xffff;
    pl.level[pole_zmen[i].y][pole_zmen[i].x][PRVEK] = pole_zmen[i].prvek;

    if(pole_zmen[i].prekresli)
      kresli_prvek_levelu(&pl,pole_zmen[i].x,pole_zmen[i].y);

    if(pole_zmen[i].dvere)
      animuj_dvere(pole_zmen[i].x,pole_zmen[i].y);

    /*   Zmeny pro samply  */
    if(pole_zmen[i].akce == STOP_SMP)
      stop_sampl(pole_zmen[i].cislo1);
    if(pole_zmen[i].akce == START_SMP)
      pole_zmen[i].cislo2 = hraj_sampl(pole_zmen[i].cislo1,pole_zmen[i].cislo2,PRIORITA_ZAVRENI);
      
    return(i);
  }
  return(A_CHYBA);
}

