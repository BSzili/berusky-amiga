/*
         .���� �    � ����.     �    � �����.  ����� .����. �    �
        .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
        �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
      .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
     .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
     �.      � �    � �      .� �   .� �    .� ����� .����. �    �
     (c) 1998 AnakreoN Freeware

   Berusky (C) Komat za Anakreon fwp. 1997-1998-1999-?

   Soubor:     sound.h  - zvukovy modul
   Autor:      Komat za Anakreon
   Datum:      furt se opravuje
   Verze:      release 0.0
   Prekladac:  djgpp -> gcc + nasm
   Nakladac:   Zetor BS-564 Mk III C

*/

#include "midasdll.h"
#include "zakl_typ.h"

#define PRVNI_SAMPL_KANAL 0
#define SAMPL_KANALU      8
#define MAX_MODU          15
#define SAMPLU            20
#define MAX_AKT_SAMPLU    50

#define RATE            20050
#define PRIORITA        0

#define TPS             18

/*
 * Definice rozlozeni zvuku
 */

#define MODRY_BLESK     0
#define ZELENY_BLESK    1
#define MODRY_KAMEN     2
#define ZELEZNY_KAMEN   3
#define KROKY_BLATO     4
#define KROKY_MRAMOR    5
#define KROKY_POZADI    6
#define MENU_SKOK       7
#define MENU_KLIK       8
#define NARAZ           9
#define ODEMYK          10
#define OTEVRENI_EXITU  11
#define OTEVRENI_DVERI  12
#define POSUN           13
#define SEBRANI         14
#define VYBUCH          15
#define ZAVRENI         16
#define SAMPL_OK        17
#define PREPNI          18
#define LISTA           19

/*
  Definice delek samplu
*/

#define DELKA_MODRY_BLESK     FPS
#define DELKA_MODRY_KAMEN     (9*4)
#define DELKA_ZELEZNY_KAMEN   (9*4)
#define DELKA_KROKY_1         (HR_FRAMU*1)
#define DELKA_KROKY_2         (HR_FRAMU*2)
#define DELKA_MENU_SKOK       FPS
#define DELKA_MENU_KLIK       FPS
#define DELKA_NARAZ           FPS
#define DELKA_POSUN_1         (HRB_FRAMU*1)
#define DELKA_POSUN_2         (HRB_FRAMU*2)
#define DELKA_ODEMYK          FPS
#define DELKA_OTEVRENI_EXITU  FPS
#define DELKA_OTEVRENI_DVERI  FPS

#define DELKA_SEBRANI         FPS
#define DELKA_VYBUCH          48
#define DELKA_ZAVRENI         FPS
#define DELKA_SAMPL_OK        (2*FPS)
#define DELKA_PREPNI          FPS

/*
  Priorita samplu
*/
#define PRIORITA_MODRY_BLESK     1
#define PRIORITA_MODRY_KAMEN     2
#define PRIORITA_ZELEZNY_KAMEN   2
#define PRIORITA_KROKY           0
#define PRIORITA_MENU_SKOK       1
#define PRIORITA_MENU_KLIK       1
#define PRIORITA_NARAZ           0
#define PRIORITA_ODEMYK          1
#define PRIORITA_OTEVRENI_EXITU  2
#define PRIORITA_OTEVRENI_DVERI  2
#define PRIORITA_POSUN           1
#define PRIORITA_SEBRANI         3
#define PRIORITA_VYBUCH          3
#define PRIORITA_ZAVRENI         2
#define PRIORITA_SAMPL_OK        3
#define PRIORITA_PREPNI          1


#define NENI_SAMPL            0xffff

#define STOP_SMP        1
#define START_SMP       2
#define HLAS_SMP        3

typedef struct __zvuky {

  dword aktivni;   // 0 - nic nedelej, 1 - proved akci
  dword akce;      // 0 - stop samplu,
                   // 1 - hraj sampl,
                   // 2 - uprav hlasitost
  dword hlasitost;

  dword                 cislo_samplu;   // pro play sampl
  MIDASsamplePlayHandle play_handle;    // pro stop sampl

  dword do_stop;
  dword prior;

} _ZVUKY;

extern _ZVUKY tbas[MAX_AKT_SAMPLU];

extern MIDASmodule a_mod_handle;
extern MIDASmodulePlayHandle a_mod_play_handle;

extern MIDASsample smp[100];

extern dword music;
extern dword samply;

extern int   vol_music;
extern int   vol_sound;

extern dword refresh;
extern dword FPS;

void zapni_midas(void);
void vypni_midas(void);
void hraj_modul(int mod, int loop);
void stop_modul(void);
void nahraj_samply(void);
void MIDASerror(void);
void uprav_kanaly(void);

MIDASsamplePlayHandle play_sampl(int cislo, int prior);
int  stop_h_sampl(int cislo);

void hraj_hudbu_levelu(void);
void hraj_hudbu_menu(void);
void hraj_hudbu_credits(void);
void hraj_hudbu_intro(void);
void hraj_hudbu_outro(int cislo);

dword hraj_sampl(dword cislo, int do_stop, int prior);
void stop_sampl(dword handle);
void updatuj_samply(void);

