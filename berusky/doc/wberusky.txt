
                                          /-----\
              ..A..N..A..K..R..E..O..N..  | � � |   29.10.1999
            /-----------------------------/  �  \--------------\
            | BBBB_  EEEE_  RRRR_  U.   U  _���_  K. K_  Y.  Y |
            | B.  B  E.     R.  R  U.   U  �.     K. K   Y.  Y |
            | BBBB   EEE    RRRR   U.   U   ���   KKK     Y.Y  |
            | B.  B  E.     R. R   U.   U      �  K. K     Y   |
            | B.. B  E..    R.  R  U..  U  �.. �  K.  K    Y   |
            | BBBB_  EEEEE  R.  R  _UUUU_  _���_  K.  K  __Y__ |
            | (c) 1999 AnakreoN freeware    CP: WINEECS (1250) |
            =--------------------------------------------------=


                            ---==<</\/+\/\>>==---


    O B S A H

     1...GNU licence
      2...Beru�ky - historie hry
       3...Ovl�d�n�
        4...Pravidla
         5...Editor �rovn�
          6...Technick� po�adavky
           7...NKZIDPD
            8...Z�v�rem  



   1. GNU licence
 ~~~~~~~~~~~~~~~~
Tato hra je naprogramovan� pod z�titou GNU licence. To znamen�, �e se jedn�
o �ist� freewarov� produkt. Nic za hru neplat�te, m��ete ji (dokonce byste
m�li) komukoliv nahr�vat, ani� byste se museli pt�t na svolen� autora a nav�c
m��ete hru i programov� upravovat. (Po�kejte si v�ak rad�ji a� uvoln�me zdro-
jov� k�dy, bude to pak o pozn�n� jednodu���.)
Co ale v ��dn�m p��pad� nesm�te, je komer�n� roz�i�ov�n� t�to hry, �i jin�
jej� zneu�it� k osobn�mu obohacen�. To opravdu ned�lejte, jinak byste n�s ve-
lice, p�evelice rozzlobili a nav�c byste na Beru�k�ch stejn� nic "netrhli",
nebo� n�co, co je zadarmo, p�ece nikdo kupovat nebude.
Podrobnosti lze naj�t v souboru COPYING, kter� mimochodem obsahuje kompletn�
zn�n� GNU licence v na�em mate�sk�m jazyce.



   2. Beru�ky - historie hry
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~
Tak, a je to tady. Neskute�n� se stalo realitou. T�m AnakreoN dokon�il n�co,
co zapo�al. Zd� se to opravdu neuv��iteln�, �e po tolika rozd�lan�ch a nedo-
kon�en�ch projektech se mezi na�imi produkty objevila �ern� ove�ka v podob�
jedn� dokon�en� logick� hry. N� t�m, vyjma mne, na n� pracoval �porn� t�m��
dva roky, co� na v�sledku �el bohu ani snad nejde p��li� vid�t.
Za tu dobu pro�ly Beru�ky rozli�n�mi zm�nami, tak�e pokud byste n�kde v po-
��ta�ov�m muzeu vid�li Sinclair ZX-Spectrum a na n�m b�et �pln� prvn� verzi
Beru�ek, mo�n� byste ani nev��ili, �e jde o jednu a tut� hru. P�vodn� hra
byla naprogramovan� v jazyku BASIC, p�i�em� grafika byla tvo�en� p�es UDG,
co� byla na Spectru mal� ��st pam�ti ... ne, ne to nebudu vysv�tlovat, to
bych se do toho zapletl a ono to u� stejn� dnes nikoho nezaj�m�. Hra m�la
tehdy asi patn�ct �rovn�, s jejich� drtivou v�t�inou se m��ete setkat i
v nyn�j�� PC verzi. Lemrouty (tak se Beru�ky jmenovaly v origin�le) nikdo
nikdy, krom� m� a bratra nehr�l, a po n�kolika t�dnech upadly v zapomn�n�.
Asi za p�t let ke m� dom� p�i�el n�jak� nezn�m� �lov�k, kter� si ��kal Komat
a �ekl: "�obule, um�m programovat a chci vyrobit n�jakou hru. A ty m� v tom
pom��e�." No, �ekl to asi trochu jinak, ale r�mcov� to tak opravdu bylo.
Chv�li jsem se sice zdr�hal, ale nakonec m� p�emluvil. Slovo dalo slovo, j�
vyt�hl z krabice od vysava�e ZX-Spectrum, sfoukl z n�j stolet� prach a na
obrazovce znovu o�ila prastar� legenda, kterou nikdo nikdy nevid�l. Kdy�
Komat poprv� spat�il Beru�ky (Lemrouty), v�bec se mu nel�bily. Dokonce
chvilkov� uva�oval o tom, �e s tvorbou her "sekne", ale nakonec to v�echno
dob�e dopadlo a p�ed v�nocemi roku 1997 byly zapo�aty prvn� p��pravn� pr�ce.
Prakticky ve stejn� �as se za�alo rozv�jet i hern� j�dro textovky Karm�n,
kter� snad v ostr� verzi vyjde n�kdy po roce 2000. O Karm�nu se tu ale ��-
�eji rozv�d�t nebudeme, nebo� jednak na n�m pracuje jin� program�tor a dru-
hak to s Beru�kami p��li� nesouvis�.
Zhruba po roce pr�ce se Komat zastavil a zjistil, �e bude muset Beru�ky od
zakl�du p�eprogramovat. Dokonce mi i vysv�tlil pro�, ale bylo to celkem zby-
te�n�, pon�vad� slov�m typu DPMI, DUMP, BUG, K**VA prost� nerozum�m. Od t� 
doby byly Beru�ky intern� m�n�ny tak�ka ka�d� t�den. Pon�vad� Komat je zna�-
n� tvrdohlav� a striktn� se dr�� zastaral�ch neefektivn�ch program�torsk�ch
metod (kdysi toti� programoval z�kaznick� obvody do mrkac�ch a mluv�c�ch
panenek), dosti �asto se st�valo, �e nam�sto pochvaln�ch e-mail� n�m doch�-
zely sp�e zpr�vy o jak�chsi podivn�ch grafick�ch kart�ch, na kter�ch Beru-
�ky nejedou. Zpo��tku to Komat s �sp�chem ignoroval, ale kdy� Beru�ky jedno-
ho dne p�estaly fungovat i na jeho grafice, rozhodl se, �e pot�et�, ale u�
opravdu naposled p�epracuje sv� ��belsk� ultra rychl� ovlada�e VESA tak,
aby skute�n� d�laly to, co maj�. Mezit�m jsme z�skali hudebn�ka, kter� se
n�m p�ihl�sil na n� inzer�t na WWW. Zakr�tko p�ibyl dal�� grafik, kter� n�m
do Beru�ek d�lal pozad�, menu a epizodn� screenshoty. A pak u� to jelo jako
na dr�tk�ch. Komat bu�il do kl�vesnice jako o �ivot, hudebn�k skl�dal a� u�i
p�ech�zely, grafici malovali, a� je z toho ruce bolely a j� tradi�n� ned�lal
nic, co� se mi tentokr�te vymstilo. P�i�el toti� den D a Beru�ky byly hoto-
v�. Tedy ne tak �pln�. Chyb�ly �rovn�, co� je tedy jak sami uzn�te u logick�
hry pom�rn� v�n� nedostatek. �rovn� jsem m�l dohromady asi 30, z �eho� 10
jich bylo ze ZX-Spectra, 15 nov�ch a 5 mn� poslali r�zn� hodn� ob�an� Inter-
netem. Vzhledem k tomu, �e jsem na na�ich WWW myln� uvedl, �e v ostr� verzi
jich bude minim�ln� 70, vyvstala pal�iv� ot�zka, kde vz�t on�ch chyb�j�c�ch
40. Nakonec, za pou�it� fyzick�ho a psychick�ho n�tlaku, m� moji spolupraco-
vn�ci z AnakreoNu p�esv�d�ili, �e to a� zase takov� probl�m nebude.
No a - dopadlo to dob�e, �rovn� je nakonec 120, co� znamen�, �e se nyn� m�-
�ete v klidu pohrou�it do tajupln�ho sv�ta spletit�ch logick�ch h���ek, kte-
r� dychtiv� �ekaj� na sv� poko�en�.



   3. Ovl�d�n�
 ~~~~~~~~~~~~~
Beru�ky se ovl�daj� rukama, kl�vesnic� a my��. My� je zapot�eb� hlavn�
v editoru, ale m��ete ji samoz�ejm� pou��t i v samotn� h�e pro pohyb v menu.
Menu jsou sv�m v�znamem natolik jasn�, �e jejich komentov�n� by bylo zbyte-
�nou ztr�tou �asu jak pro n�s, tak i pro v�s. P�i hran� si pak sta�� zapa-
matovat kl�vesu <F1>, kterou se vyvol�v� vestav�n� n�pov�da, ve kter� lze
naj�t ve�ker� pot�ebn� informace, a to dokonce i s ilustrovan�mi p��klady.



   4. Pravidla
 ~~~~~~~~~~~~~
Pravidla hry nejsou p��li� obt�n�. �kolem ka�d� �rovn� je naj�t a posb�rat
p�t zlat�ch (p�i v�voji jsme �et�ili, tak�e pouze pozlacen�ch) kl��� a potom
vej�t do dve��, kter� �st� do dal�� �rovn�. A tak po��d d�l, dokud hru celou
nedohrajete. A pon�vad� se jedn� o hru logickou, bylo pot�eba tento �kon n�-
jak�m zp�sobem zt�it. Uznejte sami - asi by v�s nebavilo chodit po pr�zdn�
m�stnosti, sb�rat kl��e, vej�t do dal��, trochu jin� m�stnosti a tam zase
to sam�. A proto jsme do hry p�idali prvky, kter� v�m budou v� �kol zt�o-
vat. Jsou to nejr�zn�j�� bedny, sudy, kameny, dve�e, pr�chody a kdov� co
v�echno je�t�. V�znam jednotliv�ch prvk� je op�t pom�rn� dob�e pops�n p��mo
ve h�e, proto se se tu o nich nebudeme d�le roz�i�ovat.



   5. Editor �rovn�
 ~~~~~~~~~~~~~~~~~~
Editor se z valn� ��sti ovl�d� pomoc� hork�ch kl�ves. Zde je jejich �pln�
seznam:

ovl�d�n� n�strojov�ch li�t:

<1> - zm�na pozad�
<2> - li�ta podlah
<3> - li�ta hern�ch prvk�
<4> - li�ta hr���

zm�na zobrazen� editoru:

<Q> - zapne pozad�
<W> - zapne podlahy
<E> - zapne hern� plochu
<R> - zapne hr��e
<A> - vypne pozad�
<S> - vypne podlahy
<D> - vypne hern� plochu
<F> - vypne hr��e

kreslen� objekt�:

nevypln�n� �tverce:
<Z> - za��tek pro kreslen� nevypln�n�ch �tverc� na pozici kurzoru
<X> - vykresli �tverec z bodu "Z" do tohoto bodu aktu�ln�m prvkem
<C> - vyma�e nevypln�n� �tverec z bodu "Z"

vypln�n� �tverce:
<V> - podobn� jako <Z> v p�edchoz�m p��kladu
<B> - dtto. <X>
<N> - dtto. <C>

posuny prvk� li�ty:

<.>,<,> - posun kurzoru po li�t�
<K>,<L> - rolov�n� celou li�tou

speci�ln� funkce:

<CTRL+R> - rotace prvku
<CTRL+S> - automatick� st�nov�n�

diskov� operace a jin� u�ite�n� p��kazy:

<ESC>  - konec
<F2>   - ulo�en� �rovn�
<F3>   - nahr�n� �rovn�
<F4>   - zobrazen� jm�na editovan�ho souboru
<F5>   - quick save
<F6>   - quick load
<F8>   - reset editoru (p�ekreslen� plochy a n�strojov� li�ty)
<F9>   - spu�t�n� �rovn�

D�le pak nen� na �kodu zn�t i parametry p��kazov� ��dky, kterou Beru�ky
samoz�ejm� podporuj�.

BERUSKY.EXE d jm�no_dema.DM - p�ehraje demo
BERUSKY.EXE e [jm�no_�rovn�.LV3] - spust� editor
BERUSKY.EXE u jm�no_�rovn�.LV3 - spust� u�ivatelskou �rove�



   6. Technick� po�adavky
 ~~~~~~~~~~~~~~~~~~~~~~~~
 Doporu�en� konfigurace:
 ^^^^^^^^^^^^^^^^^^^^^^^
 o 486DX/4-100Mhz
 o 16MB RAM (8MB bez ozvu�en�)
 o 1024Kb grafick� adapt�r, VESA 2.0 kompatibiln�
 o dvoutla��tkov� my�
 o 15MB voln�ho m�sta na pevn�m disku (1,5MB bez ozvu�en�)
 o opera�n� syst�m MS-DOS 5.0 a vy���, nebo WINDOWS



   7. NKZIDPD /nej�asn�ji kladen�, zpravidla i dosti p�ihloupl� dotazy/
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   [Tuto pas� nemus�te br�t p��li� v�n�.]

D: Beru�ky mi vymazaly m�j hardisk. ��d�m n�hradu.
   [ Vratislav Ruml, Trutnov ]

O: Tak tohle vlastn� ani nen� dotaz, sp�e jak�si konstatov�n�. Probl�m, kter�
   popisujete, je n�m zn�m a intenzivn� pracujeme na jeho odstran�n�. V�maz z�e-
   jm� zp�sobila autodetekce zvukov� karty, kter� dosti necitliv� o�ich�v� ve-
   �ker� p�eru�en�, v�etn� IRQ14 a IRQ15. U n�kter�ch disk� to �elbohu zp�-
   sobuje nep��jemn� doprovodn� efekty. Nemus�te ale zoufat, Beru�ky nevymaza-
   ly disk cel�, v�t�inou zni�� pouze FAT a p�r prvn�ch cluster� z datov� obla-
   sti, tak�e se dohromady a� zas tak moc nestalo.
   Zat�m na tuto chybu m�me pouze jedin� doporu�en�. Pokud n�hodou m�te ve sv�m
   po��ta�i zvukovou kartu a pevn� disk z�rove�, v�dy p�ed spu�t�n�m Beru�ek
   n�kterou z t�chto komponent vymontujte a t�m zamez�te nep��jemnostem.
   N�hradu v�m poskytnout nem��eme, p��t� pou��vejte kvalitn� a odzkou�en�
   software, spolehliv� se vyhnete probl�m�m.

D: Nesly��m zvuky ani hudbu. P�itom p�ete, �e Beru�ky obsahuj� mnoho hudeb
   a zvukov�ch efekt�. Porad�te jak na to?
   [ Daniela Kmoch��kov�, Chomutov ]

O: Porad�me, porad�me. V�ak od toho tu jsme. Tak�e mil� sle�no, nebo v�en�
   pan� (vyberte si), probl�m bude z�ejm� v chybn� navolen� zvukov� kart�.
   N�prava je jednoduch�. V adres��i BERUSKY\ naleznete soubor, kter� se
   jmenuje SETUP.EXE a ten spust�te. Pokud nev�te, jakou m�te zvukovou kartu,
   p�eptejte se dodavatele va�eho po��ta�e. Potom spu�te znovu Beru�ky a
   v�echno by m�lo b�t v po��dku.
   
D: St�hl jsem si z va�� str�nky  hru Beru�ky. Hra se mi sice docela l�b�, ale
   faktem teda je, �e je stra�n� naprogramovan�, proto�e mi na m�m po��ta�i
   leze jako slim�k. P��t� nepi�te takov� kraviny, �e to funguje plynule i na
   386/SX, nen� to pravda a vy to dob�e v�te.
   [ Petr Leittner, Moravsk� T�ebov� ]

O: Tak takov�to dopisy dost�v�me neradi, je�t� s v�t��m odporem je �teme a
   s hrozit�nskou nechut� na n� odepisujeme. Nicm�n� m�te pravdu, Beru�ky
   maj� o n�co vy��� HW po�adavky, ne� bylo uvedeno. Zp�sobil to zejm�na nov�
   engiene, kter� podporuje prakticky libovoln� po�et animac� ve h�e a hu-
   debn�k, kter� to tro�ku p�est�elil s po�tem kan�l� a vzorkovac� frekvenc�.
   Omlouv�me se, p��t� rad�ji minim�ln� konfiguraci trochu nadsad�me.
   M�me ale v�born� recept jak Beru�ky zrychlit: kupte si v�konn�j�� po��-
   ta�.

D: M�m grafickou kartu zna�ky CirrusLogic. Beru�ky mi s n� nejedou a ur�it�
   je to pr�v� tou kartou, proto�e kdy� si pu��m kamar�dovu  ATI, jede to
   bez probl�m�. Mohli byste mi pros�m poslat opravn� patche?
   [ Vojt�ch Sl�ma, Prachatice ]

O: Patche zat�m nem�me, pon�vad� to nen� na �em odladit. Takovou grafickou
   kartu nikdo z n�s nevlastn�, ale slibujeme, �e hned jak n�m ji n�kdo zap�j-
   ��, patche vyjde. Jinak v�m opravdu nem��eme pomoci, leda �e byste se
   s�m rozhodl pro v�m�nu grafick� karty, co� bychom v�ichni v�ele doporu�ili,
   proto�e se n�m nad t�m nechce b�dat. Dal��m mo�n�m �e�en�m, pro v�s asi
   nejsch�dn�j��m, je softwarov� emulace VESA 2.0, nap��klad pomoc� utility
   UNIVBE od firmy Scitech.

D: Nejede to.
   [ Ing. Jind�ich Drob�lek, Ben�tky nad Jizerou ]

O: No jo pane, to se lehko �ekne, ale co my s t�m? Za�lete n�m pros�m p�esnou
   konfiguraci va�eho po��ta�e, co p�esn� Beru�ky d�laj� (ve va�em p��pad�
   asi sp�e ned�laj�), jak� opera�n� syst�m pou��v�te a hlavn� bug report,
   kter� z�sk�te velmi trivi�ln�m zp�sobem: BERUSKY.EXE > BUGREP.TXT.
   Na z�klad� t�chto informac� jsme pak schopni analyzovat, kde by mohla b�t
   potenci�ln� p���ina vzniku chyby. Potom bu� po�leme e-mail s podrobn�m
   popisem odstran�n� z�vady, nebo kr�tkou zpr�vi�ku o tom, �e se to prost�
   opravit ned�.

D: M�m grafickou kartu S3 Trio64V+. Pod MS-DOSem mi Beru�ky jedou bez pro-
   bl�m�, ne u� tak pod syst�mem WINDOWS95. Zkou�el jsem v�elicos, ale nic
   nepom�h�. Nev�te co s t�m?
   [ Radim Uzen�, Chvalatice ]

O: V�me. Nespou�t�t hru pod WINDOWS95. Toho lze doc�lit dv�ma zp�soby.
   Bu� restartovat WINDOWS95 v re�imu MS-DOS, nebo p�i startu po��ta�e
   stla�it kl�vesu F8 a zvolit polo�ku "Jen syst�m MS-DOS", p��padn�
   je�t� "Minul� verze syst�mu MS-DOS". T�et� mo�nost je je�t� p�esta-
   ven� parametr� MS-DOS PROMPTu ve WINDOWS95, ale to vy�aduje hlub��
   znalost v�ci, kterou my bohu�el nedisponujeme.

D: Podporuj� Beru�ky 3DFX, MMX standard �i virtu�ln� p�ilbu?
   [ Stanislav Krupi�ka, A� ]

O: Update pro 3DFX se p�ipravuje, vyjde z�ejm� na p�elomu tohoto roku. Hra
   s 3DFX bude vypadat naprosto stejn� jako bez 3DFX a bude i stejn� rychl�,
   tak�e je opravdu se na co t�it.
   MMX Beru�ky bohu�el nepodporuj�. P�ipravujeme ale dopl�kov� patche pro
   novou technologii firmy AMD 3DNOW!. K dispozici bude s nejv�t�� pravd�-
   podobnost� n�kdy v p�li listopadu tohoto roku. Bude samoz�ejm� voln� ke
   sta�en� na na�ich WWW.
   A co se virtu�ln� p�ilby t��e, tak tu sice Beru�ky p��mo nepodporuj�,
   ale p�i hran� ji samoz�ejm� nasazenou m�t m��ete, to nen� na z�vadu.

D: Je mo�n� hra po modemu, nebo po Internetu?
   [ Ale� Kostka, Hru�ovany u Brna]

O: Ne. Ale uva�ujeme o tom.

D: Spustil jsem Beru�ky, n�jak divn� mi zachroupal pevn� disk a cel� syst�m
   se zasekl. Po op�tovn�m restartu po��ta�e ji� nenab�hl opera�n� syst�m,
   co m�m d�lat?
   [ Daniel Chod�r, N�m욝 nad Oslavou ]

O: Hmmm...tak podobn� probl�m jsme tu u� m�li. Co s t�m d�lat v�m asi neporad�-
   me, ale jestli chcete, d�me v�m e-mail na pana Rumla z Trutnova, ten m� ji-
   st� s �e�en�m podobn�ch situac� mnoho zku�enost�.



   8. Z�v�rem
 ~~~~~~~~~~~~
Pevn� douf�me, �e se v�m Beru�ky l�bili a �e jejich hran� pro v�s nebyla pouh�
ztr�ta �asu. D�le omluvte ve�ker� chyby, a to jak pravopisn�, tak i programov�,
na kter� byste n�hodou narazili. Pravdou sice je, �e GNU produkty jsou abso-
lutn� bez z�ruky, av�ak na druhou stranu i my bychom byli r�di, kdyby Beru-
�ky fungovaly na v�ech stroj�ch opravdu tak, jak maj�. Pokud tedy na n�jakou
tu chybku naraz�te, neost�chejte se n�s na ni upozornit,  v�dy� opravn�m
patchem disponuj� i mnohem slo�it�j�� hry, ne� je ta na�e, anebo ne?

 Kontakt na n�s:
 ^^^^^^^^^^^^^^^
  http://ANAKREON.BONUSWEB.CZ     [na�e domovsk� str�nka]
         ANAKREON.ZDE.CZ          [mirror]
 mail-to:BERUSKY@BONUSWEB.CZ      [dotazy t�kaj�c� se Beru�ek]
         ANAKREON@ATLAS.CZ        [na�e elektronick� adresa]
         DOLEZALL@BRNO.FERONA.CZ  [jen kdy� v�e p�ede�l� nebude fungovat]

/Konec/

