/*
         .���� �    � ����.     �    � �����.  ����� .����. �    �
        .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
        �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
      .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
     .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
     �.      � �    � �      .� �   .� �    .� ����� .����. �    �
     (c) 1998 AnakreoN Freeware

   Berusky (C) Komat za Anakreon fwp. 1997-1998-1999-?

   Soubor:     menu.h -  H pro menu v beruskach

   Autor:      Komat za Anakreon
   Datum:      furt se opravuje
   Verze:      release 0.0
   Prekladac:  djgpp -> gcc + nasm
   Nakladac:   Zetor BS-564 Mk III C
*/

// Struktury pro menu
typedef struct {
   dword    podklad;  //cislo spritu s podkladem pod menu
   dword    x_pozice; //pozice podkladu pod menu
   dword    y_pozice;

   dword    polozek;  // pocet polozek menu
   dword    stmivej;  // ma se menu stmivat ? (prezitek davnych casu)
   dword    maz;

   dword    nahoru;
   dword    dolu;
   dword    enter;
   dword    esc;      //Cislo escapove polozky, ktera se vyvola po stisku ESC

   dword    bila;

   void     (*p_fn_vstup)(void);
   //pointer na funkci, ktera se vyvola pri vstupu do menu

} CELE_MENU;

// Typy jednotlive polozky
typedef struct {

   dword  x_pozice;   // X a y pozice pro kresleni a maskovani polozky
   dword  y_pozice;
   dword  x_res;      // sirka zaberu pri testu mysi
   dword  y_res;

   dword ne_akt;     // cislo spritu neaktivni polozky
   dword akt;        // cislo spritu aktivni polozky

   void  *p_menu;
   void  (*p_fce)(void);

} POLOZKA;

/* Zakladni sekce definice menu na pevno pomoci struktur */
typedef struct {
  CELE_MENU pozadi;
  POLOZKA   pol[10];
} GENERIC_MENU;

GENERIC_MENU   *p_smenu;   // Pointer na prave aktivni menu

struct {
     CELE_MENU  pozadi;
     POLOZKA    pol[7];
} hlavni_menu;

struct {
     CELE_MENU pozadi;
     POLOZKA   pol[3];
} start_hry;

struct {
     CELE_MENU pozadi;
     POLOZKA   pol[6];
} obtiznost;

struct {
    CELE_MENU  pozadi;
     POLOZKA   pol[2];
} nastaveni;

struct {
     CELE_MENU  pozadi;
     POLOZKA   pol[4];
} napoveda;

struct {
     CELE_MENU  pozadi;
     POLOZKA   pol[3];
} konec;

struct {
     CELE_MENU  pozadi;
     POLOZKA   pol[7];
} dilema_menu;

struct {
     CELE_MENU  pozadi;
     POLOZKA   pol[3];
} ovl1;

struct {
     CELE_MENU  pozadi;
     POLOZKA   pol[3];
} ovl2;

struct {
     CELE_MENU  pozadi;
     POLOZKA   pol[3];
} ovl3;


void  inicializuj_menu(void);
void  vykresli_menu(GENERIC_MENU * p_s);
int   obsluz_menu(GENERIC_MENU *p_gen_menu, int pozadi, int akt_pol, int delay);
dword cekej_na_stisk(void);

