/*
         .���� �    � ����.     �    � �����.  ����� .����. �    �
        .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
        �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
      .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
     .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
     �.      � �    � �      .� �   .� �    .� ����� .����. �    �
     (c) 1998 AnakreoN Freeware

   Berusky (C) Komat za Anakreon fwp. 1997-1998-1999-?

   Soubor:     animace.h  - H k animacnimu modulu
   Autor:      Komat za Anakreon
   Datum:      furt se opravuje
   Verze:      release - 0.0
   Prekladac:  djgpp -> gcc + nasm
   Nakladac:   Zetor BS-564 Mk III C

*/

#ifndef __ANIMACE_H
#define __ANIMACE_H

#include "zakl_typ.h"

#define MAX_ANIM_POZADI  10
#define MAX_ANIM         700
#define ANIM_CHYBA       1000
#define CELKEM_ZMEN      700
#define CHYBA            0xffff

typedef struct { // Hlavicka jednotliveho framu

  int cs;     // cislo spritu
  int x_kor;  // relativni pozice x k bazi
  int y_kor;  // relativni poloha y k bazi
  int rotace; // rotace tohoto framu
  int delay;  // delka cekani do dalsiho framu, 0 - hned na dalsi

} ANIMACNI_FRAME;


typedef struct {

/*
  Zpusob prace s pozadim:
  pozadi = 1;

  buf_uloz_sprit(z pozadi,x,y, do animace);

  pozadi = 0;
  buf_kresli_sprit_bar( z pozadi do animace na souradnice v animaci x,y);
*/
  int  pozadi;    // priznak jestli je to pozadi a nebo samostatny
                  // sprite
  byte *p_sprit;  // pointer na sprit s pozadim pod animaci
  int  x;     // x a y souradnice ze kterych se bude obnovovat
  int  y;

} STR_POZADI;

/* Provozni struktura pri prehravani animace
   pole techto struktur a vracet od nich handle na jednotlivou animaci
   toto bude plnit nejaka fce a nebo rovnou pred pred prehranim...
*/
typedef struct {

  //----------------------------------------------------------

  int  celkem_framu;    // pocet framu
  int  loop;            // opakovat animaci
  int  wr;              // cekani na retrace ?

  int  x_baze;   // defaultni souradnice animace ve spritu/obrazovce
  int  y_baze;

  ANIMACNI_FRAME  *p_frame;

  //--------------------------------------------------------

  int pozadi;// pocet pouzitych pozadi
  STR_POZADI  poz[MAX_ANIM_POZADI];

  //---------------------------------------------------------

  byte barva;
  byte *p_buffer;// sprit do ktereho se bude kreslit animace
                 // animacni engine si tento sprite bude delat na zacatku
                 // zalozeni animace a pri skonceni jej uvolni

  byte **p_spr;  // Ukazatel na prvni sprit

  dword akt_frame;    // Frame ktery se ma ted prehrat
  int   x;        // x - souradnice animace
  int   y;        // y - souradnice animace

  //-------------------------------------------------------------------

  dword  cas;      // tato p_cas - promena se bude porovnavat s delay
  volatile dword *p_cas;

  int  play;     // 0 - nehraje, 1 - hraje
  int  update;   // pri kazdym animacnim okne se nastavi na 1 a kresleni to smaze
  int  zmena;    // 0xffff -> zadna zmena, jinak nejaka jo
  int  cas_zmeny; // 1 - proved zmenu

} ANIM_FILE;

extern volatile ANIM_FILE *p_an[MAX_ANIM];

typedef struct {
  dword aktivni;
  dword x,y;
  dword prvek;
  dword handle;
  dword prekresli;
  dword dvere;
  dword akce;
  dword cislo1;
  dword cislo2;
} ZMENY;

extern ZMENY pole_zmen[CELKEM_ZMEN];

/*
  Funkcni typy
*/
byte *nastav_res_animaci(int x_res, int y_res);
ANIM_FILE *vyrob_animaci(int framu, int loop, int wr, int buffer);
void dopln_framy(int handle, dword *p_sprity, int *p_x_kor, int *p_y_kor, int *p_rotace, dword *p_delay);
int  pridej_pozadi(int handle, int pozadi, byte *p_sprit, int x, int y);
int  registruj_animaci(ANIM_FILE *p_anim,int x_res,int y_res, byte **p_sprt, volatile int *p_casovac, byte barva);
void updatuj_animace(void);
void kresli_animace(void);
int  play_anim(int handle);
int  stop_anim(int handle);
int  status_anim(int handle);
void smaz_pozadi(int handle);
void nastav_souradnice_anim(int handle, int x, int y, int x_baze, int y_baze);
void vygeneruj_zakladni_animace(void);
void aktivuj_animace(void);
void vypni_animace(void);
int stop_vsechno(void);
int uvolni_animaci(int handle);
int uvolni_vsechny_animace(void);
void aktivuj_animace(void);
void vypni_animace(void);
int pridej_zmenu(dword handle, dword x, dword y, dword prvek, dword prekresli, dword dvere, dword akce, dword cislo1, dword cislo2);
int proved_zmenu(int i);
void init_anim(void);

#endif

