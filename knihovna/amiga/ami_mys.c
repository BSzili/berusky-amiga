#include <libanak.h>

#include <devices/input.h>
#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/graphics.h>

#define RYCHLOST	0

#define DEF_X     640
#define DEF_Y     480

static int mys_aktivni = 0;
static UWORD *pointermem = NULL;

// from ami_graf.c
extern struct Window *window;
// from ami_klav.c
extern struct IOStdReq *inputReq;

// mouse position/buttons
volatile word *_p_info = NULL;


int zapni_mys(volatile word *p_info, byte *p_sprit)
{
	//printf("%s\n", __FUNCTION__);

   /* vynuluj strukturu informaci o mysi */
//   for(i = 0;i < POLOZEK_MYSI;i++)
   p_info[0] = 0;
   _p_info = p_info; /* nastav pointer na info o mysi */
   nastav_kurzor_sprit(p_info, p_sprit);

   if (mys_aktivni)
   {
	   printf("%s mouse is already activated\n", __FUNCTION__);
	   return 1;
   }
	pointermem = (UWORD *)AllocVec(16 * 16, MEMF_CLEAR | MEMF_CHIP);

   mys_aktivni = 1;
   return(1);
}

void rozsah_mysi(int x_min, int y_min, int x_max, int y_max)
{
	//printf("%s\n", __FUNCTION__);
	// mouse range, internal only
}

void souradnice_mysi(int x, int y)
{
	//printf("%s\n", __FUNCTION__);
	// set mouse coords

	struct InputEvent ie;
	struct IEPointerPixel newpos;

	newpos.iepp_Screen = window->WScreen;
	newpos.iepp_Position.X = x + window->BorderLeft + window->LeftEdge;
	newpos.iepp_Position.Y = y + window->BorderTop + window->TopEdge;

	ie.ie_EventAddress = &newpos;
	ie.ie_NextEvent    = NULL;
	ie.ie_Class        = IECLASS_NEWPOINTERPOS;
	ie.ie_SubClass     = IESUBCLASS_PIXEL;
	ie.ie_Code         = IECODE_NOBUTTON;
	ie.ie_Qualifier    = 0;

	inputReq->io_Data    = &ie;
	inputReq->io_Length  = sizeof(struct InputEvent);
	inputReq->io_Command = IND_WRITEEVENT;

	DoIO((struct IORequest *)inputReq);

	mysi_info[1] = x;
	mysi_info[2] = y;
}

void vypni_mys(void)
{
	//printf("%s\n", __FUNCTION__);

   if(mys_aktivni != 1)
      return;

  FreeVec(pointermem);
  pointermem = NULL;
  
  _p_info = NULL;
   
   mys_aktivni = 0;
}

/* vypne zobrazovani kurzoru na obrazovce */
void vypni_kurzor(volatile word *p_info)
{
	//printf("%s\n", __FUNCTION__);

	if (window && pointermem && window->Pointer != pointermem)
	{
		SetPointer(window, pointermem, 16, 16, 0, 0);
	}
}

/* zapne zobrazovani kurzoru na obrazovce */
void zapni_kurzor(volatile word *p_info)
{
	//printf("%s\n", __FUNCTION__);

	if (window && window->Pointer == pointermem)
	{
		ClearPointer(window);
	}
}

void nastav_rychlost_mysi(word x_rychlost,word y_rychlost)
{
	//printf("%s\n", __FUNCTION__);
	// mouse speed, unused
}

void nastav_kurzor_ctverec(volatile word *p_info, byte *p_ctverec, dword x_res, dword y_res)
{
	//printf("%s\n", __FUNCTION__);
	// set cursor from buffer, unused
}

void nastav_kurzor_sprit(volatile word *p_info, byte *p_sprit)
{
	//printf("%s\n", __FUNCTION__);
	// set cursor sprite, p_info is unused
}
