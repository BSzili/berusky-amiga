#include <libanak.h>

#include <devices/input.h>
#include <intuition/intuition.h>
#include <libraries/lowlevel.h>
#include <exec/interrupts.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/lowlevel.h>

// asm
dword alt;
dword ctrl;
volatile dword blue_death_on;

volatile int klavesnice_aktivni = 0;
volatile byte key[POCET_KLAVES];
/*volatile*/ byte _a_klavesa[4];
volatile byte _a_asc[256]={' ',' ','1','2','3','4','5','6','7','8','9','0','-','+',
                        ' ',' ','q','w','e','r','t','y','u','i','o','p','[',
                        ']',' ',' ','a','s','d','f','g','h','j','k','l',';',
                        '"','~',' ','\\','z','x','c','v','b','n','m',',','.',
                        '/',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' '};


static unsigned char keyconv[] =
{
	K_TILDA, K_1, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9, K_0,	/* 10 */
	K_MINUS, K_PLUS/*K_EQUALS*/, K_BACKSLASH, 0, K_INS, K_Q, K_W, K_E, K_R, K_T,	/* 20 */
	K_Y, K_U, K_I, K_O, K_P, K_ZAV_L, K_ZAV_P, 0, K_END, K_DOLU,	/* 30 */
	K_PGDN, K_A, K_S, K_D, K_F, K_G, K_H, K_J, K_K, K_L,	/* 40 */
	K_STREDNIK, K_UVOZ, 0, 0, K_DOLEVA, K_K_5, K_DOPRAVA, '<', K_Z, K_X,	/* 50 */
	K_C, K_V, K_B, K_N, K_M, K_CARKA, K_TECKA, K_SLASH, 0, K_DEL,	/* 60 */
	K_HOME, K_NAHORU, K_PGUP, K_SPACE, K_BKSP, K_TAB, 0/*K_KP_ENTER*/, K_ENTER, K_ESC, K_DEL,	/* 70 */
	0, 0, 0, K_K_MINUS, 0, K_NAHORU, K_DOLU, K_DOPRAVA, K_DOLEVA, K_F1,	/* 80 */
	K_F2, K_F3, K_F4, K_F5, K_F6, K_F7, K_F8, K_F9, K_F10, K_NUMLOCK,	/* 90 */
	0, /*K_KP_SLASH*/0, /*K_KP_STAR*/0, /*K_K_PLUS*/0, /*K_PAUSE*/0, K_SHFT_L, K_SHFT_P, 0, K_CTRL, K_ALT,	/* 100 */
	K_ALT, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 110 */
};
#define MAX_KEYCONV (sizeof keyconv / sizeof keyconv[0])
static struct Interrupt inputHandler;
static struct MsgPort *inputPort = NULL;
/*static*/ struct IOStdReq *inputReq = NULL;
static BOOL keyRepeat = TRUE;

// from ami_graf.c
extern struct Window *window;

// joystick

ULONG oldportstate;

static void SetKey(byte k, int release)
{
	_a_klavesa[0] = k | release;
	if (release)
	{
		_a_klavesa[1] = 0;
		key[k] = 0;
	}
	else
	{
		_a_klavesa[1] = _a_asc[k];
		key[k] = 1;
	}
	// backwards compatibility stuff
	alt = key[K_ALT];
	ctrl = key[K_CTRL]; // unused?
}

#define CheckJoyEvent(key, mask) \
	do { \
		if ((portstate & mask) != (oldportstate & mask)) \
			SetKey(key, (portstate & mask) ? 0 : IECODE_UP_PREFIX); \
	} while(0)

static void ReadJoystick(ULONG port)
{
	ULONG portstate;

	portstate = ReadJoyPort(port);

	if (((portstate & JP_TYPE_MASK) == JP_TYPE_GAMECTLR) || ((portstate & JP_TYPE_MASK) == JP_TYPE_JOYSTK))
	{
		CheckJoyEvent(K_NAHORU, JPF_JOY_UP);
		CheckJoyEvent(K_DOLU, JPF_JOY_DOWN);
		CheckJoyEvent(K_DOLEVA, JPF_JOY_LEFT);
		CheckJoyEvent(K_DOPRAVA, JPF_JOY_RIGHT);

		CheckJoyEvent(K_TAB, JPF_BUTTON_RED); // switch to the next ladybug
		CheckJoyEvent(K_ENTER, JPF_BUTTON_RED);
		CheckJoyEvent(K_ESC, JPF_BUTTON_BLUE);

		oldportstate = portstate;
	}
}


static struct InputEvent * __saveds InputHandler(register struct InputEvent *input_event __asm("a0"), register APTR data __asm("a1"))
{
	struct InputEvent *ie;
	int scanCode, release;

	for (ie = input_event; ie; ie = ie->ie_NextEvent)
	{
		release = ie->ie_Code & IECODE_UP_PREFIX;
		scanCode = ie->ie_Code & ~IECODE_UP_PREFIX;

		if (!keyRepeat && ie->ie_Qualifier & IEQUALIFIER_REPEAT)
			continue;

		// keyboard
		if (ie->ie_Class == IECLASS_RAWKEY)
		{
			byte k = keyconv[scanCode];
			SetKey(k, release);
		}

		// mouse
		if ((ie->ie_Class == IECLASS_RAWMOUSE))
		{
			if (mysi_info == NULL) continue;
			if (ie->ie_Code != IECODE_NOBUTTON)
			{
				switch (scanCode)
				{
					case IECODE_LBUTTON:
						mysi_info[0] = !release;
						break;
						/*
					case IECODE_RBUTTON:
						break;
						*/
				}
			}
			/*
			mysi_info[1] = ie->ie_position.ie_xy.ie_x;
			mysi_info[2] = ie->ie_position.ie_xy.ie_y;
			*/
			mysi_info[1] = window->WScreen->MouseX;
			mysi_info[2] = window->WScreen->MouseY;
			//vypln_ctverec(mysi_info[0] ? 193 : 246,mysi_info[1], mysi_info[2], 4, 4);
		}
	}

	return input_event;
}

static BOOL InitInput(void)
{
	if ((inputPort = CreateMsgPort()))
	{
		if ((inputReq = (struct IOStdReq *)CreateIORequest(inputPort, sizeof(*inputReq))))
		{
			if (!OpenDevice((STRPTR)"input.device", 0, (struct IORequest *)inputReq, 0))
			{
				inputHandler.is_Node.ln_Type = NT_INTERRUPT;
				//inputHandler.is_Node.ln_Pri = 100;
				inputHandler.is_Node.ln_Pri = 10; // absolute mouse coordinates
				inputHandler.is_Node.ln_Name = (STRPTR)"Berusky input handler";
				inputHandler.is_Code = (void (*)())&InputHandler;
				inputReq->io_Data = &inputHandler;
				inputReq->io_Command = IND_ADDHANDLER;
				if (!DoIO((struct IORequest *)inputReq))
				{
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

static void ShutdownInput(void)
{
	if (inputReq)
	{
		inputReq->io_Data = &inputHandler;
		inputReq->io_Command = IND_REMHANDLER;
		DoIO((struct IORequest *)inputReq);
		CloseDevice((struct IORequest *)inputReq);
		DeleteIORequest((struct IORequest *)inputReq);
		inputReq = NULL;
	}

	if (inputPort)
	{
		DeleteMsgPort(inputPort);
		inputPort = NULL;
	}
}

/* rutina na zapnuti obsluhy klavesnice */
/* jako parametr je buffer na 4 znaky, delka 4 byte */
void zapni_klavesnici(void)
{
	//printf("%s\n", __FUNCTION__);
  InitInput();
  klavesnice_aktivni = 1;
}

/* vypne obsluhu klavesnice */
void vypni_klavesnici(void)
{
	//printf("%s\n", __FUNCTION__);

  if(klavesnice_aktivni != 1)
     return;
 
  ShutdownInput();

  klavesnice_aktivni = 0;

  zapni_repeat();
}

void vypni_repeat(void)
{
	//printf("%s\n", __FUNCTION__);
 keyRepeat = FALSE;
}

void zapni_repeat(void)
{
	//printf("%s\n", __FUNCTION__);
 keyRepeat = TRUE;
}

int getkb(void)
{
 _a_klavesa[0] = 0;
 ReadJoystick(1);
 while((_a_klavesa[0] == 0)||(_a_klavesa[0] > 0x79)) {
  Delay(1);
  ReadJoystick(1);
 }
 return(_a_klavesa[0]);
}

int getas(void)
{
 _a_klavesa[1] = 0;
 while(_a_klavesa[1] == 0) Delay(1);
 return(_a_klavesa[1]);
}

int getkba(void)
{
 ReadJoystick(1);
 return(_a_klavesa[0]);
}

int getasa(void)
{
 return(_a_klavesa[1]);
}

void clrkb(void)
{
 _a_klavesa[0] = 0;
 _a_klavesa[1] = 0;
}
