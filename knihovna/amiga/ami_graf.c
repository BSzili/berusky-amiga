#include <libanak.h>
#include <cybergraphx/cybergraphics.h>
#include <proto/intuition.h>
#include <proto/cybergraphics.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#define IPTR ULONG

/*******************************************
  Sekce s globalnimy promenymi
********************************************/
dword   _a_x,
        _a_y, // w/h for uloz_ctverec, kresli_ctverec, kresli_ctverec_bar
        _a_xres,
        _a_yres; // screen resolution

byte    _a_palety[10][769];

dword   _a_clip_x_min;
dword   _a_clip_x_max;
dword   _a_clip_y_min;
dword   _a_clip_y_max; // clip area, unused?

static struct Screen *screen = NULL;
/*static*/ struct Window *window = NULL;
//static UWORD *pointermem = NULL;
struct Library *CyberGfxBase = NULL;

// WPA8 temprp
static struct RastPort *temprp = NULL;
static struct RastPort wprp;
static struct BitMap *wpbm;


/*********   Kod knihovny  **********/

int prepni_do_grafiky(word video_mod, word lbf_e)
{
	//printf("%s\n", __FUNCTION__);

	ULONG modeid = INVALID_ID;

	if (!CyberGfxBase)
		CyberGfxBase = OpenLibrary("cybergraphics.library", 0);

	_a_xres = 640;
	_a_yres = 480;

	if (CyberGfxBase)
	{
		modeid = BestCModeIDTags(
			CYBRBIDTG_NominalWidth, _a_xres,
			CYBRBIDTG_NominalHeight, _a_yres,
			CYBRBIDTG_Depth, 8,
			TAG_DONE);
	}

	if (modeid == INVALID_ID)
	{
		modeid = BestModeID(
			BIDTAG_NominalWidth, _a_xres,
			BIDTAG_NominalHeight, _a_yres,
			BIDTAG_Depth, 8,
			//BIDTAG_MonitorID, DEFAULT_MONITOR_ID,
			TAG_DONE);
	}

	if ((screen = OpenScreenTags(NULL, 
		SA_DisplayID, modeid,
		SA_Width, _a_xres,
		SA_Height, _a_yres,
		SA_Depth, 8,
		SA_ShowTitle, FALSE,
		SA_Quiet, TRUE,
		SA_Draggable, FALSE,
		SA_Type, CUSTOMSCREEN,
		TAG_DONE)))
	{
		if ((window = OpenWindowTags(NULL,
			WA_Flags, WFLG_BACKDROP | WFLG_REPORTMOUSE | WFLG_BORDERLESS | WFLG_ACTIVATE | WFLG_RMBTRAP,
			WA_InnerWidth, _a_xres,
			WA_InnerHeight, _a_yres,
			WA_CustomScreen, (IPTR)screen,
			TAG_DONE)))
		{
			/*pointermem = (UWORD *)AllocVec(16 * 16, MEMF_CLEAR | MEMF_CHIP);
			SetPointer(window, pointermem, 16, 16, 0, 0);*/

			// allocate a temp rastport
			wpbm = AllocBitMap(_a_xres, 1, 8, BMF_CLEAR|/*BMF_DISPLAYABLE|*/BMF_STANDARD, NULL);
			if (wpbm)
			{
				memcpy(&wprp, window->RPort, sizeof(struct RastPort));
				wprp.Layer = NULL;
				wprp.BitMap = wpbm;
				temprp = &wprp;
			}

			return 0;
		}
	}

	prepni_do_textu(0);

 return(1);
}

/* prepne do textoveho modu */
void prepni_do_textu(word txt_mod)
{
	//printf("%s\n", __FUNCTION__);

	if (wpbm)
	{
		FreeBitMap(wpbm);
		wpbm = NULL;
	}

	if (window)
	{
		CloseWindow(window);
		window = NULL;
	}

	if (screen)
	{
		CloseScreen(screen);
		screen = NULL;
	}

	/*if (pointermem)
	{
		FreeVec(pointermem);
		pointermem = NULL;
	}*/

	if (CyberGfxBase)
	{
		CloseLibrary(CyberGfxBase);
		CyberGfxBase = NULL;
	}
}

static void SetPalette(byte firstreg, int numregs, byte *palette)
{
	int entry;
	ULONG palette32[256 * 3 + 2];

	if (!screen)
	{
		// can be called before the screen is opened
		//printf("%s no screen\n", __FUNCTION__);
		return;
	}

	palette32[0] = numregs << 16 | firstreg;
	for (entry = firstreg; entry < numregs; ++entry)
	{
		palette32[entry * 3 + 1] = palette[entry * 3 + 0] << 26;
		palette32[entry * 3 + 2] = palette[entry * 3 + 1] << 26;
		palette32[entry * 3 + 3] = palette[entry * 3 + 2] << 26;
	}
	palette32[entry * 3 + 1] = 0;
	LoadRGB32(&screen->ViewPort, palette32);
}

/* Nastavi paletu barev ze souboru p_jmeno */
void nahraj_paletu(byte *p_jmeno,int pocet)
{
 FILE   *paleta;
 int    i;
 // Nacteni palety ze souboru do Bufferu
 if((paleta = fopen(p_jmeno,"rb")) == NULL) {
        prepni_do_textu(0x3);
        printf("\n\nNemuzu otevrit paletu %s",p_jmeno);
        getchar();
        exit(0);
 }
 for(i = 0; i < pocet; i++)
    fread(_a_palety[i],1,768,paleta);

 fclose(paleta);
}

// fade palette to black
void stmav_paletu(int cislo)
{
	//printf("%s\n", __FUNCTION__);
 byte fadepal[768];
 int i,j;

 for(i = 1; i < 63; i++) {
    cekej_na_paprsek();
        for(j = 0; j < 768; j++)
			fadepal[j] = ((_a_palety[cislo][j]-i) < 0) ? 0 : (_a_palety[cislo][j]-i);
	SetPalette(0, 256, fadepal);
 }
}

// fade palette from black
void rozsvit_paletu(int cislo)
{
	//printf("%s\n", __FUNCTION__);
 byte fadepal[768];
 int i,j;

 for(i = 64; i > 0 ; i--) {
    cekej_na_paprsek();
        for(j = 0; j < 768; j++)
			fadepal[j] = ((_a_palety[cislo][j]-i) < 0) ? 0 : (_a_palety[cislo][j]-i);
	SetPalette(0, 256, fadepal);
 }
}

// set all-black palette
void smaz_paletu(void)
{
	//printf("%s\n", __FUNCTION__);
	// can be called before gfx init

	byte newpal[768];
	memset(newpal, 0, 768);
	SetPalette(0, 256, newpal);
}

// change palette
void nastav_paletu(int cislo)
{
	//printf("%s\n", __FUNCTION__);
 SetPalette(0, 256, _a_palety[cislo]);
}

// load palette
void nahraj_paletu_lep(int dat,byte *p_jmeno,int pocet)
{
	//printf("%s\n", __FUNCTION__);
 #define VEL_PAL (pocet*768)

 byte  *p_paleta;
 int    i;

 if((p_paleta = (byte *)malloc(VEL_PAL)) == NULL) {
    prepni_do_textu(0x3);
    printf("\nChyba : Nedostatek pameti !\n");
    getchar();
    exit(0);
 }

 nahraj_soubor_lep(VEL_PAL,p_paleta,dat,p_jmeno);

 for(i = 0; i < pocet; i++)
        memcpy(_a_palety[i],p_paleta+(i*768),768);
}


/* fontova knihovna */
/* (C) Asix fwp.*/

#define MAX_FONTU   20
#define MAX_PISMEN  60

typedef struct {

  //int   y; // vyska fontu je vzdy stejna
  int   bila;
  
  byte  *p_font;

} FONT_INFO;

FONT_INFO _a_fn[MAX_FONTU];
int       _a_pos_fn = 0;            // posledni font
byte     *_a_p_fn[MAX_FONTU][MAX_PISMEN];

/*--------------------------------------------------------------------------*/
/* funkce pro obsluhy fontu                                                 */
/*--------------------------------------------------------------------------*/
int ini_fn(void)
{
 _a_pos_fn = 0;
 return(0);
}

int indexuj_font(int cf)
{
 int i,p = 1;

 for(i = 0; i < MAX_PISMEN; i++) {
    _a_p_fn[cf][i] = _a_fn[cf].p_font+p;
#ifdef __AMIGA__
 SPRIT_HEAD *p_spr;

 p_spr = (SPRIT_HEAD *)_a_p_fn[cf][i];

 p_spr->x = SWAP32LE(p_spr->x);
 p_spr->y = SWAP32LE(p_spr->y);
 p_spr->barev = SWAP32LE(p_spr->barev);
#endif
    p += VH_SPRITU+x_sprit(_a_fn[cf].p_font+p)*y_sprit(_a_fn[cf].p_font+p);
 }
 i = 0;
 return(0);
}

int  nahraj_fonty(char *p_fn_file)
{
  #define VEL_FN  (velikost_file(p_fn_file)+10)

  if((_a_fn[_a_pos_fn].p_font =
    (byte *)malloc(VEL_FN)) == NULL) {
    return(CHYBA);
  }

  nahraj_soubor(velikost_file(p_fn_file),_a_fn[_a_pos_fn].p_font,
                p_fn_file,0);

  _a_fn[_a_pos_fn].bila = _a_fn[_a_pos_fn].p_font[0];
  //_a_fn[_a_pos_fn].y = y_sprit(_a_fn[_a_pos_fn].p_font);

  indexuj_font(_a_pos_fn);

  return(_a_pos_fn++);
}

int  nahraj_fonty_lep(int dat_file,char *p_fn_file)
{
  #define VEL_FNL (velikost_lepfile(dat_file,p_fn_file)+10)

  if((_a_fn[_a_pos_fn].p_font =
     (byte *)malloc(VEL_FNL)) == NULL) {
    return(CHYBA);
  }

  nahraj_soubor_lep(0,_a_fn[_a_pos_fn].p_font,dat_file,p_fn_file);

  _a_fn[_a_pos_fn].bila = _a_fn[_a_pos_fn].p_font[0];
  //_a_fn[_a_pos_fn].y = y_sprit(_a_fn[_a_pos_fn].p_font); // this is unused and starts at a wrong offset anyway

  indexuj_font(_a_pos_fn);

  return(_a_pos_fn++);
}

inline int znf(int z)
{

 if((z > 64)&&(z < 91)) { // Velky pismeno
   z -= 65;
   goto pis_znak;
 }
 if((z > 96)&&(z < 123)) {// Maly pismeno
   z -= 97;
   goto pis_znak;
 }
 if((z > 47)&&(z < 58)) { // Velky pismeno
   z -= (48-26);
   goto pis_znak;
 }
 if(z == ' ') {
   z = 57;
   goto pis_znak;
 }

 if(z == _CARKA) {
   z = 54;
   goto pis_znak;
 }
 if(z == _HACEK) {
   z = 53;
   goto pis_znak;
 }
 if(z == _KROUZEK) {
   z = 55;
   goto pis_znak;
 }

 if(z == ',') {
   z = 59;
   goto pis_znak;
 }
 if(z == '.') {
   z = 58;
   goto pis_znak;
 }

 if(z == '?') {
   z = 36;
   goto pis_znak;
 }
 if(z == '!') {
   z = 37;
   goto pis_znak;
 }
 if(z == '-') {
   z = 38;
   goto pis_znak;
 }
 if(z == '+') {
   z = 39;
   goto pis_znak;
 }
 if(z == 92) {
   z = 40;
   goto pis_znak;
 }
 if(z == '=') {
   z = 41;
   goto pis_znak;
 }
 if(z == ':') {
   z = 42;
   goto pis_znak;
 }
 if(z == ';') {
   z = 43;
   goto pis_znak;
 }
 if(z == '(') {
   z = 44;
   goto pis_znak;
 }
 if(z == ')') {
   z = 45;
   goto pis_znak;
 }
 if(z == '@') {
   z = 46;
   goto pis_znak;
 }
 if(z == '#') {
   z = 47;
   goto pis_znak;
 }
 if(z == '%') {
   z = 48;
   goto pis_znak;
 }
 if(z == '&') {
   z = 49;
   goto pis_znak;
 }
 if(z == '/') {
   z = 50;
   goto pis_znak;
 }
 if(z == '*') {
   z = 51;
   goto pis_znak;
 }
 if(z == '"') {
   z = 52;
   goto pis_znak;
 }
 if(z == '_') {
   z = 56;
   goto pis_znak;
 }

 z = 0; // Znak ktery neni v fontu se vytiskne jako mezera

pis_znak:
 return(z);
}

void printfn(int x, int y, int font, char *p_text)
{
 int i,z,d,b,ax,znam = 0,c;

 b = _a_fn[font].bila;
 d = strlen(p_text);
 ax = x;
 
 for(i = 0; i < d; i++) {
    z = p_text[i];

    if(z == ' ') {
      ax += 18;
      continue;
    }
    if(z == _CARKA) {
      znam = 54;
      continue;
    }
    if(z == _HACEK) {
      znam = 53;
      continue;
    }
    if(z == _KROUZEK) {
      znam = 55;
      continue;
    }
    c = znf(z);
    if(znam) {
      if(znam != 54)
        kresli_sprit_bar(_a_p_fn[font][znam],ax+(x_sprit(_a_p_fn[font][c])/2)-(x_sprit(_a_p_fn[font][znam])/2),y,b);
      else
        kresli_sprit_bar(_a_p_fn[font][znam],ax+(x_sprit(_a_p_fn[font][c])/2),y,b);

      znam = 0;
    }
    kresli_sprit_bar(_a_p_fn[font][c],ax,y,b);
    ax += x_sprit(_a_p_fn[font][c]);
 }
}

int putfn(int x, int y, int font, char z)
{
 int b,g;

 b = _a_fn[font].bila;
 g = znf(z);
 kresli_sprit_bar(_a_p_fn[font][g],x,y,b);
 return(x_sprit(_a_p_fn[font][g]));
}

inline int szn(int font, int z)
{
 return(x_sprit(_a_p_fn[font][znf(z)]));
}

inline int vzn(int font, int z)
{
 return(y_sprit(_a_p_fn[font][znf(z)]));
}

// funkce pro bufferovani
void buf_printfn(int x, int y, int font, char *p_text, byte *p_spr)
{
 int i,z,d,b,ax,znam = 0,c;

 b = _a_fn[font].bila;
 d = strlen(p_text);
 ax = x;
 
 for(i = 0; i < d; i++) {
    z = p_text[i];

    if(z == ' ') {
      ax += 18;
      continue;
    }
    if(z == _CARKA) {
      znam = 54;
      continue;
    }
    if(z == _HACEK) {
      znam = 53;
      continue;
    }
    if(z == _KROUZEK) {
      znam = 55;
      continue;
    }

    c = znf(z);
    if(znam) {
      if(znam != 54)
        buf_kresli_sprit_bar(_a_p_fn[font][znam],ax+(x_sprit(_a_p_fn[font][c])/2)-(x_sprit(_a_p_fn[font][znam])/2),y,b,p_spr);
      else
        buf_kresli_sprit_bar(_a_p_fn[font][znam],ax+(x_sprit(_a_p_fn[font][c])/2),y,b,p_spr);

      znam = 0;
    }
    buf_kresli_sprit_bar(_a_p_fn[font][c],ax,y,b,p_spr);
    ax += x_sprit(_a_p_fn[font][c]);
 }
}

int buf_putfn(int x, int y, int font, char z, byte *p_spr)
{
 int b,g;

 b = _a_fn[font].bila;
 g = znf(z);
 buf_kresli_sprit_bar(_a_p_fn[font][g],x,y,b,p_spr);
 return(x_sprit(_a_p_fn[font][g]));
}

inline int dlfn(char *p_str, int font)
{
 int delka = 0;

 while(*p_str != '\0') {
   if(!((*p_str == _HACEK)||(*p_str == _CARKA)||(*p_str == _KROUZEK)))
     delka += szn(font,*p_str);
   p_str++;
 }

 return(delka);
}

/*************************************************************************/
/* pom funkce ke spritum                                                 */
/*************************************************************************/
int hl_sprit(byte *p_ctverec)
{
 SPRIT_HEAD *p_sprit;

 p_sprit = (SPRIT_HEAD *)p_ctverec;
 return(p_sprit->barev);
}

int x_sprit(byte *p_ctverec)
{
 SPRIT_HEAD *p_sprit;

 p_sprit = (SPRIT_HEAD *)p_ctverec;
 return(p_sprit->x);
}

int y_sprit(byte *p_ctverec)
{
 SPRIT_HEAD *p_sprit;

 p_sprit = (SPRIT_HEAD *)p_ctverec;
 return(p_sprit->y);
}

void set_clip(int x1, int x2, int y1, int y2)
{
 /* nastaveni clipu pro X souradnice */
 if(x1 <= x2) {
   _a_clip_x_min = x1;
   _a_clip_x_max = x2;
 }
 else {
   _a_clip_x_min = x2;
   _a_clip_x_max = x1;
 }

 /* Nastaveni clipu pro Y souradnice */
 if(y1 <= y2) {
   _a_clip_y_min = y1;
   _a_clip_y_max = y2;
 }
 else {
   _a_clip_y_min = y2;
   _a_clip_y_max = y1;
 }
}

// creates an empty sprite
byte *vyrob_sprit( int x_res, int y_res, int barev, int lock)
{
 SPRIT_HEAD *p_spr;
 byte *p_sprite;
 int  velikost = (x_res*y_res)+VH_SPRITU+10;

 if((p_sprite = (byte *)malloc(velikost)) == NULL)
     return(NULL);

 p_spr = (SPRIT_HEAD *)p_sprite;

 p_spr->x = x_res;
 p_spr->y = y_res;
 p_spr->barev = barev;

 return(p_sprite);
}

// modify sprite structure
int  uprav_sprit(byte *p_sprite, int x_res, int y_res, int barev)
{
 SPRIT_HEAD *p_spr;

 p_spr = (SPRIT_HEAD *)p_sprite;

 p_spr->x = x_res;
 p_spr->y = y_res;
 p_spr->barev = barev;

 return(0);
 
}
/*
void zrus_sprit(byte *p_sprite)
{
 if(p_sprite != NULL) {
   free(p_sprite);
   p_sprite = NULL;
 }
 else
   fprintf(stderr,"Uvolnovani nuloveho spritu !\n");
}
*/
// load sprite from filesystem
byte *load_sprit(byte *p_file, int lock)
{
 byte *p_sprite;
 FILE *f;
 int  velikost;
 
 if((f = fopen(p_file,"rb")) == NULL)
    return(NULL);

 fseek(f,0,SEEK_END);
 velikost = ftell(f) + 1;
 fseek(f,0,SEEK_SET);
 
 if((p_sprite = malloc(velikost+10)) == NULL)
    return(NULL);

 fread(p_sprite,1,velikost,f);
#ifdef __AMIGA__
 SPRIT_HEAD *p_spr;

 p_spr = (SPRIT_HEAD *)p_sprite;

 p_spr->x = SWAP32LE(p_spr->x);
 p_spr->y = SWAP32LE(p_spr->y);
 p_spr->barev = SWAP32LE(p_spr->barev);
#endif
 fclose(f);
 
 return(p_sprite);
}

// load sprite from dat file
byte *load_sprit_lep(int dat_file, byte *p_file, int lock)
{
 byte *p_sprite;
 int  velikost;
 
 if((p_sprite = malloc(velikost = velikost_lepfile(dat_file,p_file)+10)) == NULL)
    return(NULL);

 if(!nahraj_soubor_lep(0,p_sprite,dat_file,p_file))
   return(NULL);

#ifdef __AMIGA__
 SPRIT_HEAD *p_spr;

 p_spr = (SPRIT_HEAD *)p_sprite;

 p_spr->x = SWAP32LE(p_spr->x);
 p_spr->y = SWAP32LE(p_spr->y);
 p_spr->barev = SWAP32LE(p_spr->barev);
#endif

// else
   return(p_sprite);
}

/*
---------------------------------------------------------------------------
 rotuj_sprit(byte *p_spr, byte *p_novy, int kolik)
 kolik -> 0 - nic
          1 - 90
          2 - 180
          3 - 270
----------------------------------------------------------------------------
*/
void rotuj_sprit(byte *p_sprit, byte *p_novy, int kolik)
{
 SPRIT_HEAD *p_spr;
 byte *p_read, *p_write;
 int i,j;

 p_spr = (SPRIT_HEAD *)p_sprit;
 p_read = p_sprit+VH_SPRITU;
 p_write = p_novy+VH_SPRITU;
 
 switch(kolik) {

   case 1: // 90 stupnu
     for(i = 0; i < p_spr->x; i++) {
        for(j = 0; j < p_spr->y; j++) {
           p_write[(i*p_spr->x)+j] = p_read[(p_spr->x*(p_spr->y-1-j))+i];
        }
     }
     break;
   case 2: // 180 stupnu -> prehodit vzhuru nohama
     for(i = 0; i < p_spr->y; i++) {
        memcpy(p_write+((p_spr->y-i-1)*p_spr->x),p_read+(i*p_spr->x),p_spr->x);
     }
     break;
   case 3: // 270 stupnu -> opak 90 stupnu
     for(i = 0; i < p_spr->x; i++) {
         for(j = 0; j < p_spr->y; j++)
            p_write[(i*p_spr->x)+j] = p_read[(p_spr->x*j)+i];
     }
     break;
   case 0:
     memcpy(p_write,p_read,p_spr->x*p_spr->y);
   default:
     break;
 }
}


// asm

// Kresleni spritu

// draw sprite to screen
void kresli_sprit(byte  *p_ctverec,dword xpozice,dword ypozice)
{
	//printf("%s\n", __FUNCTION__);

 SPRIT_HEAD *p_spr;
 byte *p_read;

 p_spr = (SPRIT_HEAD *)p_ctverec;
 p_read = p_ctverec+VH_SPRITU;

	if (p_spr->x % 16 == 0) {
		//WriteChunkyPixels(window->RPort, xpozice, ypozice, xpozice + p_spr->x - 1, ypozice + p_spr->y - 1, p_read, p_spr->x);
		WritePixelArray8(window->RPort, xpozice, ypozice, xpozice + p_spr->x - 1, ypozice + p_spr->y - 1, p_read, temprp);
	}
	else {
		int j;
		for(j = 0; j < p_spr->y; j++) {
			WritePixelLine8(window->RPort, xpozice, ypozice + j, p_spr->x, p_read, temprp);
			p_read += p_spr->x;
		}
	}
}

// draw sprite to screen masked with color key
void kresli_sprit_bar(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva)
{
	//printf("%s\n", __FUNCTION__);

 SPRIT_HEAD *p_sprr;
 byte *p_read;

 p_sprr = (SPRIT_HEAD *)p_ctverec;
 p_read = p_ctverec+VH_SPRITU;

 // TODO this function is still pretty slow
#if 0
	if (CyberGfxBase && GetCyberMapAttr(window->RPort->BitMap, CYBRMATTR_ISCYBERGFX))
	{
		int i,j;
		APTR handle;
		ULONG width, height, bpr;
		char *buffer;

		handle = LockBitMapTags(window->RPort->BitMap,
			LBMI_WIDTH, &width,
			LBMI_HEIGHT, &height,
			LBMI_BYTESPERROW, &bpr,
			LBMI_BASEADDRESS, &buffer);

		buffer += (ypozice*bpr)+xpozice;
		for(j = 0; j < p_sprr->y; j++) {
			for(i = 0; i < p_sprr->x; i++) {
				if (*p_read != barva) {
					*buffer = *p_read;
				}
				p_read++;
				buffer++;
			}
			buffer += (bpr - p_sprr->x);
		}

		UnLockBitMap(handle);
		return;
	}
#endif

#if 1
	int i,j;
	int startx;
	int width;
	byte *readptr;
	byte *drawptr;

	readptr = p_read;
	startx = width = 0;
	for(j = 0; j < p_sprr->y; j++) {
		drawptr = NULL;
		for(i = 0; i < p_sprr->x; i++) {
			if (*readptr != barva) {
				if (!drawptr)
				{
					drawptr = readptr;
					startx = i;
					width = 0;
				}
				width++;
			} else if (drawptr) {
				// transparent pixel mid-line
				WritePixelLine8(window->RPort, xpozice + startx, ypozice + j, width, drawptr, temprp);
				drawptr = NULL;
			}
			readptr++;
		}

		if (drawptr) {
			// ended on a non-transparent pixel
			WritePixelLine8(window->RPort, xpozice + startx, ypozice + j, width, drawptr, temprp);
		}
	}
#else
 int i,j,k;

 k = 0;
     for(j = ypozice; j < ypozice+p_spr->y/*-1*/; j++) {
        for(i = xpozice; i < xpozice+p_spr->x/*-1*/; i++) {
			if (p_read[k] != barva) {
				SetAPen(window->RPort, p_read[k]);
				WritePixel(window->RPort, i, j);
			}
			k++;
        }
     }
#endif
}

// draw a sprite to another sprite?
void buf_kresli_sprit(byte *p_sprite,dword xpozice,dword ypozice,byte *p_buffer)
{
	//printf("%s\n", __FUNCTION__);
 SPRIT_HEAD *p_sprr, *p_sprw;
 byte *p_read, *p_write;
 int i,j;

 p_sprr = (SPRIT_HEAD *)p_sprite;
 p_sprw = (SPRIT_HEAD *)p_buffer;
 p_read = p_sprite+VH_SPRITU;
 p_write = p_buffer+VH_SPRITU;

	p_write += (ypozice*p_sprw->x)+xpozice;
	for(j = 0; j < p_sprr->y; j++) {
		for(i = 0; i < p_sprr->x; i++) {
			*p_write = *p_read;
			p_read++;
			p_write++;
		}
		p_write += (p_sprw->x - p_sprr->x);
	}
}

// draw sprite into another sprite with color keying?
void buf_kresli_sprit_bar(byte  *p_sprite,dword xpozice,dword ypozice,dword barva,byte *p_buffer)
{
	//printf("%s\n", __FUNCTION__);
 SPRIT_HEAD *p_sprr, *p_sprw;
 byte *p_read, *p_write;
 int i,j;

 p_sprr = (SPRIT_HEAD *)p_sprite;
 p_sprw = (SPRIT_HEAD *)p_buffer;
 p_read = p_sprite+VH_SPRITU;
 p_write = p_buffer+VH_SPRITU;

	p_write += (ypozice*p_sprw->x)+xpozice;
	for(j = 0; j < p_sprr->y; j++) {
		for(i = 0; i < p_sprr->x; i++) {
			if (*p_read != barva) {
				*p_write = *p_read;
			}
			p_read++;
			p_write++;
		}
		p_write += (p_sprw->x - p_sprr->x);
	}
}

// Ukladani spritu

// copy screen to buffer, used for screenshots
void uloz_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice)
{
	//printf("%s\n", __FUNCTION__);

	//ReadPixelArray8(window->RPort, xpozice, ypozice, _a_x - xpozice, _a_y - ypozice, p_ctverec, temprp);
	int j;
	for(j = 0; j < _a_y; j++) {
		ReadPixelLine8(window->RPort, xpozice, ypozice + j, _a_x, p_ctverec, temprp);
		p_ctverec += _a_x;
	}
}

// copy screen to sprite
void uloz_sprit(byte  *p_sprite,dword xpozice,dword ypozice)
{
	//printf("%s\n", __FUNCTION__);

 SPRIT_HEAD *p_spr;
 byte *p_write;

 p_spr = (SPRIT_HEAD *)p_sprite;
 p_write = p_sprite+VH_SPRITU;

	if (p_spr->x % 16 == 0) {
		ReadPixelArray8(window->RPort, xpozice, ypozice, xpozice + p_spr->x - 1, ypozice + p_spr->y - 1, p_write, temprp);
	}
	else {
		int j;
		for(j = 0; j < p_spr->y; j++) {
			ReadPixelLine8(window->RPort, xpozice, ypozice + j, p_spr->x, p_write, temprp);
			p_write += p_spr->x;
		}
	}
}

// copy a sprite into another sprite
void buf_uloz_sprit(byte *p_sprite,dword xpozice,dword ypozice,byte *p_buffer)
{
	//printf("%s\n", __FUNCTION__);

 SPRIT_HEAD *p_sprr, *p_sprw;
 byte *p_read, *p_write;
 int i,j;

 p_sprr = (SPRIT_HEAD *)p_buffer;
 p_sprw = (SPRIT_HEAD *)p_sprite;
 p_read = p_buffer+VH_SPRITU;
 p_write = p_sprite+VH_SPRITU;

	p_read += (ypozice*p_sprr->x)+xpozice;
	for(j = 0; j < p_sprw->y; j++) {
		for(i = 0; i < p_sprw->x; i++) {
			*p_write = *p_read;
			p_read++;
			p_write++;
		}
		p_read += (p_sprr->x - p_sprw->x);
	}
}

// Vyplnovani ctvercu

// fill the screen with a color
void vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v)
{
	//printf("%s\n", __FUNCTION__);

	SetAPen(window->RPort, barva);
	RectFill(window->RPort, xpozice, ypozice, xpozice + x_v - 1, ypozice + y_v - 1);
}

// fill a sprite with a color
void spr_vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v,byte *p_sprite)
{
	//printf("%s\n", __FUNCTION__);
 SPRIT_HEAD *p_spr;
 byte *p_write;
 int i,j;

 p_spr = (SPRIT_HEAD *)p_sprite;
 p_write = p_sprite+VH_SPRITU;

	p_write += (ypozice*p_spr->x)+xpozice;
	for(j = 0; j < y_v; j++) {
		for(i = 0; i < x_v; i++) {
			*p_write++ = barva;
		}
		p_write += (p_spr->x - x_v);
	}
}

// wait for retrace
void cekej_na_paprsek(void)
{
	//printf("%s\n", __FUNCTION__);
	WaitTOF();
}

// trusty debug print function
#include <stdarg.h>
void BE_ST_DebugText(int x, int y, const char *fmt, ...)
{
	UBYTE buffer[256];
	struct IntuiText WinText = {BLOCKPEN, DETAILPEN, JAM2, 0, 0, NULL, NULL, NULL};
	va_list ap;

	WinText.IText = buffer;

	va_start(ap, fmt); 
	vsnprintf(buffer, sizeof(buffer), fmt, ap);
	va_end(ap);
// pen 246
	PrintIText(window->RPort, &WinText, x, y);
}
