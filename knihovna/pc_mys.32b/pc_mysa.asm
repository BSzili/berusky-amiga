; Mysi funkce
; pri pohybu dolu odecitat velikost x a y

; Verejne - public funkce a promene

                BITS    32
                
GLOBAL  _preruseni_mysi
GLOBAL  _delka_preruseni_mysi
GLOBAL  __sel_xy
GLOBAL  __p_info
GLOBAL  __x_velikost_kurzoru    
GLOBAL  __y_velikost_kurzoru    
GLOBAL  __p_na_kurzor   
GLOBAL  __p_na_pozadi   
GLOBAL  __barva         
GLOBAL  __aktivni_mys

EXTERN  __a_x
EXTERN  __a_y
EXTERN  _uloz_ctverec
EXTERN  _kresli_ctverec
EXTERN  _kresli_ctverec_bar

EXTERN _aktivni_kres
EXTERN _aktivni_vypln


        section .data
; Promeny soupni do kodovyho segmentu

__sel_xy        DW      0       ; Selektor pro promene
__x             DD      0
__y             DD      0
__p_info        DD      0       
__x_velikost_kurzoru    DD      0
__y_velikost_kurzoru    DD      0
__p_na_kurzor   DD      0
__p_na_pozadi   DD      0
__barva         DW      0
__aktivni_mys   DW      0

_delka_preruseni_mysi   DD      (_konec_preruseni_mysi - _preruseni_mysi)

        section .text
                
;Mysoidni funkce - procedura, ktera provede obsluhu mysi, vykresli na 
;obrazovku kurzor a nahodi info
;--------------------------------------------------------------------------
;Procedura obsluhy mysi
;--------------------------------------------------------------------------
_preruseni_mysi:

;   Nastav stav pro bezpecny navrat z tohoto pekla !!!!!!!!!!!!!!!!!!!!!

        mov     ax,word [ds:esi]        ;Nacti registr ip
        mov     word [es:edi+0x2A],ax

        mov     ax,word  [ds:esi+2]     ;Nacti registr cs
        mov     word [es:edi+0x2C],ax

        add     word [es:edi+0x2E],4    ;Pricti zasobnik
; Tento horni radek me stal cely odpoledne !!!
; Uloz registry s info o navratu
        push    es
        push    edi     

;  Selektor v DS nastav na datovy promeny
        mov     ax,word [cs:__sel_xy]
        mov     ds,ax


        cmp     dword [_aktivni_kres],0  ; Knihovni fce jsou aktivni -> nemuze kreslit
        jz     .jedem_dal 
        jmp     .konci_obsluhu

.jedem_dal:
        
;  Do edi a esi dej adresy promenych x a y 
;  urcuji velikost bitmapy kresleny gr funkcema
        mov     eax,dword [__a_x]
        mov     ebx,dword [__a_y]

; Schovej puvodni hodnoty x a y do __x a __y    
        mov     dword [__x],eax
        mov     dword [__y],ebx

        mov     eax,dword [__x_velikost_kurzoru]
        mov     ebx,dword [__y_velikost_kurzoru]
;  A nastav vlastni s velikosti kurzoru
        mov     dword [__a_x],eax
        mov     dword [__a_y],ebx

        test    dword [es:edi+0x1C],1   ;v [es:edi] je hodnota stisku
        jz      .neni_posun_1
;       Nastav esi na pole p_info kde je info o pozici mysi             
        mov     esi,dword [cs:__p_info]
        
; Obnov na mynule pozici kurzoru pozadi z bufferu
        cmp     word [cs:__aktivni_mys],1
        jnz     .neni_posun_1

        push    word 0
        push    word [esi+4] ;y
        push    word 0
        push    word [esi+2] ;x
        push    dword [__p_na_pozadi]
        call    _kresli_ctverec
        add     sp,12   
.neni_posun_1:

;       Nacteni parametru od ovladace mysi
;       Pozice kurzoru a podobne
        mov     esi,dword [cs:__p_info]

        
        mov     es,word [esp+4]
        mov     edi,dword [esp]
        

;   Nacti registry RM z callbacku a zmen v p_info
        xor     edx,edx
        mov     ebx,dword [es:edi+0x10]
        mov     ecx,dword [es:edi+0x18]
        mov     edx,dword [es:edi+0x14]

;       shr     cx,3
;       shr     dx,3

;       Nastav promenou pro nadrizeny program
;       Pozice a priznaky mysi
        mov     word [ds:esi],bx
        mov     word [ds:esi+2],cx
        mov     word [ds:esi+4],dx

        test    dword [es:edi+0x1C],1   ;v [es:edi] je hodnota stisku
        jz      .nic
        cmp     word [cs:__aktivni_mys],1
        jnz     .nic

        push    dx
        push    cx

        push    word 0
        push    word dx
        push    word 0
        push    word cx
        push    dword [__p_na_pozadi]
        call    _uloz_ctverec
        add     sp,12
        
        pop     cx
        pop     dx

        push    word 0
        push    word [__barva]
        push    word 0
        push    word dx
        push    word 0
        push    word cx
        push    dword [__p_na_kurzor]
        call    _kresli_ctverec_bar
        add     sp,16
.nic:
;   Obnov zpatky promene x a y s velikostmi kreslene bitmapy
        mov     eax,dword [__x]
        mov     ebx,dword [__y]
        
        mov     dword [__a_x],eax
        mov     dword [__a_y],ebx

; Nastav zpatky registry es:edi a proved navrat do rm
.konci_obsluhu:

        pop     edi
        pop     es

        iret

_konec_preruseni_mysi: