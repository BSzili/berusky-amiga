#include "..\libanak.h"

#define RYCHLOST	0

#define DEF_X     640
#define DEF_Y     480

static int mys_aktivni = 0;

int zapni_mys(volatile word *p_info, byte *p_sprit)
{
   __dpmi_regs  r;
   word         _x,_y;
   int          i;
   int          vel;
   
   /* detekuj pritomnost mysi */
   r.x.ax = 0x0;
   __dpmi_int(MYS_INT,&r);
   if(r.x.ax == 0)
       return(0);

   /*inicialnizuj promene ovladace mysi */
   // Nastaveni barev pozadi u kurzoru
   _barva = BILA;
   _x_velikost_kurzoru = x_sprit(p_sprit);   /* nastav velikosti kurzoru */
   _y_velikost_kurzoru = y_sprit(p_sprit);
   _p_na_kurzor = p_sprit+VH_SPRITU;

   vel = (_x_velikost_kurzoru*_y_velikost_kurzoru)+VH_SPRITU+10;

   /* alokuj pamet na pozadi */
   if((_p_na_pozadi = (byte *)malloc(vel)) == NULL)
     return(0);

   /* alokuj call-back do protektu */
   if(__dpmi_allocate_real_mode_callback(preruseni_mysi,&_registry_mysi,
                                     &_real_modova_adresa) == -1)
      return(0);

   /* vynuluj strukturu informaci o mysi */
//   for(i = 0;i < POLOZEK_MYSI;i++)
   p_info[0] = 0;

   _p_info = p_info; /* nastav pointer na info o mysi */
   _sel_xy = _my_ds();  /* nastav base selektor */

   /* oddleny zamek */
  _go32_dpmi_lock_data(_p_na_kurzor,_x_velikost_kurzoru*_y_velikost_kurzoru);
  _go32_dpmi_lock_data(&_p_na_kurzor,sizeof(_p_na_kurzor));
  _go32_dpmi_lock_data(_p_na_pozadi,vel);
  _go32_dpmi_lock_data(&_p_na_pozadi,sizeof(_p_na_pozadi));


   /* uzamkni pouzite promene */
  _go32_dpmi_lock_data(&_x_velikost_kurzoru,sizeof(_x_velikost_kurzoru));
  _go32_dpmi_lock_data(&_y_velikost_kurzoru,sizeof(_y_velikost_kurzoru));
  _go32_dpmi_lock_data(_p_info,sizeof(_p_info[POLOZEK_MYSI]));
  _go32_dpmi_lock_data(&_p_info,sizeof(_p_info));
  _go32_dpmi_lock_data(&_barva,sizeof(_barva));
  _go32_dpmi_lock_data(&_sel_xy,sizeof(_sel_xy));
  _go32_dpmi_lock_data(&_registry_mysi,sizeof(_registry_mysi));
  _go32_dpmi_lock_code(preruseni_mysi,delka_preruseni_mysi);

  /* provedeni uzamceni grafickych funkci a procedur v graficke
  knihovne */

// Nastav vlastni obsluhu mysi
   r.x.ax = 0x0C;
   r.x.cx = 0x0B;
   r.x.dx = _real_modova_adresa.offset16;
   r.x.es = _real_modova_adresa.segment;
   __dpmi_int(MYS_INT,&r);

   if(_a_av_mod == 0)
      rozsah_mysi(0,0,DEF_X-_x_velikost_kurzoru,DEF_Y-_y_velikost_kurzoru);
   else
      rozsah_mysi(0,0,_a_xres-_x_velikost_kurzoru,_a_yres-_y_velikost_kurzoru);
//      rozsah_mysi(0,0,_a_xres,_a_yres);

/* uprav mys i pro geniusy */
/*   r.x.ax = 0x1b;
   __dpmi_int(MYS_INT,&r);
   stara_rychlost_x = r.x.bx;
   stara_rychlost_y = r.x.cx;
   
//   r.x.cx *= 8;
//   r.x.bx *= 8;
   r.x.ax = 0x1a;
   __dpmi_int(MYS_INT,&r);
*/
   mys_aktivni = 1;
   return(1);
}

void rozsah_mysi(int x_min, int y_min, int x_max, int y_max)
{
   __dpmi_regs  r;

/* Nastaveni ovladace mysi */
   // Nastav vodorovny rozsah
   r.x.ax = 0x07;
   r.x.cx = x_min;
   r.x.dx = x_max;
   __dpmi_int(MYS_INT,&r);

// Nastav horizontalni rozsah
   r.x.ax = 0x08;
   r.x.cx = y_min;
   r.x.dx = y_max;//(yres-y_velikost);
   __dpmi_int(MYS_INT,&r);
}

void souradnice_mysi(int x, int y)
{
   __dpmi_regs  r;

/* Nastaveni ovladace mysi */
   // Nastav souradnice
   r.x.ax = 0x04;
   r.x.cx = mysi_info[1] = x;
   r.x.dx = mysi_info[2] = y;
   __dpmi_int(MYS_INT,&r);
}

void vypni_mys(void)
{
   __dpmi_regs  r;

   if(mys_aktivni != 1)
      return;
   
   // Provede reset mysi
   r.x.ax = 0;
   __dpmi_int(MYS_INT,&r);

   // Vypni proceduru s ovladacem
   r.x.ax = 0x0C;
   r.x.cx = 0x0;
   __dpmi_int(MYS_INT,&r);

   // Uvolni callback
   __dpmi_free_real_mode_callback(&_real_modova_adresa);

   // Vrat zpet rychlost pohybu kurzoru
/*
   r.x.cx = stara_rychlost_y;
   r.x.bx = stara_rychlost_x;
   r.x.ax = 0x1a;
   __dpmi_int(MYS_INT,&r);
*/
   free(_p_na_pozadi);
   mys_aktivni = 0;
}

/* vypne zobrazovani kurzoru na obrazovce */
void vypni_kurzor(volatile word *p_info)
{
 dword _x,_y;

 if(!_aktivni_mys)
	return;

 _x = _a_x; _y = _a_y;
 _a_x = _x_velikost_kurzoru;
 _a_y = _y_velikost_kurzoru;
 _aktivni_mys = 0;
 kresli_ctverec(_p_na_pozadi,p_info[1],p_info[2]);
 _a_x = _x; _a_y = _y;
}

/* zapne zobrazovani kurzoru na obrazovce */
void zapni_kurzor(volatile word *p_info)
{
 dword _x,_y;
 __dpmi_regs  r;

 if(_aktivni_mys)
	return;
// Nastavi polohu mysi
 r.x.ax = 0x4;
 r.x.cx = mysi_info[1];
 r.x.dx = mysi_info[2];
 __dpmi_int(MYS_INT,&r);

 _x = _a_x; _y = _a_y;
 _a_x = _x_velikost_kurzoru;
 _a_y = _y_velikost_kurzoru;
 _aktivni_mys = 1;
 uloz_ctverec(_p_na_pozadi,p_info[1],p_info[2]);
 kresli_ctverec_bar(_p_na_kurzor,p_info[1],p_info[2],BILA);
 _a_x = _x; _a_y = _y;
}

void nastav_rychlost_mysi(word x_rychlost,word y_rychlost)
{
   __dpmi_regs  r;

   r.x.bx = x_rychlost;
   r.x.cx = y_rychlost;
   r.x.ax = 0x1a;
   __dpmi_int(MYS_INT,&r);
}

void nastav_kurzor_ctverec(volatile word *p_info, byte *p_ctverec, dword x_res, dword y_res)
{
 int vel;
 
 free(_p_na_pozadi);

 _x_velikost_kurzoru = x_res;
 _y_velikost_kurzoru = y_res;
 _p_na_kurzor = p_ctverec;

 vel = (_x_velikost_kurzoru*_y_velikost_kurzoru)+VH_SPRITU+10;
 /* alokuj pamet na pozadi */
 if((_p_na_pozadi = (byte *)malloc(vel)) == NULL)
   return(0);

  /* oddleny zamek */
 _go32_dpmi_lock_data(_p_na_kurzor,_x_velikost_kurzoru*_y_velikost_kurzoru);
 _go32_dpmi_lock_data(&_p_na_kurzor,sizeof(_p_na_kurzor));
 _go32_dpmi_lock_data(_p_na_pozadi,vel);
 _go32_dpmi_lock_data(&_p_na_pozadi,sizeof(_p_na_pozadi));

 if(_a_av_mod == 0)
    rozsah_mysi(0,0,DEF_X-_x_velikost_kurzoru,DEF_Y-_y_velikost_kurzoru);
 else
    rozsah_mysi(0,0,_a_xres-_x_velikost_kurzoru,_a_yres-_y_velikost_kurzoru);

}

void nastav_kurzor_sprit(volatile word *p_info, byte *p_sprit)
{
 int vel;
 
 free(_p_na_pozadi);

 _x_velikost_kurzoru = x_sprit(p_sprit);   /* nastav velikosti kurzoru */
 _y_velikost_kurzoru = y_sprit(p_sprit);
 _p_na_kurzor = p_sprit+VH_SPRITU;

 vel = (_x_velikost_kurzoru*_y_velikost_kurzoru)+VH_SPRITU+10;

 /* alokuj pamet na pozadi */
 if((_p_na_pozadi = (byte *)malloc(vel)) == NULL)
   return(0);

  /* oddleny zamek */
 _go32_dpmi_lock_data(_p_na_kurzor,_x_velikost_kurzoru*_y_velikost_kurzoru);
 _go32_dpmi_lock_data(&_p_na_kurzor,sizeof(_p_na_kurzor));
 _go32_dpmi_lock_data(_p_na_pozadi,vel);
 _go32_dpmi_lock_data(&_p_na_pozadi,sizeof(_p_na_pozadi));

 if(_a_av_mod == 0)
    rozsah_mysi(0,0,DEF_X-_x_velikost_kurzoru,DEF_Y-_y_velikost_kurzoru);
 else
    rozsah_mysi(0,0,_a_xres-_x_velikost_kurzoru,_a_yres-_y_velikost_kurzoru);

}
