/*
 Lep funkce pro zapakovanou verzi
 dat souboru...
*/

#include "../libanak.h"

#define MAX_HANDLE  20


FILE              * lepfile[MAX_HANDLE];
POLOZKA_SOUBORU   * hlavicky[MAX_HANDLE];
HLAVICKA_LEPU       hl_lep[MAX_HANDLE];

int  posl_lep = 0;

int otevri_soubor_lep(byte *p_lep_file)
{

 if((posl_lep+1) >= MAX_HANDLE)
   return(NULL);

 if((lepfile[posl_lep] = fopen(p_lep_file,"rb")) == NULL) {
   return(NULL);
 }

 fread(&hl_lep[posl_lep],sizeof(HLAVICKA_LEPU),1,lepfile[posl_lep]);

 if((hlavicky[posl_lep] = malloc(sizeof(POLOZKA_SOUBORU)*hl_lep[posl_lep].souboru)) == NULL) {
    fclose(lepfile[posl_lep]);
    return(NULL);
 }
 fread(hlavicky[posl_lep],sizeof(POLOZKA_SOUBORU),hl_lep[posl_lep].souboru,lepfile[posl_lep]);

 return(posl_lep);
}

int zavri_soubor_lep(int handle)
{
 free(hlavicky[handle]);
 fclose(lepfile[handle]);
}

/* Nahraje soubor do pameti */
int nahraj_soubor( dword delka,byte  *p_mem,byte *p_file,long adresa)
{
FILE 	*poz;
dword 	nacteno = 0;

	if((poz = fopen(p_file,"rb")) == NULL) {
		return(0);
	}
   
	fseek(poz,adresa,SEEK_SET);
	nacteno = fread(p_mem,1,delka,poz);
	fclose(poz);
   return(nacteno);
}

/* Nahraje soubor do pameti ze slepence*/
int nahraj_soubor_lep(dword delka,byte  *p_mem,int lep_file,byte *p_file)
{
 dword nacteno = 0;
 byte *p_pack;
 int i,err,odpak;

   for (i = 0; i < hl_lep[lep_file].souboru; i++) {
      if(!strcmp(hlavicky[lep_file][i].jmeno,p_file))
         break;
   }
   if(i == hl_lep[lep_file].souboru) return(0);
   
   fseek(lepfile[lep_file],hlavicky[lep_file][i].adresa,SEEK_SET);

   if(delka == 0)
      delka = hlavicky[lep_file][i].d_unp;

   if((p_pack = malloc(hlavicky[lep_file][i].d_pak+1024)) == NULL) {
     printf("\n\nChyba alokace pameti !\n");
     exit(CHYBA);
   }
   
   nacteno = fread(p_pack,1,hlavicky[lep_file][i].d_pak,lepfile[lep_file]);
   odpak = hlavicky[lep_file][i].d_unp+24;

   if(nacteno) {
     err = uncompress(p_mem, &odpak, p_pack, delka);
     CHECK_ERR(err, "uncompress");
   }
   return(nacteno);
}

/* vrati pointer na zacatek slepence*/
/* ten si musim potom sam zavrit */
/* = NULL -> chyba */
FILE * zjisti_soubor_lep(int lep_file,byte *p_file)
{
   int i;
   for (i = 0; i < hl_lep[lep_file].souboru; i++) {
      if(!strcmp(hlavicky[lep_file][i].jmeno,p_file))
         break;
   }
   if(i == hl_lep[lep_file].souboru) return(0);
   
   fseek(lepfile[lep_file],hlavicky[lep_file][i].adresa,SEEK_SET);

   return(lepfile[lep_file]);
}

int velikost_lepfile(int lep_file, byte *p_file)
{
   int i;

   for (i = 0; i < hl_lep[lep_file].souboru; i++) {
      if(!strcmp(hlavicky[lep_file][i].jmeno,p_file))
         break;
   }
   if(i == hl_lep[lep_file].souboru) return(0);

   return(hlavicky[lep_file][i].d_unp);
}

int velikost_file(byte *p_file)
{
 FILE *f;
 int  velikost;
 
 if((f = fopen(p_file,"rb")) == NULL)
    return(NULL);

 fseek(f,0,SEEK_END);
 velikost = ftell(f);
 fclose(f);
 return(velikost);
}

int velikost_file_FILE(FILE *f)
{
 int  velikost;
 int  zal;

 zal = ftell(f);
 fseek(f,0,SEEK_END);
 velikost = ftell(f) + 1;
 fseek(f,zal,SEEK_SET);
 
 return(velikost);
}

