#include "..\libanak.h"

volatile int klavesnice_aktivni = 0;
volatile byte key[POCET_KLAVES];
volatile byte _a_klavesa[4];
volatile byte _a_asc[256]={' ',' ','1','2','3','4','5','6','7','8','9','0','-','+',
                        ' ',' ','q','w','e','r','t','y','u','i','o','p','[',
                        ']',' ',' ','a','s','d','f','g','h','j','k','l',';',
                        '"','~',' ','\\','z','x','c','v','b','n','m',',','.',
                        '/',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                        ' ',' ',' ',' ',' ',' ',' ',' ',' '};
                        

/* rutina na zapnuti obsluhy klavesnice */
/* jako parametr je buffer na 4 znaky, delka 4 byte */
void zapni_klavesnici(void)
{
  __dpmi_paddr addr;
  __dpmi_regs  r;

  r.x.ax = 0x0305;
  r.x.bx = 0x0;
  __dpmi_int(0x16,&r);

  _go32_dpmi_lock_data(_a_klavesa,sizeof(byte)*4);
  _go32_dpmi_lock_code(int_09,length_int_09);

  sel_klavesy = _my_ds();
  off_klavesy = (long)_a_klavesa;

  addr.offset32 = (long)int_09;
  addr.selector = _my_cs();

  __dpmi_get_protected_mode_interrupt_vector(KEYBOARD_INT,&old_int_09);
  __dpmi_set_protected_mode_interrupt_vector(KEYBOARD_INT,&addr);

  klavesnice_aktivni = 1;
}

/* vypne obsluhu klavesnice */
void vypni_klavesnici(void)
{
  __dpmi_regs  r;

  if(klavesnice_aktivni != 1)
     return;

  __dpmi_set_protected_mode_interrupt_vector(KEYBOARD_INT,&old_int_09);

  klavesnice_aktivni = 0;

  r.x.ax = 0x0300;
  __dpmi_int(0x16,&r);
  
  zapni_repeat();
}

void vypni_repeat(void)
{
  __dpmi_regs  r;

  r.x.ax = 0x0304;
  __dpmi_int(0x16,&r);
}

void zapni_repeat(void)
{
  __dpmi_regs  r;

  r.x.ax = 0x0300;
  __dpmi_int(0x16,&r);
}

int getkb(void)
{
 _a_klavesa[0] = 0;
 while((_a_klavesa[0] == 0)||(_a_klavesa[0] > 0x79));
 return(_a_klavesa[0]);
}

int getas(void)
{
 _a_klavesa[1] = 0;
 while(_a_klavesa[1] == 0);
 return(_a_klavesa[1]);
}

int getkba(void)
{
 return(_a_klavesa[0]);
}

int getasa(void)
{
 return(_a_klavesa[1]);
}

void clrkb(void)
{
 _a_klavesa[0] = 0;
 _a_klavesa[1] = 0;
}

