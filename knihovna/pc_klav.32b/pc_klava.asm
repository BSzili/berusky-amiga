; Rutiny v assembleru na obsluhu klavesnice

		BITS	32

		
; Verejne - public funkce a promene
GLOBAL _int_09
GLOBAL _length_int_09
GLOBAL _sel_klavesy
GLOBAL _off_klavesy
GLOBAL _ctrl
GLOBAL _alt
GLOBAL _blue_death_on

EXTERN __moje_ds
EXTERN _key
EXTERN ___djgpp_ds_alias
EXTERN __a_asc

		section .text

_length_int_09	DD	(_end_int_09 - _int_09)
_sel_klavesy	DD	0
_off_klavesy	DD	0
_ctrl		DD	0
_alt		DD	0
_blue_death_on  DD	0

;		section .text
;Klavesnicove funkce
;--------------------------------------------------------------------------
; Funkce,ktera zapne obsluhu klavesnice
;--------------------------------------------------------------------------
;Procedura obsluhy klavesnice

_int_09:

   push  es
   push  ds
   pushad
	
   mov	ax,[cs:___djgpp_ds_alias]
   mov	ds,ax
   mov   es,ax

   mov 	esi,[_off_klavesy]

   xor   eax,eax
   in	   al,0x60

   cmp	al,0xE0
   jnz	.neni
   jmp  .konec_cteni
.neni:
 
   mov	byte [ds:esi],al

   mov   edi,__a_asc
   add   edi,eax
   mov   bl,byte [ds:edi]
   mov   byte [ds:esi+1],bl

   mov   edi,_key   

   test   al,0x80
   jnz   .uvolneni

   add   edi,eax
   mov   byte [ds:edi],1
   jmp   .dale_od_hradu

.uvolneni:
   and   al,0x7f
   add   edi,eax

   mov   byte [ds:edi],0

.dale_od_hradu:
	cmp	al,0x1d
	jnz	.neni_ctrl
	mov	dword [_ctrl],1
.neni_ctrl:

	cmp	al,0x38
	jnz	.neni_alt
	mov	dword [_alt],1
.neni_alt:

	cmp	al,0x1d+0x80
	jnz	.neni_pov_ctrl
	mov	dword [_ctrl],0
.neni_pov_ctrl:

	cmp	al,0x38+0x80
	jnz	.neni_pov_alt
	mov	dword [_alt],0
.neni_pov_alt:
	
	cmp	al,0x53
	jnz	.neni_del

	test	dword [_alt],1
	jz	.neni_del

	test	dword [_ctrl],1
	jz	.neni_del

	mov	dword [_alt],0
	mov	dword [_ctrl],0
	
	mov	edi,_blue_death_on
	mov	byte [ds:edi],01
	mov	edi,[_off_klavesy]
.neni_del:	
.konec_cteni:
	mov	al,0x20
	out	0x20,al

   popad
   pop   ds
	pop	es

	iret
_end_int_09: