/*
          .���� �    � ����.     �    � �����.  ����� .����. �    �
         .�   � ��.  � �   �.    �    � �    �  �.    �.   � ��.  �
         �    � � �  � �    �    �   �. �.   �  �     �    � � �  �
       .������� �  � � �������.  ����.  �����.  ��    �    � �  � �
      .�      � �  .�� �      �. �   �  �    �  �.    �.   � �  .��
      �.      � �    � �      .� �   .� �    .� ����� .����. �    �
      (c) 1998 AnakreoN Freeware

      Graficka knihovna (C) Komat / Anakreon 1999
*/

#ifndef __ANAKREON_LIB
#define __ANAKREON_LIB

#include <go32.h>
#include <dpmi.h>
#include <stdio.h>
#include <dos.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <sys/nearptr.h>
#include <sys/farptr.h>

#include "vesahead.h"
#include "zakl_typ.h"
#include "zlib.h"

#define  BILA  0xff

#define  CHYBA   0xffff
#define  A_CHYBA 0xffff
#define  A_OK    0xfff1

#define  LOCK    1
#define  UNLOCK  0

/* pro djgpp z allegra :-) */
#define END_OF_FUNCTION(x)    void x##_end() { }
#define LOCK_VARIABLE(x)      _go32_dpmi_lock_data((void *)&x, sizeof(x))
#define LOCK_FUNCTION(x)      _go32_dpmi_lock_code(x, (long)x##_end - (long)x)

#define ENABLE()              asm volatile ("sti")
#define DISABLE()             asm volatile ("cli")


#ifndef VYPIS
#define VYPIS		stdout
#endif

#define vypni_buffer(x) setvbuf(x, NULL, _IONBF, 0);

#define VH_SPRITU  24

// Interni pointer
void (*p_kresli_ctverec)(byte  *p_ctverec,dword xpozice,dword ypozice);
void (*p_kresli_ctverec_bar)(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva);
void (*p_uloz_ctverec)(byte  *p_ctverec,dword xpozice,dword ypozice);
void (*p_pis_znak_fn)(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva,byte *p_barvy);
void (*p_vypln_ctverec)(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v);
void (*p_putpixel)(int x, int y, int barva);

/* pokud neni pristumny lbf, pouziji se nahradni bankovany funkce */
void b_kresli_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice);
void b_uloz_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice);
void b_kresli_ctverec_bar(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva);
void b_pis_znak_fn(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva,byte *p_barvy);
void b_vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v);
void b_putpixel(int x, int y, int barva);

/* standartni funkce pro lbf */
void l_kresli_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice);
void l_uloz_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice);
void l_kresli_ctverec_bar(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva);
void l_pis_znak_fn(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva,byte *p_barvy);
void l_vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v);
void l_putpixel(int x, int y, int barva);

/*
  Samotne fce.
*/

// Kresleni ctvercu
void kresli_ctverec_xy(byte  *p_ctverec,dword xpozice,dword ypozice);
void kresli_ctverec_bar_xy(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva);
void kresli_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice);
void kresli_ctverec_bar(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva);
void buf_kresli_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice, byte *p_buffer);
void buf_kresli_ctverec_bar(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva, byte *p_buffer);

// Kresleni spritu
void kresli_sprit(byte  *p_ctverec,dword xpozice,dword ypozice);
void kresli_sprit_bar(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva);
void buf_kresli_sprit(byte *p_sprite,dword xpozice,dword ypozice,byte *p_buffer);
void buf_kresli_sprit_bar(byte  *p_sprite,dword xpozice,dword ypozice,dword barva,byte *p_buffer);

// Ukladani spritu
void uloz_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice);
void uloz_sprit(byte  *p_sprite,dword xpozice,dword ypozice);
void buf_uloz_ctverec(byte *p_ctverec,dword xpozice,dword ypozice,byte *p_buffer);
void buf_uloz_sprit(byte *p_sprite,dword xpozice,dword ypozice,byte *p_buffer);

// Vyplnovani ctvercu
void vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v);
void buf_vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v,byte *p_buffer);
void spr_vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v,byte *p_sprite);

// Loading spritu
byte *load_sprit(byte *p_file, int lock);
byte *load_sprit_lep(int dat_file, byte *p_file, int lock);


//-------------------
// Zeme nikoho
//-------------------
void pis_znak_fn(byte *p_ctverec,dword xpozice,dword ypozice,dword barva,byte *p_barvy);
void putpixel(int x, int y, int barva);


/* spolecne pro vsechny drivery */
void kresli_ctverec_bar_xy_do_bufferu(byte  *p_ctverec,byte  *p_buffer,dword xpozice,dword ypozice,dword barva);

int  hl_sprit(byte *p_ctverec);
int  x_sprit(byte *p_ctverec);
int  y_sprit(byte *p_ctverec);
byte *vyrob_sprit( int x_res, int y_res, int barev, int lock);
int  uprav_sprit(byte *p_sprite, int x_res, int y_res, int barev);
/*void zrus_sprit(byte *p_sprite);*/
void rotuj_sprit(byte *p_sprit, byte *p_novy, int kolik);

#define zrus_sprit(p_spr)\
{                        \
 if(p_spr != NULL) {  \
   free(p_spr);       \
   p_spr = NULL;      \
 }                    \
}
/* else                    \
   fprintf(stderr,"Uvolnovani nuloveho spritu na %d file %s\n",__LINE__,__FILE__);\
}                                                   \
*/


void cekej_na_paprsek(void);
void cekej_na_sync(void);

void presun_vyrez_pameti(byte *p_cil, byte *p_zdroj, dword x_v, dword y_v);
void pis_znak_fn_do_bufferu(byte  *p_ctverec,byte *p_buffer,dword xpozice,dword ypozice,dword barva,byte *p_barvy);

int  prepni_do_grafiky(word video_mod, word lbf_e);
void prepni_do_textu(word txt_mod);

void nahraj_paletu(byte *p_jmeno,int pocet);
void nahraj_paletu_lep(int dat,byte *p_jmeno,int pocet);
void stmav_paletu(int cislo);
void rozsvit_paletu(int cislo);
void smaz_paletu(void);
void nastav_paletu(int cislo);
void set_clip(int x1, int x2, int y1, int y2);


extern int _a_av_mod;

extern dword    _a_x,
	 	_a_y,
	 	_a_xres,
	 	_a_yres,
	 	_a_vokno;

extern byte  	*_a_wvram,	//Adresa okna pro zapis
	        *_a_rvram;	//Adresa okna pro cteni
extern dword  	_a_wokno,	//cislo zapisoveho okna
  	        _a_rokno;	//cislo cteciho okna

extern dword   _a_x_buffer,
	       _a_y_buffer;

extern dword   _a_moje_ds;
extern dword   _a_r_zmen_okno;
extern dword   _a_granularita;

extern __dpmi_regs _a_posun_regs;

extern byte _a_palety[10][769];

extern dword aktivni_kres;
extern dword aktivni_vypln;


typedef struct {

   dword barev  __attribute__ ((packed));
   dword x      __attribute__ ((packed));
   dword y      __attribute__ ((packed));
   dword rez[3] __attribute__ ((packed));

} SPRIT_HEAD    __attribute__ ((packed));


extern dword __clip_x_min;
extern dword __clip_x_max;
extern dword __clip_y_min;
extern dword __clip_y_max;

#define vf(g)   vypln_ctverec(g,0,0,_a_xres,_a_yres)

/* knihovna na obsluhu fontu     */
/* obsluha tisku fontu a podobne */


#define _HACEK    '^'
#define _CARKA    '$'
#define _KROUZEK  '#'

int  ini_fn(void);
int  indexuj_font(int cf);

int  nahraj_fonty(char *p_fn_file);
int  nahraj_fonty_lep(int dat_file,char *p_fn_file);

void printfn(int x, int y, int font, char *p_text);
int  putfn(int x, int y, int font, char z);

void buf_printfn(int x, int y, int font, char *p_text, byte *p_spr);
int  buf_putfn(int x, int y, int font, char z, byte *p_spr);

inline int  znf(int z);          // prevod asci na font
inline int  szn(int font, int z);// sirka znaku
inline int  vzn(int font, int z);// vyska znaku
inline int  dlfn(char *p_str, int font); //delka textu v pixelech



/*
  Knihovna na obsluhu klavesnice
*/


#define  KEYBOARD_INT	 9
#define  POCET_KLAVES    0xff

/* Definice klaves v pole_key */
#define  K_ESC     1
#define  K_1       2
#define  K_2       3
#define  K_3       4
#define  K_4       5
#define  K_5       6
#define  K_6       7
#define  K_7       8
#define  K_8       9
#define  K_9       10
#define  K_0       11
#define  K_MINUS   12
#define  K_PLUS    13
#define  K_BKSP    14
#define  K_TAB     15
#define  K_Q       16
#define  K_W       17
#define  K_E       18
#define  K_R       19
#define  K_T       20
#define  K_Y       21
#define  K_U       22
#define  K_I       23
#define  K_O       24
#define  K_P       25
#define  K_ZAV_L   26
#define  K_ZAV_P   27
#define  K_ENTER   28
#define  K_CTRL    29
#define  K_A       30
#define  K_S       31
#define  K_D       32
#define  K_F       33
#define  K_G       34
#define  K_H       35
#define  K_J       36
#define  K_K       37
#define  K_L       38
#define  K_STREDNIK 39
#define  K_UVOZ    40
#define  K_TILDA   41
#define  K_SHFT_L  42
#define  K_BACKSLASH 43
#define  K_Z       44
#define  K_X       45
#define  K_C       46
#define  K_V       47
#define  K_B       48
#define  K_N       49
#define  K_M       50
#define  K_CARKA   51
#define  K_TECKA   52
#define  K_SLASH   53
#define  K_SHFT_P  54
#define  K_PTRSCR  55
#define  K_ALT     56
#define  K_SPACE   57
#define  K_CAPSLOCK 58
#define  K_F1      59
#define  K_F2      60
#define  K_F3      61
#define  K_F4      62
#define  K_F5      63
#define  K_F6      64
#define  K_F7      65
#define  K_F8      66
#define  K_F9      67
#define  K_F10     68
#define  K_NUMLOCK 69
#define  K_SCRLOCK 70
#define  K_HOME    71
#define  K_NAHORU  72
#define  K_PGUP    73
#define  K_K_MINUS 74
#define  K_DOLEVA  75
#define  K_K_5     76
#define  K_DOPRAVA 77
#define  K_K_PLUS  78
#define  K_END     79
#define  K_DOLU    80
#define  K_PGDN    81
#define  K_INS     82
#define  K_DEL     83


extern  dword alt;
extern  dword ctrl;

/* promene */
extern volatile dword off_klavesy;
extern volatile dword sel_klavesy;
extern dword length_int_09;

__dpmi_paddr old_int_09;
extern volatile int klavesnice_aktivni;
extern volatile dword blue_death_on;
extern volatile byte key[POCET_KLAVES];
extern volatile byte _a_asc[256];

/* procedury */
void zapni_klavesnici(void);
void vypni_klavesnici(void);       /* pomocne rutiny */
extern void int_09(void);          /* obsluha preruseni klavesnice */

extern byte _a_klavesa[4];

int getkb(void);
int getkba(void);
int getas(void);
int getasa(void);
void clrkb(void);

void vypni_repeat(void);
void zapni_repeat(void);

/*
 Knihovna na obsluhu mysi
 Pouziva funkce knihovny pc_graf
*/


#define OKRAJ_KURZORU    20
#define MYS_INT          0x33
#define POLOZEK_MYSI     4



/* Knihovna pro obsluhu mysi */
int  zapni_mys(volatile word *p_info,byte *p_sprit);
void vypni_mys(void);
void zapni_kurzor(volatile word *p_info);
void vypni_kurzor(volatile word *p_info);
void nastav_rychlost_mysi(word x_rychlost,word y_rychlost);
void rozsah_mysi(int x_min, int y_min, int x_max, int y_max);
void nastav_kurzor_ctverec(volatile word *p_info, byte *p_ctverec, dword x_res, dword y_res);
void nastav_kurzor_sprit(volatile word *p_info, byte *p_sprit);
void souradnice_mysi(int x, int y);

/* externi driver pro ovladac mysi v asembleru*/
extern void preruseni_mysi(void);

/* promene pro mys */
volatile word    mysi_info[POLOZEK_MYSI];   // Mysi info si voli aplikace

/* interni promene pro ovladac */
extern  word    _x_velikost_kurzoru,_y_velikost_kurzoru;
extern  byte    *_p_na_kurzor;
extern  byte    *_p_na_pozadi;
volatile extern  word	 *_p_info;
extern  word    _barva;
extern  word    _sel_xy;   // Promena se selektorem dat
extern  dword   delka_preruseni_mysi;
extern  word    _aktivni_mys;
extern  int     mys_aktivni;

__dpmi_regs       _registry_mysi;
__dpmi_raddr      _real_modova_adresa;

dword  stara_rychlost_x;
dword  stara_rychlost_y;



/********************************************/
/* funkce pro praci se slepenejma souborama */
/********************************************/

#define CHECK_ERR(err, msg) { \
    if (err != Z_OK) { \
        prepni_do_textu(0x3); \
        fprintf(VYPIS, "\n\n%s error: %d\n", msg, err); \
        exit(1); \
    } \
}

#define  SIGNUM_LEP 0x46534B41

typedef struct {

   char   jmeno[16] __attribute__ ((packed));
   dword  adresa    __attribute__ ((packed));
   dword  d_unp     __attribute__ ((packed));
   dword  d_pak     __attribute__ ((packed));

} POLOZKA_SOUBORU __attribute__ ((packed));

typedef struct {

   dword  signum         __attribute__ ((packed));
   dword  souboru        __attribute__ ((packed));
   dword  rezervace[200] __attribute__ ((packed));

} HLAVICKA_LEPU __attribute__ ((packed));

int otevri_soubor_lep(byte *p_lep_file);
int otevri_brutalware(byte *p_lep_file, byte *p_hledaci_string, dword zacatek_hledani, dword delka_oblasti);
int zavri_soubor_lep(int handle);

int nahraj_soubor( dword delka,byte  *p_mem,byte *p_file,long adresa);
int nahraj_soubor_lep( dword delka,byte  *p_mem,int lep_file, byte *p_file);
FILE * zjisti_soubor_lep(int lep_file,byte *p_file);
int velikost_lepfile(int lep_file, byte *p_file);
int velikost_file(byte *p_file);
int velikost_file_FILE(FILE *f);

#endif
