;Soubor:     Graficka knihovna 
;Typ:        Ovladace pro linear frame buffer, vesa 2.x a vysi
;Platforma:  PC/AT
;
;Prekladac:  NASM -f coff pc_graf.asm
;
;Autor:      Komat
;Tym:        AnakreoN, fwp.
;Datum:      --- vyvoj
;Verze:      0.1

        BITS    32


%define VH_SPRITU  24

;--------------------------------------------------------------------------
; Verejne - public funkce a promene
;--------------------------------------------------------------------------
GLOBAL _kresli_ctverec_xy        ;Procedura kresleni ctvercu na obrazovku
GLOBAL _kresli_ctverec_bar_xy    ;Kresli ctverec bez barvy

GLOBAL _kresli_ctverec           ;Procedura kresleni ctvercu na obrazovku
GLOBAL _kresli_ctverec_bar       ;Kresli ctverec bez barvy

GLOBAL _buf_kresli_ctverec       ; Jako predchozi, ale kresli do bufferu
GLOBAL _buf_kresli_ctverec_bar

GLOBAL _kresli_sprit            ; Kresleni spritu
GLOBAL _kresli_sprit_bar        ; Sprity bez barvy

GLOBAL _buf_kresli_sprit        ; Kresleni spritu do bufferu
GLOBAL _buf_kresli_sprit_bar    ; Sprity bez barvy do bufferu

GLOBAL _uloz_ctverec             ;Uklada xy ctverec videoram
GLOBAL _uloz_sprit              ;Uklada ctverec videoram do spritu

GLOBAL _buf_uloz_ctverec         ;Uklada xy ctverec bufferu
GLOBAL _buf_uloz_sprit          ;Uklada ctverec spritu do spritu

GLOBAL _vypln_ctverec            ;Vyplni ctverec na obrazovce
GLOBAL _buf_vypln_ctverec        ;Vyplni ctverec do bufferu
GLOBAL _spr_vypln_ctverec        ;Vyplni ctverec do spritu

GLOBAL _putpixel
GLOBAL _l_putpixel


GLOBAL _pis_znak_fn              ;pise znak fontu

GLOBAL _l_kresli_ctverec         ;Procedura kresleni ctvercu na obrazovku
GLOBAL _l_kresli_ctverec_bar     ;Kresli ctverec bez barvy

GLOBAL _l_pis_znak_fn           ;pise znak fontu
GLOBAL _pis_znak_fn_do_bufferu  ;pise znak fontu do bufferu

GLOBAL _vypln_ctverec
GLOBAL _l_vypln_ctverec

GLOBAL _l_uloz_ctverec          ;Uklada xy ctverec videoram

GLOBAL _cekej_na_paprsek    ;Ceka na zpetny beh paprsku - sync
GLOBAL _cekej_na_sync       ;Cekej na zpetny beh

GLOBAL _presun_vyrez_pameti ;Presune ctverec pameti

GLOBAL __a_zd_linear_asm
GLOBAL __a_dl_linear_asm

GLOBAL __a_linear_asm
GLOBAL __a_linear_asm_end

GLOBAL x_v
GLOBAL y_v

GLOBAL barva

GLOBAL _aktivni_kres
GLOBAL _aktivni_vypln

EXTERN _p_kresli_ctverec
EXTERN _p_kresli_ctverec_bar
EXTERN _p_uloz_ctverec
EXTERN _p_vypln_ctverec
EXTERN _p_pis_znak_fn
EXTERN _p_putpixel
;-------------------------------------------------------------------------
; Zacatek knihovny
;-------------------------------------------------------------------------
    section .data

__a_zd_linear_asm DB 0

extern __a_wvram  ;  Adresa video ram pro zapis
extern __a_rvram  ;  Adresa video ram pro cteni
extern __a_wokno  ;  Cislo okna pro zapis
extern __a_rokno  ;  Cislo okna pro cteni

extern __a_x      ;DW   20
extern __a_y      ;DW   20
extern __a_xres   ;DW   640
extern __a_yres   ;DW   480

extern __a_x_buffer
extern __a_y_buffer

extern __a_clip_x_min
extern __a_clip_x_max
extern __a_clip_y_min
extern __a_clip_y_max

info        DB  "Predane parametry Adresa %p ",0,"x %d y %d navrat %p",0

barva       DD  ;Pomocna promena na cislo barvy
barva_napln DD  ;Pomocna promena na cislo barvy

zal_x       DD  0
zal_y       DD  0

p_data      DD  0
x_v         DD  0
y_v         DD  0
p_barva     DD  0

vypln_buffer DD 0
_aktivni_kres    DD   0
_aktivni_vypln   DD   0

__a_dl_linear_asm DD  (__a_kd_linear_asm - __a_zd_linear_asm)
__a_kd_linear_asm DB  0

           section .text
;------------------------------------------------------------------------------
; pomocne promene :-)
;------------------------------------------------------------------------------
; velikost buffru je +0x2c

%define  s_p_data   dword [esp+4+0x2c]
%define  s_x        dword [esp+8+0x2c]
%define  s_y        dword [esp+12+0x2c]
%define  s_p_barva  dword [esp+16+0x2c]

__a_linear_asm:
;---------------------------------------------------------------------------
; Kresli ctverec v matici
; void   kresli_ctverec_xy(byte  *p_ctverec,dword xpozice,dword ypozice);
;---------------------------------------------------------------------------
_kresli_ctverec_xy:

    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    ; zavola a vynasobi
    ; Vynasobi x *= __a_x a y *= __a_y

    mov  esi,s_p_data
    mov  eax,s_x
    mul  dword [__a_x]
    mov  ebx,eax

    mov  eax,s_y
    mul  dword [__a_y]

    mov  edi,[_p_kresli_ctverec]
    call edi

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0

    ret

;---------------------------------------------------------------------------
; Kresli ctverec na xy souradnicich
; void   kresli_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice);
;---------------------------------------------------------------------------
_kresli_ctverec:

    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov  esi,s_p_data
    mov  ebx,s_x
    mov  eax,s_y

    mov  edi,[_p_kresli_ctverec]
    call edi

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0

    ret

;-------------------------------------------------------------------------
; Kresleni ctvercu bez barvy v matici
; void   kresli_ctverec_bar_xy(byte  *p_ctverec,dword xpozice,dword ypozice);
;-------------------------------------------------------------------------
_kresli_ctverec_bar_xy:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    ; zavola a vynasobi
    ; Vynasobi x *= __a_x a y *= __a_y

    mov  esi,s_p_data
    mov  eax,s_x
    mul  dword [__a_x]
    mov  ebx,eax

    mov  eax,s_y
    mul  dword [__a_y]

    mov  ecx,s_p_barva

    mov  edi,[_p_kresli_ctverec_bar]
    call edi

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret


;-------------------------------------------------------------------------
; Kresleni ctvercu bez barvy na xy
; void   kresli_ctverec_bar(byte  *p_ctverec,dword xpozice,dword ypozice);
;-------------------------------------------------------------------------
_kresli_ctverec_bar:

    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov  esi,s_p_data
    mov  ebx,s_x
    mov  eax,s_y
    mov  ecx,s_p_barva

    mov  edi,[_p_kresli_ctverec_bar]
    call edi

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;--------------------------------------------------------------------------
; Elementarni subrutiny pro kresleni
; Kresli ctverec
;--------------------------------------------------------------------------
_l_kresli_ctverec:

    mov ecx,ebx ; presun x do ecx

    mov bx,ds
    mov es,bx
    
    mul dword [__a_xres]   ;vynasob 
    mov edi,[__a_wvram]

    add eax,ecx     
    add edi,eax

    ;edi -> adresa do videoram a na urcene pozici

    cld
    mov edx,dword [__a_y]
    mov eax,dword [__a_xres]
    mov ebx,dword [__a_x]
    sub eax,ebx
.znovu:
    mov ecx,ebx
    shr ecx,2
    rep movsd   
    test    ebx,3
    jz  .je_delitelne_4
    test    ebx,1
    jz  .neni_liche
    movsb
.neni_liche:
    test    ebx,2
    jz  .je_delitelne_4
    movsw
.je_delitelne_4:
    add edi,eax
    dec edx
    jnz .znovu

    ret

;--------------------------------------------------------------------------
; Elementarni subrutiny pro kresleni
; Kresli ctverec bez barvy
;--------------------------------------------------------------------------
_l_kresli_ctverec_bar:
    ; ebx = x
    ; ecx = barva
    xchg ebx,ecx            ; prohod barvu z x
    mov dword [barva],ebx   ; vytahni cislo nekreslene barvy

    mov dx,ds
    mov es,dx

    mul dword [__a_xres]   ;vynasob 
    mov edi,[__a_wvram]

    add eax,ecx     
    add edi,eax

    ;edi -> adresa do videoram a na urcene pozici

    cld
    mov edx,dword [__a_y]
    mov eax,dword [__a_xres]
    mov ebx,dword [__a_x]
    sub eax,ebx

    mov ebp,[barva]
    xchg    ebp,ebx

.znovu:
    mov ecx,ebp

.porovnej:
    cmp     byte [esi],bl
    jz  .nezapisuj  
    movsb
    dec ecx
    jnz .porovnej
    jmp .konec_radku
.nezapisuj:
    inc edi
    inc esi
    dec ecx
    jnz .porovnej
.konec_radku:

    add edi,eax
    dec edx
    jnz .znovu

    ret


;--------------------------------------------------------------------------------------
;Kresli_ctverec do bufferu
;void buf_kresli_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice,byte  *p_buffer);
;--------------------------------------------------------------------------------------
_buf_kresli_ctverec:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov esi,[esp+4+0x2c]
    mov ebx,[esp+8+0x2c]
    mov eax,[esp+12+0x2c]
    mov edi,[esp+16+0x2c]
    
    call _do_bufferu_kresli_ctverec

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;----------------------------------------------------------------------------
;Kresli_ctverec_bar_xy_do_bufferu
;void buf_kresli_ctverec_bar(byte  *p_ctverec,dword xpozice,dword ypozice,int barva,byte  *p_buffer);
;----------------------------------------------------------------------------
_buf_kresli_ctverec_bar:

    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov esi,[esp+4+0x2c] ;zdrojova grafika
    mov ebx,[esp+8+0x2c] ;x souradnice
    mov eax,[esp+12+0x2c] ;y souradnice
    mov ecx,[esp+16+0x2c] ;barva
    mov edi,[esp+20+0x2c] ;cilova adresa

    call _do_bufferu_kresli_ctverec_bar

; Ukonci proceduru -> nastav zpet zasobnik
    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;----------------------------------------------------------------------------
; Subrutinby -> kresli ctverec kompletne
;----------------------------------------------------------------------------
_do_bufferu_kresli_ctverec:

    mov ecx,ebx

    mov dx,ds
    mov es,dx

    mul dword [__a_x_buffer]   ;vynasob 

    add eax,ecx     
    add edi,eax

    ;edi -> adresa do videoram a na urcene pozici

    cld
    mov edx,dword [__a_y]
    mov eax,dword [__a_x_buffer]
    mov ebx,dword [__a_x]
    sub eax,ebx

.znovu:
    mov ecx,ebx

    shr  ecx,2
    rep  movsd
    test ebx,3
    jz   .je_delitelne_4
    test ebx,1
    jz   .neni_liche
    movsb
.neni_liche:
    test ebx,2
    jz   .je_delitelne_4
    movsw
.je_delitelne_4:
    add  edi,eax
    dec  edx
    jnz  .znovu

    ret             ;konci

;----------------------------------------------------------------------------
; Subrutinby -> kresli ctverec bez barvy
;----------------------------------------------------------------------------
_do_bufferu_kresli_ctverec_bar:

    xchg ebx,ecx
    mov dword [barva],ebx   ;vytahni cislo nekreslene barvy

    mov dx,ds
    mov es,dx

    mul dword [__a_x_buffer]   ;vynasob 

    add eax,ecx     
    add edi,eax

    ;edi -> adresa do videoram a na urcene pozici

    cld
    mov edx,dword [__a_y]
    mov eax,dword [__a_x_buffer]
    mov ebx,dword [__a_x]
    sub eax,ebx

    mov ebp,[barva]
    xchg    ebp,ebx

.znovu:
    mov ecx,ebp

.porovnej:
    cmp     byte [esi],bl
    jz  .nezapisuj  
    movsb
    dec ecx
    jnz .porovnej
    jmp .konec_radku
.nezapisuj:
    inc edi
    inc esi
    dec ecx
    jnz .porovnej
.konec_radku:

    add edi,eax
    dec edx
    jnz .znovu

    ret             ;konci

;----------------------------------------------------------------------------
;funkce pro kresleni spritu
;void kresli_sprit_xy(byte *p_sprite,dword xpozice,dword ypozice);
;---------------------------------------------------------------------------
_kresli_sprit:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov eax,[__a_x]         ;Zazalohuj aktualni stav x a y
    mov ebx,[__a_y]
    mov [zal_x],eax
    mov [zal_y],ebx

    mov esi,s_p_data
    mov ebx,s_x
    mov eax,s_y

    mov edx,[esi+4]  ; Nasvihni tam x
    mov [__a_x],edx
    mov edx,[esi+8] ; Nasvihni tam y
    mov [__a_y],edx
    add  esi,VH_SPRITU

    mov  edi,[_p_kresli_ctverec]
    call edi

    mov eax,[zal_x]
    mov ebx,[zal_y]
    mov [__a_x],eax
    mov [__a_y],ebx

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;---------------------------------------------------------------------------------------------
;extern void   kresli_sprit_bar_xy(byte  *p_sprite,dword xpozice,dword ypozice,dword barva);
;---------------------------------------------------------------------------------------------
_kresli_sprit_bar:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov eax,[__a_x]         ;Zazalohuj aktualni stav x a y
    mov ebx,[__a_y]
    mov [zal_x],eax
    mov [zal_y],ebx

    mov esi,s_p_data
    mov ebx,s_x
    mov eax,s_y
    mov ecx,s_p_barva

    mov edx,[esi+4]  ; Nasvihni tam x
    mov [__a_x],edx
    mov edx,[esi+8] ; Nasvihni tam y
    mov [__a_y],edx
    add  esi,VH_SPRITU

    mov  edi,[_p_kresli_ctverec_bar]
    call edi

    mov eax,[zal_x]
    mov ebx,[zal_y]
    mov [__a_x],eax
    mov [__a_y],ebx

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;----------------------------------------------------------------------------
;funkce pro kresleni spritu do bufferu
;void buf_kresli_sprit(byte *p_sprite,dword xpozice,dword ypozice,byte p_buffer);
;---------------------------------------------------------------------------
_buf_kresli_sprit:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov eax,[__a_x]         ;Zazalohuj aktualni stav x a y
    mov ebx,[__a_y]
    mov [zal_x],eax
    mov [zal_y],ebx

    mov esi,s_p_data
    mov ebx,s_x
    mov eax,s_y
    mov edi,[esp+16+0x2c] ; adresa bufferu

    mov edx,[esi+4]  ; Nasvihni tam x
    mov [__a_x],edx
    mov edx,[esi+8] ; Nasvihni tam y
    mov [__a_y],edx
    add  esi,VH_SPRITU

    mov edx,[edi+4]  ; Nasvihni tam x rozmer bufferu
    mov [__a_x_buffer],edx
    mov edx,[edi+8] ; Nasvihni tam y rozmer bufferu
    mov [__a_y_buffer],edx
    add  edi,VH_SPRITU

    call _do_bufferu_kresli_ctverec

    mov eax,[zal_x]
    mov ebx,[zal_y]
    mov [__a_x],eax
    mov [__a_y],ebx

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;-------------------------------------------------------------------------------------------------------------
;extern void   buf_kresli_sprite_bar(byte  *p_sprite,dword xpozice,dword ypozice,dword barva,byte *p_sprite);
;-------------------------------------------------------------------------------------------------------------
_buf_kresli_sprit_bar:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov eax,[__a_x]         ;Zazalohuj aktualni stav x a y
    mov ebx,[__a_y]
    mov [zal_x],eax
    mov [zal_y],ebx

    mov esi,s_p_data
    mov ebx,s_x
    mov eax,s_y
    mov ecx,s_p_barva
    mov edi,[esp+20+0x2c] ; adresa bufferu

    mov edx,[esi+4]  ; Nasvihni tam x
    mov [__a_x],edx
    mov edx,[esi+8] ; Nasvihni tam y
    mov [__a_y],edx
    add  esi,VH_SPRITU

    mov edx,[edi+4]  ; Nasvihni tam x rozmer bufferu
    mov [__a_x_buffer],edx
    mov edx,[edi+8] ; Nasvihni tam y rozmer bufferu
    mov [__a_y_buffer],edx
    add  edi,VH_SPRITU

    call _do_bufferu_kresli_ctverec_bar

    mov eax,[zal_x]
    mov ebx,[zal_y]
    mov [__a_x],eax
    mov [__a_y],ebx

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;------------------------------------------------------------------------------
;Uschovej video ram - schova ctverec s videoram 
;void uloz_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice);
;------------------------------------------------------------------------------
_uloz_ctverec:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov edi,s_p_data ;zdrojova grafika
    mov ebx,s_x      ;x souradnice
    mov eax,s_y      ;y souradnice

    mov  edx,[_p_uloz_ctverec]
    call edx

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;------------------------------------------------------------------------------
;Uschovej video ram - schova sprite s vyrezem videoram
;void uloz_sprite(byte  *p_sprit,dword xpozice,dword ypozice);
;------------------------------------------------------------------------------
_uloz_sprit:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov eax,[__a_x]         ;Zazalohuj aktualni stav x a y
    mov ebx,[__a_y]
    mov [zal_x],eax
    mov [zal_y],ebx

    mov edi,s_p_data
    mov ebx,s_x
    mov eax,s_y

    mov edx,[edi+4]  ; Nasvihni tam x
    mov [__a_x],edx
    mov edx,[edi+8] ; Nasvihni tam y
    mov [__a_y],edx
    add edi,VH_SPRITU

    mov  edx,[_p_uloz_ctverec]
    call edx

    mov eax,[zal_x]
    mov ebx,[zal_y]
    mov [__a_x],eax
    mov [__a_y],ebx

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;--------------------------------------------------------------------------
; Subrutinka na ulozeni ctverce z videoram
;--------------------------------------------------------------------------
_l_uloz_ctverec:


    mov ecx,ebx

    mul dword [__a_xres]   ;vynasob
    mov esi,[__a_rvram]

    mov bx,ds
    mov es,bx

    add eax,ecx     
    add esi,eax

    ;esi -> adresa do videoram a na urcene pozici

    cld
    mov edx,dword [__a_y]
    mov eax,dword [__a_xres]
    mov ebx,dword [__a_x]
    sub eax,ebx
.znovu:
    mov ecx,ebx
    shr ecx,2
    rep movsd   
    test    ebx,3
    jz  .je_delitelne_4
    test    ebx,1
    jz  .neni_liche
    movsb
.neni_liche:
    test    ebx,2
    jz  .je_delitelne_4
    movsw
.je_delitelne_4:
    add esi,eax
    dec edx
    jnz .znovu

    ret


;-----------------------------------------------------------------------------------
;void buf_uloz_ctverec(byte  *p_ctverec,dword xpozice,dword ypozice,byte *p_buffer);
;
;p_buffer je ukazatel na buffer
;x_pozice - ukazatele na mistov bufferu odkud se maji brat data
;y_pozice
;p_ctverec -> sem to nasypat
; __a_x_buffer a __a_y_buffer je rozmer bufferu
; __a_x a __a_y je rozmer ctverce
;-----------------------------------------------------------------------------------
_buf_uloz_ctverec:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov edi,s_p_data

    mov esi,[esp+16+0x2c]
    add esi,s_x

    mov eax,[__a_x_buffer]
    mul s_y
    add esi,eax

    mov ebx,[__a_x]
    mov edx,[__a_y]

    call _pv_pameti

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;-----------------------------------------------------------------------------------
;void buf_uloz_sprit(byte  *p_sprite,dword xpozice,dword ypozice,byte *p_buffer);
;-----------------------------------------------------------------------------------
_buf_uloz_sprit:

    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd


    mov esi,[esp+16+0x2c]
    mov eax,[esi+4]
    mov ecx,[esi+8]
    mov [__a_x_buffer],eax
    mov [__a_y_buffer],ecx
    add esi,VH_SPRITU

    mul s_y
    add esi,eax
    add esi,s_x

    mov edi,s_p_data
    mov ebx,[edi+4]
    mov edx,[edi+8]
    add edi,VH_SPRITU

    call _pv_pameti

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;--------------------------------------------------------------------------
;subrutina na posouvani pameti
;--------------------------------------------------------------------------
_pv_pameti:

;void presun_vyrez_pameti(byte *p_ctverec, int x_v, int y_v, byte *p_buffer);

;cil je ukazatel na ctverec sirka je
;zdroj je ukazatel na buffer s wram,sirka je xres
;adresa je bude uz z __p_cil = p_pozadi[adresa]
;
;  presouva z    byte *p_zdroj
;                xres = __a_rvram
;
;  do            byte *p_cil
;                x_v - x
;                y_v - y

    mov cx,ds
    mov es,cx
    
    mov eax,[__a_x_buffer]
    sub eax,ebx
.znovu:
    mov ecx,ebx
    shr ecx,2
    rep movsd   
    test ebx,3
    jz  .je_delitelne_4
    test ebx,1
    jz  .neni_liche
    movsb
.neni_liche:
    test ebx,2
    jz  .je_delitelne_4
    movsw
.je_delitelne_4:
    add esi,eax
    dec edx
    jnz .znovu
    
    ret

;--------------------------------------------------------------------------
;Presun pamet z buferu do bufferu
;--------------------------------------------------------------------------
_presun_vyrez_pameti:

;void presun_vyrez_pameti(byte *p_ctverec, int x_v, int y_v, byte *p_buffer);

;cil je ukazatel na ctverec sirka je
;zdroj je ukazatel na buffer s wram,sirka je xres
;adresa je bude uz z __p_cil = p_pozadi[adresa]
;
;  presouva z    byte *p_zdroj
;                xres = __a_rvram
;
;  do            byte *p_cil
;                x_v - x
;                y_v - y
;
;
;   for(i = 0;i < y_v;i++) {
;       for(j = 0;j < x_v;j++)
;           p_cil[j+(i*x_v)] = p_zdroj[j];
;       p_zdroj += XRES;
;   }

    pushad
    push  es
    push  ds
    pushfd
    
    mov edi,[esp+4+0x2c]
    mov esi,[esp+8+0x2c]
    mov ebx,[esp+12+0x2c]
    mov edx,[esp+16+0x2c]

    mov cx,ds
    mov es,cx
    
    mov eax,[__a_xres]
    sub eax,ebx
.znovu:
    mov ecx,ebx
    shr ecx,2
    rep movsd   
    test ebx,3
    jz  .je_delitelne_4
    test ebx,1
    jz  .neni_liche
    movsb
.neni_liche:
    test ebx,2
    jz  .je_delitelne_4
    movsw
.je_delitelne_4:
    add esi,eax
    dec edx
    jnz .znovu
    
    popfd
    pop   ds
    pop   es
    popad

    ret

;------------------------------------------------------------------------------
;Procedura na vyplneni ctverce barvou
;void   vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v);
;------------------------------------------------------------------------------
_vypln_ctverec:


    cmp   dword [_aktivni_vypln],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_vypln],1

    pushad
    push  es
    push  ds
    pushfd

    mov eax,[__a_xres]
    mov [__a_x_buffer],eax

    mov eax,[__a_wvram]
    mov [vypln_buffer],eax

    mov esi,[esp+4+0x2c]     ; barva
    mov [barva_napln],esi

    mov ecx,[esp+8+0x2c]     ; x souradnice
    mov eax,[esp+12+0x2c]    ; y souradnice

    mov ebx,[esp+16+0x2c]    ; xres
    mov edx,[esp+20+0x2c]    ; yres

    mov [x_v],ebx
    mov [y_v],edx
    
    mov  edi,[_p_vypln_ctverec]
    call edi

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_vypln],0
    ret

;----------------------------------------------------------------------------------------------------
;Procedura na vyplneni ctverce barvou v bufferu
;void   buf_vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v,byte *p_buffer);
;----------------------------------------------------------------------------------------------------
_buf_vypln_ctverec:


    cmp   dword [_aktivni_vypln],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_vypln],1

    pushad
    push  es
    push  ds
    pushfd

    mov eax,[esp+24+0x2c]
    mov [vypln_buffer],eax

    mov esi,[esp+4+0x2c]     ; barva
    mov [barva_napln],esi

    mov ecx,[esp+8+0x2c]     ; x souradnice
    mov eax,[esp+12+0x2c]    ; y souradnice

    mov ebx,[esp+16+0x2c]    ; xres
    mov edx,[esp+20+0x2c]    ; yres

    mov [x_v],ebx
    mov [y_v],edx
    
    call _l_vypln_ctverec

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_vypln],0
    ret

;------------------------------------------------------------------------------
;Procedura na vyplneni ctverce barvou
;void   spr_vypln_ctverec(byte barva,dword xpozice,dword ypozice,dword x_v,dword y_v,byte *p_sprite);
;------------------------------------------------------------------------------
_spr_vypln_ctverec:


    cmp   dword [_aktivni_vypln],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_vypln],1

    pushad
    push  es
    push  ds
    pushfd


    mov esi,[esp+24+0x2c]

    mov eax,[esi+4]
    mov [__a_x_buffer],eax

    add esi,VH_SPRITU
    mov [vypln_buffer],esi


    mov eax,[esp+4+0x2c]     ; barva
    mov [barva_napln],eax

    mov ecx,[esp+8+0x2c]     ; x souradnice
    mov eax,[esp+12+0x2c]    ; y souradnice

    mov ebx,[esp+16+0x2c]    ; x velikost
    mov edx,[esp+20+0x2c]    ; y velikost

    mov [x_v],ebx
    mov [y_v],edx
    
    call _l_vypln_ctverec

    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_vypln],0
    ret

;-----------------------------------------------------------------------
; Subrutinka na vyplnovani ctverce v bufferu urcitou barvou
;-----------------------------------------------------------------------
_l_vypln_ctverec:

    mov bp,ds
    mov es,bp

    mul dword [__a_x_buffer]   ;vynasob sirkou bufferu
    mov edx,[y_v]
    mov edi,[vypln_buffer]

    add eax,ecx     
    add edi,eax

    ;edi -> adresa do videoram a na urcene pozici
    cld
    mov eax,[barva_napln]
    mov ah,al
    mov esi,eax
    shl esi,16
    or  eax,esi
    mov esi,dword [__a_x_buffer]
    sub esi,ebx
.znovu:
    mov ecx,ebx
    shr ecx,2
    rep stosd   
    test ebx,3
    jz   .je_delitelne_4
    test ebx,1
    jz   .neni_liche
    stosb
.neni_liche:
    test ebx,2
    jz   .je_delitelne_4
    stosw
.je_delitelne_4:
    add edi,esi
    dec edx
    jnz .znovu

    ret

;------------------------------------------------------------------------------
;Procedura na vypis fontu
;void pis_znak_fn(byte  *p_ctverec,dword xpozice,dword ypozice,int barva,byte *p_barvy);
;------------------------------------------------------------------------------
_pis_znak_fn:


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov   esi,[esp+4+0x2c]  ; Vyber source - zdroj grafiky
    mov   ebx,[esp+8+0x2c]  ; Vyber souradnici xpozice
    mov   eax,[esp+12+0x2c] ; Vyber souradnici ypozice
    mov   ecx,[esp+16+0x2c] ; Vyber nekreslitelnou barvu
    mov   [barva],ecx
    mov   ebp,[esp+20+0x2c]

    mov   edi,[_p_pis_znak_fn]
    call  edi

; Ukonci proceduru -> nastav zpet zasobnik
    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret

;--------------------------------------------------------------------------
; subrutina na psani znaku do bufferu
;--------------------------------------------------------------------------
_l_pis_znak_fn:

    xchg ebx,ecx

    mov dx,ds
    mov es,dx

    mul dword [__a_xres]   ;vynasob 
    mov edi,[__a_wvram]

    add eax,ecx
    add edi,eax

    ;edi -> adresa do videoram a na urcene pozici

    cld
    mov edx,dword [__a_y]
    mov eax,dword [__a_xres]
    mov ebx,dword [__a_x]
    sub eax,ebx

    mov ebx,[barva]

    rol     ebx,16
    mov     bx,dx
    xor     edx,edx
;----------------------------------------------------------------------------
;esi - zdroj
;edi - cil
;esp - x
;ecx - x
;bl - bila
;eax - rozdil
;ebp -
;edx -

.znovu:
    rol ebx,16
    mov ecx,[__a_x]

.porovnej:
    cmp     byte [esi],bl
    jz  .nezapisuj  

; barva[byte esi] -> edi
    mov   dl,byte [esi]
    mov   dl,[ds:ebp+edx]
    mov   byte [es:edi],dl
    inc   esi
    inc   edi
    dec ecx
    jnz .porovnej
    jmp .konec_radku
.nezapisuj:
    inc edi
    inc esi
    dec ecx
    jnz .porovnej
.konec_radku:

    add edi,eax
    rol ebx,16
    dec bx
    jnz .znovu

    ret             ;konci

;------------------------------------------------------------------------------
;Procedura na vypis fontu do buferu
;scanuje ctverec fontu
;------------------------------------------------------------------------------
_pis_znak_fn_do_bufferu:

;void   pis_znak_fn_do_bufferu(byte  *p_ctverec,byte *p_buffer,dword xpozice,dword ypozice,int barva,byte *p_barvy);


    cmp   dword [_aktivni_kres],0
    jz .neni_aktivni
    ret
.neni_aktivni:
  
    mov   dword [_aktivni_kres],1

    pushad
    push  es
    push  ds
    pushfd

    mov esi,[esp+4+0x2c] ;zdrojova grafika
    mov edi,[esp+8+0x2c]
    mov ecx,[esp+12+0x2c]    ;x souradnice
    mov eax,[esp+16+0x2c]    ;y souradnice
    mov ebx,[esp+20+0x2c]
    mov dword [barva],ebx   ;vytahni cislo nekreslene barvy
    mov ebp,[esp+24+0x2c]

    mov dx,ds
    mov es,dx

    mul dword [__a_x_buffer]   ;vynasob 

    add eax,ecx
    add edi,eax

    ;edi -> adresa do videoram a na urcene pozici

    cld
    mov edx,dword [__a_y]
    mov eax,dword [__a_x_buffer]
    mov ebx,dword [__a_x]
    sub eax,ebx

    mov ebx,[barva]

    rol     ebx,16
    mov     bx,dx
    xor     edx,edx
;----------------------------------------------------------------------------
;esi - zdroj
;edi - cil
;esp - x
;ecx - x
;bl - bila
;eax - rozdil
;ebp -
;edx -

.znovu:
    rol ebx,16
    mov ecx,[__a_x]

.porovnej:
    cmp     byte [esi],bl
    jz  .nezapisuj  

; barva[byte esi] -> edi
    mov   dl,byte [esi]
    mov   dl,[ds:ebp+edx]
    mov   byte [es:edi],dl
    inc   esi
    inc   edi
    dec ecx
    jnz .porovnej
    jmp .konec_radku
.nezapisuj:
    inc edi
    inc esi
    dec ecx
    jnz .porovnej
.konec_radku:

    add edi,eax
    rol ebx,16
    dec bx
    jnz .znovu

; Ukonci proceduru -> nastav zpet zasobnik
    popfd
    pop   ds
    pop   es
    popad

    mov   dword [_aktivni_kres],0
    ret             ;konci


;------------------------------------------------------------------------------
; Cekani na navrat paprsku
;------------------------------------------------------------------------------
_cekej_na_paprsek:

         pushfd
         push ax
         push dx

         mov  dx,0x3da   ;dx <- 3dah

.cekej_na_konec:  ; Pokud je prave retrace tak pockej na jeho konec
         in   al,dx     ;al <- port[3dah]                                     
         and  al,0x08    ;tesuje bit 8 v al
         jnz  .cekej_na_konec ;cekej na 0

.cekej_na_beh:   ;  Cekej na zacatek behu
         in   al,dx     ;al <- port[3dah]                                     
         and  al,0x08    ;testuje bit 8 v al
         jz   .cekej_na_beh ;cekej na 1

         pop  dx
         pop  ax
         popfd
         ret

;------------------------------------------------------------------------------
; Cekani na navrat sync
;------------------------------------------------------------------------------
_cekej_na_sync:
         pushfd
         push ax
         push dx

         mov     edx,0x3da   ;dx <- 3dah
.cekej:
         in  al,dx
         test    al,0x08
         jz  .cekej

         pop  dx
         pop  ax
         popfd
         ret

;-------------------------------
; Obligatni funkce putpixel
;-------------------------------
_putpixel:

    pushad
    push  es
    push  ds
    pushfd

  ; ebx - x souradnice
  ; eax - y souradnice
  ; ecx - barva

    mov  ebx,s_p_data
    mov  eax,s_x
    mov  ecx,s_y

    mov  edi,[_p_putpixel]
    call edi

    popfd
    pop   ds
    pop   es
    popad

    ret

;--------------------------------------------------------------------
;  Subrutina na linearni putpixel
;--------------------------------------------------------------------
_l_putpixel:

    mov dx,ds
    mov es,dx
    
  ; ebx - x souradnice
  ; eax - y souradnice
  ; ecx - barva
    mul dword [__a_xres]
    mov edi,[__a_wvram]

    add ebx,eax
    add edi,ebx

    mov eax,ecx
    ;edi -> adresa do videoram a na urcene pozici
    stosb

    ret

__a_linear_asm_end:
