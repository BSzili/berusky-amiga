#include "..\libanak.h"

/*******************************************
  Sekce s globalnimy promenymi
********************************************/
byte _a_cop[] = "Graficka knihovna (C) Komat / Anakreon 1999";
int  _a_av_mod = 0;

dword   _a_x,
        _a_y,
        _a_xres,
        _a_yres,
        _a_vokno;

byte    *_a_wvram,      //Adresa okna pro zapis
        *_a_rvram;      //Adresa okna pro cteni
dword   _a_wokno,       //cislo zapisoveho okna
        _a_rokno;       //cislo cteciho okna

dword   _a_x_buffer,
        _a_y_buffer;

dword   _a_moje_ds;
dword   _a_r_zmen_okno;
dword   _a_granularita;

__dpmi_regs _a_posun_regs;

byte    _a_palety[10][769];

dword   _a_clip_x_min;
dword   _a_clip_x_max;
dword   _a_clip_y_min;
dword   _a_clip_y_max;

void _a_linear_asm(void);
void _a_linear_asm_end(void);
void _a_bank_asm(void);
void _a_bank_asm_end(void);

extern dword _a_zd_linear_asm;
extern dword _a_zd_bank_asm;
extern dword _a_dl_linear_asm;
extern dword _a_dl_bank_asm;

/*********   Kod knihovny  **********/
void zamkni_promene_grf(void)
{

  LOCK_FUNCTION(_a_linear_asm);
  LOCK_FUNCTION(_a_bank_asm);
  _go32_dpmi_lock_data(&_a_zd_linear_asm,_a_dl_linear_asm);
  _go32_dpmi_lock_data(&_a_zd_bank_asm,_a_dl_bank_asm);

  LOCK_VARIABLE(_a_av_mod);
  LOCK_VARIABLE(_a_x);
  LOCK_VARIABLE(_a_y);
  LOCK_VARIABLE(_a_xres);
  LOCK_VARIABLE(_a_yres);
  LOCK_VARIABLE(_a_vokno);
  LOCK_VARIABLE(_a_wvram);
  LOCK_VARIABLE(_a_rvram);
  LOCK_VARIABLE(_a_wokno);
  LOCK_VARIABLE(_a_rokno);
  LOCK_VARIABLE(_a_x_buffer);
  LOCK_VARIABLE(_a_y_buffer);
  LOCK_VARIABLE(_a_moje_ds);
  LOCK_VARIABLE(_a_r_zmen_okno);
  LOCK_VARIABLE(_a_granularita);
  LOCK_VARIABLE(_a_posun_regs);

  LOCK_VARIABLE(_a_posun_regs);

  LOCK_VARIABLE(_a_clip_x_min);
  LOCK_VARIABLE(_a_clip_x_max);
  LOCK_VARIABLE(_a_clip_y_min);
  LOCK_VARIABLE(_a_clip_y_max);

  /*   CHYBY ME LOCK PALETY !!!!!!!!!!! */
  
}

int prepni_do_grafiky(word video_mod, word lbf_e)
{
 int    i,j,lfb;
 __dpmi_regs r;
 dword  pom;
 static VESAINFO vesainfo;
 static VESAMOD  vesamod;
 int    vesa_segment;
 int    vesa_selektor;
 char   oem_string[512];

 void   (* p_fce)(void);

 _a_granularita = 0xffff;

 lfb = lbf_e;

 /* Zamkne promene pouzite v graf knihvne */
 zamkni_promene_grf();


 /* Pokud se jedna o 320x200, nahod ho spec rutinou */
 if(video_mod == 0x13) {

  _a_posun_regs.x.cs = 0;
  _a_posun_regs.x.ip = 0;

  _a_posun_regs.x.ss = 0;
  _a_posun_regs.x.sp = 0;

/* prepni ho do 0x13 */
    r.x.ax = 0x13;
    __dpmi_int(0x10,&r);

    _a_xres = 320;
    _a_yres = 200;

//    fprintf(VYPIS,"Alokuji %d na lfb...\n",65536+1);
    if((_a_wvram = (byte *)malloc((65536)+1)) == NULL) {
        fprintf(VYPIS,"Chyba alokace lfb \n");
        lfb = 0;
    }

    if((lfb == 1)&&(__djgpp_map_physical_memory(_a_wvram,65536,0xa0000) == 0)) {
        _a_rvram = _a_wvram;
        /* provede zmenu ovladacu na lfb */
        p_kresli_ctverec = &l_kresli_ctverec;
        p_kresli_ctverec_bar = &l_kresli_ctverec_bar;
        p_uloz_ctverec = &l_uloz_ctverec;
        p_vypln_ctverec = &l_vypln_ctverec;
        p_pis_znak_fn = &l_pis_znak_fn;
        p_putpixel = &l_putpixel;
    }
    else {
//        fprintf(VYPIS,"Nejde namapovat fyzicka pamet. \nSkus oddelat emm386,qemm386,windows a jine manazery DPMI. \n");
//        fprintf(VYPIS,"Chyba %d\n",errno);

        _a_rokno = 0;
        _a_wokno = 0;
        _a_wvram = (byte *)0xA0000;
        _a_rvram = (byte *)0xA0000;

        /* provede zmenu ovladacu na bankovany*/
        p_kresli_ctverec = &b_kresli_ctverec;
        p_kresli_ctverec_bar = &b_kresli_ctverec_bar;
        p_uloz_ctverec = &b_uloz_ctverec;
        p_vypln_ctverec = &b_vypln_ctverec;
        p_pis_znak_fn = &b_pis_znak_fn;
        p_putpixel = &b_putpixel;

    }

    _a_av_mod = video_mod;
    _a_moje_ds = _dos_ds;
    return(0);
 }

 vesa_segment = __dpmi_allocate_dos_memory((512>>4)+1,&vesa_selektor);
 if(vesa_segment == -1) {
    printf("\npc_grf: Chyba pri alokaci dos pameti.");
    getch();
    exit(0);
 }

 /* detekce pritomnosti videobiosu */
 /* vyvolam dos preruseni 0x10 */
 r.x.ax = 0x4f00;
 r.x.di = 0x0;
 r.x.es = vesa_segment;
 __dpmi_int(0x10,&r);

 /* test instalace vesa */
 if(r.x.ax != 0x004F) {
        printf("\nNeni instalovan Vesa Bios.\n");
        printf("Chyba pri inicializaci vesa rozhrani ax = %x\n",r.x.ax);
        __dpmi_free_dos_memory(vesa_selektor);
        getch();
        exit(0);
 }

 /* prekopiruji nactena data do linearni pameti */
 movedata(vesa_selektor,0,_my_ds(),(unsigned)&vesainfo,512);
 dosmemget(vesainfo.OemStringPtr[0]+vesainfo.OemStringPtr[1]*16,255,&oem_string);

 /* snad to pojede */
 fprintf(VYPIS,"Hardware : %s \n",&oem_string);
 fprintf(VYPIS,"Vbe verze: %d.%d \n",vesainfo.VbeVersion[1],vesainfo.VbeVersion[0]);
 /* pokud je podporovano velikost vram, vytiskni jej */
 if((vesainfo.VbeVersion[1] >= 1)&&(vesainfo.VbeVersion[2] >= 1))
        fprintf(VYPIS,"Video RAM: %d Kb \n",vesainfo.TotalMemory*64);

/* provedu detekci videomodu */
 r.x.ax = 0x4f01;
 r.x.cx = video_mod;
 r.x.di = 0x0;
 r.x.es = vesa_segment;
 __dpmi_int(0x10,&r);

/* otestuj uspesnost sluzby */
 if(r.x.ax != 0x004F) {
    printf("\nChyba vesa funkce 01, ax = %x\n",r.x.ax);
    __dpmi_free_dos_memory(vesa_selektor);
    getch();
    exit(0);
 }


 /* prekopiruji nactena data do linearni pameti */
 movedata(vesa_selektor,0,_my_ds(),(unsigned)&vesamod,512);
 /* a provedu analizu parametru */

 /* provede test na pritomnost pozadovaneho videomodu */
 if(vesamod.ModeAttributes & 0x01) {
 ;
//    fprintf(VYPIS,"Mod 0x%x OK \n",video_mod);
//    fprintf(VYPIS,"xres %d ",vesamod.XResolution);
//    fprintf(VYPIS,"yres %d\n",vesamod.YResolution);
 }
 else {
    printf("\n\nMod 0x%x\n",video_mod);
    printf("Neni podporovan videomod 0x%x.",video_mod);
    __dpmi_free_dos_memory(vesa_selektor);
    getch();
    exit(0);
 }


 if((lfb == 1)&&(vesainfo.VbeVersion[1] >= 2)&&(vesamod.ModeAttributes & 0x08)) {
//        fprintf(VYPIS,"linear frame buffer instalovan...\n");
//        fprintf(VYPIS,"%p start flat frame buffer\n",vesamod.PhysBasePtr);
//        fprintf(VYPIS,"bank %d KB\n",vesamod.OffScreenMemSize);

//        fprintf(VYPIS,"Alokuji %d na lfb...\n",vesamod.XResolution*vesamod.YResolution);
        if((_a_wvram = (byte *)malloc((vesamod.XResolution*vesamod.YResolution)+1)) == NULL) {
//          fprintf(VYPIS,"Chyba alokace lfb \n");
          lfb = 0;
        }
        _a_rvram = _a_wvram;
        _a_xres = vesamod.XResolution;
        _a_yres = vesamod.YResolution;

    if((lfb == 1)&&(__djgpp_map_physical_memory(_a_wvram,vesamod.XResolution*vesamod.YResolution,vesamod.PhysBasePtr) != 0)) {
//                fprintf(VYPIS,"Nejde namapovat fyzicka pamet. \nSkus oddelat emm386,qemm386,windows a jine manazery DPMI. \n");
//                fprintf(VYPIS,"Chyba %d\n",errno);
                lfb = 0;
        }
        if(lfb == 1) {
         /* provede zmenu ovladacu na lfb */
                p_kresli_ctverec = &l_kresli_ctverec;
                p_uloz_ctverec = &l_uloz_ctverec;
                p_kresli_ctverec_bar = &l_kresli_ctverec_bar;
                p_vypln_ctverec = &l_vypln_ctverec;
                p_pis_znak_fn = &l_pis_znak_fn;
                p_putpixel = &l_putpixel;

//                fprintf(VYPIS,"Mapuju %p na linear frame buffer %p\n",_a_wvram,vesamod.PhysBasePtr);
//                fprintf(VYPIS,"Prepinam do videomodu 0x%x ...\n",video_mod);
                r.x.ax = 0x4f02;
                r.x.bx = video_mod;
                r.x.bx |= 0x4000;       // vymaskuj povoleni linear frame bufferu
                __dpmi_int(0x10,&r);

                if(r.x.ax != 0x004F) {
                    prepni_do_textu(0x3);
                    fprintf(VYPIS,"\n\nax = 0x%x chyba pri prepnuti do 0x%x. Mas dost vram ?",r.x.ax,video_mod);
                    getch();
                    __dpmi_free_dos_memory(vesa_selektor);
                    exit(1);
                }
       /* uspesne prepnul do lfb modu */
                __dpmi_free_dos_memory(vesa_selektor);
                _a_av_mod = video_mod;
                _a_moje_ds = _dos_ds;
                fprintf(VYPIS,"Lfb mod 0x%x OK\n",video_mod);
                return(0);
        }
 }
 else
        lfb = 0;

 /* pokud neni instalovan nebo neni pouzitelny linear frame buffer */
 /* pouzij standartni rutiny */
 if(lfb == 0) {
//  if(vesainfo.VbeVersion[1] < 2)
//    fprintf(VYPIS,"Neni instalovan linear frame buffer - > vesa %d.%d\n",vesainfo.VbeVersion[1],vesainfo.VbeVersion[0]);

//  if((!(vesamod.ModeAttributes & 0x08))&&(vesainfo.VbeVersion[1] >= 2))
//        fprintf(VYPIS,"Neni instalovan linear frame buffer -> atribut %x.\n",vesamod.ModeAttributes);
 /* provede inicializaci pomoci vesa 1.0 bez mapovani wram */

 /* provede zmenu ovladacu na bankovany*/
        p_kresli_ctverec = &b_kresli_ctverec;
        p_kresli_ctverec_bar = &b_kresli_ctverec_bar;
        p_uloz_ctverec = &b_uloz_ctverec;
        p_vypln_ctverec = &b_vypln_ctverec;
        p_pis_znak_fn = &b_pis_znak_fn;
        p_putpixel = &b_putpixel;

/* nastav adresu runiny na prehozeni okna */
    _a_posun_regs.x.cs = vesamod.WinFuncPtr[1];
    _a_posun_regs.x.ip = vesamod.WinFuncPtr[0];

    _a_posun_regs.x.ss = 0;
    _a_posun_regs.x.sp = 0;

/* nastavit wram,rvram, xres,  yres */
/* jestlize prvni videookno podporuje zapis i cteni , pouzij ho */
    if((vesamod.WinAAttributes & 7) == 7) {
        _a_wvram = (byte *)(dword)vesamod.WinASegment;
        _a_rvram = (byte *)(dword)vesamod.WinASegment;
        _a_rokno = 0;
        _a_wokno = 0;
    }
    else {      /* kdyz ne, rozhazej to */
                if((vesamod.WinAAttributes & 3) == 3){  /* Okno A je citelne ? */
                        _a_rvram = (byte *)(dword)vesamod.WinASegment;
                        _a_rokno = 0;
                }

                if((vesamod.WinAAttributes & 5) == 5) { /* Okno A Je zapisovatelne ? */
                        _a_wvram = (byte *)(dword)vesamod.WinASegment;
                        _a_wokno = 0;
                }
                if((vesamod.WinBAttributes & 3) == 3) { /* okno B je pro cteni ? */
                        _a_rvram = (byte *)(dword)vesamod.WinBSegment;
                        _a_rokno = 1;
                }
                if((vesamod.WinBAttributes & 5) == 5) { /* okno B Je pron zapis ? */
                        _a_wvram = (byte *)(dword)vesamod.WinBSegment;
                        _a_wokno = 1;
                }
    }
    pom = (dword)_a_wvram;
    pom <<= 4;
    _a_wvram = (byte *)pom;

    pom = (dword)_a_rvram;
    pom <<= 4;
    _a_rvram = (byte *)pom;

    /* Nastav rozliseni obrazovky */
        _a_xres = vesamod.XResolution;
        _a_yres = vesamod.YResolution;

    /* Nastavv velikost okna */
    _a_granularita = 0xffff;

    /* prepni do modu */
//        fprintf(VYPIS,"Prepinam do videomodu 0x%x ...\n",video_mod);
        r.x.ax = 0x4f02;
        r.x.bx = video_mod;
        __dpmi_int(0x10,&r);

        if(r.x.ax != 0x004F) {
                prepni_do_textu(0x3);
                printf("\n\nax = 0x%x chyba pri prepnuti do 0x%x. Mas dost vram ?",r.x.ax,video_mod);
                getch();
                __dpmi_free_dos_memory(vesa_selektor);
                exit(1);
        }
 }
 /* konec */
 __dpmi_free_dos_memory(vesa_selektor);
 _a_av_mod = video_mod;
 _a_moje_ds = _dos_ds;
 fprintf(VYPIS,"Bank mod 0x%x OK\n",video_mod);
 return(0);
}

/* prepne do textoveho modu */
void prepni_do_textu(word txt_mod)
{
 __dpmi_regs r;

 r.x.ax = 0x0;    /*  sluzba biosu -> nastav mod */
 r.x.ax = txt_mod;
 __dpmi_int(0x10,&r);
 _a_av_mod = txt_mod;
}



/* Nastavi paletu barev ze souboru p_jmeno */
void nahraj_paletu(byte *p_jmeno,int pocet)
{
 FILE   *paleta;
 int    i;
 // Nacteni palety ze souboru do Bufferu
 if((paleta = fopen(p_jmeno,"rb")) == NULL) {
        prepni_do_textu(0x3);
        printf("\n\nNemuzu otevrit paletu %s",p_jmeno);
        getchar();
        exit(0);
 }
 for(i = 0; i < pocet; i++)
    fread(_a_palety[i],1,768,paleta);

 fclose(paleta);
}

void stmav_paletu(int cislo)
{
 int i,j;

 for(i = 1; i < 63; i++) {
    outportb(0x3c8,0);
    cekej_na_paprsek();
        for(j = 0; j < 768; j++)
        outportb(0x3c9,((_a_palety[cislo][j]-i) < 0) ? 0 : (_a_palety[cislo][j]-i));
 }
}

void rozsvit_paletu(int cislo)
{
 int i,j;
 byte pom_pal[789];
 byte kroky[789];

 memcpy(pom_pal,_a_palety[cislo],768);
 memset(kroky,0,768);

 for(i = 64; i > 0 ; i--) {
    outportb(0x3c8,0);
    cekej_na_paprsek();
    for(j = 0; j < 768; j++) {
        if(pom_pal[j] == i) {
                pom_pal[j]--;
            kroky[j]++;
        }
        outportb(0x3c9,kroky[j]);
    }
 }
}

void smaz_paletu(void)
{
 int i;

 outportb(0x3c8,0);
 for(i = 0; i < 768; i++)
        outportb(0x3c9,0);
}

void nastav_paletu(int cislo)
{
 int i;

 outportb(0x3c8,0);
 for(i = 0; i < 768; i++) {
        outportb(0x3c9,_a_palety[cislo][i]);
 }
}

void nahraj_paletu_lep(int dat,byte *p_jmeno,int pocet)
{
 #define VEL_PAL (pocet*768)

 byte  *p_paleta;
 int    i;

 if((p_paleta = (byte *)malloc(VEL_PAL)) == NULL) {
    prepni_do_textu(0x3);
    printf("\nChyba : Nedostatek pameti !\n");
    getchar();
    exit(0);
 }

 _go32_dpmi_lock_data(p_paleta,VEL_PAL);

 nahraj_soubor_lep(VEL_PAL,p_paleta,dat,p_jmeno);

 for(i = 0; i < pocet; i++)
        memcpy(_a_palety[i],p_paleta+(i*768),768);
}


/* fontova knihovna */
/* (C) Asix fwp.*/

#define MAX_FONTU   20
#define MAX_PISMEN  60

typedef struct {

  int   y; // vyska fontu je vzdy stejna
  int   bila;
  
  byte  *p_font;

} FONT_INFO;

FONT_INFO _a_fn[MAX_FONTU];
int       _a_pos_fn = 0;            // posledni font
byte     *_a_p_fn[MAX_FONTU][MAX_PISMEN];

/*--------------------------------------------------------------------------*/
/* funkce pro obsluhy fontu                                                 */
/*--------------------------------------------------------------------------*/
int ini_fn(void)
{
 _a_pos_fn = 0;
}

int indexuj_font(int cf)
{
 int i,p = 1;

 for(i = 0; i < MAX_PISMEN; i++) {
    _a_p_fn[cf][i] = _a_fn[cf].p_font+p;
    p += VH_SPRITU+x_sprit(_a_fn[cf].p_font+p)*y_sprit(_a_fn[cf].p_font+p);
 }
 i = 0;
}

int  nahraj_fonty(char *p_fn_file)
{
  #define VEL_FN  (velikost_file(p_fn_file)+10)

  if((_a_fn[_a_pos_fn].p_font =
    (byte *)malloc(VEL_FN)) == NULL) {
    return(CHYBA);
  }
  _go32_dpmi_lock_data(_a_fn[_a_pos_fn].p_font,VEL_FN);

  nahraj_soubor(velikost_file(p_fn_file),_a_fn[_a_pos_fn].p_font,
                p_fn_file,0);

  _a_fn[_a_pos_fn].bila = _a_fn[_a_pos_fn].p_font[0];
  _a_fn[_a_pos_fn].y = y_sprit(_a_fn[_a_pos_fn].p_font);

  indexuj_font(_a_pos_fn);

  return(_a_pos_fn++);
}

int  nahraj_fonty_lep(int dat_file,char *p_fn_file)
{
  #define VEL_FNL (velikost_lepfile(dat_file,p_fn_file)+10)

  if((_a_fn[_a_pos_fn].p_font =
     (byte *)malloc(VEL_FNL)) == NULL) {
    return(CHYBA);
  }
  _go32_dpmi_lock_data(_a_fn[_a_pos_fn].p_font,VEL_FNL);

  nahraj_soubor_lep(0,_a_fn[_a_pos_fn].p_font,dat_file,p_fn_file);

  _a_fn[_a_pos_fn].bila = _a_fn[_a_pos_fn].p_font[0];
  _a_fn[_a_pos_fn].y = y_sprit(_a_fn[_a_pos_fn].p_font);

  indexuj_font(_a_pos_fn);
  return(_a_pos_fn++);
}

inline int znf(int z)
{

 if((z > 64)&&(z < 91)) { // Velky pismeno
   z -= 65;
   goto pis_znak;
 }
 if((z > 96)&&(z < 123)) {// Maly pismeno
   z -= 97;
   goto pis_znak;
 }
 if((z > 47)&&(z < 58)) { // Velky pismeno
   z -= (48-26);
   goto pis_znak;
 }
 if(z == ' ') {
   z = 57;
   goto pis_znak;
 }

 if(z == _CARKA) {
   z = 54;
   goto pis_znak;
 }
 if(z == _HACEK) {
   z = 53;
   goto pis_znak;
 }
 if(z == _KROUZEK) {
   z = 55;
   goto pis_znak;
 }

 if(z == ',') {
   z = 59;
   goto pis_znak;
 }
 if(z == '.') {
   z = 58;
   goto pis_znak;
 }

 if(z == '?') {
   z = 36;
   goto pis_znak;
 }
 if(z == '!') {
   z = 37;
   goto pis_znak;
 }
 if(z == '-') {
   z = 38;
   goto pis_znak;
 }
 if(z == '+') {
   z = 39;
   goto pis_znak;
 }
 if(z == 92) {
   z = 40;
   goto pis_znak;
 }
 if(z == '=') {
   z = 41;
   goto pis_znak;
 }
 if(z == ':') {
   z = 42;
   goto pis_znak;
 }
 if(z == ';') {
   z = 43;
   goto pis_znak;
 }
 if(z == '(') {
   z = 44;
   goto pis_znak;
 }
 if(z == ')') {
   z = 45;
   goto pis_znak;
 }
 if(z == '@') {
   z = 46;
   goto pis_znak;
 }
 if(z == '#') {
   z = 47;
   goto pis_znak;
 }
 if(z == '%') {
   z = 48;
   goto pis_znak;
 }
 if(z == '&') {
   z = 49;
   goto pis_znak;
 }
 if(z == '/') {
   z = 50;
   goto pis_znak;
 }
 if(z == '*') {
   z = 51;
   goto pis_znak;
 }
 if(z == '"') {
   z = 52;
   goto pis_znak;
 }
 if(z == '_') {
   z = 56;
   goto pis_znak;
 }

 z = 0; // Znak ktery neni v fontu se vytiskne jako mezera

pis_znak:
 return(z);
}

void printfn(int x, int y, int font, char *p_text)
{
 int i,z,d,b,ax,znam = 0,c;

 b = _a_fn[font].bila;
 d = strlen(p_text);
 ax = x;
 
 for(i = 0; i < d; i++) {
    z = p_text[i];

    if(z == ' ') {
      ax += 18;
      continue;
    }
    if(z == _CARKA) {
      znam = 54;
      continue;
    }
    if(z == _HACEK) {
      znam = 53;
      continue;
    }
    if(z == _KROUZEK) {
      znam = 55;
      continue;
    }
    c = znf(z);
    if(znam) {
      if(znam != 54)
        kresli_sprit_bar(_a_p_fn[font][znam],ax+(x_sprit(_a_p_fn[font][c])/2)-(x_sprit(_a_p_fn[font][znam])/2),y,b);
      else
        kresli_sprit_bar(_a_p_fn[font][znam],ax+(x_sprit(_a_p_fn[font][c])/2),y,b);

      znam = 0;
    }
    kresli_sprit_bar(_a_p_fn[font][c],ax,y,b);
    ax += x_sprit(_a_p_fn[font][c]);
 }
}

int putfn(int x, int y, int font, char z)
{
 int b,g;

 b = _a_fn[font].bila;
 g = znf(z);
 kresli_sprit_bar(_a_p_fn[font][g],x,y,b);
 return(x_sprit(_a_p_fn[font][g]));
}

inline int szn(int font, int z)
{
 return(x_sprit(_a_p_fn[font][znf(z)]));
}

inline int vzn(int font, int z)
{
 return(y_sprit(_a_p_fn[font][znf(z)]));
}

// funkce pro bufferovani
void buf_printfn(int x, int y, int font, char *p_text, byte *p_spr)
{
 int i,z,d,b,ax,znam = 0,c;

 b = _a_fn[font].bila;
 d = strlen(p_text);
 ax = x;
 
 for(i = 0; i < d; i++) {
    z = p_text[i];

    if(z == ' ') {
      ax += 18;
      continue;
    }
    if(z == _CARKA) {
      znam = 54;
      continue;
    }
    if(z == _HACEK) {
      znam = 53;
      continue;
    }
    if(z == _KROUZEK) {
      znam = 55;
      continue;
    }

    c = znf(z);
    if(znam) {
      if(znam != 54)
        buf_kresli_sprit_bar(_a_p_fn[font][znam],ax+(x_sprit(_a_p_fn[font][c])/2)-(x_sprit(_a_p_fn[font][znam])/2),y,b,p_spr);
      else
        buf_kresli_sprit_bar(_a_p_fn[font][znam],ax+(x_sprit(_a_p_fn[font][c])/2),y,b,p_spr);

      znam = 0;
    }
    buf_kresli_sprit_bar(_a_p_fn[font][c],ax,y,b,p_spr);
    ax += x_sprit(_a_p_fn[font][c]);
 }
}

int buf_putfn(int x, int y, int font, char z, byte *p_spr)
{
 int b,g;

 b = _a_fn[font].bila;
 g = znf(z);
 buf_kresli_sprit_bar(_a_p_fn[font][g],x,y,b,p_spr);
 return(x_sprit(_a_p_fn[font][g]));
}

inline int dlfn(char *p_str, int font)
{
 int delka = 0;

 while(*p_str != '\0') {
   if(!((*p_str == _HACEK)||(*p_str == _CARKA)||(*p_str == _KROUZEK)))
     delka += szn(font,*p_str);
   p_str++;
 }

 return(delka);
}

/*************************************************************************/
/* pom funkce ke spritum                                                 */
/*************************************************************************/
int hl_sprit(byte *p_ctverec)
{
 SPRIT_HEAD *p_sprit;

 p_sprit = (SPRIT_HEAD *)p_ctverec;
 return(p_sprit->barev);
}

int x_sprit(byte *p_ctverec)
{
 SPRIT_HEAD *p_sprit;

 p_sprit = (SPRIT_HEAD *)p_ctverec;
 return(p_sprit->x);
}

int y_sprit(byte *p_ctverec)
{
 SPRIT_HEAD *p_sprit;

 p_sprit = (SPRIT_HEAD *)p_ctverec;
 return(p_sprit->y);
}

void set_clip(int x1, int x2, int y1, int y2)
{
 /* nastaveni clipu pro X souradnice */
 if(x1 <= x2) {
   _a_clip_x_min = x1;
   _a_clip_x_max = x2;
 }
 else {
   _a_clip_x_min = x2;
   _a_clip_x_max = x1;
 }

 /* Nastaveni clipu pro Y souradnice */
 if(y1 <= y2) {
   _a_clip_y_min = y1;
   _a_clip_y_max = y2;
 }
 else {
   _a_clip_y_min = y2;
   _a_clip_y_max = y1;
 }
}

byte *vyrob_sprit( int x_res, int y_res, int barev, int lock)
{
 SPRIT_HEAD *p_spr;
 byte *p_sprite;
 int  velikost = (x_res*y_res)+VH_SPRITU+10;

 if((p_sprite = (byte *)malloc(velikost)) == NULL)
     return(NULL);

 if(lock)
   _go32_dpmi_lock_data(p_sprite,velikost);

 p_spr = (SPRIT_HEAD *)p_sprite;

 p_spr->x = x_res;
 p_spr->y = y_res;
 p_spr->barev = barev;

 return(p_sprite);
}

int  uprav_sprit(byte *p_sprite, int x_res, int y_res, int barev)
{
 SPRIT_HEAD *p_spr;

 p_spr = (SPRIT_HEAD *)p_sprite;

 p_spr->x = x_res;
 p_spr->y = y_res;
 p_spr->barev = barev;

 return(0);
 
}
/*
void zrus_sprit(byte *p_sprite)
{
 if(p_sprite != NULL) {
   free(p_sprite);
   p_sprite = NULL;
 }
 else
   fprintf(stderr,"Uvolnovani nuloveho spritu !\n");
}
*/
byte *load_sprit(byte *p_file, int lock)
{
 byte *p_sprite;
 FILE *f;
 int  velikost;
 
 if((f = fopen(p_file,"rb")) == NULL)
    return(NULL);

 fseek(f,0,SEEK_END);
 velikost = ftell(f) + 1;
 fseek(f,0,SEEK_SET);
 
 if((p_sprite = malloc(velikost+10)) == NULL)
    return(NULL);

 if(lock)
   _go32_dpmi_lock_data(p_sprite,velikost+10);

 fread(p_sprite,1,velikost,f);
 fclose(f);
 
 return(p_sprite);
}

byte *load_sprit_lep(int dat_file, byte *p_file, int lock)
{
 byte *p_sprite;
 int  velikost;
 
 if((p_sprite = malloc(velikost = velikost_lepfile(dat_file,p_file)+10)) == NULL)
    return(NULL);

 if(lock)
   _go32_dpmi_lock_data(p_sprite,velikost);

 if(!nahraj_soubor_lep(0,p_sprite,dat_file,p_file))
   return(NULL);
 else
   return(p_sprite);
}

/*
---------------------------------------------------------------------------
 rotuj_sprit(byte *p_spr, byte *p_novy, int kolik)
 kolik -> 0 - nic
          1 - 90
          2 - 180
          3 - 270
----------------------------------------------------------------------------
*/
void rotuj_sprit(byte *p_sprit, byte *p_novy, int kolik)
{
 SPRIT_HEAD *p_spr;
 byte *p_read, *p_write;
 int i,j;

 p_spr = (SPRIT_HEAD *)p_sprit;
 p_read = p_sprit+VH_SPRITU;
 p_write = p_novy+VH_SPRITU;
 
 switch(kolik) {

   case 1: // 90 stupnu
     for(i = 0; i < p_spr->x; i++) {
        for(j = 0; j < p_spr->y; j++) {
           p_write[(i*p_spr->x)+j] = p_read[(p_spr->x*(p_spr->y-1-j))+i];
        }
     }
     break;
   case 2: // 180 stupnu -> prehodit vzhuru nohama
     for(i = 0; i < p_spr->y; i++) {
        memcpy(p_write+((p_spr->y-i-1)*p_spr->x),p_read+(i*p_spr->x),p_spr->x);
     }
     break;
   case 3: // 270 stupnu -> opak 90 stupnu
     for(i = 0; i < p_spr->x; i++) {
         for(j = 0; j < p_spr->y; j++)
            p_write[(i*p_spr->x)+j] = p_read[(p_spr->x*j)+i];
     }
     break;
   case 0:
     memcpy(p_write,p_read,p_spr->x*p_spr->y);
   default:
     break;
 }
}

