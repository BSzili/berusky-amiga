;Soubor:     Graficka knihovna 
;Typ:        Ovladace pro bankovanou vesa 1.x,2.x 
;Platforma:  PC/AT
;
;Prekladac:  NASM -f coff pc_graf.asm
;
;Autor:      Komat
;Tym:        AnakreoN, fwp.
;Datum:      --- vyvoj 
;Verze:      0.1 (cert vi ?)

      BITS  32

;--------------------------------------------------------------------------
; Verejne - public funkce a promene
;--------------------------------------------------------------------------
GLOBAL _b_kresli_ctverec      ;Procedura kresleni ctvercu na obrazovku
GLOBAL _b_kresli_ctverec_bar  ;Kresli ctverec bez barvy
GLOBAL _b_uloz_ctverec        ;Uklada xy ctverec videoram

GLOBAL _b_pis_znak_fn         ;Napis znak
GLOBAL _b_vypln_ctverec

GLOBAL _b_putpixel

GLOBAL __a_zd_bank_asm
GLOBAL __a_dl_bank_asm

GLOBAL __a_bank_asm
GLOBAL __a_bank_asm_end

EXTERN __a_moje_ds     ;Promena s selektorem prostredi dosu
EXTERN __a_posun_regs
EXTERN __a_granularita
EXTERN ___dpmi_simulate_real_mode_procedure_retf
EXTERN __a_av_mod
EXTERN __a_x_buffer

EXTERN x_v
EXTERN y_v

;-------------------------------------------------------------------------
; Zacatek knihovny
;-------------------------------------------------------------------------
   section  .data

__a_zd_bank_asm   DB  0

extern __a_wvram  ;  Adresa video ram pro zapis
extern __a_rvram  ;  Adresa video ram pro cteni
extern __a_wokno  ;  Cislo okna pro zapis
extern __a_rokno  ;  Cislo okna pro cteni

extern __a_x      ;DW   20
extern __a_y      ;DW   20
extern __a_xres   ;DW   640
extern __a_yres   ;DW   480

extern __a_clip_x_min
extern __a_clip_x_max
extern __a_clip_y_min
extern __a_clip_y_max

extern __a_r_zmen_okno  ;rm adresa na zmenu video okna

extern  barva

akt_okno  DD   0
str_eax   DD   0
zal_x     DD   0

__a_dl_bank_asm DD   (__a_kd_bank_asm-__a_zd_bank_asm)
__a_kd_bank_asm DB 0

      section .text
__a_bank_asm:
;----------------------------------------------------------------------
; Zacatek elementanich subrutin
; esi - pointer na ctverec
; ebx - x
; eax - y
;----------------------------------------------------------------------
_b_kresli_ctverec:


   mov   es,[__a_moje_ds]
;Vypocet aresy ve vram pro kresleni
   xor   ecx,ecx
   xor   edx,edx

   mul   dword [__a_xres]
   add   eax,ebx  

; Vloz do edi adresu zacatku video-ram
   mov   edi,[__a_wvram]
; V eax:edx je tedka adresa pro zacatek kresleni
   add   di,ax    ;Vydel granularitou okna
            ;V edx je adresa v okne
            ;a v eax je cislo okna
   shr   eax,16
   call  _prepni_okno   ;prepni okno -> v eax je cislo okna
   
;  es:di -> adresa ve videoram
;  e(di) je 0a zacatek segmentu
   mov   dx,[__a_y]     ;Soupni do cx pocet kreslenych radek
   mov   ebx,[__a_granularita]
   rol   ebx,16
   mov   bx,[__a_x]  
; v E je __a_granularita a bx je _x

.kresli_line:  
   ;Schovej pocet opakovani do horni casti registru ecx
   rol   edx,16

   add   di,bx ;_x
   jc .posun_uprostred
   ror   ebx,16
   cmp   di,bx ;__a_granularita
; Je rozdel v polovine kresleneho radku
   pushf
   rol   ebx,16   ; -> hod tam _x
   popf
   jna   .neni_presun   
.posun_uprostred:
   sub   di,bx ;[_x]
   mov   cx,bx ; cx = [_x]
   add   cx,di ; cx + preteceni di , cx je delka pokracovani
   sub   bx,cx ; odecti bx([_x]) - cx -> bx delka ted
   xchg  bx,cx
   call  _presun_pamet  ; Vykresli radek na es:di ,cx je delka
   sub   edi,0x10000
   call  _posun_okno
   mov   cx,bx
   call  _presun_pamet
   mov   bx,[__a_x]
   jmp   .pokracuj      

.neni_presun:
   sub   di,bx ;_x
   mov   cx,bx ;_x
   call  _presun_pamet  ; Vykresli radek na es:di ,cx je delka

.pokracuj:
; Po vykresleni otestuj stav
   mov   ax,[__a_xres]
   sub   ax,bx ;[__a_x]
   ror   ebx,16
   add   di,ax
   jc .prepni
   cmp   di,bx ;__a_granularita
   jna   .neni_prenos2
;je prenos pri kresleni dalsiho radku (+ xres)
.prepni: call  _posun_okno
   
.neni_prenos2:
   rol   ebx,16

   rol   edx,16
   dec   dx
   jnz   .kresli_line

   ret


; Rutina na presun pameti
_presun_pamet:

   or ecx,ecx
   jz .je_delitelne_4

   mov   ebp,ecx
   shr   ecx,2
   rep   movsd 
   test  ebp,3
   jz .je_delitelne_4
   test  ebp,1
   jz .neni_liche
   movsb
.neni_liche:
   test  ebp,2
   jz .je_delitelne_4
   movsw
.je_delitelne_4:
   ret
   
_prepni_okno:
   cmp   dword [__a_av_mod],0x13
   jz    .vga

   mov   [akt_okno],eax
   mov   [__a_posun_regs+0x14],eax

   xor   eax,eax
   mov   ax,[__a_wokno]
   mov   [__a_posun_regs+0x10],eax

   push  dword __a_posun_regs
   call  ___dpmi_simulate_real_mode_procedure_retf
   add   sp,4

.vga:
   ret

_posun_okno:
   cmp   dword [__a_av_mod],0x13
   jz    .vga

   mov   dword [str_eax],eax
   inc   dword [akt_okno]
   mov   eax,[akt_okno]
   mov   [__a_posun_regs+0x14],eax

   xor   eax,eax
   mov   ax,[__a_wokno]
   mov   [__a_posun_regs+0x10],eax

   push  dword __a_posun_regs
   call  ___dpmi_simulate_real_mode_procedure_retf
   add   sp,4
   mov   eax,dword [str_eax]

.vga:
   ret


;--------------------------------------------------------------------------
;_b_kresli_ctverec_bar: ;Kresli bez barvy na xy
; esi - pointer na ctverec
; ebx - x
; eax - y
; ecx - barva
;--------------------------------------------------------------------------
_b_kresli_ctverec_bar:

   mov   ebp,ecx

   mov   es,[__a_moje_ds]
;Vypocet aresy ve vram pro kresleni
   xor   ecx,ecx
   xor   edx,edx

   mul   dword [__a_xres]
   add   eax,ebx  

; Vloz do edi adresu zacatku video-ram
   mov   edi,[__a_wvram]
; V eax:edx je tedka adresa pro zacatek kresleni
   add   di,ax    ;Vydel granularitou okna
            ;V edx je adresa v okne
            ;a v eax je cislo okna
   shr   eax,16
   call  _prepni_okno   ;prepni okno -> v eax je cislo okna
   
;  es:di -> adresa ve videoram
;  e(di) je 0a zacatek segmentu
   mov   dx,[__a_y]     ;Soupni do cx pocet kreslenych radek
   mov   ebx,[__a_granularita]
   rol   ebx,16
   mov   bx,[__a_x]  
; v E je __a_granularita a bx je _x

.kresli_line:  
   ;Schovej pocet opakovani do horni casti registru ecx
   rol   edx,16

   add   di,bx ;__a_x
   jc .posun_uprostred

   ror   ebx,16
   cmp   di,bx ;__a_granularita
; Je rozdel v polovine kresleneho radku
   pushf
   rol   ebx,16   ; -> hod tam __a_x
   popf
   jna   .neni_presun   
.posun_uprostred:
   sub   di,bx ;[__a_x]
   mov   cx,bx ; cx = [__a_x]
   add   cx,di ; cx + preteceni di , cx je delka pokracovani
   sub   bx,cx ; odecti bx([__a_x]) - cx -> bx delka ted
   xchg  bx,cx
   call  _presun_pamet_bar ; Vykresli radek na es:di ,cx je delka
   sub   edi,0x10000
   call  _posun_okno
   mov   cx,bx
   call  _presun_pamet_bar
   mov   bx,[__a_x]
   jmp   .pokracuj      

.neni_presun:
   sub   di,bx ;__a_x
   mov   cx,bx ;__a_x
   call  _presun_pamet_bar ; Vykresli radek na es:di ,cx je delka

.pokracuj:
; Po vykresleni otestuj stav
   mov   ax,[__a_xres]
   sub   ax,bx ;[__a_x]
   ror   ebx,16
   add   di,ax
   jc .prepni
   cmp   di,bx ;__a_granularita
   jna   .neni_prenos2
;je prenos pri kresleni dalsiho radku (+ xres)
.prepni: call  _posun_okno
   
.neni_prenos2:
   rol   ebx,16

   rol   edx,16
   dec   dx
   jnz   .kresli_line

   ret


; Rutina na presun pameti
_presun_pamet_bar:
        
   or ecx,ecx
   jz .konec_radku
   mov   eax,ebp

.porovnej:
   cmp   byte [esi],al
   jz .nezapisuj  
   movsb
   dec   cx
   jnz   .porovnej
   jmp   .konec_radku
.nezapisuj:
   inc   edi
   inc   esi
   dec   cx
   jnz   .porovnej
.konec_radku:
   ret
   
;--------------------------------------------------------------------------
;_b_uloz_ctverec:    ;Uklada xy ctverec videoram
;--------------------------------------------------------------------------
_b_uloz_ctverec:

   mov   es,[__a_moje_ds]
;Vypocet aresy ve vram pro kresleni
   xor   ecx,ecx
   xor   edx,edx

   mul   dword [__a_xres]
   add   eax,ebx  

; Vloz do edi adresu zacatku video-ram
   mov   esi,[__a_rvram]
; V eax:edx je tedka adresa pro zacatek kresleni
   add   si,ax    ;Vydel granularitou okna
            ;V edx je adresa v okne
            ;a v eax je cislo okna
   shr   eax,16
   call  _prepni_read_okno ;prepni okno -> v eax je cislo okna
   
;  es:di -> adresa ve videoram
;  e(di) je 0a zacatek segmentu
   mov   dx,[__a_y]     ;Soupni do cx pocet kreslenych radek
   mov   ebx,[__a_granularita]
   rol   ebx,16
   mov   bx,[__a_x]  
; v E je __a_granularita a bx je _x

.kresli_line:  
   ;Schovej pocet opakovani do horni casti registru ecx
   rol   edx,16

   add   si,bx ;__a_x
   jc .posun_uprostred
   ror   ebx,16
   cmp   si,bx ;__a_granularita
; Je rozdel v polovine kresleneho radku
   pushf
   rol   ebx,16   ; -> hod tam _x
   popf
   jna   .neni_presun   
.posun_uprostred:
   sub   si,bx ;[_x]
   mov   cx,bx ; cx = [_x]
   add   cx,si ; cx + preteceni di , cx je delka pokracovani
   sub   bx,cx ; odecti bx([_x]) - cx -> bx delka ted
   xchg  bx,cx
   call  _presun_read_pamet   ; Vykresli radek na es:di ,cx je delka
   sub   esi,0x10000
   call  _posun_read_okno
   mov   cx,bx
   call  _presun_read_pamet
   mov   bx,[__a_x]
   jmp   .pokracuj      

.neni_presun:
   sub   si,bx ;_x
   mov   cx,bx ;_x
   call  _presun_read_pamet   ; Vykresli radek na es:di ,cx je delka

.pokracuj:
; Po vykresleni otestuj stav
   mov   ax,[__a_xres]
   sub   ax,bx ;[_x]
   ror   ebx,16
   add   si,ax
   jc .prepni
   cmp   si,bx ;__a_granularita
   jna   .neni_prenos2
;je prenos pri kresleni dalsiho radku (+ xres)
.prepni: call  _posun_read_okno
   
.neni_prenos2:
   rol   ebx,16

   rol   edx,16
   dec   dx
   jnz   .kresli_line

   ret


; Rutina na presun pameti
_presun_read_pamet:
;  cld
;  rep   movsb
;  ret

   or ecx,ecx
   jz konci_pr
      
   mov   ax,es
   shl   eax,16
   mov   ax,ds
   mov   es,ax
   shr   eax,16
   mov   ds,ax

   mov   ebp,ecx
   shr   ecx,2
   rep   movsd 
   test  ebp,3
   jz .je_delitelne_4
   test  ebp,1
   jz .neni_liche
   movsb
.neni_liche:
   test  ebp,2
   jz .je_delitelne_4
   movsw
.je_delitelne_4:

   mov   ax,es
   shl   eax,16
   mov   ax,ds
   mov   es,ax
   shr   eax,16
   mov   ds,ax
konci_pr:
   ret
   

_prepni_read_okno:
   cmp   dword [__a_av_mod],0x13
   jz    .vga

   mov   [akt_okno],eax
   mov   [__a_posun_regs+0x14],eax

   xor   eax,eax
   mov   ax,[__a_rokno]
   mov   [__a_posun_regs+0x10],eax

   push  dword __a_posun_regs
   call  ___dpmi_simulate_real_mode_procedure_retf
   add   sp,4

.vga:
   ret

_posun_read_okno:
   cmp   dword [__a_av_mod],0x13
   jz    .vga

   mov   dword [str_eax],eax
   inc   dword [akt_okno]
   mov   eax,[akt_okno]
   mov   [__a_posun_regs+0x14],eax

   xor   eax,eax
   mov   ax,[__a_rokno]
   mov   [__a_posun_regs+0x10],eax

   push  dword __a_posun_regs
   call  ___dpmi_simulate_real_mode_procedure_retf
   add   sp,4
   mov eax,dword [str_eax]
   
.vga:
   ret

;--------------------------------------------------------------------------
;_b_pis_znak_fn:  ;Kresli bez barvy na xy
;--------------------------------------------------------------------------
_b_pis_znak_fn:   ;Kresli bez barvy na xy

; extern void   pis_text_fn(byte  *p_ctverec,dword xpozice,dword ypozice,dword barva,byte *p_barvy);
   ;na [esp] je navratova adresa


   mov   es,[__a_moje_ds]
;Vypocet aresy ve vram pro kresleni
   xor   ecx,ecx
   xor   edx,edx

   mul   dword [__a_xres]
   add   eax,ebx  

; Vloz do edi adresu zacatku video-ram
   mov   edi,[__a_wvram]
; V eax:edx je tedka adresa pro zacatek kresleni
   add   di,ax    ;Vydel granularitou okna
            ;V edx je adresa v okne
            ;a v eax je cislo okna
   shr   eax,16
   call  _prepni_okno   ;prepni okno -> v eax je cislo okna
   
;  es:di -> adresa ve videoram
;  e(di) je 0a zacatek segmentu
   mov   edx,[barva]
   rol   edx,16
   mov   dx,[__a_y]     ;Soupni do cx pocet kreslenych radek
   mov   ebx,[__a_granularita]
   rol   ebx,16
   mov   bx,[__a_x]  
; v E je __a_granularita a bx je _x


.kresli_line:  
   ;Schovej pocet opakovani do horni casti registru ecx
   rol   edx,16

   add   di,bx ;__a_x
   jc .posun_uprostred

   ror   ebx,16
   cmp   di,bx ;__a_granularita
; Je rozdel v polovine kresleneho radku
   pushf
   rol   ebx,16   ; -> hod tam _x
   popf
   jna   .neni_presun   
.posun_uprostred:
   sub   di,bx ;[_x]
   mov   cx,bx ; cx = [_x]
   add   cx,di ; cx + preteceni di , cx je delka pokracovani
   sub   bx,cx ; odecti bx([_x]) - cx -> bx delka ted
   xchg  bx,cx
   call  _presun_znak   ; Vykresli radek na es:di ,cx je delka
   sub   edi,0x10000
   call  _posun_okno
   mov   cx,bx
   call  _presun_znak
   mov   bx,[__a_x]
   jmp   .pokracuj      

.neni_presun:
   sub   di,bx ;_x
   mov   cx,bx ;_x
   call  _presun_znak   ; Vykresli radek na es:di ,cx je delka

.pokracuj:
; Po vykresleni otestuj stav
   mov   ax,[__a_xres]
   sub   ax,bx ;[__a_x]
   ror   ebx,16
   add   di,ax
   jc .prepni
   cmp   di,bx ;__a_granularita
   jna   .neni_prenos2
;je prenos pri kresleni dalsiho radku (+ xres)
.prepni: call  _posun_okno
   
.neni_prenos2:
   rol   ebx,16

   rol   edx,16
   dec   dx
   jnz   .kresli_line

   ret

; Rutina na presun pameti
_presun_znak:
;  cld
;  rep   movsb
;  ret
;       test    cx,0xff00
;       jnz .konec_radku
;-------------------------------------
;al - barva
;ebp - barvy
   or ecx,ecx
   jz konec_radku
   xor   eax,eax

.porovnej:
   cmp   byte [esi],dl
   jz .nezapisuj  

   mov      al,byte [esi]
   mov      al,[ds:ebp+eax]
   mov      byte [es:edi],al
   
   inc   esi
   inc   edi
   dec   cx
   jnz   .porovnej
   jmp   konec_radku
.nezapisuj:
   inc   edi
   inc   esi
   dec   cx
   jnz   .porovnej
konec_radku:
   ret
   
;-------------------------------------------------------------------------
; Subrutina na vyplneni ctverce
;-------------------------------------------------------------------------
_b_vypln_ctverec:


;    mov esi,[esp+4+0x2c]     ; barva
;    mov ecx,[esp+8+0x2c]     ; x souradnice
;    mov eax,[esp+12+0x2c]    ; y souradnice
   mov   edx,esi

   mov   dh,dl
   mov   esi,edx
   shl   edx,16
   or    esi,edx

   mov   ebx,ecx
;   mov   ebx,x ; Vyber souradnici xpozice
;   mov   eax,y ; Vyber souradnici ypozice


   mov   es,[__a_moje_ds]
;Vypocet aresy ve vram pro kresleni
   xor   ecx,ecx
   xor   edx,edx

   mul   dword [__a_x_buffer]
   add   eax,ebx  

; Vloz do edi adresu zacatku video-ram
   mov   edi,[__a_wvram]
; V eax:edx je tedka adresa pro zacatek kresleni
   add   di,ax    ;Vydel granularitou okna
            ;V edx je adresa v okne
            ;a v eax je cislo okna
   shr   eax,16
   call  _prepni_okno   ;prepni okno -> v eax je cislo okna
   
;  es:di -> adresa ve videoram
;  e(di) je 0a zacatek segmentu
   mov   dx,[y_v]      ;Soupni do cx pocet kreslenych radek
   mov   ebx,[__a_granularita]
   rol   ebx,16
   mov   bx,[x_v]
; v E je __a_granularita a bx je __a_x

.kresli_line:  
   ;Schovej pocet opakovani do horni casti registru ecx
   rol   edx,16

   add   di,bx ;_x
   jc .posun_uprostred
   ror   ebx,16
   cmp   di,bx ;__a_granularita
; Je rozdel v polovine kresleneho radku
   pushf
   rol   ebx,16   ; -> hod tam _x
   popf
   jna   .neni_presun   
.posun_uprostred:
   sub   di,bx ;[_x]
   mov   cx,bx ; cx = [_x]
   add   cx,di ; cx + preteceni di , cx je delka pokracovani
   sub   bx,cx ; odecti bx([_x]) - cx -> bx delka ted
   xchg  bx,cx
   call  _zapis_pamet   ; Vykresli radek na es:di ,cx je delka
   sub   edi,0x10000
   call  _posun_okno
   mov   cx,bx
   call  _zapis_pamet
   mov   bx,[x_v] ;-> vloz puvodni [x]
   jmp   .pokracuj      

.neni_presun:
   sub   di,bx ;_x
   mov   cx,bx ;_x
   call  _zapis_pamet   ; Vykresli radek na es:di ,cx je delka

.pokracuj:
; Po vykresleni otestuj stav
   mov   ax,[__a_xres]
   sub   ax,bx ;[_x]
   ror   ebx,16
   add   di,ax
   jc .prepni
   cmp   di,bx ;__a_granularita
   jna   .neni_prenos2
;je prenos pri kresleni dalsiho radku (+ xres)
.prepni: call  _posun_okno
   
.neni_prenos2:
   rol   ebx,16

   rol   edx,16
   dec   dx
   jnz   .kresli_line

   ret


; Rutina na presun pameti
_zapis_pamet:
;  cld
;  rep   movsb
;  ret
; esi je barva
   
   or ecx,ecx
   jz .konec_k

   xchg  esi,eax  ;-> eax je barva

   mov   ebp,ecx
   shr   ecx,2
   rep   stosd 
   test  ebp,3
   jz .je_delitelne_4
   test  ebp,1
   jz .neni_liche
   stosb
.neni_liche:
   test  ebp,2
   jz .je_delitelne_4
   stosw
.je_delitelne_4:
   xchg  esi,eax
.konec_k
   ret

;----------------------------------------------------------------------------
; obligatni subrutinka na kresleni pixelu zadane barvy
;----------------------------------------------------------------------------
_b_putpixel:

  ; ebx - x souradnice
  ; eax - y souradnice
  ; ecx - barva
  mov  ebp,ecx

  mul  dword [__a_xres]
  add  ebx,eax
  mov  eax,ebx

  shr  eax,16
  call  _prepni_okno   ;prepni okno -> v eax je cislo okna

  mov  edi,[__a_wvram]
  add  di,bx

  mov  ecx,ebp
  mov  al,cl

  mov  dx,word [__a_moje_ds]
  mov  es,dx
  stosb

  ret
__a_bank_asm_end:
