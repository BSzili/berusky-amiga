/* hlavicka struktur pro vesa bios */

#include "zakl_typ.h"

#ifndef __VESA_2_0
#define __VESA_2_0

/* hlavicka informaci o vesabiosu */
typedef struct {

byte     VbeSignature[4]	__attribute__ ((packed));        //dword 'VESA'     VBE Signature
byte     VbeVersion[2]		__attribute__ ((packed));          //word  0200h      VBE Version
word     OemStringPtr[2]	__attribute__ ((packed));        //dword ?          Pointer to OEM String
byte     Capabilities[4]	__attribute__ ((packed));        //byte  4 dup (?)  Capabilities of graphics cont.
word     VideoModePtr[2]	__attribute__ ((packed));        //dword ?          Pointer to Video Mode List
word     TotalMemory		__attribute__ ((packed));            //word  ?          Number of 64kb memory blocks
                                 //                 Added for VBE 2.0
word     OemSoftwareRev		__attribute__ ((packed));         //word  ?          VBE implementation Software revision
dword    OemVendorNamePtr	__attribute__ ((packed));       //dword ?          Pointer to Vendor Name String
dword    OemProductNamePtr	__attribute__ ((packed));      //dword ?          Pointer to Product Name String
dword    OemProductRevPtr	__attribute__ ((packed));       //dword ?          Pointer to Product Revision String
byte     Reserved[222]		__attribute__ ((packed));          //byte  222 dup (?) Reserved for VBE implementation
                                 //                 scratch area
byte     OemData[256]	__attribute__ ((packed));           //byte  256 dup (?) Data Area for OEM Strings

} VESAINFO __attribute__ ((packed));

/* hlavicka informaci o vesa modu */
typedef struct {

word    ModeAttributes 	__attribute__ ((packed));           // mode attributes
byte    WinAAttributes 	__attribute__ ((packed));           // window A attributes
byte    WinBAttributes 	__attribute__ ((packed));           // window B attributes
word    WinGranularity 	__attribute__ ((packed));           // window granularity
word    WinSize 		__attribute__ ((packed));           // window size
word    WinASegment 	__attribute__ ((packed));              // window A start segment
word    WinBSegment 	__attribute__ ((packed));              // window B start segment
word    WinFuncPtr[2] 	__attribute__ ((packed));               // pointer to window function
word    BytesPerScanLine __attribute__ ((packed));         // bytes per scan line
     // Mandatory information for VBE 1.2 and above
word    XResolution 	__attribute__ ((packed));              // horizontal resolution in pixels or chars
word    YResolution 	__attribute__ ((packed));              // vertical resolution in pixels or chars
byte    XCharSize 		__attribute__ ((packed));                // character cell width in pixels
byte    YCharSize 		__attribute__ ((packed));                // character cell height in pixels
byte    NumberOfPlanes 	__attribute__ ((packed));           // number of memory planes
byte    BitsPerPixel 	__attribute__ ((packed));              // bits per pixel
byte    NumberOfBanks 	__attribute__ ((packed));             // number of banks
byte    MemoryModel 	__attribute__ ((packed));               // memory model type
byte    BankSize 		__attribute__ ((packed));                  // bank size in KB
byte    NumberOfImagePages __attribute__ ((packed));        // number of images
byte    Reserved 		__attribute__ ((packed));                  // reserved for page function
     // Direct Color fields (required for direct/6 and YUV/7 memory models)
byte    RedMaskSize 	__attribute__ ((packed));               // size of direct color red mask in bits
byte    RedFieldPosition __attribute__ ((packed));          // bit position of lsb of red mask
byte    GreenMaskSize 	__attribute__ ((packed));             // size of direct color green mask in bits
byte    GreenFieldPosition __attribute__ ((packed));        // bit position of lsb of green mask
byte    BlueMaskSize 	__attribute__ ((packed));              // size of direct color blue mask in bits
byte    BlueFieldPosition __attribute__ ((packed));         // bit position of lsb of blue mask
byte    RsvdMaskSize 	__attribute__ ((packed));              // size of direct color reserved mask in bits
byte    RsvdFieldPosition __attribute__ ((packed));         // bit position of lsb of reserved mask
byte    DirectColorModeInfo __attribute__ ((packed));       // direct color mode attributes
     // Mandatory information for VBE 2.0 and above
dword   PhysBasePtr 	__attribute__ ((packed));               // physical address for flat frame buffer
dword   OffScreenMemOffset __attribute__ ((packed));        // pointer to start of off screen memory
word    OffScreenMemSize __attribute__ ((packed));          // amount of off screen memory in 1k units
byte    Reserv[206] __attribute__ ((packed));             // remainder of ModeInfoBlock

} VESAMOD __attribute__ ((packed));

#endif
