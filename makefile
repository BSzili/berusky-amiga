CC = m68k-amigaos-gcc
RM = rm -f
STRIPBIN = m68k-amigaos-strip -K ___stack
DEBUG = 0

CFLAGS = -Wall -noixemul -m68020 -msoft-float -fsigned-char
#CFLAGS += --save-temp

ifeq ($(DEBUG), 1)
CFLAGS += -g
else
CFLAGS += -O2 -fomit-frame-pointer
endif

CFLAGS += -Iinclude -Iamiga

LDFLAGS = -lm -noixemul
LDFLAGS += -Lamiga/lib -lz

BERUSKY_OBJ = \
	berusky/animace.o \
	berusky/berusky.o \
	berusky/konver.o \
	berusky/menu.o \
	berusky/sound.o

LIBANAK_OBJ = \
	knihovna/lep/f_lep.o \
	knihovna/amiga/ami_graf.o \
	knihovna/amiga/ami_klav.o \
	knihovna/amiga/ami_mys.o

AMIGA_OBJ = \
	amiga/findfirst.o \
	amiga/fnmatch.o \
	amiga/midas_xmp.o

#	amiga/midas_ahi.o \

OBJ = \
	$(BERUSKY_OBJ) \
	$(LIBANAK_OBJ) \
	$(AMIGA_OBJ)

SETUP_OBJ = \
	util/setup/setup.o \
	amiga/midas_xmp.o

all: setup.exe berusky.exe

berusky.exe: $(OBJ)
	$(CC) -o $@ $(OBJ) $(LDFLAGS)
ifeq ($(DEBUG),0)
	$(STRIPBIN) $@
endif

setup.exe: $(SETUP_OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)
ifeq ($(DEBUG),0)
	$(STRIPBIN) $@
endif

clean:
	$(RM) $(OBJ)
