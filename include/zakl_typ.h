/* definice zakladnich datovych typu */

#ifndef __ZAKLADNI_TYPY
#define __ZAKLADNI_TYPY

#if defined(__DJGPP__) || defined(__AMIGA__)

typedef unsigned int   dword;
typedef unsigned char  byte;
typedef unsigned short word;

#else

typedef unsigned long  dword;
typedef unsigned int   word;
typedef unsigned char  byte;

#endif

#define vypni_buffer(x) setvbuf(x, NULL, _IONBF, 0);

#endif
