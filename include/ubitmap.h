// Nadefinovana hlavicka bmp souboru
// (C) Anakreon
// bitmap.h

#ifndef 	__UBITMAP_
#define 	__UBITMAP_

#include "zakl_typ.h"

typedef struct {

	word		btType   __attribute__ ((packed));			//"BM"
	dword   	bfSize   __attribute__ ((packed));			//Velikost souboru s obrazovymi udaji
	word		bfReserved1 __attribute__ ((packed));	// 0
	word		bfReserved2 __attribute__ ((packed));	// 0
	dword  	bfOffBits __attribute__ ((packed));		// Posun na zacatek pixelu od zacatku souboru

} BITMAPFILEHEADER __attribute__ ((packed));

typedef struct {

	dword	biSize __attribute__ ((packed));			//velikost struktury bf
	dword	biWidth __attribute__ ((packed));		//sirka v pixelech
	dword	biHeight __attribute__ ((packed));		//vyska v pixelech
	word		biPlanes __attribute__ ((packed));		//Pocet rovin - musi byt 1
	word		biBitCount __attribute__ ((packed));		//Bitu na pixel 1,4,8,24
	dword	biCompression __attribute__ ((packed));	//Typ komprese 0-BI_RGB,1-BI_RLE8,2-BI_RLE4
	dword	biSizeImage __attribute__ ((packed));	//Velikost obrazu v bitech (pri BI_RGB muze byt 1)
	dword	biXPelsPerMeter __attribute__ ((packed));	//velikost vystupniho zarizeni x v pix/m
	dword	biYPelsPerMeter __attribute__ ((packed));	// y v pix/m
	dword	biClrUsed __attribute__ ((packed));		//pocet pouzitych barev (0 = vsechny mozne barvy)
	dword	biClrImportant __attribute__ ((packed));	//pocet dulezitych barev (0 = vsechny sou pouzite)

} BITMAPINFOHEADER __attribute__ ((packed));

#endif
/* Konec souboru */

