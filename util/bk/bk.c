/* Pomocny konvertor z bmp

   1997 (C) Anakreon 17.11

*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dir.h>
#include "ubitmap.h"


#define     OBRACENA_BMP   "pmb" /*Pripona souboru s obracenym
                           poradim zobrazovanim*/
#define     JEN_OBR_DATA   "obr"   /* Pripona souboru,kde je jen obraz
                           bez palety a hlavicky s obracenym ctenim*/
#define     PALETA         "pal" /* Soubor s paletou */

#define     SPRIT          "spr"

#define     NARAZ_KONVERTOVAT 64000  // Kolik ma precist na 1 zatah


/* Funkce pouzite v tomto modulu */
void OtevriCil(char *pripona);
void PrevratBody(void);
void help(void);
void KopirujPaletu(void);
void bmpinfo(void);
void KopirujPaletuExt(void);
void hlavicka_spritu(void);

/* Globalni data */
typedef struct {

   BITMAPFILEHEADER  bf __attribute__ ((packed));
   BITMAPINFOHEADER  bi __attribute__ ((packed));

} BMPHEAD __attribute__ ((packed));

typedef struct {

   dword barev __attribute__ ((packed));
   dword x __attribute__ ((packed));
   dword y __attribute__ ((packed));
   dword rez[3] __attribute__ ((packed));

} SPRIT_HEAD __attribute__ ((packed));

char **__crt0_glob_function(char *_argument)
{
 return(0);
}

FILE *bmp,*pmb;
int   i,done;

char  *p_char;
char  fz[20],fc[20];
char  file[20];
char  barvy[9];
struct ffblk f;

BMPHEAD  bmph;
SPRIT_HEAD sph;


void main(int argc,char *argv[]) // => argv[x] == const !!!
{                           // => argv je pole pointeru na retezce !
                     // => A sou to konstanty !

   /* parametry sou 2,soubor,funkce; cil otevren stejneho jmena
   s jinou priponou */
   setvbuf(stdout, NULL, _IONBF, 0);
   printf("Konvertor *.bmp do souboru s *.pmb *.pal *.obr *.spr pro interni pouziti\n\
(C) Anakreon 17.11.1997\nVerze z %s, %s\n\n",__DATE__,__TIME__);

   if(argc == 1 || argc > 3) {
      help();
      if(argc > 3)
         printf("\nNejak moc argumentu,mam se kvuli tobe strhat ?!");
      exit(1);
   }
   done = findfirst(argv[2],&f,FA_RDONLY|FA_ARCH);

/* Otevri zdrojovy soubor */
   while(!done) {

      p_char = f.ff_name;
      if((bmp = fopen(p_char,"rb")) == NULL) {
         printf("\nNemuzu otevrit zdrojovy soubor %s",p_char);
         exit(2);
      }
     
     /* Nactu hlavicku ze zdrojoveho souboru */
      if(fread(&bmph,1,54,bmp) != 54) {
         printf("\nCteny soubor neni bitmapa.");
           exit(1);
      }
     
     /* Odrizne ze zdrojoveho jmena priponu*/
      i = 0;
      while(p_char[i++] != '.');
      if(p_char[i-1] != '.') {
         printf("\nVnitrni chyba pri zmene pripony");
         exit(255);
      }
      else
         p_char[i] = '\0';
     
     
     
     /* neni ta bitmapa pakovana ? */
      if(bmph.bi.biCompression != 0) {
         printf("\nNepracuji z komprimovanymi soubory !");
         exit(1);
      }
     
     
     /* Testovani na pocet barev */
      if(bmph.bi.biBitCount != 8) {
         printf("\nNepracuji s jinym poctem barev nez 256.");
         exit(1);
      }
     
     
      printf("%c Konvertuji bitmapu\n",4);
     /* rozdeleni prace podle zadaneho parametru */
      switch (argv[1][0]) {
         case 'o':
            OtevriCil(OBRACENA_BMP);
            KopirujPaletu();
            PrevratBody();
            break;
     
         case 'p' :
            OtevriCil(PALETA);
            KopirujPaletuExt();
            break;
     
           case 'b' :
            OtevriCil(JEN_OBR_DATA);
            PrevratBody();
            break;
     
           case 's' :
            OtevriCil(SPRIT);
            hlavicka_spritu();
            PrevratBody();
            break;

           default  :
            printf("\nCo to tam pises za blaboly ?!");
            exit(1);
      }
     
      fclose(bmp);
      fclose(pmb);
      done = findnext(&f);
 }

}


void OtevriCil(char *pripona)
{
   printf("   Oteviram soubor %s...\n",strcat(p_char,pripona));
   if((pmb = fopen(strcat(p_char,pripona),"wb")) == NULL) {
      printf("\nNemuzu otevrit cilovy soubor %s",p_char);
      fclose(bmp);
      exit(1);
   }
}

void help(void)
{
   char  text[] = " bk.exe [o] [p] [b] [s] soubor.ext \
   \n\
   \n\
   o - prevraceni obrazu\n\
   p - vyrizni paletu a  hlavicku\n\
   b - vyrizni jen obrazove body\n\
   s - prevede do spritu\n";

   printf("%s",text);
}

// Zkopiruje paletu barev
void KopirujPaletu(void) {
   // Prekopiruj hlavicku
   printf("   Kopiruji hlavicku...\n");
   if(fwrite(&bmph,1,54,pmb) != 54) {
      printf("\nChyba pri zapisu do souboru %s",p_char);
      exit(1);
   }

   printf("   Kopiruji paletu barev...\n");
   /* Prekopiruj paletu barev */
   if(bmph.bi.biClrUsed == 0)  {
      fread(&barvy,4,256,bmp);
      fwrite(&barvy,4,256,pmb);
   }
   else {
      fread(&barvy,4,bmph.bi.biClrUsed,bmp);
      fwrite(&barvy,4,bmph.bi.biClrUsed,pmb);
   }
}

/* Vypise informace o bitmape */
void bmpinfo(void)
{
   printf("\n%c Analizuji soubor %s\n",4,p_char);
   printf("   Rozliseni obrazku: %dx%d bodu\n",(int)bmph.bi.biWidth,(int)bmph.bi.biHeight);
   printf("   Pocet bitu na pixel: %d\n",bmph.bi.biBitCount);
   printf("   Pocet barev: %d\n",(bmph.bi.biClrUsed == 0) ? 265 : bmph.bi.biClrUsed);
   printf("   Pocet dulezitych barev: %d\n\n",(bmph.bi.biClrImportant == 0) ? 256 : bmph.bi.biClrImportant);

}

// Zkopiruje paletu barev v opacnym poradi
void KopirujPaletuExt(void) {
   // Prekopiruj hlavicku
int   pocet_barev;
int i;
/*
   printf("   Kopiruji hlavicku...\n");
   if(fwrite(&bmph,1,54,pmb) != 54) {
      printf("\nChyba pri zapisu do souboru %s",p_char);
      exit(1);
   }
*/
   printf("   Kopiruji paletu barev...\n");

   /* Prekopiruj paletu barev */
   if(bmph.bi.biClrUsed == 0)
      pocet_barev = 256;
   else
      pocet_barev = bmph.bi.biClrUsed; //bmp
// fseek(bmp,54,SEEK_SET);
   for(i = 0;i < pocet_barev;i++) {
      fread(barvy,1,4,bmp);
      barvy[4] = (barvy[2] >> 2);   // Zkopiruje paletu a >> 0 2
      barvy[5] = (barvy[1] >> 2);
      barvy[6] = (barvy[0] >> 2);
      fwrite(&barvy[4],1,3,pmb);
      if(barvy[4] > 64)
         printf("Shit");
      if(barvy[5] > 64)
         printf("Shit");
      if(barvy[6] > 64)
         printf("Shit");

   }

}

void PrevratBody(void)
{
// bmp zdroj
// pmb cil
  byte  body[NARAZ_KONVERTOVAT+5];
  int   pocet_bodu;
  int   i = 0;
  int   citac = 0;

   printf("   Konvertuji poradi bodu...\n");
   printf("   [");
   //Posun na konec bitmapy
   if((fseek(bmp,0,SEEK_END)) == 1) {
      printf("\nNejak ti ta bitmapa uhniva,menuzu pres tu kasi pokracovat!");
      exit(1);
   }
   pocet_bodu = bmph.bi.biWidth*bmph.bi.biHeight;
   //odecti velikost hlavicky a pocet pouzitych barev
   //prekopirovani bodu
   i = pocet_bodu;

   do  {

      i -= (int)bmph.bi.biWidth;

      if((bmph.bi.biWidth%4) !=0 )
        fseek(bmp,-(4-(bmph.bi.biWidth%4)),SEEK_CUR);
      fseek(bmp,-bmph.bi.biWidth,SEEK_CUR);

      fread(&body,1,bmph.bi.biWidth,bmp);

      fseek(bmp,-bmph.bi.biWidth,SEEK_CUR);

      fwrite(&body,1,bmph.bi.biWidth,pmb);

      citac++;
      if(citac == 4) {
         citac = 0;
         putchar('.');
      }

   } while (i > 0);
   putchar(']');
   putchar('\n');
}

void hlavicka_spritu(void)
{
   printf("   Zapisuji hlavicku spritu... \n");

   sph.barev = 8;
   sph.x = bmph.bi.biWidth;
   sph.y = bmph.bi.biHeight;
   memset(sph.rez,0,sizeof(dword)*3);
   fwrite(&sph,sizeof(sph),1,pmb);

}

