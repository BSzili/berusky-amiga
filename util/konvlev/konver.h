/* definice zavislosti */

// Zacatek definic hernich prvku,jejich cislo v levelu a zaroven poradi
// v grafickem souboru
/* Definice hernich objektu
*/

#include "zakl_typ.h"

#ifndef __KONVER_H
#define __KONVER_H


#define PRVNI_GLOBAL_LEVEL          0
#define POSLEDNI_GLOBAL_LEVEL       99
#define PRVNI_KLASIK_LEVEL          100
#define POSLEDNI_KLASIK_LEVEL       199
#define PRVNI_KYBER_LEVEL           200
#define POSLEDNI_KYBER_LEVEL        299
#define PRVNI_ANIMACE_HRACE         500
#define PRVNI_POZADI                580
#define PRVNI_OSTATNI               600
#define PRVNI_HRAC                  700

#define PRVNI_KLIC      (PRVNI_OSTATNI+4)

#define UKAZATEL_2      (PRVNI_OSTATNI)
#define EDIT_ZEME       (PRVNI_OSTATNI+10)
#define UKAZATEL_3      (PRVNI_OSTATNI+15)
#define LEVA_SIPKA      (PRVNI_OSTATNI+11)
#define PRAVA_SIPKA     (PRVNI_OSTATNI+13)

#define HERNICH_PRVKU   62
#define PODPRVKU        8
#define SLEPEJ_PRVEK    0xffff
#define SLEPEJ_SPRIT    0xffff
#define NENI_PODLAHA    0xffff

#define PRVEK       0
#define VARIANTA    1
#define ROTACE      2

/* Hlavni herni prvky */
#define P_ZEME      0
#define P_HRAC_1    1
#define P_HRAC_2    2
#define P_HRAC_3    3
#define P_HRAC_4    4
#define P_HRAC_5    5
#define P_BEDNA     6
#define P_TNT       7
#define P_STENA     8
#define P_EXIT      9
#define P_KAMEN     10
#define P_KLIC      11
#define P_KRUMPAC   12
#define P_KLIC1     13
#define P_KLIC2     14
#define P_KLIC3     15
#define P_KLIC4     16
#define P_KLIC5     17

#define P_DVERE1_H_O    18
#define P_DVERE2_H_O    19
#define P_DVERE3_H_O    20
#define P_DVERE4_H_O    21
#define P_DVERE5_H_O    22
//--- mam
#define P_DVERE1_H_Z    23
#define P_DVERE2_H_Z    24
#define P_DVERE3_H_Z    25
#define P_DVERE4_H_Z    26
#define P_DVERE5_H_Z    27
//--- mam
#define P_DVERE1_V_O    28
#define P_DVERE2_V_O    29
#define P_DVERE3_V_O    30
#define P_DVERE4_V_O    31
#define P_DVERE5_V_O    32
//--- mam
#define P_DVERE1_V_Z    33
#define P_DVERE2_V_Z    34
#define P_DVERE3_V_Z    35
#define P_DVERE4_V_Z    36
#define P_DVERE5_V_Z    37
//--- mam
#define P_ID_DVERE1_H_O 38
#define P_ID_DVERE2_H_O 39
#define P_ID_DVERE3_H_O 40
#define P_ID_DVERE4_H_O 41
#define P_ID_DVERE5_H_O 42
//--- mam
#define P_ID_DVERE1_H_Z 43
#define P_ID_DVERE2_H_Z 44
#define P_ID_DVERE3_H_Z 45
#define P_ID_DVERE4_H_Z 46
#define P_ID_DVERE5_H_Z 47
//--- mam
#define P_ID_DVERE1_V_O 48
#define P_ID_DVERE2_V_O 49
#define P_ID_DVERE3_V_O 50
#define P_ID_DVERE4_V_O 51
#define P_ID_DVERE5_V_O 52
//--- mam
#define P_ID_DVERE1_V_Z 53
#define P_ID_DVERE2_V_Z 54
#define P_ID_DVERE3_V_Z 55
#define P_ID_DVERE4_V_Z 56
#define P_ID_DVERE5_V_Z 57
//--- mam
#define P_DV_H_O        58
#define P_DV_H_Z        59
#define P_DV_V_O        60
#define P_DV_V_Z        61

/* Herni podprvku */
#define PP_LEVY_FUTRO_O  0x0  // Otevreny futra
#define PP_PRAVY_FUTRO_O 0x1
#define PP_HORNI_FUTRO_O 0x2
#define PP_DOLNI_FUTRO_O 0x3

#define PP_LEVY_FUTRO_Z  0x4  // Zavreny futra
#define PP_PRAVY_FUTRO_Z 0x5
#define PP_HORNI_FUTRO_Z 0x6
#define PP_DOLNI_FUTRO_Z 0x7


/* stara definice hernich prvku */
#define ZEM       0
// Definice beden
#define HRAC1     1
#define HRAC5     5
// Definice beden
#define BEDNA1    6
#define BEDNA2    0x20
#define BEDNA3    0x21
#define BEDNA4    0x22
#define BEDNA5    82
#define BEDNA6    83
#define BEDNA7    84
// Definice tnt beden
#define TNT1       7
#define TNT2       0x23
#define TNT3       0x24
#define TNT4       0x2b
#define TNT5       0x2c
#define TNT6       0x2d

#define TNT7       85
#define TNT8       86
#define TNT9       87

//Definice exitu
#define EXIT1      9
#define EXIT2      0x28
#define EXIT3      0x27
#define EXIT4      88
#define EXIT5      89
// Definice kamenu
#define KAMEN1     10
// Definice klice
#define KLIC       11
// Definice lopat
#define KRUMPAC1   12
#define KRUMPAC2   0x29
// Definice barevnych klicu
#define KLIC1      0xd
#define KLIC2      0xe
#define KLIC3      0xf
#define KLIC4      0x10
#define KLIC5      0x11
// Definice bar dveri
#define DVERE1     0x12
#define DVERE2     0x13
#define DVERE3     0x14
#define DVERE4     0x15
#define DVERE5     0x16
// definice individualnich dveri
#define ID_DVERE1  0x17
#define ID_DVERE2  0x18
#define ID_DVERE3  0x19
#define ID_DVERE4  0x1a
#define ID_DVERE5  0x1b
// definice zaviracich dveri
#define DV_H_O     0x1c
#define DV_H_Z     0x1d
#define DV_V_O     0x1e
#define DV_V_Z     0x1f

/*
  rozdeleni:
  ----------
  struktura:
  word pocet druhu grafiky
  word varianty prvku - cisla grafiky [100] (tj. pole indexu do spr)

  word cislo fce pri prichazeni
  word cislo fce pri dojiti

  veci zapsane v levelu
  ---------------------
  cislo prvku (vlastne soucastna varianta druhu)
  cislo variany (tj. cislo druhu zobrazeni, tzn. poradove cislo spritu od 0 do urciteho cisla)
  rotace prvku (pouze u jednoprvkovych spritu)

  rozdeleni:
  ----------
  word podlaha[YPOLI][XPOLI][10]   __attribute__ ((packed));   // -> podlaha
  word level[YPOLI][XPOLI][10] __attribute__ ((packed));   //-> herni plocha (dnesni level)
  word hraci[YPOLI][XPOLI]     __attribute__ ((packed));   //-> mapa hracu (dneska sou hraci n levelu)
  [0] -> cislo prvku v poli
  [1] -> poradove cislo grafiky
  [3] -> rotace
  [4] ... [9] -> rezerva
*/

typedef struct {

  int  druh;          // funkcni zarazeni prvku (dvere, podlaha, ...), 0xff -> slepej prvek
  int  variant;       // druhu jeho grafickych variant
  int  varianty[100]; // pole variant, kazdy prvek muze mit az 100 ruznych spritu
  int  podprvku;
  int  podprvky[100]; // Podprvky
  int  pdp_vr[100];   // cislo varianty podprvku
  int  minus_x;       //kolik ctverecku doprava navic
  int  plus_x;        //kolik ctverecku doprava
  
} HERNI_OBJEKT;

typedef struct {

  int  druh;          // druh herniho prvku
  int  x_kor;
  int  y_kor;
  int  variant;       // variant podprvku
  int  varianty[100]; // jednotlive varianty

} PODPRVEK;


extern HERNI_OBJEKT   prv[HERNICH_PRVKU];
extern PODPRVEK       podprv[PODPRVKU];

void napln_zavislosti(void);
void obr_do_bmp(dword cislo_p, byte *p_ctverec, dword x_res, dword y_res,byte *p_b);

#endif

/* Modul pro obsluhu levelu */
/* soucast hry berusky */
/* (C) ANAKREON 1999 */

#ifndef __LEVELY_
#define __LEVELY_

#include "zakl_typ.h"

#define XPOLI     32
#define YPOLI     21

#define h_levelu  "Level 3 (C) Anakreon 1999"

typedef struct  {

   byte 	signum[30]      __attribute__ ((packed));    //	 -> retezec "Berusky (C) Anakreon 1998"
   byte	pozadi	       __attribute__ ((packed));    //-> jmeno souboru s pozadim
   byte	hudba[16]	    __attribute__ ((packed));    //-> hudba k tomuto levelu
   byte  natoceni[5]     __attribute__ ((packed));
   byte	rezerved[66+10] __attribute__ ((packed));    // -> pro pozdejsi pouziti
   word	podlaha[YPOLI][XPOLI] __attribute__ ((packed));   // -> podlaha
   word	level[YPOLI][XPOLI][2]  __attribute__ ((packed));   //-> herni plocha (dnesni level)
   word	hraci[YPOLI][XPOLI]  __attribute__ ((packed));   //-> mapa hracu (dneska sou hraci n levelu)
   byte* animace[YPOLI][XPOLI]__attribute__ ((packed)); //-> mapa animovanych prvku
// (po konci tahu to projede a  udela dalsi faze animaci)
// jsou to pointery na animace

} _S_DISK_LEVEL __attribute__ ((packed));

typedef struct  {

   byte  signum[30]      __attribute__ ((packed));    //	 -> retezec "Berusky (C) Anakreon 1998"
   byte	pozadi	       __attribute__ ((packed));    // -> cislo pozadi
   byte	hudba    	    __attribute__ ((packed));    // -> cislo hudby k tomuto levelu
   byte  natoceni[5]     __attribute__ ((packed));    // -> natoceni hracu
   byte	rezerved[100]   __attribute__ ((packed));    // -> pro pozdejsi pouziti
   word	podlaha[YPOLI][XPOLI][10] __attribute__ ((packed));     // -> podlaha
   word	level[YPOLI][XPOLI][10]   __attribute__ ((packed));      //-> herni plocha (dnesni level)
   word	hraci[YPOLI][XPOLI]       __attribute__ ((packed));      //-> mapa hracu (dneska sou hraci n levelu)

} DISK_LEVEL __attribute__ ((packed));

int  obnov_level(byte *p_level_file, DISK_LEVEL *pl);
int  obnov_level_lep(byte *p_level_file,int dat, DISK_LEVEL *pl);

#endif

