/* konvertor ze starych do novych levelu */
/* (C) KOMAT 1999 */
#include <stdio.h>
#include "zakl_typ.h"
#include <dir.h>
#include <crt0.h>
#include "levely.h"

#define  NOVY  "lve"
#define  STARY "lv"

#define ZEM       0
// Definice beden
#define HRAC1     1
#define HRAC5     5
// Definice beden
#define BEDNA1    6
#define BEDNA2    0x20
#define BEDNA3    0x21
#define BEDNA4    0x22
// Definice tnt beden
#define TNT1       7
#define TNT2        0x23
#define TNT3        0x24
#define TNT4        0x2b
#define TNT5        0x2c
#define TNT6        0x2d

//Definice exitu
#define EXIT1       9
#define EXIT2       0x28
#define EXIT3       0x27
// Definice kamenu
#define KAMEN1    10
// Definice klice
#define KLIC      11
// Definice lopat
#define KRUMPAC1     12
#define KRUMPAC2    0x29
// Definice barevnych klicu
#define KLIC1     0xd
#define KLIC2     0xe
#define KLIC3     0xf
#define KLIC4     0x10
#define KLIC5     0x11
// Definice bar dveri
#define DVERE1    0x12
#define DVERE2    0x13
#define DVERE3    0x14
#define DVERE4    0x15
#define DVERE5    0x16
// definice individualnich dveri
#define ID_DVERE1 0x17
#define ID_DVERE2 0x18
#define ID_DVERE3 0x19
#define ID_DVERE4 0x1a
#define ID_DVERE5 0x1b
// definice zaviracich dveri
#define DV_H_O    0x1c
#define DV_H_Z    0x1d
#define DV_V_O    0x1e
#define DV_V_Z    0x1f


struct      ffblk f;
_1_DISK_LEVEL  ln;
byte        ls[32*21];
byte        hlavicka[] = "Berusky (C) Anakreon 1999";

/* nez sem na to prisel, malem sem se POSRAL !!!!!!!!!!!!!!!!!!!!!*/
char **__crt0_glob_function(char *_argument)
{
 return(0);
}

// konvertuje prvni parametr do druheho
void main(int argc, char *argv[])
{
 FILE *l_s;
 FILE *l_d;
 int  i,x,y,j;
 byte level_src[20];
 byte level_des[20];
 char *p_pom;
 int done;
 int prvek;
 int por;


 setvbuf(stdout, NULL, _IONBF, 0);

 printf("Prevodnik starych levelu na nove, pridava koncovku *.lve\n");
 printf("(C) Anakreon 1999\nCas kompilace %s, %s\n\n",__TIME__,__DATE__);

 if(argc < 2) {
    printf("Pouziti : Zadne, toto je interni utilitka !\n\n");
    printf("Zadej jmeno konvertovaneho levelu v hvezdickove\nkonvenci.\n\n");
    exit(0);
 }


 strcpy(level_src,argv[1]);

 done = findfirst(level_src,&f,FA_RDONLY|FA_ARCH);

 while (!done) {
    strcpy(level_src,f.ff_name);
   
    if((l_s = fopen(level_src,"rb")) == NULL) {
       printf("Nemuzu otevrit %s",level_des);
       exit(0);
    }
    printf("\n %s ...",level_src);

    fread(&ln,sizeof(_1_DISK_LEVEL),1,l_s);
    ln.signum[29] = 0;
   
    if(strstr(ln.signum,hlavicka) == NULL) {
       memset(&ln,0,sizeof(ln));
       // je to starej level -> preved ho na novej
       fseek(l_s,0,SEEK_SET);
       fread(ls,sizeof(byte),32*21,l_s);
       strcpy(ln.signum,hlavicka);
       ln.pozadi = 0;

       strcpy(ln.hudba,"default.xm");
       for(i = 0; i < YPOLI; i++)
           for(j = 0; j < XPOLI; j++)
               ln.animace[i][j] = NULL;
   
       for(y = 0; y < 21; y++)  {
           for(x = 0; x < 32; x++)  {
              por = (XPOLI*y)+x;
              prvek = ls[por];
              if((prvek < 1) || (prvek > 5)) {
                  if(prvek < 0x20)
                     ln.level[y][x][0] = ln.level[y][x][1] = prvek;
                  else {
                     ln.level[y][x][1] = prvek;

                     switch(prvek) {
                        case BEDNA1:
                        case BEDNA2:
                        case BEDNA3:
                        case BEDNA4:
                           ln.level[y][x][0] = BEDNA1;
                           break;

                        case TNT1:
                        case TNT2:
                        case TNT3:
                        case TNT4:
                        case TNT5:
                        case TNT6:
                           ln.level[y][x][0] = TNT1;
                           break;

                        case EXIT1:
                        case EXIT2:
                        case EXIT3:
                           ln.level[y][x][0] = EXIT1;
                           break;

                        case KRUMPAC1:
                        case KRUMPAC2:
                           ln.level[y][x][0] = KRUMPAC1;
                           break;

                        default:
                           ln.level[y][x][0] = 8;
                           break;
                     }
                 }
              }
              else
                  ln.hraci[y][x] = ls[por];
           }
       }
       strcpy(level_des,level_src);
       p_pom = (char *) strchr(level_des,'.');
       if(p_pom == NULL) {printf("Shit."); exit(0); }
       p_pom++;
       *p_pom = 0;
       strcat(level_des,NOVY);
   
       if((l_d = fopen(level_des,"wb")) == NULL) {
         printf("nemuzu otvrit %s",level_des);
         exit(1);
       }
       fwrite(&ln,sizeof(ln),1,l_d);
       fclose(l_d);
       fclose(l_s);
       printf(" %s ",level_des);
    }
    done = findnext(&f);
 }
 exit(0);
}



