/* konvertor ze starych do novych levelu */
/* (C) KOMAT 1999 */
/*
  Spatne se konvertuje
  posledni hrac !

*/

#include <stdio.h>
#include "zakl_typ.h"
#include <dir.h>
#include <crt0.h>
#include "levely.h"
#include "konver.h"

#define  NOVY  "lv3"
#define  STARY "lve"

#define ZEM       0
// Definice beden
#define HRAC1     1
#define HRAC5     5
// Definice beden
#define BEDNA1    6
#define BEDNA2    0x20
#define BEDNA3    0x21
#define BEDNA4    0x22
// Definice tnt beden
#define TNT1       7
#define TNT2        0x23
#define TNT3        0x24
#define TNT4        0x2b
#define TNT5        0x2c
#define TNT6        0x2d

//Definice exitu
#define EXIT1       9
#define EXIT2       0x28
#define EXIT3       0x27
// Definice kamenu
#define KAMEN1    10
// Definice klice
#define KLIC      11
// Definice lopat
#define KRUMPAC1     12
#define KRUMPAC2    0x29
// Definice barevnych klicu
#define KLIC1     0xd
#define KLIC2     0xe
#define KLIC3     0xf
#define KLIC4     0x10
#define KLIC5     0x11
// Definice bar dveri
#define DVERE1    0x12
#define DVERE2    0x13
#define DVERE3    0x14
#define DVERE4    0x15
#define DVERE5    0x16
// definice individualnich dveri
#define ID_DVERE1 0x17
#define ID_DVERE2 0x18
#define ID_DVERE3 0x19
#define ID_DVERE4 0x1a
#define ID_DVERE5 0x1b
// definice zaviracich dveri
#define DV_H_O    0x1c
#define DV_H_Z    0x1d
#define DV_V_O    0x1e
#define DV_V_Z    0x1f


struct      ffblk f;
DISK_LEVEL     ln;
_1_DISK_LEVEL  st;
byte        ls[32*21];
byte        hlavicka[] = "Berusky (C) Anakreon 1999";
byte        hlavicka2[] = "Level 3 (C) Anakreon 1999";

/* nez sem na to prisel, malem sem se POSRAL !!!!!!!!!!!!!!!!!!!!!*/
char **__crt0_glob_function(char *_argument)
{
 return(0);
}

// konvertuje prvni parametr do druheho
void main(int argc, char *argv[])
{
 FILE *l_s;
 FILE *l_d;
 int  i,x,y,j;
 byte level_src[20];
 byte level_des[20];
 char *p_pom;
 int done;
 int prvek;
 int por;
 int hrac;

 setvbuf(stdout, NULL, _IONBF, 0);

 printf("Prevodnik starych levelu na nove, pridava koncovku *.lv3\n");
 printf("*.lve -> *.lv3, obsahuje i specialni patch na zachovani grafiky\n");
 printf("(C) Anakreon 1999\nCas kompilace %s, %s\n\n",__TIME__,__DATE__);

 if(argc < 2) {
    printf("Pouziti : Zadne, toto je interni utilitka !\n\n");
    printf("Zadej jmeno konvertovaneho levelu v hvezdickove\nkonvenci.\n\n");
    exit(0);
 }


 strcpy(level_src,argv[1]);

 done = findfirst(level_src,&f,FA_RDONLY|FA_ARCH);

 while (!done) {
    strcpy(level_src,f.ff_name);
   
    if((l_s = fopen(level_src,"rb")) == NULL) {
       printf("Nemuzu otevrit %s",level_des);
       exit(0);
    }
    printf("\n %s ...",level_src);

    fread(&st,sizeof(_1_DISK_LEVEL),1,l_s);
    st.signum[29] = 0;
   
    if(strstr(st.signum,hlavicka) != NULL) {
       // je to starej level -> preved ho na novej
       memset(&ln,0,sizeof(ln));
       strcpy(ln.signum,hlavicka2);
       ln.pozadi = 0;
       ln.hudba = 0;

       for(y = 0; y < 21; y++)  {
           for(x = 0; x < 32; x++)  {
              if(st.hraci[y][x] != 0) {
                ln.hraci[y][x] = hrac = ((st.hraci[y][x]-1)%5);
                ln.natoceni[hrac] = (st.hraci[y][x]-1)/5;
              }
              else
                ln.hraci[y][x] = 0xffff;
                
              if (st.podlaha[y][x] != 0) {
                 ln.podlaha[y][x][0] = P_ZEME;
                 if(st.podlaha[y][x] == 1)
                   ln.podlaha[y][x][1] = 9;
                 else
                   ln.podlaha[y][x][1] = 4;
              }
              else {
                 ln.podlaha[y][x][0] = NENI_PODLAHA;
                 ln.podlaha[y][x][1] = 0;
              }

              switch(st.level[y][x][1]) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 24+st.level[y][x][1];
                 ln.level[y][x][2] = 0;
                 break;

                case 6:
                 ln.level[y][x][0] = P_BEDNA;
                 ln.level[y][x][1] = 1;
                 ln.level[y][x][2] = 0;
                 break;
                
                case 7:
                 ln.level[y][x][0] = P_TNT;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;
                
                case 8:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                case 9:
                 ln.level[y][x][0] = P_EXIT;
                 ln.level[y][x][1] = 4;
                 ln.level[y][x][2] = 0;
                 break;

                case 10:
                 ln.level[y][x][0] = P_KAMEN;
                 ln.level[y][x][1] = 1;
                 ln.level[y][x][2] = 0;
                 break;

                case 11:
                 ln.level[y][x][0] = P_KLIC;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                case 12:
                 ln.level[y][x][0] = P_KRUMPAC;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                 ln.level[y][x][0] = P_KLIC1+st.level[y][x][1]-13;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                 ln.level[y][x][0] = P_DVERE1_H_Z+st.level[y][x][1]-18;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                 ln.level[y][x][0] = P_ID_DVERE1_H_Z+st.level[y][x][1]-23;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                case DV_H_O:
                 ln.level[y][x][0] = P_DV_V_O;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;
                case DV_H_Z:
                 ln.level[y][x][0] = P_DV_V_Z;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;
                case DV_V_O:
                 ln.level[y][x][0] = P_DV_H_O;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;
                case DV_V_Z:
                 ln.level[y][x][0] = P_DV_H_Z;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                case 32:
                 ln.level[y][x][0] = P_BEDNA;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 33:
                 ln.level[y][x][0] = P_BEDNA;
                 ln.level[y][x][1] = 4;
                 ln.level[y][x][2] = 0;
                 break;

                case 34:
                 ln.level[y][x][0] = P_BEDNA;
                 ln.level[y][x][1] = 5;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 35:
                 ln.level[y][x][0] = P_TNT;
                 ln.level[y][x][1] = 1;
                 ln.level[y][x][2] = 0;
                 break;

                case 36:
                 ln.level[y][x][0] = P_TNT;
                 ln.level[y][x][1] = 4;
                 ln.level[y][x][2] = 0;
                 break;

                case 37:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 3;
                 ln.level[y][x][2] = 0;
                 break;

                case 38:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 2;
                 ln.level[y][x][2] = 0;
                 break;

                case 39:
                 ln.level[y][x][0] = P_EXIT;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                case 40:
                 ln.level[y][x][0] = P_EXIT;
                 ln.level[y][x][1] = 1;
                 ln.level[y][x][2] = 0;
                 break;

                case 41:
                 ln.level[y][x][0] = P_KRUMPAC;
                 ln.level[y][x][1] = 1;
                 ln.level[y][x][2] = 0;
                 break;

                case 42:
                case 43:
                case 44:
                 ln.level[y][x][0] = P_TNT;
                 ln.level[y][x][1] = 1;
                 ln.level[y][x][2] = 0;
                 break;

                case 45:
                 ln.level[y][x][0] = P_ZEME;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                case 46:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 7;
                 ln.level[y][x][2] = 0;
                 break;

                case 47:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 8;
                 ln.level[y][x][2] = 0;
                 break;

                case 48:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 9;
                 ln.level[y][x][2] = 0;
                 break;

                case 49:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 4;
                 ln.level[y][x][2] = 0;
                 break;

                case 50:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 5;
                 ln.level[y][x][2] = 0;
                 break;

                case 51:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 6;
                 ln.level[y][x][2] = 0;
                 break;

                case 52:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 1;
                 ln.level[y][x][2] = 0;
                 break;

                case 53:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 40+22;
                 ln.level[y][x][2] = 0;
                 break;

                case 54:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 35+22;
                 ln.level[y][x][2] = 0;
                 break;

                case 55:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 37+22;
                 ln.level[y][x][2] = 0;
                 break;

                case 56:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 19;
                 ln.level[y][x][2] = 0;
                 break;

                case 57:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 17;
                 ln.level[y][x][2] = 0;
                 break;

                case 58:
                 ln.level[y][x][0] = P_BEDNA;
                 ln.level[y][x][1] = 6;
                 ln.level[y][x][2] = 0;
                 break;

                case 59:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 32+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 60:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 30+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 61:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 31+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 62:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 34+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 63:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 36+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 64:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 38+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 65:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 39+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 66:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 41+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 67:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 42+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 68:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 43+22;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 69:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 16;
                 ln.level[y][x][2] = 0;
                 break;

                case 70:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 10;
                 ln.level[y][x][2] = 0;
                 break;

                case 71:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 11;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 72:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 15;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 73:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 14;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 74:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 21;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 75:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 22;
                 ln.level[y][x][2] = 0;
                 break;

                case 76:
                case 77:
                case 78:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 79:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 18;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 80:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 13;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 81:
                 ln.level[y][x][0] = P_STENA;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                 
                case 82:
                 ln.level[y][x][0] = P_BEDNA;
                 ln.level[y][x][1] = 2;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 83:
                 ln.level[y][x][0] = P_BEDNA;
                 ln.level[y][x][1] = 3;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 84:
                 ln.level[y][x][0] = P_BEDNA;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 85:
                 ln.level[y][x][0] = P_TNT;
                 ln.level[y][x][1] = 2;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 86:
                 ln.level[y][x][0] = P_TNT;
                 ln.level[y][x][1] = 3;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 87:
                 ln.level[y][x][0] = P_TNT;
                 ln.level[y][x][1] = 4;
                 ln.level[y][x][2] = 0;
                 break;

                case 88:
                 ln.level[y][x][0] = P_EXIT;
                 ln.level[y][x][1] = 4;
                 ln.level[y][x][2] = 0;
                 break;
                 
                case 89:
                 ln.level[y][x][0] = P_EXIT;
                 ln.level[y][x][1] = 0;
                 ln.level[y][x][2] = 0;
                 break;

                default:
                  break;
              }
//  [0] -> cislo prvku
//  [1] -> poradove cislo grafiky
//  [3] -> rotace
           }
       }
       strcpy(level_des,level_src);
       p_pom = (char *) strchr(level_des,'.');
       if(p_pom == NULL) {printf("Shit."); exit(0); }
       p_pom++;
       *p_pom = 0;
       strcat(level_des,NOVY);
   
       if((l_d = fopen(level_des,"wb")) == NULL) {
         printf("nemuzu otvrit %s",level_des);
         exit(1);
       }
       fwrite(&ln,sizeof(ln),1,l_d);
       fclose(l_d);
       fclose(l_s);
       printf(" %s ",level_des);
    }
    done = findnext(&f);
 }
 exit(0);
}



