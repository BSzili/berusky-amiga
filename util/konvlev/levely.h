/* soucast hry berusky */
/* (C) ANAKREON 1999 */

#ifndef __LEVELY_
#define __LEVELY_

#include "zakl_typ.h"

#define XPOLI     32
#define YPOLI     21

#define h_levelu  "Berusky (C) Anakreon 1999"


typedef struct  {

   byte 	signum[30]      __attribute__ ((packed));    //	 -> retezec "Berusky (C) Anakreon 1998"
   byte	pozadi	       __attribute__ ((packed));    //-> jmeno souboru s pozadim
   byte	hudba[16]	    __attribute__ ((packed));    //-> hudba k tomuto levelu
   byte natoceni[5]     __attribute__ ((packed));
   byte	rezerved[66+10] __attribute__ ((packed));    // -> pro pozdejsi pouziti
   word	podlaha[YPOLI][XPOLI] __attribute__ ((packed));   // -> podlaha
   word	level[YPOLI][XPOLI][2]  __attribute__ ((packed));   //-> herni plocha (dnesni level)
   word	hraci[YPOLI][XPOLI]  __attribute__ ((packed));   //-> mapa hracu (dneska sou hraci n levelu)
   byte* animace[YPOLI][XPOLI]__attribute__ ((packed)); //-> mapa animovanych prvku

} _1_DISK_LEVEL __attribute__ ((packed));

typedef struct  {

   byte signum[30]      __attribute__ ((packed));    //	 -> retezec "Berusky (C) Anakreon 1998"
   byte	pozadi	       __attribute__ ((packed));    // -> cislo pozadi
   byte	hudba    	    __attribute__ ((packed));    // -> cislo hudby k tomuto levelu
   byte natoceni[5]     __attribute__ ((packed));    // -> natoceni hracu
   byte	rezerved[100] __attribute__ ((packed));    // -> pro pozdejsi pouziti
   word	podlaha[YPOLI][XPOLI][10] __attribute__ ((packed));     // -> podlaha
   word	level[YPOLI][XPOLI][10]  __attribute__ ((packed));      //-> herni plocha (dnesni level)
   word	hraci[YPOLI][XPOLI]  __attribute__ ((packed));      //-> mapa hracu (dneska sou hraci n levelu)

} DISK_LEVEL __attribute__ ((packed));

int  obnov_level(byte *p_level_file, DISK_LEVEL *pl);
int  obnov_level_lep(byte *p_level_file, byte *p_dat, DISK_LEVEL *pl);

#endif
