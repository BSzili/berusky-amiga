/*
  Konvertor z latin 2 do Berusciho formatu textu
*/
#include "libanak.h"
#include <stdio.h>
#include "zakl_typ.h"
#include <dir.h>
#include <crt0.h>

#define  NOVY  "txb"
#define  STARY "txt"

struct      ffblk f;

/* nez sem na to prisel, malem sem se POSRAL !!!!!!!!!!!!!!!!!!!!!*/
char **__crt0_glob_function(char *_argument)
{
 return(0);
}

void main(int argc, char *argv[])
{
 FILE *txt;
 FILE *txb;
 int  i;
 byte text_src[20];
 byte text_des[20];
 char *p_pom;
 int done;
 char z1;
 byte z;

 setvbuf(stdout, NULL, _IONBF, 0);

 printf("Prevodnik textu do Berusciho formatu v.2 (C) Anakreon 1999\n");
 printf("Prevadi z *.txt do *.txb\n");
 printf("Cas kompilace %s, %s\n\n",__TIME__,__DATE__);

 if(argc < 2) {
    printf("Pouziti : Zadne, toto je interni utilitka !\n\n");
    printf("Zadej jmeno konvertovaneho textu v hvezdickove\nkonvenci.\n\n");
    exit(0);
 }


 strcpy(text_src,argv[1]);

 done = findfirst(text_src,&f,FA_RDONLY|FA_ARCH);

 while (!done) {
    strcpy(text_src,f.ff_name);

    if((txt = fopen(text_src,"r")) == NULL) {
       printf("Nemuzu otevrit %s",text_src);
       exit(0);
    }
    printf("\nOteviram %s ...",text_src);

    strcpy(text_des,text_src);
    p_pom = (char *) strchr(text_des,'.');
    if(p_pom == NULL) {printf("Shit."); exit(0); }
    p_pom++;
    *p_pom = 0;
    strcat(text_des,NOVY);

    if((txb = fopen(text_des,"w")) == NULL) {
      printf("nemuzu otvrit %s",text_des);
      exit(1);
    }

    i = 0;
    while((z1 = getc(txt)) != EOF) {

      z = (byte)z1;
      switch(z) {

        case 130:
          putc(_CARKA,txb);
          putc('e',txb);
          break;

        case 133:
          putc(_KROUZEK,txb);
          putc('u',txb);
          break;

        case 144:
          putc(_CARKA,txb);
          putc('e',txb);
          break;

        case 155:
          putc(_HACEK,txb);
          putc('t',txb);
          break;

        case 156:
          putc(_HACEK,txb);
          putc('t',txb);
          break;
          
        case 159:
          putc(_HACEK,txb);
          putc('c',txb);
          break;

        case 160:
          putc(_CARKA,txb);
          putc('a',txb);
          break;

        case 161:
          putc(_CARKA,txb);
          putc('i',txb);
          break;

        case 162:
          putc(_CARKA,txb);
          putc('o',txb);
          break;

        case 163:
          putc(_CARKA,txb);
          putc('u',txb);
          break;

        case 166:
          putc(_HACEK,txb);
          putc('z',txb);
          break;

        case 167:
          putc(_HACEK,txb);
          putc('z',txb);
          break;

        case 172:
          putc(_HACEK,txb);
          putc('c',txb);
          break;

        case 183:
          putc(_HACEK,txb);
          putc('e',txb);
          break;

        case 210:
          putc(_HACEK,txb);
          putc('d',txb);
          break;

        case 212:
          putc(_HACEK,txb);
          putc('d',txb);
          break;

        case 213:
          putc(_HACEK,txb);
          putc('n',txb);
          break;

        case 214:
          putc(_CARKA,txb);
          putc('i',txb);
          break;

        case 216:
          putc(_HACEK,txb);
          putc('e',txb);
          break;

        case 222:
          putc(_KROUZEK,txb);
          putc('u',txb);
          break;

        case 224:
          putc(_CARKA,txb);
          putc('o',txb);
          break;

        case 229:
          putc(_HACEK,txb);
          putc('n',txb);
          break;

        case 230:
          putc(_HACEK,txb);
          putc('s',txb);
          break;

        case 231:
          putc(_HACEK,txb);
          putc('s',txb);
          break;

        case 233:
          putc(_CARKA,txb);
          putc('u',txb);
          break;

        case 236:
          putc(_CARKA,txb);
          putc('y',txb);
          break;

        case 237:
          putc(_CARKA,txb);
          putc('y',txb);
          break;

        case 252:
          putc(_HACEK,txb);
          putc('r',txb);
          break;

        case 253:
          putc(_HACEK,txb);
          putc('r',txb);
          break;

        default:
          putc(z,txb);
          break;
        
      }
//      printf("\rPrevadim %d znaku ...",++i);
    }
    fclose(txt);
    fclose(txb);

    printf("\nZapisuji %s ... ",text_des);

    done = findnext(&f);
 }
 exit(0);
}


