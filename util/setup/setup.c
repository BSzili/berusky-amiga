/*
  Setup k midasovy
*/
#include <stdio.h>
#ifdef __AMIGA__
#include <libanak.h>
const char __stdiowin[] = "CON://400/200/Berusky Sound Setup/AUTO/CLOSE/"; // WAIT
#endif
#include "midasdll.h"

#define CF_FILE  "berusky.cfg"
#define DEF_SOUND 55
#define DEF_MUSIC 15


void MIDASerror(void)
{
    int         error;

    error = MIDASgetLastError();
    printf("\nMIDAS error: %s\n", MIDASgetErrorMessage(error));
    if ( !MIDASclose() )
    {
        printf("\nBIG PANIC! MIDASclose Failed: %s\n", MIDASgetErrorMessage(
            MIDASgetLastError()));
    }
    exit(0);
}

void main(void)
{
int  vol_sound = DEF_SOUND;
int  vol_music = DEF_MUSIC;
FILE *f;

  if(!MIDASstartup())
    MIDASerror();
    
  if(!MIDASconfig())
    MIDASerror();

  MIDASsaveConfig(CF_FILE);

  MIDASclose();

  if((f = fopen(CF_FILE,"rb+")) != NULL) {
    fseek(f,28,SEEK_SET);
#ifdef __AMIGA__
    vol_sound = SWAP32LE(vol_sound);
    vol_music = SWAP32LE(vol_music);
#endif
    fwrite(&vol_sound,sizeof(int),1,f);
    fwrite(&vol_music,sizeof(int),1,f);
    fclose(f);
  }
  exit(0);
}
