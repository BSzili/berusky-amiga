/*
  H k lepu
*/

#include <stdio.h>
#include <stdlib.h>
#include "zakl_typ.h"
#include "zlib.h"
//#include "libanak.h"

#define  LIST	"lep"
#define  CHYBA  0xf0
#define  SIGNUM 0x46534B41

int  scanuj_list(void);
void nacti_argumenty(void);
void otevri_soubory(void);


typedef struct {

   char   jmeno[16] __attribute__ ((packed));
   dword  adresa    __attribute__ ((packed));
   dword  d_unp     __attribute__ ((packed));
   dword  d_pak     __attribute__ ((packed));

} POLOZKA_SOUBORU __attribute__ ((packed));

typedef struct {

   dword  signum         __attribute__ ((packed));
   dword  souboru        __attribute__ ((packed));
   dword  rezervace[200] __attribute__ ((packed));

} HLAVICKA_LEPU __attribute__ ((packed));


int nahraj_soubor( dword delka,byte  *p_mem,byte *p_file,long adresa);
int velikost_file(byte *p_file);


#define CHECK_ERR(err, msg) { \
    if (err != Z_OK) { \
        fprintf(stderr, "%s error: %d\n", msg, err); \
        exit(1); \
    } \
}

