//lepeni souboru k sobe
/* mozne parametry
 lep.exe jmeno_souboru.ext -> slepi standartnim spusobem a vypise do
 jmeno_souboru.lst + pripoji na zacatek hlavicku

 lep.exe jmeno_souboru.ext p -> rozsirene lepeni, prilepi seznam na zacatek
 lepeneho souboru + spakuje zlibou
*/

#include "lep.h"

int  i = 0;
FILE *cil,*zdrj,*list,*prt,*seznam;

char cilovy[256],
     zdroj[256];

long  pozice;
byte  *p_buffer;
byte  *p_pack;

long  nacteno;


int   pack = 0; // balene soubory se budou zaroven komprimovat
int   scan = 1; // na zacatek zapise hlavicku

dword   souboru;
dword   velikost;
dword   velikost_pack;
dword   zal;

POLOZKA_SOUBORU  *p_s;

void main(int argc,char *argv[])
{
 int err,j;

 printf("Interni utilitka Lep v.2.0.1 (C) Komat / Anakreon 1999\n\n");

 if((argc > 2)&&(argv[2][0] == 'l')) {
   listuj_soubory(argv[1]);
   exit(0);
 }


 if((list = fopen(&argv[1][0],"r")) == NULL) {
   printf("Zadej jmeno lep souboru...\n");
   exit(0);
//	printf("\nNemuzu otevrit soubor %s\nZadavani prikazu bude rucni !\n",&argv[1][0]);
//	list = stdin;
 }

 fscanf(list,"%s",&cilovy);
 printf("\nZakladam soubor %s\n",cilovy);
 if((cil = fopen(cilovy,"wb"))==NULL) {
	printf("\nChyba otevreni ciloveho souboru.\n");
	exit(0);
 }

 while(cilovy[i++] != '.');
 cilovy[i] = '\0';
 strcat(cilovy,LIST);
 prt = fopen(cilovy,"w");

 if((argc > 2)&&(argv[2][0] == 'p'))
    pack = 1;

 if(scan) {
   souboru = scanuj_list();
 }

 if((p_s = malloc(sizeof(POLOZKA_SOUBORU)*souboru)) == NULL) {
   printf("\n\nNedostatek pameti na hlavicky !\n");
   exit(CHYBA);
 }

 zal = ftell(cil);

 for(i = 0; i < souboru+1; i++) {
    memset(p_s[i].jmeno,0,16);
    p_s[i].adresa = 0;
    p_s[i].d_unp = 0;
    p_s[i].d_pak = 0;
 }
 fwrite(p_s,sizeof(POLOZKA_SOUBORU),souboru+1,cil);

 i = 0;
 do {
    if((fscanf(list,"%s",&zdroj)) == EOF) {
      break;
    }
    if((zdroj[0] == ';')||(zdroj[0] == ' '))
      continue;

    printf("\nLepim soubor %s",zdroj);
    if((zdrj = fopen(zdroj,"rb")) == NULL) {
      printf("\nChyba cteni souboru %s",zdroj);
      break;
    }

    fseek(zdrj,0,SEEK_END);

    for(j = 0; zdroj[j]; j++) {
       p_s[i].jmeno[j] = tolower(zdroj[j]);
    }
    
    p_s[i].adresa = ftell(cil);
    p_s[i].d_unp = velikost = ftell(zdrj);
    p_s[i].d_pak = 0;

    fseek(zdrj,0,SEEK_SET);

    if((p_buffer = malloc(velikost+1024)) == NULL) {
      printf("\n\nNedostatek pameti na buffer !\n");
      exit(CHYBA);
    }

    if(pack) {
      if((p_pack = malloc(velikost+1024)) == NULL) {
        printf("\n\nNedostatek pameti na buffer !\n");
        exit(CHYBA);
      }
    }

    fread(p_buffer,sizeof(byte),velikost,zdrj);

    if(!pack)
      fwrite(p_buffer,sizeof(byte),velikost,cil);
    else {
      printf(" ...pakuji... ");
      velikost_pack = velikost + 1024;
      err = compress(p_pack, &velikost_pack, p_buffer, velikost);
      CHECK_ERR(err, "compress");
      fwrite(p_pack,sizeof(byte),velikost_pack,cil);
      p_s[i].d_pak = velikost_pack;
    }
    free(p_buffer);
    fclose(zdrj);

    if(pack)
      fprintf(prt,"%s    adresa %lu    delka %lu pakovano %lu\n",zdroj,p_s[i].adresa,velikost,velikost_pack);
    else
      fprintf(prt,"%s    adresa %lu    delka %lu\n",zdroj,p_s[i].adresa,velikost);

    i++;
    
 } while(1);

 printf("\nUpdatuji hlavicku ...\n",zdroj);
 fseek(cil,zal,SEEK_SET);
 fwrite(p_s,sizeof(POLOZKA_SOUBORU),souboru,cil);

 fclose(cil);
 fclose(list);
}

int scanuj_list(void)
{
 HLAVICKA_LEPU    hlavicka;
 dword posl_pozice = 0;
 dword zal;

 zal = ftell(list);

 do {
   printf("\rVytvarim seznam souboru ... %d ",posl_pozice);

   if((fscanf(list,"%s",zdroj)) == EOF) {
     break;
   }
   
   if((zdroj[0] == ';')||(zdroj[0] == ' '))
     continue;
   
   if((zdrj = fopen(zdroj,"rb")) == NULL) {
     printf("\nChyba cteni souboru %s",zdroj);
     break;
   }
   fclose(zdrj);
   posl_pozice++;

 } while(1);

 printf("OK\n");

 fseek(list,zal,SEEK_SET);

 printf("Zapisuji hlavicku ...\n");

 hlavicka.signum = SIGNUM;
 hlavicka.souboru = posl_pozice;
 memset(hlavicka.rezervace,0,sizeof(dword)*200);
 strcpy(hlavicka.rezervace,"lep file");
 fwrite(&hlavicka,sizeof(hlavicka),1,cil);

 printf("Zacatek na %ld\n",posl_pozice*sizeof(POLOZKA_SOUBORU) + sizeof(HLAVICKA_LEPU));

 return(posl_pozice);
}

void listuj_soubory(byte *p_file)
{
 FILE   *zdroj;
 POLOZKA_SOUBORU  soubor;

 if((zdroj = fopen(p_file,"rb")) == NULL) {
   printf("\nNemuzu otevrit soubor %s\n",p_file);
   exit(0);
 }

 fseek(zdroj,sizeof(HLAVICKA_LEPU),SEEK_SET);

 do {
    fread(&soubor,sizeof(soubor),1,zdroj);
    if(soubor.jmeno[0] == 0)
       break;
    printf("%s adresa    %ld delka    orig %ld    delka pack %ld\n",
    soubor.jmeno,soubor.adresa, soubor.d_unp,soubor.d_pak);
 } while(1);

 fclose(zdroj);
}


