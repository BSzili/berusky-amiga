Utility potrebne nebo pomocne k delani berusek:

bk ->      bitmapovy konvertor -> konvertuje bitmapy
           do spritu (info v libanak.h) a exportuje paletu

konvlev -> tyto utilitky 
           konvertuji stare (*.lv) levely na novejsi (*.lve)
           a tyto na konecny format levelu (*.lv3)

l2ber   -> Prevadi texty napsane v latin2 do berusciho
           formatu textu (hacky a carky jsou udelany pomoci
           takovejch divnejch znaminek)
           (konvertuje pouze diakritiku)
           
lep	-> lepi vic souboru k sobe, pri tom je 
           muze pakovat pomoci zliby. Nutne na budovani
           berusciho dat souboru
           
rw      -> prevadi soubory *.wav do *.raw

setup   -> standartni setup do berusek


           