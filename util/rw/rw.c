/*
  Prevodnik z wav do raw

*/
#include <stdio.h>
#include <dir.h>
#include "zakl_typ.h"

#define KONC_RAW ".raw"

char **__crt0_glob_function(char *_argument)
{
 return(0);
}

void preved_soubor(char *p_jmeno)
{
  byte file[100];
  byte *p_buffer;
  byte *p_p;
  FILE *wav;
  FILE *raw;
  dword i = 1;
  dword offset;
  dword *p_dl;
  dword delka;

  if((wav = fopen(p_jmeno,"rb")) == NULL) {
    exit(0);
  }
  strcpy(file,p_jmeno);
  p_p = (byte *)strchr(file,'.');
  *p_p = 0;
  strcat(file,KONC_RAW);

  if((raw = fopen(file,"wb")) == NULL) {
    exit(0);
  }

  fread(file,1,90,wav);

  for(i = 0; i < 95; i++) {
    if((file[i] == 'd')||(file[i] == 'D')) {
      if((p_p = (byte *)strstr(file+i,"data")) == NULL) {
        if((p_p = (byte *)strstr(file+i,"DATA")) != NULL)
          break;
      }
      else {
        break;
      }
    }
  }

  
  if(p_p == NULL) {
    printf("\nNemuzu najit hlavicku...\n");
    return;
  }
  offset = (dword)(p_p+8 - file);

  p_dl = p_p+4;
  delka = *p_dl;

  fseek(wav,offset,SEEK_SET);

  if((p_buffer = (byte *)malloc(delka+10)) == NULL) {
    printf("\nMalo pameti, nemuzu alokovat %d B\n",delka+10);
    return;
  }
  
  i = fread(p_buffer,1,delka,wav);
  fwrite(p_buffer,1,i,raw);
  if(i != delka)
    printf("\n soubor %s delka %d -> nacteno %d\n",p_jmeno,delka,i);

  free(p_buffer);
    
  fclose(raw);
  fclose(wav);  
}


void main(int argc, char *argv[])
{
  int done;
  struct ffblk f;

  printf("Prevodnik WAV do RAW v.1.1   Komat / AnakreoN \n");
  
  if(argc != 2) {
    printf("\n Zadej argument...\n");
    exit(0);
  }

  done = findfirst(argv[1],&f,FA_RDONLY|FA_ARCH);

  while(!done) {
    printf("\nPrevadim soubor %s ",f.ff_name);
    preved_soubor(f.ff_name);
    done = findnext(&f);
  }

  exit(0);
}


