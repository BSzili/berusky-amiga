Zdrojaky k Beruskam
(C) Komat / Anakreon 1999
17.11.1999

Tak vy vsichni zvedavci, toto jsou kompletni zdrojaky k beruskam,
takze si je pekne uzivejte. Veskere dotazy, chyby a podobne
mi piste na adresu "kom@atlas.cz" nebo "komat@email.cz"
ci primo na "xstran02@stud.fee.vutbr.cz"
(ale ne na vsechny 3 zaraz, pak mi to dojde trojmo !)
Pokud nekdo udela berusky na nejakou platformu, 
na ktere se na to budu moc podivat (linux/windows apod.)
tak mi o tom napiste at se muzeme podivat/hodit odkaz na www.
Taktez nas zajimaji ostatni stranky o berusich,
nejaky lihne uzivatelskych levelu a podobne,
a vubec vsechny dotazy a pripominky.

Pokud chcete skompilovat kompletne berusky,
spustte davku makeall.bat ktera vam to zajisti.
Pokudvam chybi nejake knihovny nebo
includy, muzete si je natahnout z adresaru
LIB a INCLUDE (tyto soubory prikopirujete
do adresaru vasi distribuce DJGPP -  INCLUDE a LIB)

Pouzivani techto zdrojaku (tj. veskere grafiky,
programu, levelu, hudby a podobne) je vazano GNU
licenci, jenz je sepsana souboru "copying" nebo "wcopying", 
kde je zhruba napsano, ze je muzete sirit
a pozmenovat jak chcete (ovsem s oznacenim ze to je modifikace
puvodniho dila) a muzete jakekoliv casti zahrnou do nekomercniho dila.

Pokud by jste snad chteli nektere casti zahrnout do komercnich
programu (nejspise grafiku a nebo hudbu), kontaktujte autory techto casti
(treba zrovna grafiky nebo hudebnika) a domluvte se primo s nimi na 
pripadnych podminkach. Dalsi informace taky ziskate na nasi spolecne
emailove adrese anakreon@atlas.cz.

Za AnakreoN
Komat 
