// reimplementation of a subset of the MIDAS audio system using lowlevel.libary and xmplayer.library

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/lowlevel.h>
#include <libraries/xmplayer.h>
#include <proto/xmplayer.h>

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <midasdll.h>

// constants

enum ErrorCodes
{
	OK = 0,
	errUndefined,
	errOutOfMemory,

	errNoSampleHandles,
	errFileOpen,
	errFileRead,

	errInvalidModule,
	errInvalidChanNumber,
	errInvalidSampleHandle,
	errNoChannels,

	errInvalidArguments,
	errFileNotFound,

	errDeviceNotAvailable,

	errInvalidSampleType,
	errModuleNotPlaying,
	errOutOfChannels,
	errUnsuppSampleFormat,

	errNumErrorCodes
};

//#define MIDAS_DEBUG
#ifdef MIDAS_DEBUG
#define kprintf printf
#else
#define kprintf(...)
#endif


// globals

static char *errorMsg[] =
{
	"OK",
	"Undefined error",
	"Out of memory",

	"Out of sample handles",
	"Unable to open file",
	"Unable to read file",

	"Invalid module file",
	"Invalid channel number",
	"Invalid sample handle",
	"Sound Device channels not open",

	"Invalid function arguments",
	"File does not exist",

	"Device is not available",

	"Invalid sample type",
	"No module is being played",
	"Out of sound channels",
	"Unsupported sample data format"
};

static int mLastError = OK;

static struct Library *XMPlayerBase = NULL;
static APTR timerIntHandle = NULL;

static APTR buffer = NULL;
static struct XMPlayerInfo info;

// config
int midasSDNumber = -1;
int midasSDPort = -1;
int midasSDIRQ = -1;
int midasSDDMA = -1;
int midasSDCard = -1;
unsigned midasMixRate = 11025;
unsigned midasOutputMode = 0;

enum sdMode
{
	sdMono = 1,
	sdStereo = 2,
	sd8bit = 4,
	sd16bit = 8
};

#define NUMRATES 8
#define DEFAULTRATE 7
static unsigned mixRates[NUMRATES] = {8000, 11025, 16000, 22050, 27429, 32000, 37800, 44100};


// helper functions

static void TimerInterrupt(register APTR intData __asm("a1"))
{
	static void (*timerServiceFunc)(void);
	timerServiceFunc = intData;
	timerServiceFunc();
}

static APTR LoadData(char *fileName)
{
	BPTR file;
	char str[256];
	int i;
	APTR data;
	LONG length;

	kprintf("%s(%s)\n", __FUNCTION__, fileName);

	// replace backslashes
	strcpy(str, fileName);
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == '\\')
			str[i] = '/';
		i++;
	}

	//if (!(file = Open(fileName, MODE_OLDFILE)))
	if (!(file = Open(str, MODE_OLDFILE)))
	{
		mLastError = errFileOpen;
		return NULL;
	}

	Seek(file, 0, OFFSET_END);
	length = Seek(file, 0, OFFSET_BEGINNING);
	data = AllocVec(length, MEMF_PUBLIC);
	if (!data)
	{
		mLastError = errOutOfMemory;
		Close(file);
		return NULL;
	}

	Read(file, data, length);
	Close(file);

	return data;
}

static void Read32LE(BPTR file, APTR buffer)
{
	char *buf = (char *)buffer;

	Read(file, &buf[3], 1);
	Read(file, &buf[2], 1);
	Read(file, &buf[1], 1);
	Read(file, &buf[0], 1);
}

static void Write32LE(BPTR file, APTR buffer)
{
	char *buf = (char *)buffer;

	Write(file, &buf[3], 1);
	Write(file, &buf[2], 1);
	Write(file, &buf[1], 1);
	Write(file, &buf[0], 1);
}


// error handling

int MIDASgetLastError(void)
{
	return mLastError;
}

char *MIDASgetErrorMessage(int errorCode)
{
	return errorMsg[errorCode];
}


// screen refresh rate

DWORD MIDASgetDisplayRefreshRate(void)
{
	// TODO: return the actual screen refresh rate?
	return 50*1000;
}


// startup

BOOL MIDASstartup(void)
{
	kprintf("%s()\n", __FUNCTION__);
	// nothing to do here
	return TRUE;
}

BOOL MIDASstartBackgroundPlay(DWORD pollRate)
{
	kprintf("%s(%u)\n", __FUNCTION__, pollRate);

	return TRUE;
}

BOOL MIDASstopBackgroundPlay(void)
{
	kprintf("%s()\n", __FUNCTION__);

	return TRUE;
}

BOOL MIDASconfig(void)
{
	int answer, i;

	kprintf("%s()\n", __FUNCTION__);

	printf("Select mixing rate:\n");
	for (i = 0; i < NUMRATES; i++)
	{
		printf(" %d: %d %s\n", i, mixRates[i], midasMixRate == mixRates[i] ? "*" : "");
	}
	printf("[0-%d]> ", NUMRATES);
	scanf("%d", &answer);
	if (answer >= 0 && answer < NUMRATES)
	{
		midasMixRate = mixRates[answer];
	}
	printf("Mixing rate: %d\n\n", midasMixRate);

	printf("Select output mode:\n");
	printf(" 0: mono %s\n", midasOutputMode & sdMono ? "*" : "");
	printf(" 1: stereo %s\n", midasOutputMode & sdStereo ? "*" : "");
	printf("[0-1]> ");
	scanf("%d", &answer);
	switch (answer)
	{
		case 0:
			midasOutputMode |= sdMono;
			midasOutputMode &= ~sdStereo;
			break;
		case 1:
			midasOutputMode |= sdStereo;
			midasOutputMode &= ~sdMono;
			break;
	}
	printf("Output mode: %s\n\n", midasOutputMode & sdMono ? "mono" : "stereo");
	printf("Configuration saved. Press Enter to finish.\n");
	scanf("%c", &answer);

	return TRUE;
}

BOOL MIDASloadConfig(char *fileName)
{
	BPTR file;

	kprintf("%s(%s)\n", __FUNCTION__, fileName);

	if (!(file = Open(fileName, MODE_OLDFILE)))
	{
		mLastError = errFileOpen;
		return FALSE;
	}

	Read32LE(file, &midasSDNumber);
	Read32LE(file, &midasSDCard);
	Read32LE(file, &midasSDPort);
	Read32LE(file, &midasSDIRQ);
	Read32LE(file, &midasSDDMA);
	Read32LE(file, &midasMixRate);
	Read32LE(file, &midasOutputMode);

	kprintf("rate %u mode %08x %s %s\n", midasMixRate, midasOutputMode,
		midasOutputMode & sdStereo ? "stereo" : "", midasOutputMode & sdMono ? "mono" : "");

	Close(file);

	return TRUE;
}

BOOL MIDASsaveConfig(char *fileName)
{
	BPTR file;

	kprintf("%s(%s)\n", __FUNCTION__, fileName);

	if (!(file = Open(fileName, MODE_NEWFILE)))
	{
		mLastError = errFileOpen;
		return FALSE;
	}

	Write32LE(file, &midasSDNumber);
	Write32LE(file, &midasSDCard);
	Write32LE(file, &midasSDPort);
	Write32LE(file, &midasSDIRQ);
	Write32LE(file, &midasSDDMA);
	Write32LE(file, &midasMixRate);
	Write32LE(file, &midasOutputMode);

	Close(file);

	return TRUE;
}


// init

BOOL MIDASinit(void)
{
	kprintf("%s()\n", __FUNCTION__);

	XMPlayerBase = OpenLibrary("xmplayer.library", 1);

	return TRUE;
}

BOOL MIDASclose(void)
{
	kprintf("%s()\n", __FUNCTION__);

	MIDASremoveTimerCallbacks();

	if (XMPlayerBase != NULL)
	{
		CloseLibrary(XMPlayerBase);
		XMPlayerBase = NULL;
	}

	return TRUE;
}

BOOL MIDASopenChannels(int numChannels)
{
	kprintf("%s(%d)\n", __FUNCTION__, numChannels);
	return TRUE;
}

BOOL MIDAScloseChannels(void)
{
	kprintf("%s()\n", __FUNCTION__);
	return TRUE;
}


BOOL MIDASsetTimerCallbacks(DWORD rate, BOOL displaySync, void (MIDAS_CALL *preVR)(), void (MIDAS_CALL *immVR)(), void (MIDAS_CALL *inVR)())
{
	kprintf("%s(%d, %d, %p, %p, %p)\n", __FUNCTION__, rate, displaySync, preVR, immVR, inVR);

	// TODO shouldn't this be a vblank intterrupt?
	timerIntHandle = AddTimerInt((APTR)TimerInterrupt, inVR);
	StartTimerInt(timerIntHandle, (1000 * 1000) / (rate/1000), TRUE);

	return TRUE;
}

BOOL MIDASremoveTimerCallbacks(void)
{
	kprintf("%s()\n", __FUNCTION__);

	if (timerIntHandle)
	{
		StopTimerInt(timerIntHandle);
		RemTimerInt(timerIntHandle);
		timerIntHandle = NULL;
	}

	return TRUE;
}


// sound samples

MIDASsample MIDASloadRawSample(char *fileName, int sampleType, int loopSample)
{
	return 1;
}

BOOL MIDASfreeSample(MIDASsample sample)
{
	kprintf("%s(%u)\n", __FUNCTION__, sample);
	return TRUE;
}

BOOL MIDASallocAutoEffectChannels(unsigned numChannels)
{
	kprintf("%s(%d)\n", __FUNCTION__, numChannels);
	return TRUE;
}

BOOL MIDASfreeAutoEffectChannels(void)
{
	kprintf("%s()\n", __FUNCTION__);
	return TRUE;
}

MIDASsamplePlayHandle MIDASplaySample(MIDASsample sample, unsigned channel, int priority, unsigned rate, unsigned volume, int panning)
{
	kprintf("%s(%u, %d, %d, %d, %d, %d)\n", __FUNCTION__, sample, channel, priority, rate, volume, panning);
	return 1;
}

BOOL MIDASstopSample(MIDASsamplePlayHandle sample)
{
	kprintf("%s(%u)\n", __FUNCTION__, sample);
	return TRUE;
}


// music

MIDASmodule MIDASloadModule(char *fileName)
{
	kprintf("%s(%s)\n", __FUNCTION__, fileName);

	if (!XMPlayerBase)
	{
		// pretend to be working
		return (MIDASmodule)1;
	}

	// TODO multiple module support
	if ((buffer = LoadData(fileName)) == NULL)
	{
		return (MIDASmodule)0;
	}

	info.XMPl_Cont = buffer;
	//info.XMPl_Mixtype = XM_STEREO14;
	//info.XMPl_Mixtype = XM_MONO;
	info.XMPl_Mixtype = midasOutputMode & sdMono ? XM_MONO : XM_STEREO14;
	//info.XMPl_Mixfreq = 22050;
	//info.XMPl_Mixfreq = 16000;
	info.XMPl_Mixfreq = midasMixRate;
	info.XMPl_Vboost = 2;
	info.XMPl_PrName = "Berusky Player";
	info.XMPl_PrPri = 1;

	if (!XMPl_Init(&info))
	{
		FreeVec(buffer);
		buffer = NULL;
		mLastError = errInvalidModule;
		return (MIDASmodule)0;
	}

	return (MIDASmodule)1;
}

MIDASmodulePlayHandle MIDASplayModule(MIDASmodule module, BOOL loopSong)
{
	kprintf("%s(%p,%d)\n", __FUNCTION__, module, loopSong);

	if (!XMPlayerBase)
	{
		// pretend to be working
		return 1;
	}

	XMPl_Play();

	return 1;
}

BOOL MIDASstopModule(MIDASmodulePlayHandle playHandle)
{
	kprintf("%s(%u)\n", __FUNCTION__, playHandle);

	if (!XMPlayerBase)
	{
		// pretend to be working
		return TRUE;
	}

	XMPl_StopPlay();

	return TRUE;
}

BOOL MIDASfreeModule(MIDASmodule module)
{
	kprintf("%s(%p)\n", __FUNCTION__, module);

	if (!XMPlayerBase)
	{
		// pretend to be working
		return TRUE;
	}

	if (buffer)
	{
		XMPl_DeInit();
		FreeVec(buffer);
		buffer = NULL;
	}

	return TRUE;
}

BOOL MIDASsetMusicVolume(MIDASmodulePlayHandle playHandle, unsigned volume)
{
	kprintf("%s(%u,%d)\n", __FUNCTION__, playHandle, volume);
	return TRUE;
}

BOOL MIDASgetModuleInfo(MIDASmodule module, MIDASmoduleInfo *info)
{
	kprintf("%s(%p,%p)\n", __FUNCTION__, module, info);
	info->numChannels = 0;
	return TRUE;
}
