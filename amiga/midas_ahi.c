// reimplementation of a subset of the MIDAS audio system using AHI

#include <clib/alib_protos.h>
#include <utility/hooks.h>
#include <devices/ahi.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/ahi.h>

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <midasdll.h>

// constants

#ifndef __AROS__
#define IPTR ULONG
#endif

#define MAXSAMPLES 32 // berusky has 20 samples

enum ErrorCodes
{
	OK = 0,
	errUndefined,
	errOutOfMemory,

	errNoSampleHandles,
	errFileOpen,
	errFileRead,

	errInvalidModule,
	errInvalidChanNumber,
	errInvalidSampleHandle,
	errNoChannels,

	errInvalidArguments,
	errFileNotFound,

	errDeviceNotAvailable,

	errInvalidSampleType,
	errModuleNotPlaying,
	errOutOfChannels,
	errUnsuppSampleFormat,

	errNumErrorCodes
};

//#define MIDAS_DEBUG
#ifdef MIDAS_DEBUG
#define kprintf printf
#else
#define kprintf(...)
#endif


// globals

static char *errorMsg[] =
{
	"OK",
	"Undefined error",
	"Out of memory",

	"Out of sample handles",
	"Unable to open file",
	"Unable to read file",

	"Invalid module file",
	"Invalid channel number",
	"Invalid sample handle",
	"Sound Device channels not open",

	"Invalid function arguments",
	"File does not exist",

	"Device is not available",

	"Invalid sample type",
	"No module is being played",
	"Out of sound channels",
	"Unsupported sample data format"
};

static int mLastError = OK;

static struct
{
	APTR address;
	ULONG type;
	LONG length;
	BOOL loop;
} samples[MAXSAMPLES];

static struct Library      *AHIBase;
static struct MsgPort      *AHImp     = NULL;
static struct AHIRequest   *AHIio     = NULL;
static BYTE                 AHIDevice = -1;
static struct AHIAudioCtrl *actrl     = NULL;

static BOOL playing = FALSE;
static unsigned int autoFxChannels = 0;
static unsigned int maxChannels = 0;

static struct Hook playerHook;
static Fixed playerFreq = 50<<16;


// helper functions

static ULONG PlayerFunc(struct Hook *hook, struct AHIAudioCtrl *actrl, APTR unused)
{
	static void (*timerServiceFunc)(void);
	timerServiceFunc = hook->h_Data;
	timerServiceFunc();
	return 0;
}

void SwaB(char *src, char *dest, LONG n)
{
	for (; n>1; n-=2)
	{
		dest[0] = src[1];
		dest[1] = src[0];
		dest += 2;
		src += 2;
	}
}

static int AHIErrToMIDAS(ULONG error)
{
	switch (error)
	{
	case AHIE_OK:
		mLastError = OK;
		break;
	case AHIE_NOMEM:
		mLastError = errOutOfMemory;
		break;
	case AHIE_BADSOUNDTYPE:
		mLastError = errUndefined; // TODO fixme
		break;
	case AHIE_BADSAMPLETYPE:
		mLastError = errInvalidSampleType;
		break;
	//#define AHIE_ABORTED		(4)
	//#define AHIE_HALFDUPLEX		(6)
	case AHIE_UNKNOWN:
	default:
		mLastError = errUndefined;
		break;
	}

	return mLastError;
}

ULONG LoadSound(UWORD id)
{
	struct AHISampleInfo sample;
	ULONG error;

	kprintf("%s(%u)\n", __FUNCTION__, id);

	sample.ahisi_Address = samples[id].address;
	sample.ahisi_Type = samples[id].type;
	sample.ahisi_Length = samples[id].length / AHI_SampleFrameSize(samples[id].type);
	error = AHI_LoadSound(id, AHIST_SAMPLE, &sample, actrl);

	return error;
}

static void ReloadSounds(void)
{
	int id;

	kprintf("%s()\n", __FUNCTION__);

	for (id = 0; id < MAXSAMPLES; id++)
	{
		if (samples[id].address)
		{
			LoadSound(id);
		}
	}
}


// error handling

int MIDASgetLastError(void)
{
	return mLastError;
}

char *MIDASgetErrorMessage(int errorCode)
{
	return errorMsg[errorCode];
}


// screen refresh rate

DWORD MIDASgetDisplayRefreshRate(void)
{
	// TODO: return the actual screen refresh rate?
	return 50*1000;
}


// startup

BOOL MIDASstartup(void)
{
	kprintf("%s()\n", __FUNCTION__);
	// nothing to do here
	return TRUE;
}

BOOL MIDASstartBackgroundPlay(DWORD pollRate)
{
	kprintf("%s(%u)\n", __FUNCTION__, pollRate);

	playing = TRUE;
	if (actrl)
	{
		AHI_ControlAudio(actrl,
			AHIC_Play, playing,
			TAG_END); 
	}

	return TRUE;
}

BOOL MIDASstopBackgroundPlay(void)
{
	kprintf("%s()\n", __FUNCTION__);

	playing = FALSE;
	if (actrl)
	{
		AHI_ControlAudio(actrl,
			AHIC_Play, playing,
			TAG_END);
	}

	return TRUE;
}

BOOL MIDASloadConfig(char *fileName)
{
	kprintf("%s(%s)\n", __FUNCTION__, fileName);

	// we don't need no config!
	return TRUE;
}


// init

BOOL MIDASinit(void)
{
	kprintf("%s()\n", __FUNCTION__);

	if ((AHImp = CreateMsgPort()))
	{
		if ((AHIio = (struct AHIRequest *)CreateIORequest(AHImp, sizeof(struct AHIRequest))))
		{
			AHIio->ahir_Version = 4;
			if (!(AHIDevice = OpenDevice(AHINAME, AHI_NO_UNIT, (struct IORequest *)AHIio, 0)))
			{
				AHIBase = (struct Library *)AHIio->ahir_Std.io_Device;
				return TRUE;
			}
		}
	}
	MIDASclose();
	return FALSE;
}

BOOL MIDASclose(void)
{
	int id;

	kprintf("%s()\n", __FUNCTION__);

	MIDASremoveTimerCallbacks();

	MIDAScloseChannels();

	for (id = 0; id < MAXSAMPLES; id++)
	{
		if (samples[id].address)
		{
			FreeVec(samples[id].address);
			samples[id].address = NULL;
		}
	}

	if (!AHIDevice)
	{
		AHIBase = NULL;
		CloseDevice((struct IORequest *)AHIio);
		AHIDevice = -1;
	}

	if (AHIio)
	{
		DeleteIORequest((struct IORequest *)AHIio);
		AHIio = NULL;
	}

	if (AHImp)
	{
		DeleteMsgPort(AHImp);
		AHImp = NULL;
	}

	return TRUE;
}

BOOL MIDASopenChannels(int numChannels)
{
	ULONG error;

	kprintf("%s(%d)\n", __FUNCTION__, numChannels);

	if (!AHIBase)
	{
		mLastError = errDeviceNotAvailable;
		return FALSE;
	}

	actrl = AHI_AllocAudio(
		AHIA_Channels, numChannels,
		AHIA_Sounds, MAXSAMPLES,

		AHIA_PlayerFunc, (IPTR)&playerHook,
		AHIA_PlayerFreq, playerFreq,
		AHIA_MinPlayerFreq, playerFreq,
		AHIA_MaxPlayerFreq, playerFreq,
		TAG_END);

	if (!actrl)
	{
		mLastError = errNoChannels;
		return FALSE;
	}

	maxChannels = numChannels;

	// TODO handle errors in reloadsounds
	ReloadSounds();

	error = AHI_ControlAudio(actrl,
		AHIC_Play, playing,
		TAG_END);

	if (error != AHIE_OK)
	{
		MIDAScloseChannels();
		AHIErrToMIDAS(error);
		return 0;
	}

	return TRUE;
}

BOOL MIDAScloseChannels(void)
{
	kprintf("%s()\n", __FUNCTION__);

	if (!AHIBase)
	{
		mLastError = errDeviceNotAvailable;
		return FALSE;
	}

	if (actrl)
	{
		AHI_ControlAudio(actrl,
			AHIC_Play, FALSE,
			TAG_END);

		maxChannels = 0;

		AHI_FreeAudio(actrl);
		actrl = NULL;
	}

	return TRUE;
}


BOOL MIDASsetTimerCallbacks(DWORD rate, BOOL displaySync, void (MIDAS_CALL *preVR)(), void (MIDAS_CALL *immVR)(), void (MIDAS_CALL *inVR)())
{
	kprintf("%s()\n", __FUNCTION__);

	// TODO shouldn't this be a vblank intterrupt?
	playerHook.h_Entry = HookEntry;
	playerHook.h_SubEntry = (HOOKFUNC)PlayerFunc;
	playerHook.h_Data = (APTR)inVR;
	playerFreq = (rate/1000) << 16;

	return TRUE;
}

BOOL MIDASremoveTimerCallbacks(void)
{
	kprintf("%s()\n", __FUNCTION__);

	// TODO remove the interrupt?

	return TRUE;
}


// sound samples

MIDASsample MIDASloadRawSample(char *fileName, int sampleType, int loopSample)
{
	BPTR file;
	UWORD id = 0;
	char str[256];
	int i;
	ULONG error;

	kprintf("%s(%s, %d, %d)\n", __FUNCTION__, fileName, sampleType, loopSample);

	if (!actrl)
	{
		mLastError = errNoChannels;
		return FALSE;
	}

	for (id = 0; id < MAXSAMPLES; id++)
	{
		if (samples[id].address == NULL)
		{
			break;
		}
	}

	if (id == MAXSAMPLES)
	{
		mLastError = errNoSampleHandles;
		return 0;
	}

	// replace backslashes
	strcpy(str, fileName);
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == '\\')
			str[i] = '/';
		i++;
	}

	//if (!(file = Open(fileName, MODE_OLDFILE)))
	if (!(file = Open(str, MODE_OLDFILE)))
	{
		mLastError = errFileOpen;
		return 0;
	}

	Seek(file, 0, OFFSET_END);
	samples[id].length = Seek(file, 0, OFFSET_BEGINNING);
	samples[id].address = AllocVec(samples[id].length, MEMF_PUBLIC);
	if (!samples[id].address)
	{
		mLastError = errOutOfMemory;
		Close(file);
		return 0;
	}

	Read(file, samples[id].address, samples[id].length);
	Close(file);

	switch (sampleType)
	{
		case MIDAS_SAMPLE_8BIT_MONO:
			samples[id].type = AHIST_M8S;
			break;
		case MIDAS_SAMPLE_16BIT_MONO:
			samples[id].type = AHIST_M16S;
			SwaB(samples[id].address, samples[id].address, samples[id].length);
			break;
		case MIDAS_SAMPLE_8BIT_STEREO:
			samples[id].type = AHIST_S8S;
			break;
		case MIDAS_SAMPLE_16BIT_STEREO:
			samples[id].type = AHIST_S16S;
			SwaB(samples[id].address, samples[id].address, samples[id].length);
			break;
		default:
			samples[id].address = NULL;
			mLastError = errInvalidSampleType;
			return 0;
	}

	error = LoadSound(id);
	if (error != AHIE_OK)
	{
		//MIDASfreeSample(id);
		FreeVec(samples[id].address);
		samples[id].address = NULL;
		AHIErrToMIDAS(error);
		return 0;
	}

	return id+1;
}

BOOL MIDASfreeSample(MIDASsample sample)
{
	UWORD id = sample-1;

	kprintf("%s(%u)\n", __FUNCTION__, sample);

	if (!actrl)
	{
		mLastError = errNoChannels;
		return FALSE;
	}

	if (samples[id].address)
	{
		AHI_UnloadSound(id, actrl);
		FreeVec(samples[id].address);
		samples[id].address = NULL;
	}

	return TRUE;
}

BOOL MIDASallocAutoEffectChannels(unsigned numChannels)
{
	kprintf("%s(%d)\n", __FUNCTION__, numChannels);

	autoFxChannels = numChannels;

	return TRUE;
}

BOOL MIDASfreeAutoEffectChannels(void)
{
	kprintf("%s()\n", __FUNCTION__);

	autoFxChannels = 0;

	return TRUE;
}

MIDASsamplePlayHandle MIDASplaySample(MIDASsample sample, unsigned channel, int priority, unsigned rate, unsigned volume, int panning)
{
	UWORD id = sample-1;

	kprintf("%s(%u, %d, %d, %d, %d, %d)\n", __FUNCTION__, sample, channel, priority, rate, volume, panning);

	if (!actrl)
	{
		mLastError = errNoChannels;
		return FALSE;
	}

	if (channel == MIDAS_CHANNEL_AUTO)
	{
		// there's no way to tell which AHI channel is playing a sound
		// pick a random channel, and hope for the best
		channel = rand() % autoFxChannels;
	}

	if (channel > maxChannels)
	{
		mLastError = errInvalidChanNumber;
		return FALSE;
	}

	if (volume > 64 || panning < -64 || panning > 64)
	{
		mLastError = errInvalidArguments;
		return FALSE;
	}

	AHI_Play(actrl,
		AHIP_BeginChannel, channel,
		AHIP_Freq, rate,
		AHIP_Vol, volume << 10, // 0-64 -> 16.16 Fixed
		AHIP_Pan, (panning + 64) << 9, // -64 - 64 -> 0-128 -> 16.16 Fixed
		AHIP_Sound, id,
		samples[id].loop ? TAG_IGNORE : AHIP_LoopSound, AHI_NOSOUND,
		AHIP_EndChannel, (ULONG)NULL,
		TAG_END); 

	return channel+1;
}

BOOL MIDASstopSample(MIDASsamplePlayHandle sample)
{
	ULONG channel = sample-1;

	kprintf("%s(%u)\n", __FUNCTION__, sample);

	if (!actrl)
	{
		mLastError = errNoChannels;
		return FALSE;
	}

	AHI_SetSound(channel, AHI_NOSOUND, 0, 0, actrl, AHISF_IMM);

	return TRUE;
}


// music

MIDASmodule MIDASloadModule(char *fileName)
{
	kprintf("%s(%s)\n", __FUNCTION__, fileName);
	return (MIDASmodule)1;
}

MIDASmodulePlayHandle MIDASplayModule(MIDASmodule module, BOOL loopSong)
{
	kprintf("%s(%p,%d)\n", __FUNCTION__, module, loopSong);
	return 1;
}

BOOL MIDASstopModule(MIDASmodulePlayHandle playHandle)
{
	kprintf("%s(%u)\n", __FUNCTION__, playHandle);
	return TRUE;
}

BOOL MIDASfreeModule(MIDASmodule module)
{
	kprintf("%s(%p)\n", __FUNCTION__, module);
	return TRUE;
}

BOOL MIDASsetMusicVolume(MIDASmodulePlayHandle playHandle, unsigned volume)
{
	kprintf("%s(%u,%d)\n", __FUNCTION__, playHandle, volume);
	return TRUE;
}

BOOL MIDASgetModuleInfo(MIDASmodule module, MIDASmoduleInfo *info)
{
	kprintf("%s(%p,%p)\n", __FUNCTION__, module, info);
	info->numChannels = 0;
	return TRUE;
}
