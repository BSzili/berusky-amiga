/*
Copyright (C) 1996 Jason Mathews
Copyright (C) 2012 Erwin Waterlander

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
  */

#include <string.h>
#include <sys/syslimits.h>
#include <sys/stat.h>
#include <errno.h>
#include <fnmatch.h>
#include "findfirst.h"

int findnext(struct ffblk *fb)
{
	struct stat dd_sstat;
	if (!fb->dd_dirp) goto findnext_err;
	while ((fb->dd_dp = readdir(fb->dd_dirp)) != NULL)
	{
		if (stat(fb->dd_dp->d_name, &dd_sstat))
			continue;
		if (dd_sstat.st_mode & S_IFDIR && !(fb->dd_attrib & FA_DIREC))
			continue;
		if (!fnmatch(fb->dd_filespec, fb->dd_dp->d_name, FNM_PATHNAME))
		{
			/* fill in file info */
			strncpy(fb->ff_name, fb->dd_dp->d_name, sizeof(fb->ff_name));
			fb->ff_fsize = dd_sstat.st_size;
			return 0;       /* successful match */
		}
	}  /* while */

	closedir(fb->dd_dirp);

findnext_err:

	memset(fb, 0, sizeof(struct ffblk)); /* invalidate structure */
	errno = ENOENT;       /* no file found */
	return -1;
}

int findfirst(const char *path, struct ffblk *fb, int attrib)
{
	char dir[PATH_MAX];		/* directory path */
	char *s = strrchr(path, '/');
	if (!s)
	{
		s = strrchr(path, ':');
	}
	if (s)
	{
		strcpy(fb->dd_filespec, s+1);
		strncpy(dir, path, (size_t)(s-path));
	}
	else
	{
		getcwd(dir, sizeof(dir));		/* use current directory */
		strcpy(fb->dd_filespec, path);
	}
	fb->dd_attrib = (char)attrib;
	fb->dd_dirp   = opendir(dir);
	return findnext(fb);
}
